﻿using System;
using System.Collections.Generic;
using System.Linq;
using PanasonicDMS.Models;
using PanasonicDMS.Models.DbContext;

namespace PanasonicDMS.Security
{
	public class UserLocationMonduleFunction
	{
		private panasonicdmsEntities db = new panasonicdmsEntities();
		private List<UserModuleFunctionModel> arrayUserModule;
		public List<UserModuleFunctionModel> getAllUserRoleFunctionLocation(int UserId, int locationId)
		{

			try
			{
				if (arrayUserModule == null || arrayUserModule.Count==0)
				{
					arrayUserModule = new List<UserModuleFunctionModel>();

					var userFunctions = db.tbuserroleloclinks.Join(db.tbrolemodfunccondlinks, userRole =>
					userRole.fdRoleId, RoleModule => RoleModule.fdRoleId,
					(userRole, RoleModule) =>
					new
					{
						userRole,
						RoleModule
					}).Join(db.tbmodulemasters, userLocRole => userLocRole.RoleModule.fdModuleId,
					  moduleMaster => moduleMaster.fdModuleId,
						(userLocRole, moduleMaster) =>
						new
						{
							userLocRole,
							moduleMaster
						}).Join(db.tbfunctionmasters, userlocroleModule => userlocroleModule.userLocRole.RoleModule.fdFunctionId,
						functionMaster => functionMaster.fdFunctionId,
						(userlocroleModule, functionMaster) =>
						new UserModuleFunctionModel()
						{
							functionCode = functionMaster.fdFunctionCode,
							moduleCode = userlocroleModule.moduleMaster.fdModuleCode,
							locationId = userlocroleModule.userLocRole.userRole.fdLocationId,
							userId = userlocroleModule.userLocRole.userRole.fdUserId,
							roleId = userlocroleModule.userLocRole.userRole.fdRoleId
						}).Where(x => x.locationId == locationId).Where(y => y.userId == UserId);

					arrayUserModule = userFunctions.ToList();

				}
			}
			catch (Exception exc)
			{
				throw exc;

			}
			return arrayUserModule;

		}

		public UserInfo getUserAssociatedRoleDetails(UserInfo userInfo)
		{
			tbrolemaster liValidation = null;
			var roleLink = db.tbuserroleloclinks.Where(x => x.fdUserId == userInfo.userId).FirstOrDefault();
			if (roleLink != null)
			{
				liValidation = db.tbrolemasters.Where(s => s.fdRoleId == roleLink.fdRoleId).FirstOrDefault();
				userInfo.roleId = liValidation.fdRoleId;
			}
			return userInfo;
		}

		public List<UserModuleFunctionModel> getUserRoleFunctionLocationWithModuleCode(string moduleCode)
		{
			if (arrayUserModule == null)
			{
				throw new NotImplementedException();
			}
			else
			{
				arrayUserModule.Where(x => x.moduleCode == moduleCode);

			}
			return arrayUserModule;

		}

	}
}