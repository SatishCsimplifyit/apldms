﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(PanasonicDMS.Startup))]
namespace PanasonicDMS
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
