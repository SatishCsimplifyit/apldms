﻿using System.Web;
using System.Web.Optimization;

namespace PanasonicDMS
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/datepickerjs").Include(
                       "~/Scripts/datepicker/*.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));



            bundles.Add(new ScriptBundle("~/bundles/calendarJS").Include(
                        "~/Scripts/date/tail.datetime.min.js",
                        "~/Scripts/date/tail.datetime-de.js",
                         "~/Scripts/date/tail.datetime - es.js"                        
                      ));

            bundles.Add(new ScriptBundle("~/bundles/calendarCss").Include(
                      "~/Content/date/tail.datetime-default.css",
                      "~/Content/date//tail.datetime.white.css"
                    ));

       

            bundles.Add(new StyleBundle("~/Layout/Css").Include(
                      "~/Content/bootstrap.min.css",
                      "~/Content/jquery.dataTables.min.css",
                      "~/Content/font-awesome.css",
                    "~/Content/common.css",
                      "~/Content/Upload/upload.css",
                       "~/Content/Fonts/Fonts.css"
                       //"~/Content/fixedHeader.dataTables.css"
                     ));


            bundles.Add(new StyleBundle("~/Layout/datepickercss").Include(
                     "~/Content/datepicker/*.css",
                     "~/Content/datepicker/*.map"
                     ));

            bundles.Add(new ScriptBundle("~/Layout/js").Include(
                       "~/Scripts/jquery.min.js",
                       "~/Scripts/bootstrap.min.js",
                       "~/Scripts/popper.min.js",
                                             
                       "~/Scripts/jquery.dataTables.min.js",
                       "~/Scripts/Upload/upload.js",
                       "~/Scripts/common.js",
                      "~/Scripts/bootbox/bootbox.min.js"
                      //"~/Scripts/dataTables.fixedHeader.js"                    
                       ));

            bundles.Add(new StyleBundle("~/Layout/prettyPhotoCss").Include(
                      "~/Content/prettyPhoto.css"
                     ));
            

            bundles.Add(new StyleBundle("~/Layout/homeCss").Include(
                      "~/Content/Home/home.css"
                     ));

            bundles.Add(new ScriptBundle("~/Layout/homeJS").Include(
                       "~/Scripts/Home/home.js"
                       ));

            bundles.Add(new ScriptBundle("~/Layout/prettyPhotoJS").Include(
                       "~/Scripts/prettyPhoto.js"
                       ));

            bundles.Add(new ScriptBundle("~/Layout/Document").Include(
                       "~/Scripts/Home/Document.js"
                       ));
            bundles.Add(new ScriptBundle("~/Layout/UserMasterJS").Include(
                      "~/Scripts/UserMaster/UserMaster.js"
                      ));
            bundles.Add(new StyleBundle("~/Layout/UserMaster").Include(
                     "~/Content/UserMaster/usermaster.css"
                     ));

            bundles.Add(new StyleBundle("~/Layout/DocumentClass").Include(
                    "~/Content/DocumentClass/DocumentClass.css"
                    ));
            bundles.Add(new StyleBundle("~/Layout/DocumentAudit").Include(
                    "~/Content/DocumentAudit/DocumentAudit.css"
                    ));
            bundles.Add(new ScriptBundle("~/Layout/DocumentClassJS").Include(
                     "~/Scripts/DocumentClass/DocumentClass.js"
                     ));
            bundles.Add(new ScriptBundle("~/Layout/DocumentAuditJS").Include(
                     "~/Scripts/DocumentAudit/DocumentAudit.js"
                     ));
            bundles.Add(new StyleBundle("~/Layout//ChartDashBoardCss").Include(
                    "~/Content/ChartDashBoard/ChartDashBoard.css"
                    ));
            bundles.Add(new ScriptBundle("~/Layout/ChartDashboardJS").Include(
                     "~/Scripts/ChartDashboard/ChartDashboard.js"
                     ));
            bundles.Add(new ScriptBundle("~/Layout/highchartsJS").Include(
                    "~/Scripts/highcharts.js"
                    ));
            bundles.Add(new ScriptBundle("~/Layout/highcharts_exportingJS").Include(
                    "~/Scripts/highcharts_exporting.js"
                    ));
            bundles.Add(new ScriptBundle("~/Layout/uploadDocumentJS").Include(
                     "~/Scripts/uploadDocument/uploadDocument.js"
                     ));
            bundles.Add(new ScriptBundle("~/Layout/uploadScreen").Include(
                     "~/Scripts/Upload/uploadScreen.js"
                     ));

        }
    }
}
