﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Resources {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public class MessageFile {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal MessageFile() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("PanasonicDMS.MessageFile.MessageFile", typeof(MessageFile).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Access Denied.
        /// </summary>
        public static string Access {
            get {
                return ResourceManager.GetString("Access", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to User does not exist in AD.
        /// </summary>
        public static string ADNotExist {
            get {
                return ResourceManager.GetString("ADNotExist", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Audit Entries.
        /// </summary>
        public static string Audit {
            get {
                return ResourceManager.GetString("Audit", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Charts genereated successfully.
        /// </summary>
        public static string Chart {
            get {
                return ResourceManager.GetString("Chart", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to User does not have any role.Please contact Admin..
        /// </summary>
        public static string CheackRole {
            get {
                return ResourceManager.GetString("CheackRole", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Enter  document type.
        /// </summary>
        public static string ClassName {
            get {
                return ResourceManager.GetString("ClassName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Your Mobile Number must be 1 to 10 Integers.
        /// </summary>
        public static string ContactLength {
            get {
                return ResourceManager.GetString("ContactLength", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Enter the Contact Number.
        /// </summary>
        public static string ContactNumber {
            get {
                return ResourceManager.GetString("ContactNumber", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Enter the field datatype.
        /// </summary>
        public static string DataType {
            get {
                return ResourceManager.GetString("DataType", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to First you need to add at least one department.
        /// </summary>
        public static string DeleteAddDepartment {
            get {
                return ResourceManager.GetString("DeleteAddDepartment", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to First you need to add at least one property.
        /// </summary>
        public static string DeleteAddProperty {
            get {
                return ResourceManager.GetString("DeleteAddProperty", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Department successfully deleted.
        /// </summary>
        public static string DeleteDepartment {
            get {
                return ResourceManager.GetString("DeleteDepartment", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Property successfully deleted.
        /// </summary>
        public static string DeletePro {
            get {
                return ResourceManager.GetString("DeletePro", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Document details fetched successfully.
        /// </summary>
        public static string DocDetails {
            get {
                return ResourceManager.GetString("DocDetails", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Document Search complete.
        /// </summary>
        public static string DocSearch {
            get {
                return ResourceManager.GetString("DocSearch", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Select a document.
        /// </summary>
        public static string DocSelection {
            get {
                return ResourceManager.GetString("DocSelection", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Cannot parse the document type.
        /// </summary>
        public static string DocTypeParse {
            get {
                return ResourceManager.GetString("DocTypeParse", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Duplicate Class Name.
        /// </summary>
        public static string Duplicate {
            get {
                return ResourceManager.GetString("Duplicate", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Enter the email address.
        /// </summary>
        public static string Email {
            get {
                return ResourceManager.GetString("Email", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Enter the email id.
        /// </summary>
        public static string EmailID {
            get {
                return ResourceManager.GetString("EmailID", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to No file selected. Please provide valid file location by clicking on &quot;Browse&quot;.
        /// </summary>
        public static string EmptyFile {
            get {
                return ResourceManager.GetString("EmptyFile", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Invalid file type.
        /// </summary>
        public static string FileType {
            get {
                return ResourceManager.GetString("FileType", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Select &quot;From&quot; date.
        /// </summary>
        public static string FromDate {
            get {
                return ResourceManager.GetString("FromDate", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Invalid function.
        /// </summary>
        public static string Function {
            get {
                return ResourceManager.GetString("Function", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to No record found.
        /// </summary>
        public static string GetNoResult {
            get {
                return ResourceManager.GetString("GetNoResult", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Search Result found.
        /// </summary>
        public static string GetResult {
            get {
                return ResourceManager.GetString("GetResult", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to User details fetched.
        /// </summary>
        public static string GetUser {
            get {
                return ResourceManager.GetString("GetUser", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to You have successfully logged in.
        /// </summary>
        public static string Login {
            get {
                return ResourceManager.GetString("Login", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Enter the User Name.
        /// </summary>
        public static string Name {
            get {
                return ResourceManager.GetString("Name", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to New Version Updated.
        /// </summary>
        public static string NewVersion {
            get {
                return ResourceManager.GetString("NewVersion", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Document not found.
        /// </summary>
        public static string NoDocDetails {
            get {
                return ResourceManager.GetString("NoDocDetails", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to User does not exist in DMS.
        /// </summary>
        public static string NotMatch {
            get {
                return ResourceManager.GetString("NotMatch", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to User detail not found.
        /// </summary>
        public static string NoUser {
            get {
                return ResourceManager.GetString("NoUser", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Enter the password.
        /// </summary>
        public static string Password {
            get {
                return ResourceManager.GetString("Password", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Record(s) found.There is more result for search criteria,refine your search.
        /// </summary>
        public static string Record {
            get {
                return ResourceManager.GetString("Record", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Record(s) found!.
        /// </summary>
        public static string RecordFound {
            get {
                return ResourceManager.GetString("RecordFound", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Fill the mandatory details.
        /// </summary>
        public static string Requset {
            get {
                return ResourceManager.GetString("Requset", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to File saved successfully.
        /// </summary>
        public static string Save {
            get {
                return ResourceManager.GetString("Save", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to The session is expired, please login again.
        /// </summary>
        public static string SessionTimeOut {
            get {
                return ResourceManager.GetString("SessionTimeOut", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Enter user status.
        /// </summary>
        public static string Status {
            get {
                return ResourceManager.GetString("Status", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Success.
        /// </summary>
        public static string Success {
            get {
                return ResourceManager.GetString("Success", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Saving Document.
        /// </summary>
        public static string SuccessClass {
            get {
                return ResourceManager.GetString("SuccessClass", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Select &quot;To&quot; date.
        /// </summary>
        public static string toDate {
            get {
                return ResourceManager.GetString("toDate", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Update successfully completed.
        /// </summary>
        public static string Update {
            get {
                return ResourceManager.GetString("Update", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Updating docs.
        /// </summary>
        public static string UpdateClass {
            get {
                return ResourceManager.GetString("UpdateClass", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to User details saved successfully.
        /// </summary>
        public static string UserSave {
            get {
                return ResourceManager.GetString("UserSave", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to User details updated successfully.
        /// </summary>
        public static string UserUpdate {
            get {
                return ResourceManager.GetString("UserUpdate", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Date is not a correct format.(&quot;dd-MMM-yyyy&quot;).
        /// </summary>
        public static string ValidateDate {
            get {
                return ResourceManager.GetString("ValidateDate", resourceCulture);
            }
        }
    }
}
