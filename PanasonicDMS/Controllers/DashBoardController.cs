﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http.Cors;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using AWP.App_Code;
using AWP.Models;
using AWP.Models.DBContext;
using AWP.Models.Masters;
using MySql.Data.MySqlClient;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace AWP.Controllers
{
    
    public class DashboardController : AWPTIPSController
    {
        private Dictionary<string, object> moResponse;
        private AWPEntities db = new AWPEntities();

        // GET: Dashboard
        public ActionResult Index()
        {
            UserInfo userInfo = getUserInfo();
            if (userInfo != null)
            {

                ViewBag.getCaseconfigurationDetails = getconfigurationDetails(null, 1);
                
                var x = getAllCheckList();
                List<CheckList> liResult = (List<CheckList>) x["Result"];
                ViewBag.Checklists= liResult;
                ViewBag.checklistcount = liResult.Count();
            }

            return View("Dashboard");
        }

        public ActionResult getAllChecklists()
        {
            try
            {
                var x = getAllCheckList();
                List<CheckList> loResult = (List<CheckList>)x["Result"];
                moResponse = RespMsgFormatter.formatResp(1,"Checklist", loResult, "getAllChecklist");
            }
            catch (Exception ex)
            {
                moResponse = RespMsgFormatter.formatResp(0, ex.Message, null, "getAllChecklist");
            }
            return Json(moResponse, JsonRequestBehavior.AllowGet);
        }

        public ActionResult getDashBoardData(String requestData)
        {
            try
            {
                if (CanAccess("DASHBOARDDATA", "SEARCH", requestData))
                {
                    string conn = "server=localhost;user id=root;password=root;persistsecurityinfo=True;database=awptips";
                    MySqlConnection sql_conn = new MySqlConnection(conn);
                    MySqlCommand cmd = new MySqlCommand();
                    cmd.Connection = sql_conn;

                    bool lbParse = false;
                    int liCaseId, liStepId, liChecklistId, liClientId = 0;
                    DateTime ldFromDate, ldToDate;
                    JObject jobj = JObject.Parse(requestData);
                    



                    if (jobj["spName"] != null && !jobj["spName"].ToString().Equals("") && !jobj["spName"].ToString().Equals("undefined"))
                    {
                        cmd.CommandText = jobj["spName"].ToString();
                        cmd.CommandType = CommandType.StoredProcedure;
                    }
                    else
                    {
                        moResponse = RespMsgFormatter.formatResp(0, "SP name not found!", null, "getDashBoardData");
                        return Json(moResponse, JsonRequestBehavior.AllowGet);
                    }

                    if (jobj["caseId"] != null && !jobj["caseId"].ToString().Equals("") && !jobj["caseId"].ToString().Equals("undefined"))
                    {
                        lbParse = int.TryParse(jobj["caseId"].ToString(), out liCaseId);
                        if (lbParse == false)
                        {
                            moResponse = RespMsgFormatter.formatResp(0, "Can not Parse Case Id!", null, "getDashBoardData");
                            return Json(moResponse, JsonRequestBehavior.AllowGet);
                        }
                        cmd.Parameters.AddWithValue("@pCaseId", liCaseId);
                        cmd.Parameters["@pCaseId"].Direction = ParameterDirection.Input;
                    }
                    /*else
                    {
                        moResponse = RespMsgFormatter.formatResp(0, "Case Id not found!", null, "getDashBoardData");
                        return Json(moResponse, JsonRequestBehavior.AllowGet);
                    }*/

                    if (jobj["stepId"] != null && !jobj["stepId"].ToString().Equals("") && !jobj["stepId"].ToString().Equals("undefined"))
                    {
                        lbParse = int.TryParse(jobj["stepId"].ToString(), out liStepId);
                        if (lbParse == false)
                        {
                            moResponse = RespMsgFormatter.formatResp(0, "Can not Parse Step Id!", null, "getDashBoardData");
                            return Json(moResponse, JsonRequestBehavior.AllowGet);
                        }
                        cmd.Parameters.AddWithValue("@pStepId", liStepId);
                        cmd.Parameters["@pStepId"].Direction = ParameterDirection.Input;
                    }
                    /*else
                    {
                        moResponse = RespMsgFormatter.formatResp(0, "Step Id not found!", null, "getDashBoardData");
                        return Json(moResponse, JsonRequestBehavior.AllowGet);
                    }*/

                    if (jobj["checklistId"] != null && !jobj["checklistId"].ToString().Equals("") && !jobj["checklistId"].ToString().Equals("undefined"))
                    {
                        lbParse = int.TryParse(jobj["checklistId"].ToString(), out liChecklistId);
                        if (lbParse == false)
                        {
                            moResponse = RespMsgFormatter.formatResp(0, "Can not Parse Checklist Id!", null, "getDashBoardData");
                            return Json(moResponse, JsonRequestBehavior.AllowGet);
                        }
                        cmd.Parameters.AddWithValue("@pChecklistId", liChecklistId);
                        cmd.Parameters["@pChecklistId"].Direction = ParameterDirection.Input;
                    }
                    /*else
                    {
                        moResponse = RespMsgFormatter.formatResp(0, "Checklist Id not found!", null, "getDashBoardData");
                        return Json(moResponse, JsonRequestBehavior.AllowGet);
                    }*/

                    if (jobj["clientId"] != null && !jobj["clientId"].ToString().Equals("") && !jobj["clientId"].ToString().Equals("undefined"))
                    {
                        lbParse = int.TryParse(jobj["clientId"].ToString(), out liClientId);
                        if (lbParse == false)
                        {
                            moResponse = RespMsgFormatter.formatResp(0, "Can not Parse Client Id!", null, "getDashBoardData");
                            return Json(moResponse, JsonRequestBehavior.AllowGet);
                        }
                        cmd.Parameters.AddWithValue("@pLocationId", liClientId);
                        cmd.Parameters["@pLocationId"].Direction = ParameterDirection.Input;
                    }
                    /*else
                    {
                        moResponse = RespMsgFormatter.formatResp(0, "Client Id not found!", null, "getDashBoardData");
                        return Json(moResponse, JsonRequestBehavior.AllowGet);
                    }*/

                    if (jobj["fromDate"] != null && !jobj["fromDate"].ToString().Equals("") && !jobj["fromDate"].ToString().Equals("undefined"))
                    {
                        lbParse = DateTime.TryParseExact(jobj["fromDate"].ToString(), Constant.dateFormat, CultureInfo.InvariantCulture, DateTimeStyles.None, out ldFromDate);
                        if (lbParse == false)
                        {
                            moResponse = RespMsgFormatter.formatResp(0, "Can not Parse From Date!", null, "getDashBoardData");
                            return Json(moResponse, JsonRequestBehavior.AllowGet);
                        }
                        cmd.Parameters.AddWithValue("@pFromDate", ldFromDate);
                        cmd.Parameters["@pFromDate"].Direction = ParameterDirection.Input;
                    }
                    /*else
                    {
                        moResponse = RespMsgFormatter.formatResp(0, "From Date not found!", null, "getDashBoardData");
                        return Json(moResponse, JsonRequestBehavior.AllowGet);
                    }*/

                    if (jobj["toDate"] != null && !jobj["toDate"].ToString().Equals("") && !jobj["toDate"].ToString().Equals("undefined"))
                    {
                        lbParse = DateTime.TryParseExact(jobj["toDate"].ToString(), Constant.dateFormat, CultureInfo.InvariantCulture, DateTimeStyles.None, out ldToDate);
                        if (lbParse == false)
                        {
                            moResponse = RespMsgFormatter.formatResp(0, "Can not Parse To Date!", null, "getDashBoardData");
                            return Json(moResponse, JsonRequestBehavior.AllowGet);
                        }
                        cmd.Parameters.AddWithValue("@pToDate", ldToDate);
                        cmd.Parameters["@pToDate"].Direction = ParameterDirection.Input;
                    }
                    /*else
                    {
                        moResponse = RespMsgFormatter.formatResp(0, "To Date not found!", null, "getDashBoardData");
                        return Json(moResponse, JsonRequestBehavior.AllowGet);
                    }*/

                    sql_conn.Open();
                    MySqlDataReader dr = cmd.ExecuteReader();
                    DataTable dt = new DataTable();
                    dt.Load(dr);
                    String jsonStr = DataTableToJSONWithJavaScriptSerializer(dt);

                    moResponse = RespMsgFormatter.formatResp(1, "Successfully Executed!", jsonStr, "getDashBoardData");
                }
                else
                {
                    moResponse = RespMsgFormatter.formatResp(2, "Access Denied!", null, "getDashBoardDatas");
                    return Json(moResponse, JsonRequestBehavior.AllowGet);
                }

            }
            catch (Exception ex)
            {
                moResponse = RespMsgFormatter.formatResp(0, ex.Message, null, "getDashBoardData");
            }

            Response.AppendHeader("Access-Control-Allow-Origin", "*");
            return Json(moResponse, JsonRequestBehavior.AllowGet);
        }


        public ActionResult getDashboardGraphsInfo(String requestData)
        {
            try
            {
                
                if (CanAccess("DASHBOARDDATA", "SEARCH", requestData))
                {
                    JObject jobj = JObject.Parse(requestData);
                    MySqlParameter loPkId = new MySqlParameter();
                    MySqlParameter loGraphType = new MySqlParameter();
                    bool lbParse = false;
                    int liPkId = 0;
                    String lsGraphType = String.Empty;

                    String lsGetDashboardGraphs = " select fdPkId, fdSpName, fdGraphTitle, fdGraphType, fdGraphSequenceNo, fdComment from tbgraphsinfo where 1=1 ";

                    if (jobj["pkId"] != null && !jobj["pkId"].ToString().Equals("") && !jobj["pkId"].ToString().Equals("undefined"))
                    {
                        lbParse = int.TryParse(jobj["pkId"].ToString(), out liPkId);
                        if (lbParse == false)
                        {
                            moResponse = RespMsgFormatter.formatResp(0, "Can not Parse PkId Id!", null, "getDashboardGraphsInfo");
                            return Json(moResponse, JsonRequestBehavior.AllowGet);
                        }
                        lsGetDashboardGraphs += " and fdPkId=@pkId ";
                        loPkId = new MySqlParameter("@pkId", liPkId);
                    }

                    if (jobj["graphType"] != null && !jobj["graphType"].ToString().Equals("") && !jobj["graphType"].ToString().Equals("undefined"))
                    {
                        loGraphType = new MySqlParameter("@graphType", jobj["graphType"].ToString());
                        lsGetDashboardGraphs += " and fdGraphType=@graphType ";
                    }

                    object[] parameters = new object[] { loPkId, loGraphType };

                    var loGetDashboardGraphs = db.Database.SqlQuery<getDashBoardGraph>(lsGetDashboardGraphs, parameters).ToList();
                    moResponse = RespMsgFormatter.formatResp(1, "Successfully Executed!", loGetDashboardGraphs, "getDashBoardData");
                }
                else
                {
                    moResponse = RespMsgFormatter.formatResp(2, "Access Denied!", null, "getDashBoardData");
                    return Json(moResponse, JsonRequestBehavior.AllowGet);
                }

            }

            catch (Exception ex)
            {
                moResponse = RespMsgFormatter.formatResp(0, ex.Message, null, "getDashBoardData");
            }
            Response.AppendHeader("Access-Control-Allow-Origin", "*");
            return Json(moResponse, JsonRequestBehavior.AllowGet);
        }

        public ActionResult sendMailChecklist(String lsAttachmentPath, int liCaseId)
        {
            try
            {
                UserInfo loUserInfo = getUserInfo();
                Dictionary<string, object> CaseDetailsWithParticipants = getCaseDetailsWithParticipants(liCaseId);
                var lvGetCaseParticipants = (List<String>)CaseDetailsWithParticipants["CaseParticipants"];
                string[] msg = new string[2];
                msg[0] = loUserInfo.userName;
                msg[1] = "Checklist";

                Task Thetask = Task.Run(() => sendMailWithTemplate("Requested Checklist Attachment", loUserInfo.email, msg, "Templates/sendChecklistAttachmentTxt .txt", lsAttachmentPath, lvGetCaseParticipants));
            }
            catch (Exception ex)
            {
                moResponse = RespMsgFormatter.formatResp(0, ex.Message, null, "sendMailChecklist");
            }
            return Json(moResponse, JsonRequestBehavior.AllowGet);
        }

        public ActionResult SaveCaseForChecklist(String requestData)
        {

            try
            {
                if (CanAccess("DASHBOARDDATA", "SEARCH", requestData))
                {
                    tbcase loCase = new tbcase();
                    tbcaseconfiguration loCaseConfig = new tbcaseconfiguration();
                    bool lbSuccess = false;
                    int liChecklistId, liClient, liStepId = 0;

                    JObject jObj = JObject.Parse(requestData);
                    int liCaseType = 0;
                    liCaseType = db.tbcodelkups.Where(s => s.fdLkUpCode == "STDY_CASE_TYPE" && s.fdKey1 == "Audit").Select(s => s.fdKeyCode).FirstOrDefault();
                    if (liCaseType == 0)
                    {
                        liCaseType = 1;
                    }
                    DbContextTransaction tx = db.Database.BeginTransaction();
                    foreach (var x in jObj)
                    {
                        if (x.Key.Equals("caseChecklist"))
                        {
                            JArray laCaseDetail = JArray.Parse(x.Value.ToString());
                            foreach (JObject data in laCaseDetail.Children<JObject>())
                            {
                                String lsChecklistName = "";
                                foreach (JProperty p in data.Properties())
                                {
                                    if (p.Name == "checklistId")
                                    {
                                        lbSuccess = int.TryParse(p.Value.ToString(), out liChecklistId);
                                        if (lbSuccess == true && liChecklistId != 0)
                                        {
                                            lsChecklistName = db.tbchecklists.Where(s => s.fdPkId == liChecklistId).Select(s => s.fdChkListTitle).FirstOrDefault();
                                            liStepId = db.tbcasesteps.Where(s => s.fdStepName == lsChecklistName).Select(s => s.fdPkId).FirstOrDefault();
                                            if (liStepId == 0)
                                            {
                                                tbcasestep loCaseStep = new tbcasestep();
                                                loCaseStep.fdStepName = lsChecklistName;
                                                loCaseStep.fdCreatedOn = DateTime.Now;
                                                loCaseStep.fdCreatedBy = getUserId();
                                                db.tbcasesteps.Add(loCaseStep);
                                                db.SaveChanges();
                                                liStepId = loCaseStep.fdPkId;
                                            }
                                            loCase.fdCaseTitle = lsChecklistName;
                                            loCase.fdCaseDescription = "Case from Dashboard";
                                            loCase.fdCreatedBy = getUserId();
                                            loCase.fdCreatedOn = DateTime.Now;
                                            loCase.fdOwner = getUserId();
                                            loCase.fdModifiedBy = getUserId();
                                            loCase.fdModifiedOn = DateTime.Now;
                                            loCase.fdCaseType = liCaseType;
                                            loCase.fdStatus = 1;
                                            loCase.fdParticipants = getUserId().ToString();
                                            loCase.fdCurrentStepStartedOn = DateTime.Now;
                                            //
                                            loCaseConfig.fdCheckListId = liChecklistId;
                                            loCaseConfig.fdCreatedBy = getUserId();
                                            loCaseConfig.fdCreatedOn = DateTime.Now;
                                            loCaseConfig.fdDueDate = 2;
                                            //loCaseConfig.fdEndDate = "";
                                            loCaseConfig.fdParticipants = getUserId().ToString();
                                            loCaseConfig.fdStartDate = DateTime.Now;
                                            loCaseConfig.fdStatus = 1;
                                            loCaseConfig.fdStepSequenceNo = 1;
                                        }
                                    }
                                    if (p.Name == "client")
                                    {
                                        lbSuccess = int.TryParse(p.Value.ToString(), out liClient);
                                        if (lbSuccess == true && liClient != 0)
                                        {
                                            String lsLocationName = db.tblocationmasters.Where(s => s.fdLocationid == liClient).Select(s => s.fdLocationName).FirstOrDefault();
                                            loCase.fdLocCode = liClient;
                                            loCase.fdLocation = liClient;

                                            loCase.fdCaseTitle = lsLocationName + "_" + lsChecklistName;
                                        }
                                    }

                                }
                                db.tbcases.Add(loCase);
                                db.SaveChanges();
                                loCaseConfig.fdCaseId = loCase.fdPkId;
                                loCaseConfig.fdStepId = liStepId;
                                db.tbcaseconfigurations.Add(loCaseConfig);
                                db.SaveChanges();
                                var lvCase = db.tbcases.Find(loCase.fdPkId);
                                lvCase.fdAssCurrCaseConfigId = loCaseConfig.fdPkId;
                                db.Entry(lvCase).State = System.Data.Entity.EntityState.Modified;
                                db.SaveChanges();
                            }
                        }

                    }
                    tx.Commit();

                    moResponse = RespMsgFormatter.formatResp(1, "Case added successfully", loCase.fdPkId, "SaveCaseForChecklist");

                }
                else
                {
                    moResponse = RespMsgFormatter.formatResp(2, "Access Denied!", null, "getDashBoardDatas");
                    return Json(moResponse, JsonRequestBehavior.AllowGet);
                }
                
            }
            catch (Exception ex)
            {
                moResponse = RespMsgFormatter.formatResp(0, ex.Message, null, "SaveCaseForChecklist");
            }
            return Json(moResponse, JsonRequestBehavior.AllowGet);
        }

        
        public ActionResult button1_Click(String requestData)
        {
            try
            {
                string conn = "server=localhost;user id=root;password=root;persistsecurityinfo=True;database=awptips";
                MySqlConnection sql_conn = new MySqlConnection(conn);

                MySqlCommand cmd = new MySqlCommand();
                cmd.Connection = sql_conn;
                cmd.CommandText = "spFetchCase";
                cmd.CommandType = CommandType.StoredProcedure;


                //an out parameter
                cmd.Parameters.AddWithValue("@liCaseId", 8);
                cmd.Parameters["@liCaseId"].Direction = ParameterDirection.Input;

                cmd.Parameters.AddWithValue("@liStepId", 1);
                cmd.Parameters["@liStepId"].Direction = ParameterDirection.Input;

                cmd.Parameters.AddWithValue("@liChecklistId", 1);
                cmd.Parameters["@liChecklistId"].Direction = ParameterDirection.Input;

                cmd.Parameters.AddWithValue("@ldFromDate", "2017-01-01");
                cmd.Parameters["@ldFromDate"].Direction = ParameterDirection.Input;

                cmd.Parameters.AddWithValue("@ldToDate", "2017-01-01");
                cmd.Parameters["@ldToDate"].Direction = ParameterDirection.Input;

                sql_conn.Open();

                MySqlDataReader dr = cmd.ExecuteReader();
                DataTable dt = new DataTable();
                dt.Load(dr);
                String jsonStr = DataTableToJSONWithJavaScriptSerializer(dt);
                moResponse = RespMsgFormatter.formatResp(1, "Datatable", jsonStr, "SaveCaseForChecklist");
                return Json(moResponse, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                moResponse = RespMsgFormatter.formatResp(0, ex.Message, null, "SaveCaseForChecklist");
                return Json(moResponse, JsonRequestBehavior.AllowGet);
            }
    }



        public string DataTableToJSONWithJavaScriptSerializer(DataTable table)
        {
            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            List<Dictionary<string, object>> parentRow = new List<Dictionary<string, object>>();
            Dictionary<string, object> childRow;
            foreach (DataRow row in table.Rows)
            {
                childRow = new Dictionary<string, object>();
                foreach (DataColumn col in table.Columns)
                {
                    childRow.Add(col.ColumnName, row[col]);
                }
                parentRow.Add(childRow);
            }
            return jsSerializer.Serialize(parentRow);
        }

    }
}