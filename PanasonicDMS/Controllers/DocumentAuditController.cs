﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Highsoft.Web.Mvc.Charts;
using PanasonicDMS.Models.DbContext;
using System.Data.Entity;
using PanasonicDMS.App_Code;
using PanasonicDMS.Models;
using Newtonsoft.Json.Linq;
using System.IO;
using System.Threading.Tasks;
using System.Data.Entity.Core.Objects;
using System.Text;
using MySql.Data.MySqlClient;
using PanasonicDMS.Controllers;
using System.Data.SqlClient;
using System.Globalization;

namespace PanasonicDMS.Controllers
{
    public class DocumentAuditController : CommonController
    {
        private panasonicdmsEntities db = new panasonicdmsEntities();
        // GET: DocumentChartDashboard
        private Dictionary<string, object> moResponse;
        public ActionResult Index()
        {
            int userid = getUserId();
          
            if (userid == 0)
            {
                TempData["msg"] = "<script>showMessage('red', 'The session is expired, please login again'); </script>";
                return View("Login");
            }
            if (CanAccess("DOCAUDIT", "SEARCH", "User Log In ID" + userid.ToString()) == true)
            {
                return View();
            }
            else
            {
                return View();
            }
        }
        public ActionResult GetDetails(string requestData)
        {
            try
            {

                int userid = getUserId();
                if (userid == 0)
                {
                    moResponse = RespMsgFormatter.formatResp(0, getMessage("SessionTimeOut"), null, "SaveDocumentClass");
                    return Json(moResponse, JsonRequestBehavior.AllowGet);
                }

                if (CanAccess("DOCAUDIT", "SEARCH", requestData) == true)
                {
                    JObject jObject = JObject.Parse(requestData);

                    MySqlParameter loActionName = new MySqlParameter("@Actionname", "");
                    MySqlParameter loFromDate = new MySqlParameter("@FromDate", "");
                    MySqlParameter loToDate = new MySqlParameter("@ToDate", "");
                    MySqlParameter loUserName = new MySqlParameter("@UserName", "");

                    string qry = "select ua.documentId,ua.auditId as auditId,FM.fdFunctionName as actionName, TUM.fdUserName, date_format(ua.createdOn,'%d-%M-%Y') as createdOn,ifnull(TD.fdDocTitle,'') as fdDocTitle,TU.fdUserName as auditBy,"
                      + " date_format(TD.fdCreatedOn,'%d-%M-%Y') as fdCreatedOn, case when ua.actionType like 'DC%' and ua.actionName='VIEW' then 'View Document' else  case when ua.actionName='UPLOAD_DOCUMENT' then 'Upload New Document' else  mo.fdModuleName end end AS fdModuleName from user_action_audit as ua left join tbusermaster as TUM on  TUM.fdUserId=ua.createdBy"
                      + " left join tbdocuments as TD on  TD.fdDocId=ua.documentId left join tbusermaster as TU on  TU.fdUserId=TD.fdCreatedBy"
                      + " JOIN tbfunctionmaster FM ON FM.fdFunctionCode=ua.actionName "
                      + " join tbmodulemaster mo on mo.fdModuleCode=ua.actionType  "
                      + " where  (ua.documentId != '0' or ua.documentId is not null) and ua.actionName !='Draft' ";

                    //string qry = " select top 500 ua.documentId,ua.auditId as auditId,FM.fdFunctionName as actionName,  TUM.fdUserName, FORMAT(ua.createdOn,'dd-MMM-yyyy') as createdOn,isnull(TD.fdDocTitle,'') as fdDocTitle,TU.fdUserName as auditBy,"
                    //    + " FORMAT(TD.fdCreatedOn,'dd-MMM-yyyy') as fdCreatedOn from user_action_audit as ua  join tbusermaster as TUM on  TUM.fdUserId=ua.createdBy"
                    //    + " left join tbdocuments as TD on  TD.fdDocId=ua.documentId  join tbusermaster as TU on  TU.fdUserId=TD.fdCreatedBy "
                    //    + " left join tbcodelkup docclass on docclass.fdKeyCode=TD.fdDocType and docclass.fdLkUpCode='DOC_CLASS_TYPE' "
                    //    + " JOIN tbfunctionmaster FM ON FM.fdFunctionCode=ua.actionName "
                    //    + " where  (ua.documentId != '0' or ua.documentId is not null) ";


                    if (jObject["searchActionName"] != null && jObject["searchActionName"].ToString() != string.Empty)
                    {
                        qry = qry + " and ua.actionName like @Actionname ";
                        loActionName = new MySqlParameter("@Actionname", jObject["searchActionName"].ToString()+"%");
                    }

                    if (jObject["searchFromDate"] != null && jObject["searchFromDate"].ToString() != string.Empty)
                    {
                        qry = qry + " and date(ua.createdOn)>=STR_TO_DATE(@FromDate,'%d/%m/%Y') ";
                        //var d = (DateTime.ParseExact(jObject["searchFromDate"].ToString(), "dd/MM/yyyy", CultureInfo.InvariantCulture));
                        loFromDate = new MySqlParameter("@FromDate", jObject["searchFromDate"].ToString());
                    }

                    if (jObject["searchToDate"] != null && jObject["searchToDate"].ToString() != string.Empty)
                    {
                        qry = qry + " and date(ua.createdOn)<=STR_TO_DATE(@ToDate,'%d/%m/%Y') ";
                        //var d = (DateTime.ParseExact(jObject["searchToDate"].ToString(), "dd/MM/yyyy", CultureInfo.InvariantCulture));
                        loToDate = new MySqlParameter("@ToDate", jObject["searchToDate"].ToString());
                    }

                    if (jObject["searchUserName"] != null && jObject["searchUserName"].ToString() != string.Empty)
                    {
                        qry = qry + " and  TUM.fdUserName like @UserName ";
                        loUserName = new MySqlParameter("@UserName", jObject["searchUserName"].ToString()+"%");
                    }


                    object[] parameters = new object[] { loActionName, loFromDate, loToDate, loUserName };

                    var doclist = db.Database.SqlQuery<DMSDocumentAudit>(qry + " order by ua.auditId desc limit 500 ", parameters).ToList();
                    
                    if (doclist.Count > 0)
                    {
                        
                        moResponse = RespMsgFormatter.formatResp(1, doclist.Count +" "+ getMessage("Audit"), doclist, "searchDocuments");
                    }
                    else
                    {
                        moResponse = RespMsgFormatter.formatResp(0, getMessage("GetNoResult"), doclist, "searchDocuments");
                    }
                }
                else
                {
                    moResponse = RespMsgFormatter.formatResp(0, getMessage("Access"), null, "searchDocuments");
                }
            }
            catch(Exception ex)
            {
                moResponse = RespMsgFormatter.formatResp(0, " Message" + ex.Message + " InnerException:" + ex.InnerException, "", "searchUserMasterTab");

            }


            return Json(moResponse, JsonRequestBehavior.AllowGet);
        }


        public ActionResult searchDocumentsMasterTab(string requestData)
        {
            try
            {
                int userid = getUserId();
                if (userid == 0)
                {
                    moResponse = RespMsgFormatter.formatResp(0, getMessage("SessionTimeOut"), null, "searchDocumentsMasterTab");
                    return Json(moResponse, JsonRequestBehavior.AllowGet);
                }
                if (CanAccess("DOCAUDIT", "SEARCH", requestData) == true)
                {
                    String liUserName = String.Empty;
                    String liContactNumber = String.Empty;
                    String liUserId = String.Empty;
                    MySqlParameter liUserIDParam = new MySqlParameter("@", "");
                    MySqlParameter liUserNameparam = new MySqlParameter("@userName", "");
                    MySqlParameter liContactNoparam = new MySqlParameter("@contactNo", "");
                    JObject jsonObj = JObject.Parse(requestData);
                    if (jsonObj["userName"] != null && jsonObj["userName"].ToString() != string.Empty)
                    {
                        liUserName = jsonObj["userName"].ToString();
                    }
                    else
                    {
                        liUserName = "";
                    }
                    if (jsonObj["contactNumber"] != null && jsonObj["contactNumber"].ToString() != string.Empty)
                    {
                        liContactNumber = jsonObj["contactNumber"].ToString();
                    }
                    else
                    {
                        liContactNumber = "";
                    }

                    //if (jsonObj["searchUserId"] != null && jsonObj["searchUserId"].ToString() != string.Empty)
                    //{
                    //    liUserId = jsonObj["searchUserId"].ToString();
                    //}
                    //else
                    //{
                    //    liUserId = "";
                    //}

                    String query = "select top " + Constant.SEARCH_LIMIT + " fduserid,fdusername,fdUserContactNo,fdUserEmail,case when fdIsActive=1 then 'Active' else 'Inactive' end as UserStatus "
                                    + " from tbusermaster where 1 = 1 ";

                    if (liUserName != null && liUserName != "")
                    {
                        query = query + " and fdusername like @userName";
                        liUserNameparam = new MySqlParameter("@userName", liUserName + "%");
                    }
                    if (liContactNumber != null && liContactNumber != "")
                    {
                        query = query + " and fdUserContactNo like @contactNo";
                        liContactNoparam = new MySqlParameter("@contactNo", liContactNumber + "%");
                    }
                    //if (liUserId != null && liUserId != "")
                    //{
                    //    query = query + " and user.id = @UserID";
                    //    liUserIDParam = new MySqlParameter("@UserID", liUserId);
                    //}

                    var loParam = new object[] { liUserNameparam, liContactNoparam, liUserIDParam };
                    var getDataFromTable = db.Database.SqlQuery<UserSearch>(query, loParam).ToList();

                    if (getDataFromTable.Count >= Constant.SEARCH_LIMIT)
                    {
                        moResponse = RespMsgFormatter.formatResp(1, Constant.SEARCH_LIMIT + " " + getMessage("Record"), getDataFromTable, "getUserMasterDataTable");
                    }
                    else if (getDataFromTable.Count == 0)
                    {
                        moResponse = RespMsgFormatter.formatResp(0, getMessage("GetNoResult"), null, "getUserMasterDataTable");
                        // moResponse = RespMsgFormatter.formatResp(1, "Succesfull Execution", getDataFromTable, "getUserMasterDataTable");
                    }
                    else
                    {
                        moResponse = RespMsgFormatter.formatResp(1, Constant.SEARCH_LIMIT + " " + getMessage("RecordFound"), getDataFromTable, "getUserMasterDataTable");
                    }
                }
                else
                {
                    moResponse = RespMsgFormatter.formatResp(0, getMessage("Access"), null, "getUserMasterDataTable");
                }
            }
            catch (Exception ex)
            {
                moResponse = RespMsgFormatter.formatResp(0, " Message" + ex.Message + " InnerException:" + ex.InnerException, "", "searchUserMasterTab");

            }
            return Json(moResponse, JsonRequestBehavior.AllowGet);
        }

        public ActionResult searchDocumentsUsingSearchBar(string requestData)
        {
            try
            {
                int userid = getUserId();
                if (userid == 0)
                {
                    moResponse = RespMsgFormatter.formatResp(0, getMessage("SessionTimeOut"), null, "searchDocumentsUsingSearchBar");
                    return Json(moResponse, JsonRequestBehavior.AllowGet);
                }

                if (CanAccess("DOCAUDIT", "SEARCH", requestData) == true)
                {

                    JObject jObject = JObject.Parse(requestData);
                    MySqlParameter loName = new MySqlParameter("@searchkeyword", "");
                    string qry = "select ua.documentId,ua.auditId as auditId,FM.fdFunctionName as actionName, TUM.fdUserName, date_format(ua.createdOn,'%d-%M-%Y') as createdOn,ifnull(TD.fdDocTitle,'') as fdDocTitle,TU.fdUserName as auditBy,"
                        + " date_format(TD.fdCreatedOn,'%d-%M-%Y') as fdCreatedOn, case when ua.actionType like 'DC%' and ua.actionName='VIEW' then 'View Document' else  case when ua.actionName='UPLOAD_DOCUMENT' then 'Upload New Document' else  mo.fdModuleName end end AS fdModuleName from user_action_audit as ua left join tbusermaster as TUM on  TUM.fdUserId=ua.createdBy"
                        + " left join tbdocuments as TD on  TD.fdDocId=ua.documentId left join tbusermaster as TU on  TU.fdUserId=TD.fdCreatedBy"
                        + " JOIN tbfunctionmaster FM ON FM.fdFunctionCode=ua.actionName "
                        + " join tbmodulemaster mo on mo.fdModuleCode=ua.actionType  "
                        + " where  (ua.documentId != '0' or ua.documentId is not null) and ua.actionName !='Draft' ";

                    string value = string.Empty;

                    if (jObject["searchInputKeyword"].ToString() != null && jObject["searchInputKeyword"].ToString() != string.Empty)
                    {
                        qry = qry + "and ( TUM.fdUserName like @searchkeyword or TD.fdDocTitle like @searchkeyword or ua.actionName like @searchkeyword or mo.fdModuleName like @searchkeyword or date(ua.createdOn)=STR_TO_DATE(@searchkeyword,'%d-%M-%Y') or date(ua.createdOn)=STR_TO_DATE(@searchkeyword,'%d-%M-%Y')  )";
                        value = jObject["searchInputKeyword"].ToString();
                        loName = new MySqlParameter("@searchkeyword", value + "%");
                    }



                    //DateTime Searchdt; // "yyyy-MMM-dd", "yyyy-MM-dd"
                    //MySqlParameter lodatepmtr = new MySqlParameter("@searchDate", "");
                    //string[] formats = { "dd-MMM-yyyy", "dd-MM-yyyy" };
                    //if (DateTime.TryParseExact(value, formats, CultureInfo.InvariantCulture, DateTimeStyles.None, out Searchdt))
                    //{
                    //    lodatepmtr = new MySqlParameter("@searchDate", Searchdt);
                    //}

                    var doclist = db.Database.SqlQuery<DMSDocumentAudit>(qry + " order by ua.auditId desc", loName).ToList();
                    
                    if (doclist.Count > 0)
                    {
                        moResponse = RespMsgFormatter.formatResp(1, doclist.Count + " " + getMessage("Audit"), doclist, "searchDocuments");
                    }
                    else
                    {
                        moResponse = RespMsgFormatter.formatResp(0, " " + getMessage("GetNoResult"), doclist, "searchDocuments");
                    }
                }
                else
                {
                    moResponse = RespMsgFormatter.formatResp(0, " " + getMessage("Access"), null, "searchDocuments");
                }         
            }
            catch(Exception ex )
            {
                moResponse = RespMsgFormatter.formatResp(0, " Message" + ex.Message + " InnerException:" + ex.InnerException, "", "searchUserMasterTab");

            }
            return Json(moResponse, JsonRequestBehavior.AllowGet);
        }

    }
}