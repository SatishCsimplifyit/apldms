﻿
﻿using PanasonicDMS.App_Code;
using MySql.Data.MySqlClient;
using Newtonsoft.Json.Linq;
using PanasonicDMS.Controllers;
using PanasonicDMS.Models.DbContext;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PanasonicDMS.Models;
using System.Data.SqlClient;

namespace PanasonicDMS.Controllers
{
	public class UserMasterController : CommonController
	{
		// GET: UserMaster
		public ActionResult Index()
		{
			int userid = getUserId();
			if (userid == 0)
			{
				TempData["msg"] = "<script>showMessage('red', 'The session is expired, please login again'); </script>";
				return View("Login");
			}
			if (CanAccess("USERMAS", "SEARCH", "User Log In ID" + userid.ToString()) == true)
			{
				return View("UserMaster");
			}
			else
			{
				return View("Login");
			}
		}

		private panasonicdmsEntities db = new panasonicdmsEntities();
		private Dictionary<string, object> moResponse;


		public ActionResult fetchAndSetRoles()

		{
			List<tbrolemaster> rolesList = null;
			try
			{
				int userid = getUserId();
				if (userid == 0)
				{
					moResponse = RespMsgFormatter.formatResp(0, getMessage("SessionTimeOut"), null, "searchDocumentsMasterTab");
					return Json(moResponse, JsonRequestBehavior.AllowGet);
				}
				if (CanAccess("USERMAS", "SEARCH", "") == true)
				{
					MySqlParameter loRolesList = new MySqlParameter("@", "");
					var loParam = new object[] {
				   loRolesList
			   };
					String qury = "select fdRoleId, fdRoleCode, fdRoleName, fdDescription,fdStatus,fdCreatedBy,fdCreatedOn,fdUpdatedBy,fdUpdatedOn from tbrolemaster order by fdCreatedBy,fdDescription ";
					rolesList = db.Database.SqlQuery<tbrolemaster>(qury, loParam).ToList();
					if (rolesList != null)
					{
						moResponse = RespMsgFormatter.formatResp(1, getMessage("Success"), rolesList, "fetchAndSetRoles");
					}
					else
					{
						moResponse = RespMsgFormatter.formatResp(0, getMessage("GetNoResult"), null, "fetchAndSetRoles");
					}
				}
				else
				{
					moResponse = RespMsgFormatter.formatResp(0, getMessage("Access"), null, "fetchAndSetRoles");
				}
				return Json(moResponse, JsonRequestBehavior.AllowGet);

			}
			catch (Exception ex)
			{
				moResponse = RespMsgFormatter.formatResp(0, "MSG:" + ex.Message + " InnerException:" + ex.InnerException + "StackTrace:" + ex.StackTrace, null, "fetchAndSetRoles");
				throw new Exception();

			}

		}
		[HttpPost]
		public ActionResult saveUserMasterData(string requestData, UserInfo userInfo)
		{
			DbContextTransaction transaction = null;
			try
			{
				int userid = getUserId();
				if (userid == 0)
				{
					moResponse = RespMsgFormatter.formatResp(0, getMessage("SessionTimeOut"), null, "searchDocumentsMasterTab");
					return Json(moResponse, JsonRequestBehavior.AllowGet);
				}
				/*Function change to support tbusermaster instead of user table for pannadox user creation
				 *  so user can logged in  all the code related to user table has been commented revert comment to 
				 *  change set tbusermaster to user
				 *  Modified by "Manoj Kumar Shukla" on Dec 15,2018
				 * */

				JObject jobj = JObject.Parse(requestData);
				//user userMasterdata = new user();
				tbusermaster userMasterdata = new tbusermaster();
				int liUserId;
				int liRoleUserLinkID;
				string liUsertName = string.Empty;
				string liEmail = string.Empty;
				string liContactNo = string.Empty;
	//            DateTime liDateOfBirth;
	//            int liCollege;
	//            String liCity;
				//String liState;
				//String liCountry;
				//int liPincode;
				string liPassword = string.Empty;
				string liStatus = string.Empty;
				//string liAddress = string.Empty;
				int roleID,hdnuserid=0;
				string getModulefunction = string.Empty;

			  

					if (jobj["hdnUserId"] != null && jobj["hdnUserId"].ToString() != string.Empty)
					{
						hdnuserid = Convert.ToInt32(jobj["hdnUserId"].ToString());
						//userMasterdata.firstName = liUsertName;
						userMasterdata.fdUserId = hdnuserid;
					}

					if(hdnuserid==0)
				{
					getModulefunction = "CREATE";
				}
					else
				{
					getModulefunction = "UPDATE";
				}



				if (CanAccess("USERMAS", getModulefunction, requestData) == true)
				{

					if (jobj["userName"] != null && jobj["userName"].ToString() != string.Empty)
					{
						liUsertName = jobj["userName"].ToString();
						//userMasterdata.firstName = liUsertName;
						userMasterdata.fdUserName = liUsertName;
					}
					else
					{
						moResponse = RespMsgFormatter.formatResp(0, getMessage("Name"), null, "saveUserMasterData");
						return Json(moResponse, JsonRequestBehavior.AllowGet);
					}

					if (jobj["eMail"] != null && jobj["eMail"].ToString() != string.Empty)
					{
						liEmail = jobj["eMail"].ToString();
						//userMasterdata.email = liEmail;
						userMasterdata.fdUserEmail = liEmail;
						if (hdnuserid == 0)
						{
							var checkMail = db.tbusermasters.Where(s => s.fdUserEmail == liEmail).FirstOrDefault();
							if (checkMail != null)
							{
								moResponse = RespMsgFormatter.formatResp(0, "Email ID Already Register.", null, "saveUserMasterData");
								return Json(moResponse, JsonRequestBehavior.AllowGet);
							}
						}
					}
					else
					{
						moResponse = RespMsgFormatter.formatResp(0, getMessage("Email"), null, "saveUserMasterData");
						return Json(moResponse, JsonRequestBehavior.AllowGet);
					}

					if (jobj["contactNo"] != null && jobj["contactNo"].ToString() != string.Empty)
					{
						liContactNo = jobj["contactNo"].ToString();
						//userMasterdata.contactNumber = liContactNo;
						userMasterdata.fdUserContactNo = Convert.ToInt64(liContactNo);
					}
					else
					{
						moResponse = RespMsgFormatter.formatResp(0, getMessage("ContactNumber"), null, "SaveContactUsDetails");
						return Json(moResponse, JsonRequestBehavior.AllowGet);
					}


					if ( (liContactNo.Length < 10) || (liContactNo.Length > 10))
					{
						moResponse = RespMsgFormatter.formatResp(0, getMessage("ContactLength"), null, "SaveContactUsDetails");
						return Json(moResponse, JsonRequestBehavior.AllowGet);
					  //  alert(" Your Mobile Number must be 1 to 10 Integers");
					 
					}

					if (jobj["Department"] != null && jobj["Department"].ToString() != string.Empty)
					{

						//userMasterdata.email = liEmail;
						userMasterdata.fdDepartment = Convert.ToInt16(jobj["Department"].ToString());
					}
					else
					{
						if (hdnuserid == 0)
						{
							moResponse = RespMsgFormatter.formatResp(0, "Select Department", null, "saveUserMasterData");
							return Json(moResponse, JsonRequestBehavior.AllowGet);
						}
					}


					if (jobj["password"] != null && jobj["password"].ToString() != string.Empty)
					{
						liPassword = jobj["password"].ToString();
						//userMasterdata.password = liPassword;
						userMasterdata.fdUserPassword = liPassword;
					}
					else if (userMasterdata.fdUserId == 0)
					{
						moResponse = RespMsgFormatter.formatResp(0, getMessage("Password"), null, "saveUserMasterData");
						return Json(moResponse, JsonRequestBehavior.AllowGet);
					}

					if (jobj["status"] != null && jobj["status"].ToString() != string.Empty)
					{

						liStatus = jobj["status"].ToString();
						if (liStatus == "1")
						{
							//userMasterdata.isActive = true;
							userMasterdata.fdIsActive = 1;
						}
						else
						{
							//userMasterdata.isActive = false;
							userMasterdata.fdIsActive = 0;
						}


					}
					else
					{
						moResponse = RespMsgFormatter.formatResp(0, getMessage("Status"), null, "saveUserMasterData");
						return Json(moResponse, JsonRequestBehavior.AllowGet);
					}
					transaction = db.Database.BeginTransaction();


					if (userMasterdata.fdUserId != 0)
					{
						var getuser = db.tbusermasters.Find(userMasterdata.fdUserId);
						if (getuser != null)
						{
							getuser.fdUserName = userMasterdata.fdUserName;
							getuser.fdUserEmail = userMasterdata.fdUserEmail;
							getuser.fdUserContactNo = userMasterdata.fdUserContactNo;
							getuser.fdIsActive = userMasterdata.fdIsActive;
							if (userMasterdata.fdUserPassword != string.Empty && userMasterdata.fdUserPassword != null)
							{
								getuser.fdUserPassword = userMasterdata.fdUserPassword;
							}
						}
						//liUserId = Int32.Parse(jobj["userId"].ToString());
						////userMasterdata.id = liUserId;
						//userMasterdata.fdUserId = liUserId;
						db.Entry(getuser).State = System.Data.Entity.EntityState.Modified;
						db.SaveChanges();
						moResponse = RespMsgFormatter.formatResp(1, getMessage("UserUpdate"), "", "saveUserMasterData");
					}
					else
					{
						//db.users.Add(userMasterdata);
						db.tbusermasters.Add(userMasterdata);
						db.SaveChanges();
						//var getcontactUsDetail = db.users.Select(d => new { d.firstName, d.email, d.contactNumber, d.dob, d.college, d.city, d.state, d.country, d.pincode, d.password, d.isActive, d.address }).FirstOrDefault();

					}

					int roleStatus = 0;
					if (jobj["roleStatus"] != null && jobj["roleStatus"].ToString() != string.Empty)
					{
						roleStatus = int.Parse(jobj["roleStatus"].ToString());
					}

					if(roleStatus==-1 || roleStatus==1)
					{
						var getAllExistingROles = db.tbuserroleloclinks.Where(s => s.fdUserId == userMasterdata.fdUserId).ToList();
						foreach (var role in getAllExistingROles)
						{
							db.Entry(role).State = System.Data.Entity.EntityState.Deleted;
						}
						db.SaveChanges();
					}


					if (hdnuserid == 0)
					{
						JArray roles = JArray.Parse(jobj["userRoles"].ToString());
						List<tbuserroleloclink> llRoleLink = new List<tbuserroleloclink>();
						foreach (var role in roles)
						{
							tbuserroleloclink obj = new tbuserroleloclink();
							obj.fdRoleId = Convert.ToInt32(role.ToString());
							obj.fdUserId = userMasterdata.fdUserId;
							obj.fdCreatedBy = 1;
							obj.fdCreatedOn = DateTime.Now;
							obj.fdUpdatedBy = 1;
							obj.fdUpdatedOn = DateTime.Now;
							obj.fdLocationId = 1;
							llRoleLink.Add(obj);
						}
						db.tbuserroleloclinks.AddRange(llRoleLink);
						db.SaveChanges();
					}
					else
					{
						
							// clean All Roles
							JArray roles = JArray.Parse(jobj["userRoles"].ToString());
							List<tbuserroleloclink> llRoleLink = new List<tbuserroleloclink>();
							foreach (var role in roles)
							{
								tbuserroleloclink obj = new tbuserroleloclink();
								obj.fdRoleId = Convert.ToInt32(role.ToString());
								obj.fdUserId = userMasterdata.fdUserId;
								obj.fdCreatedBy = 1;
								obj.fdCreatedOn = DateTime.Now;
								obj.fdUpdatedBy = 1;
								obj.fdUpdatedOn = DateTime.Now;
								obj.fdLocationId = 1;
								llRoleLink.Add(obj);
							}
							db.tbuserroleloclinks.AddRange(llRoleLink);
							db.SaveChanges();

						
					}
					transaction.Commit();
					RefreshRoles();

					if (getUserId() == userMasterdata.fdUserId)
					{
						RefreshRoles();
					}
					var getcontactUsDetail = db.tbusermasters.Select(d => new
					{
						firstName = d.fdUserName,
						email = d.fdUserEmail,
						contactNumber = d.fdUserContactNo
					}).FirstOrDefault();

					moResponse = RespMsgFormatter.formatResp(1, getMessage("UserSave"), getcontactUsDetail, "saveUserMasterData");
				}
				else
				{
					moResponse = RespMsgFormatter.formatResp(0, getMessage("Access"), null, "saveUserMasterData");
				}

			}

			catch (Exception ex)
			{
				moResponse = RespMsgFormatter.formatResp(0, "MSG:" + ex.Message + " InnerException:" + ex.InnerException + "StackTrace:" + ex.StackTrace, null, "saveUserMasterData");

				try
				{
					if (transaction != null && transaction.UnderlyingTransaction != null)
					{
						transaction.Rollback();
					}
				}
				catch(Exception x)
				{

				}
				
			}
			return Json(moResponse, JsonRequestBehavior.AllowGet);

		}
		// Get College name List In SearchField

		//public ActionResult getCollegeList(string requestData)
		//{
		//    try
		//    {
		//        string licollegeName = string.Empty;
		//        JObject jsonObj = JObject.Parse(requestData);
		//        if (jsonObj["userCollege"] != null && jsonObj["userCollege"].ToString() != string.Empty)
		//        {
		//            licollegeName = jsonObj["userCollege"].ToString();
		//        }
		//        else
		//        {
		//            moResponse = RespMsgFormatter.formatResp(0, "Please Provide Location City!", null, "getCollegeList");
		//            return Json(moResponse, JsonRequestBehavior.AllowGet);
		//        }
		//        String query = "select CollegeId as id, CollegeName as name, CityId, fdCreatedBy, fdCreatedOn, fdUpdatedBy, fdUpdatedOn " +
		//            " from tbcollege where CollegeName like @college";

		//        MyMySqlParameter loStateParam = new MyMySqlParameter("@college", licollegeName + "%");
		//        var loParam = new object[] { loStateParam };
		//        var getDataFromTable = db.Database.SqlQuery<College>(query, loParam).ToList();

		//        if (getDataFromTable != null)
		//        {
		//            moResponse = RespMsgFormatter.formatResp(1, "Succesfull Execution", getDataFromTable, "getCollegesList");

		//        }
		//        else
		//        {
		//            moResponse = RespMsgFormatter.formatResp(0, "No record found ", null, "getCollegesList");

		//        }
		//    }
		//    catch (Exception ex)
		//    {
		//        moResponse = RespMsgFormatter.formatResp(0, " " + ex.Message, "", "getCollegesList");
		//    }
		//    return Json(moResponse, JsonRequestBehavior.AllowGet);
		//}


		// Get Cities List in search fileld.
		//public ActionResult getCitiesList(string requestData)
		//{
		//    try
		//    {
		//        String liUserCity = String.Empty;
		//        JObject jsonObj = JObject.Parse(requestData);
		//        if (jsonObj["userCity"] != null && jsonObj["userCity"].ToString() != string.Empty)
		//        {
		//            liUserCity = jsonObj["userCity"].ToString();
		//        }
		//        else
		//        {
		//            moResponse = RespMsgFormatter.formatResp(0, "Please Provide Location City!", null, "getCitiesList");
		//            return Json(moResponse, JsonRequestBehavior.AllowGet);
		//        }
		//        String query = "select CityId as id,CityName as name,StateId,IsActive,fdCreatedBy,fdCreatedOn,UPDBy,UPDBy " +
		//            " from tbcity where CityName like @city";

		//        MyMySqlParameter loStateParam = new MyMySqlParameter("@city", liUserCity + "%");
		//        var loParam = new object[] { loStateParam };
		//        var getDataFromTable = db.Database.SqlQuery<Cities>(query, loParam).ToList();

		//        if (getDataFromTable != null)
		//        {
		//            moResponse = RespMsgFormatter.formatResp(1, "Succesfull Execution", getDataFromTable, "getCitiesList");

		//        }
		//        else
		//        {
		//            moResponse = RespMsgFormatter.formatResp(0, "No record found ", null, "getCitiesList");

		//        }
		//    }
		//    catch (Exception ex)
		//    {
		//        moResponse = RespMsgFormatter.formatResp(0, " " + ex.Message, "", "getCitiesList");

		//    }
		//    return Json(moResponse, JsonRequestBehavior.AllowGet);
		//}

		//Get State List in search field

		//public ActionResult getStateList(string requestData)
		//{
		//    try
		//    {
		//        String liUserState = String.Empty;
		//        JObject jsonObj = JObject.Parse(requestData);
		//        if (jsonObj["userState"] != null && jsonObj["userState"].ToString() != string.Empty)
		//        {
		//            liUserState = jsonObj["userState"].ToString();
		//        }

		//        String query = "select StateId as id, CountryId, StateDesc as name, externalstate, isActive, fdCreatedBy, fdCreatedOn, UPDBy, UPDdate, RowVersion " +
		//            " from tbstate where StateDesc like @state";

		//        MyMySqlParameter loStateParam = new MyMySqlParameter("@state", liUserState + "%");
		//        var loParam = new object[] { loStateParam };
		//        var getDataFromTable = db.Database.SqlQuery<States>(query, loParam).ToList();

		//        if (getDataFromTable != null)
		//        {
		//            moResponse = RespMsgFormatter.formatResp(1, "Succesfull Execution", getDataFromTable, "getStateList");

		//        }
		//        else
		//        {
		//            moResponse = RespMsgFormatter.formatResp(0, "No record found ", null, "getStateList");

		//        }
		//    }
		//    catch (Exception ex)
		//    {
		//        moResponse = RespMsgFormatter.formatResp(0, " " + ex.Message, "", "getStateList");

		//    }
		//    return Json(moResponse, JsonRequestBehavior.AllowGet);
		//}

		////Get State List in search field

		//public ActionResult getCountryList(string requestData)
		//{
		//    try
		//    {
		//        String liUserCountry = String.Empty;
		//        JObject jsonObj = JObject.Parse(requestData);
		//        if (jsonObj["userCountry"] != null && jsonObj["userCountry"].ToString() != string.Empty)
		//        {
		//            liUserCountry = jsonObj["userCountry"].ToString();
		//        }

		//        String query = "select CountryId as id, ISOCountryCode, ISOCountryCode2, Countryname as name, isActive, fdCreatedBy, fdCreatedOn, UPDBy, UPDdate, associatedCurrency, currencyCode " +
		//            " from tbcountry where Countryname like @country";

		//        MyMySqlParameter loStateParam = new MyMySqlParameter("@country", liUserCountry + "%");
		//        var loParam = new object[] { loStateParam };
		//        var getDataFromTable = db.Database.SqlQuery<tbcountry>(query, loParam).ToList();

		//        if (getDataFromTable != null)
		//        {
		//            moResponse = RespMsgFormatter.formatResp(1, "Succesfull Execution", getDataFromTable, "getCountryList");

		//        }
		//        else
		//        {
		//            moResponse = RespMsgFormatter.formatResp(0, "No record found ", null, "getCountryList");

		//        }
		//    }
		//    catch (Exception ex)
		//    {
		//        moResponse = RespMsgFormatter.formatResp(0, " " + ex.Message, "", "getCountryList");

		//    }
		//    return Json(moResponse, JsonRequestBehavior.AllowGet);
		//}



		/// <summary>
		/// This method will return detail data of requested user id
		/// </summary>
		/// <returns>ActionResult</returns>
		/// <remarks></remarks>
		public ActionResult GetUserDetail(string requestData)
		{
			try
			{
				int userid = getUserId();
				if (userid == 0)
				{
					moResponse = RespMsgFormatter.formatResp(0, getMessage("SessionTimeOut"), null, "searchDocumentsMasterTab");
					return Json(moResponse, JsonRequestBehavior.AllowGet);
				}
				if (CanAccess("USERMAS", "SEARCH", requestData) == true)
				{
					int liUserId = Convert.ToInt32(requestData);
					//string lsUserDetails = "select usr.fduserid ,usr.fdusername as fdusername,usr.fdUserContactNo as fdUserContactNo,usr.fdUserEmail as fdUserEmail,usr.fdDepartment as fdDepartment, usr.fdIsActive as UserStatus ,"
					//					 + " ifnull( STUFF((SELECT ',', '' + rollnk.fdRoleId  FROM tbusermaster um "
					//					 + " join tbuserroleloclink rollnk on rollnk.fdUserId = um.fdUserId where um.fdUserId = " + liUserId + " "
					//					 + " FOR XML PATH('')), 1, 1, ''),'')   as Role"
					//					 + " from tbusermaster usr left join tbuserroleloclink lnk on lnk.fdUserId = usr.fdUserId where usr.fduserid =" + liUserId + " group by usr.fdUserId,usr.fdusername,usr.fdUserContactNo,usr.fdUserEmail,usr.fdDepartment,usr.fdIsActive";

					string lsUserDetails = "select usr.fduserid ,usr.fdusername as fdusername,usr.fdUserContactNo as fdUserContactNo,usr.fdUserEmail as fdUserEmail,"
											+" usr.fdDepartment as fdDepartment, usr.fdIsActive as UserStatus , group_concat(lnk.fdRoleId) as Role from tbusermaster usr"
											+" left join tbuserroleloclink lnk on lnk.fdUserId = usr.fdUserId where usr.fduserid =" + liUserId  + " group by usr.fdUserId,usr.fdusername,"
											+" usr.fdUserContactNo,usr.fdUserEmail,usr.fdDepartment,usr.fdIsActive";

					var dbReult = db.Database.SqlQuery<UserDetails>(lsUserDetails).FirstOrDefault();
					if (dbReult.fduserid == 0)
					{
						moResponse = RespMsgFormatter.formatResp(0, getMessage("GetNoResult"), null, "GetUserDetail");
					}
					else
					{
						moResponse = RespMsgFormatter.formatResp(1, getMessage("Success"), dbReult, "GetUserDetail");
					}
				}
				else
				{
					moResponse = RespMsgFormatter.formatResp(1, getMessage("Access"), null, "GetUserDetail");
				}
			}
			catch(Exception ex)
			{
				moResponse = RespMsgFormatter.formatResp(0, "message:" + ex.Message + " inner Exception:" + ex.InnerException, null, "GetUserDetail");
			}
			return Json(moResponse,JsonRequestBehavior.AllowGet);
		}


		//---------This function is used to search user in db modified by manoj shukla on Dec 15 ,2018 .before modification code was commented-------//

		/// <param name="requestData"></param>
		[HttpPost]
		public ActionResult searchUserMasterTab(string requestData)
		{
			try
			{
				int userid = getUserId();
				if (userid == 0)
				{
					moResponse = RespMsgFormatter.formatResp(0, getMessage("SessionTimeOut"), null, "SaveDocumentClass");
					return Json(moResponse, JsonRequestBehavior.AllowGet);
				}
				if (CanAccess("USERMAS", "SEARCH", requestData) == true)
				{
					String liUserName = String.Empty;
					String liContactNumber = String.Empty;
					String liUserId = String.Empty;
					MySqlParameter liUserIDParam = new MySqlParameter("@", "");
					MySqlParameter liUserNameparam = new MySqlParameter("@userName", "");
					MySqlParameter liContactNoparam = new MySqlParameter("@contactNo", "");
					JObject jsonObj = JObject.Parse(requestData);
					if (jsonObj["userName"] != null && jsonObj["userName"].ToString() != string.Empty)
					{
						liUserName = jsonObj["userName"].ToString();
					}
					else
					{
						liUserName = "";
					}
					if (jsonObj["contactNumber"] != null && jsonObj["contactNumber"].ToString() != string.Empty)
					{
						liContactNumber = jsonObj["contactNumber"].ToString();
					}
					else
					{
						liContactNumber = "";
					}

					//if (jsonObj["searchUserId"] != null && jsonObj["searchUserId"].ToString() != string.Empty)
					//{
					//    liUserId = jsonObj["searchUserId"].ToString();
					//}
					//else
					//{
					//    liUserId = "";
					//}

					String query = "select fduserid,fdusername,fdUserContactNo,fdUserEmail,case when fdIsActive=1 then 'Active' else 'Inactive' end as UserStatus "
									+ " from tbusermaster where 1 = 1 ";

					if (liUserName != null && liUserName != "")
					{
						query = query + " and fdusername like @userName";
						liUserNameparam = new MySqlParameter("@userName", liUserName + "%");
					}
					if (liContactNumber != null && liContactNumber != "")
					{
						query = query + " and fdUserContactNo like @contactNo";
						liContactNoparam = new MySqlParameter("@contactNo", liContactNumber + "%");
					}
					//if (liUserId != null && liUserId != "")
					//{
					//    query = query + " and user.id = @UserID";
					//    liUserIDParam = new MyMySqlParameter("@UserID", liUserId);
					//}

					var loParam = new object[] { liUserNameparam, liContactNoparam, liUserIDParam };
					var getDataFromTable = db.Database.SqlQuery<UserSearch>(query+ " limit " + Constant.SEARCH_LIMIT, loParam).ToList();

					if (getDataFromTable.Count == Constant.SEARCH_LIMIT)
					{
						moResponse = RespMsgFormatter.formatResp(1, Constant.SEARCH_LIMIT + " " + getMessage("Record"), getDataFromTable, "getUserMasterDataTable");
					}
					else if (getDataFromTable.Count == 0)
					{
						moResponse = RespMsgFormatter.formatResp(0, getMessage("GetNoResult"), null, "getUserMasterDataTable");
						// moResponse = RespMsgFormatter.formatResp(1, "Succesfull Execution", getDataFromTable, "getUserMasterDataTable");
					}
					else
					{
						moResponse = RespMsgFormatter.formatResp(1, getDataFromTable.Count + " " + getMessage("RecordFound"), getDataFromTable, "getUserMasterDataTable");
					}
				}
				else
				{
					moResponse = RespMsgFormatter.formatResp(0, getMessage("Access"), null, "getUserMasterDataTable");
				}
			}
			catch (Exception ex)
			{
				moResponse = RespMsgFormatter.formatResp(0, " Message" + ex.Message + " InnerException:" + ex.InnerException, "", "searchUserMasterTab");

			}
			return Json(moResponse, JsonRequestBehavior.AllowGet);
		}

		public ActionResult searchUsersUsingSearchBar(string requestData)
		{
			try
			{
				int userid = getUserId();
				if (userid == 0)
				{
					moResponse = RespMsgFormatter.formatResp(0, getMessage("SessionTimeOut"), null, "SaveDocumentClass");
					return Json(moResponse, JsonRequestBehavior.AllowGet);
				}

				if (CanAccess("USERMAS", "SEARCH", requestData) == true)
				{
					JObject jObject = JObject.Parse(requestData);
					MySqlParameter loName = new MySqlParameter("@searchkeyword", "");
					string qry =
						"select fduserid,fdusername,fdUserContactNo,fdUserEmail,case when fdIsActive=1 then 'Active' else 'Inactive' end as UserStatus "
										+ " from tbusermaster where 1 = 1 ";

					if (jObject["searchInputKeyword"].ToString() != null && jObject["searchInputKeyword"].ToString() != string.Empty)
					{
						qry = qry + "and ( fdUserContactNo like @searchkeyword or fdUserName like @searchkeyword or fdUserEmail like @searchkeyword)";
						string value = jObject["searchInputKeyword"].ToString();
						loName = new MySqlParameter("@searchkeyword", value + "%");
					}

					var doclist = db.Database.SqlQuery<UserSearch>(qry, loName).ToList();
					if (doclist.Count > 0)
					{
						moResponse = RespMsgFormatter.formatResp(1, doclist.Count + " " + getMessage("RecordFound"), doclist, "searchDocuments");
					}
					else
					{
						moResponse = RespMsgFormatter.formatResp(0, getMessage("GetNoResult"), doclist, "searchDocuments");
					}
				}
				else
				{
					moResponse = RespMsgFormatter.formatResp(0, getMessage("Access"), null, "searchDocuments");
				}
				
			}
			catch(Exception ex)
			{
				moResponse = RespMsgFormatter.formatResp(0, " Message" + ex.Message + " InnerException:" + ex.InnerException, "", "searchUserMasterTab");

			}
			return Json(moResponse, JsonRequestBehavior.AllowGet);
		}
	   


		public ActionResult GetUserRole(string requestData)
		{
			try
			{
				int userid = getUserId();
				if (userid == 0)
				{
					moResponse = RespMsgFormatter.formatResp(0, getMessage("SessionTimeOut"), null, "SaveDocumentClass");
					return Json(moResponse, JsonRequestBehavior.AllowGet);
				}

				
				int CheckRoleSigng = 0;
				var getRole = "";


				JObject jsonObj = JObject.Parse(requestData);

				int UserID = 0;
				if (jsonObj["userID"] != null && jsonObj["userID"].ToString() != string.Empty)
				{
					UserID =int.Parse(jsonObj["userID"].ToString());
				}
				MySqlParameter UserIDPmtr = new MySqlParameter("@UserId", UserID);
				if (jsonObj["RoleStatus"] != null && jsonObj["RoleStatus"].ToString() != string.Empty)
				{
					CheckRoleSigng = int.Parse(jsonObj["RoleStatus"].ToString());
				}


				 

					if (CheckRoleSigng== 1)
					{
						 getRole = "select rol.fdRoleId as fdRoleId,rol.fdRoleCode as fdRoleCode ,fdRoleName as fdRoleName ,1 as Signed from tbrolemaster rol "
								   + " join tbuserroleloclink uslink on uslink.fdRoleId = rol.fdRoleId "
								   + " where uslink.fdUserId =@UserId ";
					}
				  else  if (CheckRoleSigng == 0)
					{
						 getRole = "select rol.fdRoleId,rol.fdRoleCode,fdRoleName,0 as Signed from tbrolemaster rol "
								 + " left join tbuserroleloclink uslink on uslink.fdRoleId = rol.fdRoleId and uslink.fdUserId = @UserId "
								 + " where rol.fdRoleId not in (select rol.fdRoleId from tbrolemaster rol "
								 +" join tbuserroleloclink uslink on uslink.fdRoleId = rol.fdRoleId "
								 + " where uslink.fdUserId = @UserId)";
					}
					else
					{
						getRole = "select rol.fdRoleId,rol.fdRoleCode,fdRoleName, case when uslink.fdRoleId is null then 0 else 1 end as Signed from tbrolemaster rol "
								  + " left join tbuserroleloclink uslink on uslink.fdRoleId = rol.fdRoleId and uslink.fdUserId =@UserId where 1 = 1 ";
					}
				
			   

				var getResult = db.Database.SqlQuery<User>(getRole, UserIDPmtr).ToList();
				if(getResult.Count>0)
				{
					moResponse = RespMsgFormatter.formatResp(1, getMessage("Success"), getResult, "SaveDocumentClass");
				}
				else
				{
					moResponse = RespMsgFormatter.formatResp(0, getMessage("GetNoResult"), null, "SaveDocumentClass");
				}
			   


			}
			catch(Exception ex)
			{
				moResponse = RespMsgFormatter.formatResp(0, " Message" + ex.Message + " InnerException:" + ex.InnerException, "", "GetUserRole");

			}
			return Json(moResponse, JsonRequestBehavior.AllowGet);
		}



	}


}











