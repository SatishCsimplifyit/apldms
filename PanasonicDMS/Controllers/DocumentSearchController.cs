﻿using PanasonicDMS.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PanasonicDMS.Models;

namespace PanasonicDMS.Controllers
{
    public class DocumentSearchController : CommonController
    {
        // GET: DocumentSearch
        private UserLocationMonduleFunction moLocModuleFunction = new UserLocationMonduleFunction();
        public ActionResult Index()
        {

            int userid = getUserId();
            
            if (userid == 0)
            {
                TempData["msg"] = "<script>showMessage('red', 'The session is expired, please login again'); </script>";
                return View("Login");
            }
            if (CanAccess("HOMEMAS", "SEARCH", "User Log In ID" + userid.ToString()) == true)
            {
                return View("DocumentSearch");
            }
            else
            {
                return View("Login");
            }
           
           
          

        }
    }
}