﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PanasonicDMS.App_Code;
using PanasonicDMS.Models;
using Newtonsoft.Json.Linq;
using System.IO;
using System.Data.Entity;
using System.Threading.Tasks;
using System.Text;
using PanasonicDMS.Models.DbContext;
using System.Web.Configuration;
using System.Data.SqlClient;
using PanasonicDMS.QR_Code;
using System.Drawing;
using Newtonsoft.Json;
using System.Web.Script.Serialization;
using System.Globalization;
using System.Web.UI.WebControls;
using System.Net.Http;

namespace PanasonicDMS.Controllers
{
    public class DocumentController : CommonController
    {
       
        private panasonicdmsEntities db = new panasonicdmsEntities();
        private Dictionary<string, object> moResponse;
        // GET: Document

        public ActionResult Index()
        {
            int userid = getUserId();
            if(userid==0)
            {
                TempData["msg"] = "<script>showMessage('red', 'The session is expired, please login again'); </script>";
                return View("Login");
            }
            if (CanAccess("UPLOADMAS", "SEARCH", "User Log In ID" + userid.ToString()) == true)
            {
               
            }
            return View("Document");
        }
        
        // function for get document form controls
        public ActionResult getDocumentForm(string requestData)
        {
            try
            {
                int liDocType = 0;
                if (requestData != string.Empty)
                {
                    bool lbsucces = int.TryParse(requestData.ToString(), out liDocType);
                    if (lbsucces == true)
                    {
                        int liuseId = 1;// getUserId();
                        if (liuseId == 0)
                        {
                            moResponse = RespMsgFormatter.formatResp(0, getMessage("SessionTimeOut"), null, "getDocumentForm");
                            return Json(moResponse, JsonRequestBehavior.AllowGet);
                        }
                        var getFormControlField = db.tbdocumentclasses.Where(s => s.associatedDocType == liDocType && s.Status==0)
                            .Select(s => new
                            {
                                s.dcid,
                                s.dcColumnName,
                                s.dcColumnType,
                                s.isRequired,
                                s.sequence,
                                s.maxValue,
                                s.minValue,
                                s.defaultValue,
                                s.x1,
                                s.x2,
                                s.y1,
                                s.y2,
                                s.lov
                            }).OrderByDescending(s=>s.sequence).ToList()
                        .OrderBy(s => s.sequence);


                        if(getFormControlField.Count()>0)
                        {
                            moResponse = RespMsgFormatter.formatResp(1,getMessage("Success"), getFormControlField, "getDocumentForm");
                        }
                        else
                        {
                            moResponse = RespMsgFormatter.formatResp(0,getMessage("GetNoResult"), getFormControlField, "getDocumentForm");
                        }
                       
                        return Json(moResponse, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        moResponse = RespMsgFormatter.formatResp(0,getMessage("DocTypeParse"), null, "getDocumentForm");
                        return Json(moResponse, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    moResponse = RespMsgFormatter.formatResp(0, getMessage("Requset"), null, "getDocumentForm");
                    return Json(moResponse, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                moResponse = RespMsgFormatter.formatResp(0, "" + ex.Message + "  Inner Exception:" + ex.InnerException + "<br> Stack Trace:" + ex, null, "getDocumentForm");
            }

            return Json(moResponse, JsonRequestBehavior.AllowGet);


        }


        public async Task<ActionResult> UploadPOD()
        {
            DbContextTransaction transection = null;
            try
            {

                string[] lsVinID; int liVinID; string[] lsaLatitude, lsaLongitude, lsaAddress;
                string lsLatitude = string.Empty, lsLongitude = string.Empty, lsAddress = string.Empty;
                int liUserId = 0;
                liUserId = getUserId();
                if (liUserId == 0)
                {
                    moResponse = RespMsgFormatter.formatResp(0, getMessage("SessionTimeOut"), null, "fetchContract");
                    return Json(moResponse, JsonRequestBehavior.AllowGet);
                }


                string[] lsaDocumentID; int lsDocumentID = 0;

                if (Request.Form.AllKeys.Contains("hdnDocumentID"))
                {
                    lsaDocumentID = Request.Form.GetValues("hdnDocumentID");
                    if (lsaDocumentID[0] != "")
                    {
                        lsDocumentID = int.Parse(lsaDocumentID[0].ToString());
                    }

                }
                //if (CanAccess("UPLOADMAS", "UPLOAD_DOCUMENT", "Document" + lsDocumentID.ToString()) == true)
                //{
                    string[] lsaFileType; int lsFileType;


                    if (Request.Form.AllKeys.Contains("FileType"))
                    {
                        lsaFileType = Request.Form.GetValues("FileType");
                        lsFileType = int.Parse(lsaFileType[0].ToString());
                    }
                    else
                    {
                        moResponse = RespMsgFormatter.formatResp(0, getMessage("FileType"), null, "fetchContract");
                        return Json(moResponse, JsonRequestBehavior.AllowGet);
                    }
                var GetModuleCode = db.tbmodulemasters.Where(s => s.fdModuleCode == "DC" + lsFileType).Select(s => s.fdModuleCode).FirstOrDefault();

                if (lsDocumentID == 0)
                    {
                        if (Request.Files.Count > 0)

                            if (lsDocumentID == 0)

                            {
                               
                                HttpFileCollectionBase files = Request.Files;
                                HttpPostedFileBase file = files[0];

                                transection = db.Database.BeginTransaction();
                                tbdocument liDocument = new tbdocument();
                                List<tbdocument> lldoc = new List<tbdocument>();

                                // set its properties
                                var filename = "";
                             
                                //if (Request.Browser.Browser.ToUpper() == "IE" || Request.Browser.Browser.ToUpper() == "INTERNETEXPLORER")
                                //{
                                //    string[] testfiles = file.FileName.Split(new char[] { '\\' });
                                //    filename = testfiles[testfiles.Length - 1];
                                //}
                               
                                //else
                                //{
                                
                                filename = Path.GetFileName(file.FileName);
                                
                                //}


                                liDocument = null;
                                liDocument = new tbdocument();
                                liDocument.fdDocType = lsFileType;
                                liDocument.fdDocTitle = filename;
                                liDocument.fdCreatedBy = getUserId();
                                liDocument.fdCreatedOn = DateTime.Now;
                                liDocument.fdStatus = 1;
                                liDocument.fdVersion = 1;
                                liDocument.fdFileUrl = "";
                                liDocument.fdDocGuID = System.Guid.NewGuid().ToString();
                                db.tbdocuments.Add(liDocument);
                                db.SaveChanges();

                                if (CanAccess(GetModuleCode, "UPLOAD_DOCUMENT", "", liDocument.fdDocId) == false)
                                {

                                    transection.Rollback();
                                    moResponse = RespMsgFormatter.formatResp(0, getMessage("Access"), null, "GetDocetumentDetails");
                                    return Json(moResponse, JsonRequestBehavior.AllowGet);
                                }
                                string lsExtension = Path.GetExtension(file.FileName);

                               


                                if (lsDocumentID == 0)
                                {
                                    if (lsExtension.ToLower() != ".pdf" && lsExtension.ToLower() != ".jpg" && lsExtension.ToLower() != ".jpeg" && lsExtension.ToLower() != ".png")
                                    {
                                        transection.Rollback();
                                        moResponse = RespMsgFormatter.formatResp(0, "Please upload a Pdf File.", null, "GetDocumentDetails");
                                        return Json(moResponse, JsonRequestBehavior.AllowGet);
                                    }

                                }


                                string lsServerPath = Server.MapPath("~/Documents/");
                                //string lsRelative = "Documents/" + liDocument.fdDocId.ToString() + lsExtension;


                                var path = liDocument.fdDocGuID.ToString() + lsExtension;
                                var getSpID = db.tbdocumentclasses.Where(s => s.associatedDocType == lsFileType).Select(d => d.AssSpId).FirstOrDefault();
                                var uncPath = "";
                                var QrImgPath = "";
                                if (getSpID != null)
                                {
                                    var getUncPath = db.tbstoragepolicies.Where(s => s.PKID == getSpID).FirstOrDefault();
                                    if (getUncPath != null)
                                    {
                                        uncPath = @getUncPath.fdSpUNCpath + path;
                                        QrImgPath = @getUncPath.fdSpUNCpath;
                                    }
                                    else
                                    {
                                        uncPath = @"\\Tarun-PC\Documents/" + path;
                                        QrImgPath = @"\\Tarun-PC\Documents/";
                                    }

                                }
                                else
                                {
                                    uncPath = @"\\Tarun-PC\Documents/" + path;
                                    QrImgPath = @"\\Tarun-PC\Documents/";
                                }


                                string lsRelative = "Documents/" + liDocument.fdDocGuID.ToString() + lsExtension;
                                string lsFileNameWithPath = Path.Combine(System.Web.HttpRuntime.AppDomainAppPath, lsRelative);


                                file.SaveAs(uncPath);
                                liDocument.fdFileUrl = uncPath;
                                liDocument.fdDocTitle = filename;
                                db.tbdocuments.Attach(liDocument);
                                var lvUpdateQuery = db.Entry(liDocument);
                                lvUpdateQuery.Property(e => e.fdFileUrl).IsModified = true;
                                lvUpdateQuery.Property(e => e.fdDocTitle).IsModified = true;
                                db.SaveChanges();
                                tbdocumentmetadata loMeta;
                                int itmNo = 0;
                                List<string> keys = new List<string>();
                                keys = Request.Form.AllKeys
                                          .Select(s => s).ToList();
                                foreach (var key in keys)
                                {
                                    if (key != "FileType" && key != "hdnDocumentID" && key != "Status" && key != "Version")
                                    {
                                        var lsaKey = Request.Form.GetValues(key);
                                        var lsKeyValue = lsaKey[0].ToString();
                                        itmNo = itmNo + 1;
                                        loMeta = new tbdocumentmetadata();
                                        loMeta.fdItemNo = itmNo;
                                        loMeta.fdKey = key;
                                        loMeta.fdValue = lsKeyValue;
                                        loMeta.fdDocId = liDocument.fdDocId;
                                        db.tbdocumentmetadatas.Add(loMeta);
                                        loMeta = null;
                                    }
                                }
                                db.SaveChanges();

                                int liUserID = getUserId();
                                var username = db.tbusermasters.Where(s => s.fdUserId == liUserID).FirstOrDefault();

                                var jsonData = "{\"docid\":" + liDocument.fdDocId + ",\"url\":\"csimplifyit://docunow/\"" + liDocument.fdDocGuID + ", "
                                   + " \"appname\":\"docunow\",\"defaulttemplate\":" + liDocument.fdDocGuID + ",\"createdon\":" + DateTime.Now + ",\"createdBy\":" + username.fdUserName + "}";

                                string json = new JavaScriptSerializer().Serialize(jsonData);
                                //  var result = JsonConvert.DeserializeObject(jsonData);
                                Bitmap x = QR_Code_Generator.QRCode_Generator(json);
                                // string lsExtension1 = Path.GetExtension(x.);
                                var QrCodeFolderName = db.tbcodelkups.Where(s => s.fdLkUpCode == "QR_Code_URL" && s.fdKeyCode == 1).Select(s => new { folderName = s.fdKey1 }).ToList();

                                tbqrcode qrcd = new tbqrcode();
                                qrcd.fdAssDocID = liDocument.fdDocId;
                                qrcd.fdFileUrl = QrImgPath+ QrCodeFolderName[0].folderName.ToString()  + liDocument.fdDocGuID + ".png";
                                db.tbqrcodes.Add(qrcd);
                                db.SaveChanges();
                                x.Save(QrImgPath+ QrCodeFolderName[0].folderName.ToString()  + liDocument.fdDocGuID + ".png");
                                transection.Commit();
                                var imageUrl = qrcd.fdFileUrl;


                                Dictionary<string, object> Result = new Dictionary<string, object>();
                                Result.Add("FileUrl", qrcd.fdFileUrl);
                                var byteArray = System.IO.File.ReadAllBytes(qrcd.fdFileUrl);
                                var base64 = Convert.ToBase64String(byteArray);
                                Result.Add("DocumentID", liDocument.fdDocId);
                                Result.Add("ImageData", base64);
                                moResponse = RespMsgFormatter.formatResp(1, getMessage("Save"), Result, "UploadPOD");
                            }
                            else
                            {
                                moResponse = RespMsgFormatter.formatResp(0, getMessage("EmptyFile"), null, "UploadPOD");
                            }


                    }
                    else
                    {

                       if (CanAccess(GetModuleCode, "UPDATE", "", lsDocumentID) == false)
                        {               
                             moResponse = RespMsgFormatter.formatResp(0, getMessage("Access"), null, "GetDocetumentDetails");
                             return Json(moResponse, JsonRequestBehavior.AllowGet);
                        }

                         List<string> keys = new List<string>();
                         keys = Request.Form.AllKeys.Select(s => s).ToList();

                        tbdocumentmetadata loMeta = new tbdocumentmetadata();
                       
                        int itmNo = 0;
                         var getItemNumbers = db.tbdocumentmetadatas.Where(s => s.fdDocId == lsDocumentID).Count();
                        int getItemNo = 0;
                        if (getItemNumbers!=0)
                          {
                              getItemNo = db.tbdocumentmetadatas.Where(s => s.fdDocId == lsDocumentID).Max(d => d.fdItemNo);
                          }
                        
                        if (getItemNo != 0 || getItemNo != null)
                        {
                            itmNo = getItemNo;
                        }
                        foreach (var key in keys)

                        {
                            itmNo = getItemNo;
                        }
                        foreach (var key in keys)
                        {
                            if (key != "FileType" && key != "hdnDocumentID" && key != "Status" && key != "Version")
                            {
                                var lsaKey = Request.Form.GetValues(key);
                                var lsKeyValue = lsaKey[0].ToString();
                                var docMetaDeta = db.tbdocumentmetadatas.Where(s => s.fdDocId == lsDocumentID && s.fdKey == key).FirstOrDefault();
                                if (docMetaDeta != null)
                                {
                                    docMetaDeta.fdValue = lsKeyValue;
                                    db.Entry(docMetaDeta).State = System.Data.Entity.EntityState.Modified;
                                    db.SaveChanges();
                                }
                                else
                                {

                                    itmNo = itmNo + 1;
                                    loMeta = new tbdocumentmetadata();
                                    loMeta.fdItemNo = itmNo;
                                    loMeta.fdKey = key;
                                    loMeta.fdValue = lsKeyValue;
                                    loMeta.fdDocId = lsDocumentID;
                                    db.tbdocumentmetadatas.Add(loMeta);
                                    loMeta = null;

                                }

                            }
                        }
                        db.SaveChanges();
                        moResponse = RespMsgFormatter.formatResp(1, getMessage("Update"), null, "UploadPOD");
                    }
               // }
                //else
                //{
                //    moResponse = RespMsgFormatter.formatResp(0, getMessage("Access"), null, "UploadPOD");
                //}
            }

            catch (Exception ex)

            {

                if (transection != null && transection.UnderlyingTransaction != null)
                {
                    transection.Rollback();
                }

                moResponse = RespMsgFormatter.formatResp(0, "message" + ex.Message + "InnerException:" + ex.InnerException, null, "UploadPOD");

            }
            return Json(moResponse, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public ActionResult uploadFileJsonResult( int fileType)

        {
            

            DbContextTransaction transection = null;

            var UserID = getUserId();
            var file1 = System.Web.HttpContext.Current.Request.Files.Count > 0 ? System.Web.HttpContext.Current.Request.Files[0] : null;
            //var data=System.Web.HttpContext.Current.Request
            //HttpContext.Current.Request.Files.Count > 0 ? HttpContext.Current.Request.Files[0] : null;
          


            if (file1.ContentLength > 0)
            {
                var fileName = Path.GetFileName(file1.FileName);
                var path = Path.Combine(System.Web.HttpContext.Current.Server.MapPath("~/tempdocs"), fileName);
                file1.SaveAs(path);
            }

            string[] lsaDocumentID; int lsDocumentID = 0;

           
            string[] lsaFileType; int lsFileType;


            if (fileType!=0)
            {
                lsaFileType = Request.Form.GetValues("FileType");
                lsFileType = fileType;
            }
            else
            {
                moResponse = RespMsgFormatter.formatResp(0, getMessage("FileType"), null, "fetchContract");
                return Json(moResponse, JsonRequestBehavior.AllowGet);
            }
            var GetModuleCode = db.tbmodulemasters.Where(s => s.fdModuleCode == "DC" + lsFileType).Select(s => s.fdModuleCode).FirstOrDefault();

            
                

                    if (lsDocumentID == 0)

                    {

                        HttpFileCollectionBase files = Request.Files;
                        HttpPostedFileBase file = files[0];
                        var name = file.FileName;

                        transection = db.Database.BeginTransaction();
                        tbdocument liDocument = new tbdocument();
                        List<tbdocument> lldoc = new List<tbdocument>();

                        // set its properties
                        var filename = "";

                        //if (Request.Browser.Browser.ToUpper() == "IE" || Request.Browser.Browser.ToUpper() == "INTERNETEXPLORER")
                        //{
                        //    string[] testfiles = file.FileName.Split(new char[] { '\\' });
                        //    filename = testfiles[testfiles.Length - 1];
                        //}

                        //else
                        //{

                        filename = Path.GetFileName(file.FileName);

                        //}

                        


                        liDocument = null;
                        liDocument = new tbdocument();
                        liDocument.fdDocType = lsFileType;
                        liDocument.fdDocTitle = filename;
                        liDocument.fdCreatedBy = UserID;
                        liDocument.fdCreatedOn = DateTime.Now;
                        liDocument.fdStatus = 1;
                        liDocument.fdVersion = 1;
                        liDocument.fdFileUrl = "";
                        liDocument.fdDocGuID = System.Guid.NewGuid().ToString();
                        

                         

                        db.tbdocuments.Add(liDocument);
                        db.SaveChanges();

                     if (CanAccess(GetModuleCode, "UPLOAD_DOCUMENT", "", liDocument.fdDocId) == false)
                        {

                            transection.Rollback();
                            moResponse = RespMsgFormatter.formatResp(0, getMessage("Access"), null, "GetDocetumentDetails");
                            return Json(moResponse, JsonRequestBehavior.AllowGet);
                        }

                        string lsExtension = Path.GetExtension(file.FileName);


                        

                        //if (lsDocumentID == 0)
                        //{
                        //    if (lsExtension.ToLower() != ".pdf")
                        //    {
                        //        transection.Rollback();
                        //        moResponse = RespMsgFormatter.formatResp(0, "Please upload a Pdf File.", null, "GetDocumentDetails");
                        //        return Json(moResponse, JsonRequestBehavior.AllowGet);
                        //    }

                        //}


                        string lsServerPath = Server.MapPath("~/Documents/");
                        //string lsRelative = "Documents/" + liDocument.fdDocId.ToString() + lsExtension;


                        var path = liDocument.fdDocGuID.ToString() + lsExtension;
                        var getSpID = db.tbdocumentclasses.Where(s => s.associatedDocType == lsFileType).Select(d => d.AssSpId).FirstOrDefault();
                        var uncPath = "";
                        var QrImgPath = "";
                        if (getSpID != null)
                        {
                            var getUncPath = db.tbstoragepolicies.Where(s => s.PKID == getSpID).FirstOrDefault();
                            if (getUncPath != null)
                            {
                                uncPath = @getUncPath.fdSpUNCpath + path;
                                QrImgPath = @getUncPath.fdSpUNCpath;
                            }
                            else
                            {
                                uncPath = @"\\Tarun-PC\Documents/" + path;
                                QrImgPath = @"\\Tarun-PC\Documents/";
                            }

                        }
                        else
                        {
                            uncPath = @"\\Tarun-PC\Documents/" + path;
                            QrImgPath = @"\\Tarun-PC\Documents/";
                        }


                        string lsRelative = "Documents/" + liDocument.fdDocGuID.ToString() + lsExtension;
                        string lsFileNameWithPath = Path.Combine(System.Web.HttpRuntime.AppDomainAppPath, lsRelative);


                        file.SaveAs(uncPath);
                        liDocument.fdFileUrl = uncPath;
                        liDocument.fdDocTitle = filename;
                        db.tbdocuments.Attach(liDocument);
                        var lvUpdateQuery = db.Entry(liDocument);
                        lvUpdateQuery.Property(e => e.fdFileUrl).IsModified = true;
                        lvUpdateQuery.Property(e => e.fdDocTitle).IsModified = true;
                        db.SaveChanges();
                        //tbdocumentmetadata loMeta;
                        //int itmNo = 0;
                        //List<string> keys = new List<string>();
                        //keys = Request.Form.AllKeys
                        //          .Select(s => s).ToList();
                        //foreach (var key in keys)
                        //{
                        //    if (key != "FileType" && key != "hdnDocumentID" && key != "Status" && key != "Version")
                        //    {
                        //        var lsaKey = Request.Form.GetValues(key);
                        //        var lsKeyValue = lsaKey[0].ToString();
                        //        itmNo = itmNo + 1;
                        //        loMeta = new tbdocumentmetadata();
                        //        loMeta.fdItemNo = itmNo;
                        //        loMeta.fdKey = key;
                        //        loMeta.fdValue = lsKeyValue;
                        //        loMeta.fdDocId = liDocument.fdDocId;
                        //        db.tbdocumentmetadatas.Add(loMeta);
                        //        loMeta = null;
                        //    }
                        //}
                        //db.SaveChanges();

                        int liUserID = UserID;
                        var username = db.tbusermasters.Where(s => s.fdUserId == liUserID).FirstOrDefault();

                        var jsonData = "{\"docid\":" + liDocument.fdDocId + ",\"url\":\"csimplifyit://docunow/\"" + liDocument.fdDocGuID + ", "
                           + " \"appname\":\"docunow\",\"defaulttemplate\":" + liDocument.fdDocGuID + ",\"createdon\":" + DateTime.Now + ",\"createdBy\":" + username.fdUserName + "}";

                        string json = new JavaScriptSerializer().Serialize(jsonData);
                        //  var result = JsonConvert.DeserializeObject(jsonData);
                        Bitmap x = QR_Code_Generator.QRCode_Generator(json);
                        // string lsExtension1 = Path.GetExtension(x.);
                        var QrCodeFolderName = db.tbcodelkups.Where(s => s.fdLkUpCode == "QR_Code_URL" && s.fdKeyCode == 1).Select(s=> new {folderName=s.fdKey1}).ToList();


                        tbqrcode qrcd = new tbqrcode();
                        qrcd.fdAssDocID = liDocument.fdDocId;
                        qrcd.fdFileUrl = QrImgPath+QrCodeFolderName[0].folderName.ToString() + liDocument.fdDocGuID + ".png";
                        db.tbqrcodes.Add(qrcd);
                        db.SaveChanges();
                        x.Save(QrImgPath+ QrCodeFolderName[0].folderName.ToString() + liDocument.fdDocGuID + ".png");
                        transection.Commit();
                        var imageUrl = qrcd.fdFileUrl;


                        Dictionary<string, object> Result = new Dictionary<string, object>();
                        Result.Add("FileUrl", qrcd.fdFileUrl);
                        var byteArray = System.IO.File.ReadAllBytes(qrcd.fdFileUrl);
                        var base64 = Convert.ToBase64String(byteArray);
                        Result.Add("DocumentID", liDocument.fdDocId);
                        Result.Add("ImageData", base64);
                        moResponse = RespMsgFormatter.formatResp(1, getMessage("Save"), Result, "UploadPOD");
                    }
                    else
                    {
                        moResponse = RespMsgFormatter.formatResp(0, getMessage("EmptyFile"), null, "UploadPOD");
                    }


            

            return Json(moResponse, JsonRequestBehavior.AllowGet);
        }




        //public async Task<ActionResult> uploadFileJsonResult(int hdnDocumentID, int fileType)
        //{


        //    DbContextTransaction transection = null;

        //    string[] lsaDocumentID; int lsDocumentID = 0;

        //    if (Request.Form.AllKeys.Contains("hdnDocumentID"))
        //    {
        //        lsaDocumentID = Request.Form.GetValues("hdnDocumentID");
        //        if (lsaDocumentID[0] != "")
        //        {
        //            lsDocumentID = int.Parse(lsaDocumentID[0].ToString());
        //        }

        //    }


        //    string[] lsaFileType; int lsFileType;


        //    if (Request.Form.AllKeys.Contains("FileType"))
        //    {
        //        lsaFileType = Request.Form.GetValues("FileType");
        //        lsFileType = int.Parse(lsaFileType[0].ToString());
        //    }
        //    else
        //    {
        //        moResponse = RespMsgFormatter.formatResp(0, getMessage("FileType"), null, "fetchContract");
        //        return Json(moResponse, JsonRequestBehavior.AllowGet);
        //    }
        //    var GetModuleCode = db.tbmodulemasters.Where(s => s.fdModuleCode == "DC" + lsFileType).Select(s => s.fdModuleCode).FirstOrDefault();

        //    if (lsDocumentID == 0)
        //    {
        //        if (Request.Files.Count > 0)

        //            if (lsDocumentID == 0)

        //            {

        //                HttpFileCollectionBase files = Request.Files;
        //                HttpPostedFileBase file = files[0];

        //                transection = db.Database.BeginTransaction();
        //                tbdocument liDocument = new tbdocument();
        //                List<tbdocument> lldoc = new List<tbdocument>();

        //                // set its properties
        //                var filename = "";

        //                //if (Request.Browser.Browser.ToUpper() == "IE" || Request.Browser.Browser.ToUpper() == "INTERNETEXPLORER")
        //                //{
        //                //    string[] testfiles = file.FileName.Split(new char[] { '\\' });
        //                //    filename = testfiles[testfiles.Length - 1];
        //                //}

        //                //else
        //                //{

        //                filename = Path.GetFileName(file.FileName);

        //                //}


        //                liDocument = null;
        //                liDocument = new tbdocument();
        //                liDocument.fdDocType = lsFileType;
        //                liDocument.fdDocTitle = filename;
        //                liDocument.fdCreatedBy = getUserId();
        //                liDocument.fdCreatedOn = DateTime.Now;
        //                liDocument.fdStatus = 1;
        //                liDocument.fdVersion = 1;
        //                liDocument.fdFileUrl = "";
        //                liDocument.fdDocGuID = System.Guid.NewGuid().ToString();
        //                db.tbdocuments.Add(liDocument);
        //                db.SaveChanges();

        //                if (CanAccess(GetModuleCode, "UPLOAD_DOCUMENT", "", liDocument.fdDocId) == false)
        //                {

        //                    transection.Rollback();
        //                    moResponse = RespMsgFormatter.formatResp(0, getMessage("Access"), null, "GetDocetumentDetails");
        //                    return Json(moResponse, JsonRequestBehavior.AllowGet);
        //                }
        //                string lsExtension = Path.GetExtension(file.FileName);




        //                if (lsDocumentID == 0)
        //                {
        //                    if (lsExtension.ToLower() != ".pdf")
        //                    {
        //                        transection.Rollback();
        //                        moResponse = RespMsgFormatter.formatResp(0, "Please upload a Pdf File.", null, "GetDocumentDetails");
        //                        return Json(moResponse, JsonRequestBehavior.AllowGet);
        //                    }

        //                }


        //                string lsServerPath = Server.MapPath("~/Documents/");
        //                //string lsRelative = "Documents/" + liDocument.fdDocId.ToString() + lsExtension;


        //                var path = liDocument.fdDocGuID.ToString() + lsExtension;
        //                var getSpID = db.tbdocumentclasses.Where(s => s.associatedDocType == lsFileType).Select(d => d.AssSpId).FirstOrDefault();
        //                var uncPath = "";
        //                var QrImgPath = "";
        //                if (getSpID != null)
        //                {
        //                    var getUncPath = db.tbstoragepolicies.Where(s => s.PKID == getSpID).FirstOrDefault();
        //                    if (getUncPath != null)
        //                    {
        //                        uncPath = @getUncPath.fdSpUNCpath + path;
        //                        QrImgPath = @getUncPath.fdSpUNCpath;
        //                    }
        //                    else
        //                    {
        //                        uncPath = @"\\Tarun-PC\Documents/" + path;
        //                        QrImgPath = @"\\Tarun-PC\Documents/";
        //                    }

        //                }
        //                else
        //                {
        //                    uncPath = @"\\Tarun-PC\Documents/" + path;
        //                    QrImgPath = @"\\Tarun-PC\Documents/";
        //                }


        //                string lsRelative = "Documents/" + liDocument.fdDocGuID.ToString() + lsExtension;
        //                string lsFileNameWithPath = Path.Combine(System.Web.HttpRuntime.AppDomainAppPath, lsRelative);


        //                file.SaveAs(uncPath);
        //                liDocument.fdFileUrl = uncPath;
        //                liDocument.fdDocTitle = filename;
        //                db.tbdocuments.Attach(liDocument);
        //                var lvUpdateQuery = db.Entry(liDocument);
        //                lvUpdateQuery.Property(e => e.fdFileUrl).IsModified = true;
        //                lvUpdateQuery.Property(e => e.fdDocTitle).IsModified = true;
        //                db.SaveChanges();
        //                tbdocumentmetadata loMeta;
        //                int itmNo = 0;
        //                List<string> keys = new List<string>();
        //                keys = Request.Form.AllKeys
        //                          .Select(s => s).ToList();
        //                foreach (var key in keys)
        //                {
        //                    if (key != "FileType" && key != "hdnDocumentID" && key != "Status" && key != "Version")
        //                    {
        //                        var lsaKey = Request.Form.GetValues(key);
        //                        var lsKeyValue = lsaKey[0].ToString();
        //                        itmNo = itmNo + 1;
        //                        loMeta = new tbdocumentmetadata();
        //                        loMeta.fdItemNo = itmNo;
        //                        loMeta.fdKey = key;
        //                        loMeta.fdValue = lsKeyValue;
        //                        loMeta.fdDocId = liDocument.fdDocId;
        //                        db.tbdocumentmetadatas.Add(loMeta);
        //                        loMeta = null;
        //                    }
        //                }
        //                db.SaveChanges();

        //                int liUserID = getUserId();
        //                var username = db.tbusermasters.Where(s => s.fdUserId == liUserID).FirstOrDefault();

        //                var jsonData = "{\"docid\":" + liDocument.fdDocId + ",\"url\":\"csimplifyit://docunow/\"" + liDocument.fdDocGuID + ", "
        //                   + " \"appname\":\"docunow\",\"defaulttemplate\":" + liDocument.fdDocGuID + ",\"createdon\":" + DateTime.Now + ",\"createdBy\":" + username.fdUserName + "}";

        //                string json = new JavaScriptSerializer().Serialize(jsonData);
        //                //  var result = JsonConvert.DeserializeObject(jsonData);
        //                Bitmap x = QR_Code_Generator.QRCode_Generator(json);
        //                // string lsExtension1 = Path.GetExtension(x.);

        //                tbqrcode qrcd = new tbqrcode();
        //                qrcd.fdAssDocID = liDocument.fdDocId;
        //                qrcd.fdFileUrl = QrImgPath + liDocument.fdDocGuID + ".png";
        //                db.tbqrcodes.Add(qrcd);
        //                db.SaveChanges();
        //                x.Save(QrImgPath + liDocument.fdDocGuID + ".png");
        //                transection.Commit();
        //                var imageUrl = qrcd.fdFileUrl;


        //                Dictionary<string, object> Result = new Dictionary<string, object>();
        //                Result.Add("FileUrl", qrcd.fdFileUrl);
        //                var byteArray = System.IO.File.ReadAllBytes(qrcd.fdFileUrl);
        //                var base64 = Convert.ToBase64String(byteArray);
        //                Result.Add("DocumentID", liDocument.fdDocId);
        //                Result.Add("ImageData", base64);
        //                moResponse = RespMsgFormatter.formatResp(1, getMessage("Save"), Result, "UploadPOD");
        //            }
        //            else
        //            {
        //                moResponse = RespMsgFormatter.formatResp(0, getMessage("EmptyFile"), null, "UploadPOD");
        //            }


        //    }















        //    return Json(moResponse, JsonRequestBehavior.AllowGet);
        //}




        public ActionResult SerchDocument(string requestData)
        {
            try
            {
                StringBuilder sql = new StringBuilder(800);
                MySqlParameter loFromDateParam = new MySqlParameter("@StartDate", "");
                MySqlParameter loToDateParam = new MySqlParameter("@EndDate", "");
                MySqlParameter loTitleParam = new MySqlParameter("@Title", "");
                MySqlParameter loContentParam = new MySqlParameter("@ClassName", "");

                bool lbSuccess = false; DateTime ldtFromDate, ldtToDate;
               
                int UserID = getUserId();
                if (CanAccess("HOMEMAS", "SEARCH", "User Log In ID" + UserID.ToString()) == true)
                {
                }
                
                if (UserID == 0)
                {
                    moResponse = RespMsgFormatter.formatResp(0, getMessage("SessionTimeOut"), null, "searchDocuments");
                    return Json(moResponse, JsonRequestBehavior.AllowGet);
                }

               
                    MySqlParameter userIDpmtr = new MySqlParameter("@UserID", UserID);


                sql.Append("select doc.fdDocId,substring_index(group_concat(concat(clk.fdKey1)),',', 1) as DocClass,substring_index(group_concat(concat(doc.fdDocTitle)),',', 1) as fdDocTitle,"
                                        + " max(DATE_FORMAT(doc.fdCreatedOn, '%d-%M-%Y')) as fdCreatedOn,  substring_index(group_concat(concat(doc.fdFileUrl)),',', 1) as fdFileUrl,"
                                        + " substring_index(group_concat(concat(usr.fdUserName)),',', 1) as user,  substring_index(group_concat(concat(ifnull(doc.fdVersion,1))),',', 1) as fdVersion,"
                                        + " case when rol.fdRoleCode = upper(concat(replace(clk.fdKey1, ' ', ''), 'Auth')) then count(1) else 0 end as AuthRight  from tbdocuments doc"
                                        + " join tbcodelkup clk on clk.fdLkUpCode = 'DOC_CLASS_TYPE' and doc.fdDocType = clk.fdKeyCode"
                                        + " join tbmodulemaster md on md.fdModuleCode = concat('DC', doc.fdDocType)"
                                        + " join tbuserroleloclink uslink on uslink.fdUserId =@UserID"
                                        + " join tbrolemaster rol on rol.fdRoleId = uslink.fdRoleId  "
                                        + " join tbusermaster usr on usr.fdUserId = doc.fdCreatedBy and doc.fdStatus in(1)"
                                        + " join tbrolemodfunccondlink rmlink on rmlink.fdRoleId = uslink.fdRoleId and rmlink.fdModuleId = md.fdModuleId"
                                        + " where 1 = 1 and rol.fdRoleCode = upper(concat(replace(clk.fdKey1, ' ', ''), 'AUTH')) ");

                var getViewAccess = "  select doc.fdDocId,substring_index(group_concat(concat(clk.fdKey1)),',', 1) as DocClass,substring_index(group_concat(concat(doc.fdDocTitle)),',', 1) as fdDocTitle, "
                                        + " max(DATE_FORMAT(doc.fdCreatedOn, '%d-%M-%Y')) as fdCreatedOn,substring_index(group_concat(concat(doc.fdFileUrl)),',', 1) as fdFileUrl,  substring_index(group_concat(concat(usr.fdUserName)),',', 1) as user,"
                                        + " substring_index(group_concat(concat(ifnull(doc.fdVersion,1))),',', 1) as fdVersion,  case when rol.fdRoleCode = upper(concat(replace(clk.fdKey1, ' ', ''), 'Auth')) then 1 else 0 end as AuthRight  from tbdocuments doc"
                                        + " join tbcodelkup clk on clk.fdLkUpCode = 'DOC_CLASS_TYPE' and doc.fdDocType = clk.fdKeyCode"
                                        + " join tbmodulemaster md on md.fdModuleCode = concat('DC', doc.fdDocType) "
                                        + " join tbuserroleloclink uslink on uslink.fdUserId = @UserID  "
                                        + " join tbrolemaster rol on rol.fdRoleId = uslink.fdRoleId "
                                        + " join tbrolemodfunccondlink rmlink on rmlink.fdRoleId = uslink.fdRoleId and rmlink.fdModuleId = md.fdModuleId  "
                                        + " join tbusermaster usr on usr.fdUserId = doc.fdCreatedBy and doc.fdStatus in(1)  "
                                        + " where 1 = 1 and rol.fdRoleCode = upper(concat(replace(clk.fdKey1, ' ', ''), 'VIEW')) "
                                        + " and doc.fdDocId not in(select  doc.fdDocId from tbdocuments doc "
                                        + " join tbcodelkup clk on clk.fdLkUpCode = 'DOC_CLASS_TYPE' and doc.fdDocType = clk.fdKeyCode "
                                        + " join tbmodulemaster md on md.fdModuleCode = concat('DC', doc.fdDocType)  "
                                        + " join tbuserroleloclink uslink on uslink.fdUserId = 1  join tbrolemaster rol on rol.fdRoleId = uslink.fdRoleId "
                                        + " join tbrolemodfunccondlink rmlink on rmlink.fdRoleId = uslink.fdRoleId and rmlink.fdModuleId = md.fdModuleId "
                                        + " where 1 = 1  and rol.fdRoleCode = upper(concat(replace(clk.fdKey1, ' ', ''), 'AUTH')))";


                JObject reqData = JObject.Parse(requestData);
                    if (reqData["txtschTitle"] != null && reqData["txtschTitle"].ToString() != string.Empty)
                    {
                        sql.Append(" AND  doc.fdDocTitle like @Title");
                        getViewAccess = getViewAccess + " AND  doc.fdDocTitle = @Title ";
                        loTitleParam = new MySqlParameter("@Title", reqData["txtschTitle"].ToString()+'%');
                        lbSuccess = false;
                    }
                    if (reqData["txtschClassName"] != null && reqData["txtschClassName"].ToString() != string.Empty)
                    {
                        sql.Append(" AND  clk.fdKey1 like @ClassName");
                        getViewAccess = getViewAccess + " AND  clk.fdKey1 like @ClassName ";
                        loContentParam = new MySqlParameter("@ClassName", reqData["txtschClassName"].ToString()+'%');
                        lbSuccess = false;
                    }
                    if (reqData["dtfromdate"] != null && reqData["dtfromdate"].ToString() != string.Empty)
                    {
                        //lbSuccess = DateTime.TryParse(reqData["dtfromdate"].ToString(), out ldtFromDate);
                       // if (lbSuccess == true)
                        //{
                            sql.Append(" AND  date(doc.fdCreatedOn) >=STR_TO_DATE(@StartDate, '%d/%m/%Y')  ");
                           getViewAccess = getViewAccess + "  AND  date(doc.fdCreatedOn) >=STR_TO_DATE(@StartDate, '%d/%m/%Y') ";
                        var d = (DateTime.ParseExact(reqData["dtfromdate"].ToString(), "dd/MM/yyyy", CultureInfo.InvariantCulture));
                        loFromDateParam = new MySqlParameter("@StartDate", reqData["dtfromdate"].ToString());
                            lbSuccess = false;
                        //}
                    }
                    if (reqData["dttodate"] != null && reqData["dttodate"].ToString() != string.Empty)
                    {
                        //lbSuccess = DateTime.TryParse(reqData["dttodate"].ToString(), out ldtToDate);
                        //if (lbSuccess == true)
                       // {
                            sql.Append(" AND  date(doc.fdCreatedOn)  <=STR_TO_DATE(@EndDate, '%d/%m/%Y') ");
                        getViewAccess = getViewAccess + " AND  date(doc.fdCreatedOn) <= STR_TO_DATE(@EndDate, '%d/%m/%Y')";
                        var d = (DateTime.ParseExact(reqData["dttodate"].ToString(), "dd/MM/yyyy", CultureInfo.InvariantCulture));
                        loToDateParam = new MySqlParameter("@EndDate", reqData["dttodate"].ToString());
                            lbSuccess = false;
                        //}
                    }


                    object[] parameters = new object[] { loFromDateParam, loToDateParam, loContentParam, loTitleParam, userIDpmtr };

                    var Query = db.Database.SqlQuery<DocumentSearch>(sql.ToString() + " Group by doc.fdDocId, clk.fdKey1, doc.fdDocTitle, doc.fdFileUrl, usr.fdUserName, doc.fdCreatedOn, "
                               +" doc.fdVersion, rol.fdRoleCode order by doc.fdCreatedOn desc ", parameters).ToList();

                    var Query1 = db.Database.SqlQuery<DocumentSearch>(getViewAccess + " Group by doc.fdDocId, clk.fdKey1, doc.fdDocTitle, doc.fdFileUrl, usr.fdUserName, doc.fdCreatedOn, "
                               + " doc.fdVersion, rol.fdRoleCode order by doc.fdCreatedOn desc ", parameters.Select(x => ((ICloneable)x).Clone()).ToArray()).ToList();

                DocumentSearch doc = new DocumentSearch();

                if (Query.Count > 0 || Query1.Count > 0)
                    {
                    if(Query1.Count>0)
                    {
                        Query.AddRange(Query1);
                    }
                    
                    
                        moResponse = RespMsgFormatter.formatResp(1, getMessage("Success"), Query, "SerchDocument");
                    }
                    else
                    {
                        moResponse = RespMsgFormatter.formatResp(0, getMessage("GetNoResult"), null, "SerchDocument");
                    }
               
               

            }
            catch (Exception ex)
            {
                moResponse = RespMsgFormatter.formatResp(0, ex.Message + "InnerException:" + ex.InnerException, null, "SerchDocument");
            }
            return Json(moResponse, JsonRequestBehavior.AllowGet);


        }


        public ActionResult GetDocumentDetails(string requestData)
        {
            try
            {
               int liUserId = getUserId();
                if (liUserId == 0)
                {
                    moResponse = RespMsgFormatter.formatResp(0, getMessage("SessionTimeOut"), null, "fetchContract");
                    return Json(moResponse, JsonRequestBehavior.AllowGet);
                }

                JObject jobj = JObject.Parse(requestData);
                var lsDocClass = jobj["docClass"].ToString();
                var lsDocName = jobj["docName"].ToString();
                var lsUserName = "";
                var lsCreatedOn = "";
                var lsVersion = "";

                int liDocId = Convert.ToInt32(jobj["docId"].ToString());     
                if(liDocId != null && liDocId != 0 )
                {
                    var qry= "select doc.fdDocTitle,DATE_FORMAT(doc.fdCreatedOn, '%d-%M-%Y') as fdCreatedOn,ifnull(doc.fdVersion,1) as fdVersion,tbuser.fdUserName as UserName,clk.fdKey1 from tbdocuments doc join tbcodelkup clk on clk.fdKeyCode = doc.fdDocType and clk.fdLkUpCode = 'DOC_CLASS_TYPE' join tbusermaster tbuser on tbuser.fdUserId=doc.fdCreatedBy where doc.fdDocId= @fdId ";
                   
                    MySqlParameter loToId = new MySqlParameter("@fdId", liDocId);
                    var Query = db.Database.SqlQuery<getDocClass>(qry, loToId).FirstOrDefault();
                    lsDocClass = Query.fdKey1;
                    lsDocName = Path.GetFileName(Query.fdDocTitle);
                    lsUserName = Query.UserName;
                    lsCreatedOn = Query.fdCreatedOn;
                    lsVersion = Query.fdVersion.ToString();
                };
                
                           
                int liDoctype = db.tbdocuments.Where(s => s.fdDocId == liDocId).Select(s => s.fdDocType).AsEnumerable().Select(s=>s!=null?s.Value:0).FirstOrDefault();
                var GetModuleCode = db.tbmodulemasters.Where(s => s.fdModuleCode == "DC" + liDoctype).Select(s => s.fdModuleCode).FirstOrDefault();
                if (CanAccess(GetModuleCode, "VIEW", "",liDocId))
                {                   
                    var getDocumentURL = db.tbdocuments.Where(s => s.fdDocId == liDocId).Select(s => new { s.fdFileUrl ,s.fdDocType}).FirstOrDefault();
                    var getDocumentData = db.tbdocumentmetadatas.Where(s => s.fdDocId == liDocId).Select(s => new { s.fdKey, s.fdValue });
                    Dictionary<string, object> Result = new Dictionary<string, object>();
                    Result.Add("FileUrl", getDocumentURL);
                    var byteArray = System.IO.File.ReadAllBytes(getDocumentURL.fdFileUrl);

                    var CreatedTempFileSuccessfully = 0;
                    var tempFileName = System.Guid.NewGuid() + ".pdf";
                    var serverPathFileURL = Server.MapPath("~/tempdocs/" + tempFileName);

                    try
                    {
                     
                        System.IO.File.WriteAllBytes(serverPathFileURL, byteArray.ToArray());
                        CreatedTempFileSuccessfully = 1;
                    }
                    catch(Exception e)
                    {
                        CreatedTempFileSuccessfully = 0;
                    }
                    var base64 = Convert.ToBase64String(byteArray);
                    Result.Add("DocumentData", getDocumentData);
                    Result.Add("DocumentClass", lsDocClass);
                    Result.Add("DocumentName", lsDocName); 
                    Result.Add("DocumentID", liDocId);
                    Result.Add("UserName", lsUserName);
                    Result.Add("DocCreatedOn", lsCreatedOn);
                    Result.Add("DocVersion", lsVersion);
                    Result.Add("ImageData", base64);
                    Result.Add("isPDFURLAvailable", CreatedTempFileSuccessfully);
                    Result.Add("PDFURL", "~/tempdocs/0a26d250-6ddc-4ddb-acf3-4911b2465aad.pdf");
                    Result.Add("FileExtention",Path.GetExtension(lsDocName).ToString());
                               moResponse = RespMsgFormatter.formatResp(1, getMessage("Success"), Result, "GetDocumentDetails");
                }
                else moResponse = RespMsgFormatter.formatResp(0, getMessage("Access"), null, "GetDocumentDetails");
            }
            catch (Exception ex)
            {
                moResponse = RespMsgFormatter.formatResp(0, "Msg:"+ex.Message+" InnerException:"+ex.InnerException, null, "GetDocumentDetails");
            }
            return new JsonResult()
            {
                Data = moResponse,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet,
                MaxJsonLength = Int32.MaxValue
            };
        }

        

        public ActionResult SearchDocumentsUsingSearchBar(string requestData)
        {
            try
            { // UPLOADMAS

                int UserID = getUserId();

                if (CanAccess("HOMEMAS", "SEARCH", "User Log In ID" + UserID.ToString()) == true)
                {
                }
               
                if (UserID == 0)
                    {
                        moResponse = RespMsgFormatter.formatResp(0, getMessage("SessionTimeOut"), null, "searchDocuments");
                        return Json(moResponse, JsonRequestBehavior.AllowGet);
                    }


                    MySqlParameter userIDpmtr = new MySqlParameter("@UserID", UserID);

                    JObject jObject = JObject.Parse(requestData);
                    MySqlParameter loName = new MySqlParameter("@searchkeyword", "");

                    var getAuthAccess = "  select doc.fdDocId,substring_index(group_concat(concat(clk.fdKey1)),',', 1) as DocClass,substring_index(group_concat(concat(doc.fdDocTitle)),',', 1) as fdDocTitle," 
                                        + " max(DATE_FORMAT(doc.fdCreatedOn, '%d-%M-%Y')) as fdCreatedOn,  substring_index(group_concat(concat(doc.fdFileUrl)),',', 1) as fdFileUrl,"
                                        + " substring_index(group_concat(concat(usr.fdUserName)),',', 1) as user,  substring_index(group_concat(concat(ifnull(doc.fdVersion,1))),',', 1) as fdVersion,"
                                        + " case when rol.fdRoleCode = upper(concat(replace(clk.fdKey1, ' ', ''), 'Auth')) then count(1) else 0 end as AuthRight  from tbdocuments doc"  
                                        +" join tbcodelkup clk on clk.fdLkUpCode = 'DOC_CLASS_TYPE' and doc.fdDocType = clk.fdKeyCode"  
                                        +" join tbmodulemaster md on md.fdModuleCode = concat('DC', doc.fdDocType)"
                                        +" join tbuserroleloclink uslink on uslink.fdUserId =@UserID"
                                        +" join tbrolemaster rol on rol.fdRoleId = uslink.fdRoleId  "
                                        +" join tbusermaster usr on usr.fdUserId = doc.fdCreatedBy and doc.fdStatus in(1)"
                                        +" join tbrolemodfunccondlink rmlink on rmlink.fdRoleId = uslink.fdRoleId and rmlink.fdModuleId = md.fdModuleId"
                                        +" where 1 = 1 and rol.fdRoleCode = upper(concat(replace(clk.fdKey1, ' ', ''), 'AUTH')) ";

                    var getViewAccess = "  select doc.fdDocId,substring_index(group_concat(concat(clk.fdKey1)),',', 1) as DocClass,substring_index(group_concat(concat(doc.fdDocTitle)),',', 1) as fdDocTitle, "
                                        + " max(DATE_FORMAT(doc.fdCreatedOn, '%d-%M-%Y')) as fdCreatedOn,substring_index(group_concat(concat(doc.fdFileUrl)),',', 1) as fdFileUrl,  substring_index(group_concat(concat(usr.fdUserName)),',', 1) as user,"
                                        + " substring_index(group_concat(concat(ifnull(doc.fdVersion,1))),',', 1) as fdVersion,  case when rol.fdRoleCode = upper(concat(replace(clk.fdKey1, ' ', ''), 'Auth')) then 1 else 0 end as AuthRight  from tbdocuments doc"
                                        + " join tbcodelkup clk on clk.fdLkUpCode = 'DOC_CLASS_TYPE' and doc.fdDocType = clk.fdKeyCode"
                                        +" join tbmodulemaster md on md.fdModuleCode = concat('DC', doc.fdDocType) "
                                        +" join tbuserroleloclink uslink on uslink.fdUserId = @UserID  "
                                        +" join tbrolemaster rol on rol.fdRoleId = uslink.fdRoleId "
                                        +" join tbrolemodfunccondlink rmlink on rmlink.fdRoleId = uslink.fdRoleId and rmlink.fdModuleId = md.fdModuleId  "
                                        +" join tbusermaster usr on usr.fdUserId = doc.fdCreatedBy and doc.fdStatus in(1)  "
                                        +" where 1 = 1 and rol.fdRoleCode = upper(concat(replace(clk.fdKey1, ' ', ''), 'VIEW')) "
                                        +" and doc.fdDocId not in(select  doc.fdDocId from tbdocuments doc "
                                        +" join tbcodelkup clk on clk.fdLkUpCode = 'DOC_CLASS_TYPE' and doc.fdDocType = clk.fdKeyCode "
                                        +" join tbmodulemaster md on md.fdModuleCode = concat('DC', doc.fdDocType)  "
                                        +" join tbuserroleloclink uslink on uslink.fdUserId = 1  join tbrolemaster rol on rol.fdRoleId = uslink.fdRoleId "
                                        +" join tbrolemodfunccondlink rmlink on rmlink.fdRoleId = uslink.fdRoleId and rmlink.fdModuleId = md.fdModuleId "
                                        +" where 1 = 1  and rol.fdRoleCode = upper(concat(replace(clk.fdKey1, ' ', ''), 'AUTH')))";


                    string value = string.Empty;
                    if (jObject["searchInputKeyword"].ToString() != null && jObject["searchInputKeyword"].ToString() != string.Empty)
                    {
                        getAuthAccess = getAuthAccess + " and ( fdDocTitle like @searchkeyword or clk.fdKey1 like @searchkeyword or usr.fdUserName like @searchkeyword or doc.fdVersion like @searchkeyword or date(doc.fdCreatedOn)=STR_TO_DATE(@searchkeyword,'%d-%M-%Y') or date(doc.fdCreatedOn)=STR_TO_DATE(@searchkeyword, '%d-%m-%Y'))";
                        getViewAccess = getViewAccess + " and ( fdDocTitle like @searchkeyword or clk.fdKey1 like @searchkeyword or usr.fdUserName like @searchkeyword or doc.fdVersion like @searchkeyword or date(doc.fdCreatedOn)=STR_TO_DATE(@searchkeyword,'%d-%M-%Y') or date(doc.fdCreatedOn)=STR_TO_DATE(@searchkeyword, '%d-%m-%Y'))";
                        value = jObject["searchInputKeyword"].ToString();
                        loName = new MySqlParameter("@searchkeyword", value + "%");
                    }


                    //DateTime Searchdt; // "yyyy-MMM-dd", "yyyy-MM-dd"
                    //MySqlParameter lodatepmtr = new MySqlParameter("@searchDate", "");
                    //string[] formats = { "dd-MMM-yyyy", "dd-MM-yyyy" };
                    //if (DateTime.TryParseExact(value, formats, CultureInfo.InvariantCulture, DateTimeStyles.None, out Searchdt))
                    //{
                    //    lodatepmtr = new MySqlParameter("@searchDate", Searchdt);
                    //}
                    

                    getAuthAccess = getAuthAccess + " Group by doc.fdDocId,rol.fdRoleCode order by doc.fdCreatedOn desc";

                    getViewAccess = getViewAccess + " Group by doc.fdDocId,rol.fdRoleCode order by doc.fdCreatedOn desc";

                    object[] parameters = new object[] { loName, userIDpmtr};

                var doclist = db.Database.SqlQuery<DocumentSearch>(getAuthAccess, parameters).ToList();
                    var Viewdoclist = db.Database.SqlQuery<DocumentSearch>(getViewAccess, parameters.Select(x => ((ICloneable)x).Clone()).ToArray()).ToList();
                    if (doclist.Count > 0 || Viewdoclist.Count > 0)
                    {
                        if (Viewdoclist.Count > 0)
                        {
                            doclist.AddRange(Viewdoclist);
                        }
                        moResponse = RespMsgFormatter.formatResp(1, doclist.Count + " " + getMessage("RecordFound"), doclist, "searchDocuments");
                    }
                    else
                    {
                        moResponse = RespMsgFormatter.formatResp(0, getMessage("GetNoResult"), doclist, "searchDocuments");
                    }
               
               
            }
            catch(Exception ex)
            {
                moResponse = RespMsgFormatter.formatResp(0, ex.Message + "InnerException:" + ex.InnerException, null, "SerchDocument");
            }
            return Json(moResponse, JsonRequestBehavior.AllowGet);
        }


        public ActionResult getUploadDocumentdetails(string requestData)
        {
            try
            {
                int liUserId = getUserId();
                if (liUserId == 0)
                {
                    moResponse = RespMsgFormatter.formatResp(0, getMessage("SessionTimeOut"), null, "fetchContract");
                    return Json(moResponse, JsonRequestBehavior.AllowGet);
                }
                int DocID = int.Parse(requestData);

                var getlsFileType = db.tbdocuments.Where(s => s.fdDocId == DocID).Select(d => d.fdDocType).FirstOrDefault();
                var GetModuleCode = db.tbmodulemasters.Where(s => s.fdModuleCode == "DC" + getlsFileType).Select(s => s.fdModuleCode).FirstOrDefault();

                if (CanAccess(GetModuleCode, "VIEW", requestData, DocID) == true)
                {
                    if (requestData != null)
                    {

                        MySqlParameter liDocIdpmtr = new MySqlParameter("@fdDocId", DocID);

                        string query = "select substring_index(group_concat(concat(doc.fdDocId)),',', 1) as fdDocId, substring_index(group_concat(concat(doc.fdDocTitle)),',', 1) as fdDocTitle,"
                                        +" substring_index(group_concat(concat(doc.fdDocType)),',', 1) as fdDocType,substring_index(group_concat(concat(doc.fdFileUrl)),',', 1) as fdFileUrl," 
                                        +" substring_index(group_concat(concat(dcmeta.fdKey)),',', 1) as fdKey,  substring_index(group_concat(concat(dcmeta.fdValue)),',', 1) as fdValue,"
                                        +" substring_index(group_concat(concat(doccls.dcColumnName)),',', 1) as dcColumnName,substring_index(group_concat(concat(doccls.dcColumnType)),',', 1) as dcColumnType,"
                                        +" substring_index(group_concat(concat(doccls.dcid)),',', 1) as dcid,ifnull(substring_index(group_concat(concat(doccls.defaultValue)), ',', 1), '') as defaultValue,"
                                        +" substring_index(group_concat(concat(doccls.isRequired)), ',', 1) as isRequired,  substring_index(group_concat(concat(doccls.sequence)), ',', 1) as sequence,"
                                        +" ifnull(substring_index(group_concat(concat(doccls.maxValue)), ',', 1), '') as mxValue,ifnull(substring_index(group_concat(concat(dcmeta.fdValue)), ',', 1), '') as minValue,"
                                        +" substring_index(group_concat(concat(lkup.fdKey1)), ',', 1) as fdStatus, substring_index(group_concat(concat(doc.fdVersion)), ',', 1) as fdVersion,"
                                        +" max(DATE_FORMAT(doc.fdCreatedOn, '%d-%M-%Y')) as fdCreatedOn,substring_index(group_concat(concat(lkpdoclcs.fdKey1)), ',', 1) as DocClass,"
                                        +" substring_index(group_concat(concat(um.fdUserName)), ',', 1) as fdUserName from tbdocuments doc"
                                        +" left join tbdocumentclass doccls on doc.fdDocType = doccls.associatedDocType"
                                        +" left join tbdocumentmetadata dcmeta on dcmeta.fdDocId = doc.fdDocId and doccls.dcColumnName = dcmeta.fdKey  "
                                        +" left join tbusermaster um on um.fdUserId = doc.fdCreatedBy"
                                        +" join tbcodelkup lkup on lkup.fdKeyCode = doc.fdStatus and lkup.fdLkUpCode = 'DOC_STATUS'"
                                        +" join tbcodelkup lkpdoclcs on lkpdoclcs.fdKeyCode = doc.fdDocType and lkpdoclcs.fdLkUpCode = 'DOC_CLASS_TYPE'"
                                        +" where doc.fdDocId = @fdDocId ";

                        //string query = "select doc.fdDocId as fdDocId,doc.fdDocTitle as fdDocTitle,doc.fdDocType as fdDocType,doc.fdFileUrl as fdFileUrl, "
                        //               + " dcmeta.fdKey as fdKey,group_concat(distinct dcmeta.fdValue) as fdValue, group_concat(distinct doccls.dcColumnName) as dcColumnName,group_concat(distinct doccls.dcColumnType) as dcColumnType, "
                        //               + " group_concat(distinct doccls.dcid) as dcid,group_concat(distinct doccls.defaultValue) as defaultValue,group_concat(distinct doccls.isRequired) as isRequired,group_concat(distinct doccls.sequence) as  sequence, "
                        //               + " group_concat(distinct doccls.maxValue) as mxValue,group_concat(distinct doccls.minValue) as minValue,group_concat(distinct lkup.fdKey1) as fdStatus, group_concat(distinct doc.fdVersion) as fdVersion  "
                        //               + "  ,group_concat(distinct date_format(doc.fdCreatedOn,'%d-%b-%Y')) as fdCreatedOn,lkpdoclcs.fdKey1 as  DocClass from tbdocuments doc "
                        //               + " join tbdocumentmetadata dcmeta on dcmeta.fdDocId = doc.fdDocId "
                        //               + " join tbdocumentclass doccls on doccls.dcColumnName=dcmeta.fdKey"
                        //               + " join tbcodelkup lkup on lkup.fdKeyCode=doc.fdStatus and lkup.fdLkUpCode='DOC_STATUS' "
                        //               + " join tbcodelkup lkpdoclcs on lkpdoclcs.fdKeyCode=doc.fdDocType and lkpdoclcs.fdLkUpCode='DOC_CLASS_TYPE' "
                        //               + " where doc.fdDocId=@fdDocId";

                        object[] parameters = new object[] { liDocIdpmtr };

                        var result = db.Database.SqlQuery<getDocumentdetails>(query + " group by dcmeta.fdKey,doc.fdDocId,doc.fdDocTitle,doc.fdDocType,doc.fdFileUrl,dcmeta.fdKey,dcmeta.fdValue,doccls.dcColumnName,doccls.dcColumnType, "
                                   + " doccls.dcid, doccls.defaultValue, doccls.isRequired, doccls.sequence, doccls.maxValue, doccls.minValue, lkup.fdKey1, doc.fdVersion, "
                                   + " doc.fdCreatedOn, lkpdoclcs.fdKey1,um.fdUserName ", parameters).ToList();

                        

                        Dictionary<string, object> Result = new Dictionary<string, object>();
                        Result.Add("FileUrl", result[0].fdFileUrl);
                        var byteArray = System.IO.File.ReadAllBytes(result[0].fdFileUrl);
                        var base64 = Convert.ToBase64String(byteArray);



                        var getQrCodeFileUrl = "select doc.fdDocGuID as fdDocGuID, sp.fdSpUNCpath as fdSpUNCpath from tbdocuments doc "
                                 + " join(select associatedDocType, AssSpId as AsSpId from tbdocumentclass where 1 = 1 group by associatedDocType, AssSpId) doccls on doc.fdDocType = doccls.associatedDocType "
                                 + " join tbstoragepolicy sp on sp.PKID = doccls.AsSpId "
                                 + " join tbqrcode qr on qr.fdAssDocID = doc.fdDocId where doc.fdDocId = @fdDocId";
                        var getQrCodeFileUrlResult = db.Database.SqlQuery<DocPathDetetails>(getQrCodeFileUrl, parameters.Select(x => ((ICloneable)x).Clone()).ToArray()).FirstOrDefault();
                        var Qrcodebase64 = string.Empty;
                        var QrcodeFolder = db.tbcodelkups.Where(s => s.fdLkUpCode == "QR_Code_URL" && s.fdKeyCode == 1).Select(s=> new { folderName=s.fdKey1}).ToList();

                        if (getQrCodeFileUrlResult.fdSpUNCpath != null)
                        {
                            var QrcodebyteArray = System.IO.File.ReadAllBytes(getQrCodeFileUrlResult.fdSpUNCpath+ QrcodeFolder[0].folderName + getQrCodeFileUrlResult.fdDocGuID + ".png");
                            Qrcodebase64 = Convert.ToBase64String(QrcodebyteArray);
                        }


                        Result.Add("QRCodeImageData", Qrcodebase64);
                        Result.Add("DocumentData", result);
                        Result.Add("ImageData", base64);
                        Result.Add("FileExtention",Path.GetExtension(result[0].fdDocTitle.ToString()));



                        if (result.Count > 0)
                        {
                            moResponse = RespMsgFormatter.formatResp(1, getMessage("Success"), Result, "getUploadDocumentdetails");
                        }
                        else
                        {
                            moResponse = RespMsgFormatter.formatResp(0, getMessage("RecordFound"), result, "getUploadDocumentdetails");
                        }
                    }
                    else
                    {
                        moResponse = RespMsgFormatter.formatResp(0, getMessage("DocSelection"), null, "getUploadDocumentdetails");
                    }
                }
                else
                {
                    moResponse = RespMsgFormatter.formatResp(0, getMessage("Access"), null, "getUploadDocumentdetails");
                }
            }
            catch (Exception ex)           
            {
                moResponse = RespMsgFormatter.formatResp(0, ex.Message + "InnerException:" + ex.InnerException, null, "getUploadDocumentdetails");
            }
            return new JsonResult()
            {
                Data = moResponse,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet,
                MaxJsonLength = Int32.MaxValue
            };
        }


        public ActionResult saveArchiveDocument(string requestData)
        {
           
            try
            {
                int liUserId = getUserId();
                if (liUserId == 0)
                {
                    moResponse = RespMsgFormatter.formatResp(0, getMessage("SessionTimeOut"), null, "fetchContract");
                    return Json(moResponse, JsonRequestBehavior.AllowGet);
                }
                if (requestData != null)
                    {
                        JObject jobt = JObject.Parse(requestData);
                        if (jobt["docID"] != null && jobt["docID"].ToString() != string.Empty)
                {
                            int LiDocID = int.Parse(jobt["docID"].ToString());

                        if (CanAccess("ARCV_DOC", "ARCHIVE", requestData, LiDocID) == true)
                        {

                     var getDoc = db.tbdocuments.Where(s => s.fdDocId == LiDocID).FirstOrDefault();
                            if(getDoc==null)
                            {
                                moResponse = RespMsgFormatter.formatResp(0,"Document not found in DMS", null, "saveArchiveDocument");
                                return Json(moResponse, JsonRequestBehavior.AllowGet);
                            }
                    getDoc.fdStatus = -1;
                    db.Entry(getDoc).State = System.Data.Entity.EntityState.Modified;
                    db.SaveChanges();
                    int Status = MoveContentToRrchive(LiDocID);

                     if (getDoc.fdParentId != null)
                    {
                        var getAllPreviousDoc = db.tbdocuments.Where(s => (s.fdParentId == getDoc.fdParentId || s.fdDocId == getDoc.fdParentId) && s.fdDocId != getDoc.fdDocId).Select(d => d.fdDocId).ToList();
                        if (getAllPreviousDoc.Count > 0)
                        {
                            foreach (var docID in getAllPreviousDoc)
                            {
                                var getDocs = db.tbdocuments.Where(s => s.fdDocId == docID).FirstOrDefault();
                                getDocs.fdStatus = -1;
                                db.Entry(getDocs).State = System.Data.Entity.EntityState.Modified;
                                db.SaveChanges();
                                int Status1 = MoveContentToRrchive(LiDocID);
                            }
                                    moResponse = RespMsgFormatter.formatResp(1, getMessage("Success"), null, "getUploadDocumentdetails");
                                    return Json(moResponse, JsonRequestBehavior.AllowGet);

                                }
                    }
                            else
                            {
                                moResponse = RespMsgFormatter.formatResp(1, getMessage("Success"), null, "getUploadDocumentdetails");
                                return Json(moResponse, JsonRequestBehavior.AllowGet);
                            }

                        }
                        else
                        {
                            moResponse = RespMsgFormatter.formatResp(0, getMessage("Access"), null, "getUploadDocumentdetails");
                            return Json(moResponse, JsonRequestBehavior.AllowGet);
                        }




                       
                }
                else
                {
                    moResponse = RespMsgFormatter.formatResp(0, getMessage("DocSelection"), null, "getUploadDocumentdetails");
                        return Json(moResponse, JsonRequestBehavior.AllowGet);
                    }
                    }
                    else
                    {
                        moResponse = RespMsgFormatter.formatResp(0, getMessage("DocSelection"), null, "getUploadDocumentdetails");
                    return Json(moResponse, JsonRequestBehavior.AllowGet);
                }

            }
                catch (Exception ex)
            {
                moResponse = RespMsgFormatter.formatResp(0, ex.Message + "InnerException:" + ex.InnerException, null, "getUploadDocumentdetails");
            }
           
            return Json(moResponse, JsonRequestBehavior.AllowGet);
        }


        public ActionResult getviewDocumenthistory(string requestData)
        {
            try
            {
                int liUserId = getUserId();
                if (liUserId == 0)
                {
                    moResponse = RespMsgFormatter.formatResp(0, getMessage("SessionTimeOut"), null, "fetchContract");
                    return Json(moResponse, JsonRequestBehavior.AllowGet);
                }
                if (CanAccess("UPLOADMAS", "VIEW_DOC_HISTORY", requestData) == true)
                {
                }
                
                if (requestData != null && requestData !="")
                {
                    int liDocId = int.Parse(requestData);
                    MySqlParameter liDocIDpmtr = new MySqlParameter("@fdDocId", liDocId);
                    var getDocDetails = " select doc.fdDocId as fdDocId, doc.fdDocTitle as fdDocTitle, ifnull(doc.fdVersion,1) as fdVersion ,ifnull(doc.fdParentId,0) as fdParentId, "
                                      + " date_format(doc.fdCreatedOn, '%d-%M-%Y %H:%i:%S') as fdCreatedOn,um.fdUserName as fdUserName ,date_format(docClass.createdOn, '%d-%M-%Y') as lastRevison, clk.fdKey1 as DocStatus from tbdocuments doc "
                                      + " join tbusermaster um on um.fdUserId = doc.fdCreatedBy left join (select distinct  documentId,max(createdOn) as createdOn from user_action_audit where documentId= @fdDocId and actionName='VIEW' group by documentId)   as docClass on doc.fdDocId = docClass.documentId"
                                      + " join tbcodelkup clk on clk.fdKeyCode=doc.fdStatus and clk.fdLkUpCode='DOC_STATUS' "
                                      + " where doc.fdDocId = @fdDocId";


                    object[] parameters = new object[] { liDocIDpmtr } ;

                    var getresult = db.Database.SqlQuery<getdocumentVersionHistory>(getDocDetails, parameters).ToList();

                    

                    if (getresult.Count>0)
                    {
                        if(getresult[0].fdParentId!=0)
                        {
                            int? ParetdId = getresult[0].fdParentId;
                            var getalldocuments = db.tbdocuments.Where(s => s.fdDocId == ParetdId || s.fdParentId == ParetdId && s.fdDocId != liDocId).ToList();
                            if(getalldocuments.Count>0)
                            { 
                                foreach(var docid in getalldocuments)
                                {
                                    MySqlParameter liParentDocIDpmtr = new MySqlParameter("@fdParentId", docid.fdDocId);
                                    var getParentdocDetails = "select doc.fdDocId as fdDocId, doc.fdDocTitle as fdDocTitle,ifnull(doc.fdVersion,1) as fdVersion ,doc.fdParentId as fdParentId, "
                                                           + " date_format(doc.fdCreatedOn, '%d-%M-%Y %H:%i:%S') as fdCreatedOn,um.fdUserName as fdUserName,date_format(docClass.createdOn, '%d-%M-%Y') as lastRevison,clk.fdKey1 as DocStatus from tbdocuments doc "
                                                           + " join tbusermaster um on um.fdUserId = doc.fdCreatedBy "
                                                           + "  left join (select distinct  documentId,max(createdOn) as createdOn from user_action_audit where documentId= @fdParentId and actionName='VIEW' group by documentId)   as docClass on doc.fdDocId = docClass.documentId"
                                                          + " join tbcodelkup clk on clk.fdKeyCode=doc.fdStatus and clk.fdLkUpCode='DOC_STATUS' "
                                                           + " where 1=1 and ( doc.fdDocId = @fdParentId) and doc.fdDocId!=@fdDocId ";

                                    object[] pmtr = new object[] { liParentDocIDpmtr, liDocIDpmtr };
                                    var getParentDetails = db.Database.SqlQuery<getdocumentVersionHistory>(getParentdocDetails, pmtr.Select(x => ((ICloneable)x).Clone()).ToArray()).ToList();
                                    getresult.AddRange(getParentDetails);
                                }
                              }
                          

                        }
                                         
                    }
                    moResponse = RespMsgFormatter.formatResp(1, getMessage("Success"), getresult, "getUploadDocumentdetails");
                }
                else
                {
                    moResponse = RespMsgFormatter.formatResp(0, getMessage("DocSelection"), null, "getUploadDocumentdetails");
                }
               
                
            }
            catch(Exception ex)
            {
                moResponse = RespMsgFormatter.formatResp(0, ex.Message + "InnerException:" + ex.InnerException, null, "getviewDocumenthistory");
            }
            return Json(moResponse, JsonRequestBehavior.AllowGet);
        }


        public async Task<ActionResult> UploadNewDocument()
        {
            DbContextTransaction transection = null;
            try
            {
                string[] lsVinID; int liVinID; string[] lsaLatitude, lsaLongitude, lsaAddress;
                string lsLatitude = string.Empty, lsLongitude = string.Empty, lsAddress = string.Empty;
                int liUserId = 0;
                liUserId = getUserId();

                if (liUserId == 0)
                {
                    moResponse = RespMsgFormatter.formatResp(0, getMessage("SessionTimeOut"), null, "fetchContract");
                    return Json(moResponse, JsonRequestBehavior.AllowGet);
                }
                if (Request.Files.Count > 0)
                {
                    string[] lsaFileType; int lsFileType;
                    string[] lsaDocumentID; int lsDocumentID;


                    if (Request.Form.AllKeys.Contains("hdnDocumentID"))
                    {
                        lsaDocumentID = Request.Form.GetValues("hdnDocumentID");
                        lsDocumentID = int.Parse(lsaDocumentID[0].ToString());
                    }
                    else
                    {
                        moResponse = RespMsgFormatter.formatResp(0, getMessage("DocSelection"), null, "fetchContract");
                        return Json(moResponse, JsonRequestBehavior.AllowGet);
                    }

                    //if (CanAccess("UPLOADMAS", "UPLOAD_DOCUMENT", "Document" + lsDocumentID.ToString()) == true)
                    //{

                        if (Request.Form.AllKeys.Contains("FileType"))
                        {
                            lsaFileType = Request.Form.GetValues("FileType");
                            lsFileType = int.Parse(lsaFileType[0].ToString());
                        }
                        else
                        {
                            moResponse = RespMsgFormatter.formatResp(0, getMessage("FileType"), null, "fetchContract");
                            return Json(moResponse, JsonRequestBehavior.AllowGet);
                        }
                        var GetModuleCode = db.tbmodulemasters.Where(s => s.fdModuleCode == "DC" + lsFileType).Select(s => s.fdModuleCode).FirstOrDefault();



                        HttpFileCollectionBase files = Request.Files;
                        HttpPostedFileBase file = files[0];

                        string lsExtension = Path.GetExtension(file.FileName);

                        if (lsExtension.ToLower() != ".pdf" && lsExtension.ToLower() != ".jpg" && lsExtension.ToLower() != ".jpeg" && lsExtension.ToLower() != ".png")
                        {
                            transection.Rollback();
                            moResponse = RespMsgFormatter.formatResp(0, "Please upload only pdf,jpg,jpeg and png File.", null, "GetDocumentDetails");
                            return Json(moResponse, JsonRequestBehavior.AllowGet);
                        }

                        transection = db.Database.BeginTransaction();
                        tbdocument liDocument = new tbdocument();
                        List<tbdocument> lldoc = new List<tbdocument>();

                     

                        int? liParentID = 0;
                        var getdocumentParentID = db.tbdocuments.Where(s => s.fdDocId == lsDocumentID).FirstOrDefault();
                        if (getdocumentParentID.fdParentId != null)
                        {
                            liParentID = getdocumentParentID.fdParentId;
                        }
                        else
                        {
                            liParentID = lsDocumentID;
                        }


                        var filename = "";

                        if (Request.Browser.Browser.ToUpper() == "IE" || Request.Browser.Browser.ToUpper() == "INTERNETEXPLORER")
                        {
                            string[] testfiles = file.FileName.Split(new char[] { '\\' });
                            filename = testfiles[testfiles.Length - 1];
                        }
                        else
                        {
                            filename = file.FileName;
                        }


                    // set its properties

                    liDocument = null;
                        liDocument = new tbdocument();
                        liDocument.fdDocType = lsFileType;
                        liDocument.fdDocTitle = file.FileName;
                        liDocument.fdCreatedBy = getUserId();
                        liDocument.fdCreatedOn = DateTime.Now;
                        liDocument.fdStatus = 1;
                        liDocument.fdVersion = getdocumentParentID.fdVersion + 1;
                        liDocument.fdParentId = liParentID;
                        liDocument.fdFileUrl = "";
                        liDocument.fdDocGuID = System.Guid.NewGuid().ToString();
                   
                        db.tbdocuments.Add(liDocument);
                        db.SaveChanges();


                    getdocumentParentID.fdStatus = 2;
                    db.Entry(getdocumentParentID).State = System.Data.Entity.EntityState.Modified;
                    db.SaveChanges();

                    if (CanAccess(GetModuleCode, "UPLOAD_DOCUMENT", "", liDocument.fdDocId) == false)
                    {
                        transection.Rollback();
                        moResponse = RespMsgFormatter.formatResp(0, getMessage("Access"), null, "GetDocumentDetails");
                        return Json(moResponse, JsonRequestBehavior.AllowGet);
                    }

                    string lsServerPath = Server.MapPath("~/Documents/");
                  
                        var QrImgPath = "";
                        var path = liDocument.fdDocGuID.ToString() + lsExtension;
                        var getSpID = db.tbdocumentclasses.Where(s => s.associatedDocType == lsFileType).Select(d => d.AssSpId).FirstOrDefault();
                        var uncPath = "";
                        if (getSpID != null)

                        {
                            var getUncPath = db.tbstoragepolicies.Where(s => s.PKID == getSpID).FirstOrDefault();
                            if (getUncPath != null)
                            {
                                uncPath = @getUncPath.fdSpUNCpath + path;
                                QrImgPath = @getUncPath.fdSpUNCpath;
                            }
                            else
                            {
                                uncPath = @"\\SIROHI-PC\Documents/" + path;
                                QrImgPath = @"\\Tarun-PC\Documents/";
                            }

                        }
                        else
                        {
                            uncPath = @"\\SIROHI-PC\Documents/" + path;
                            QrImgPath = @"\\Tarun-PC\Documents/";
                        }


                        string lsRelative = "Documents/" + liDocument.fdDocGuID.ToString() + lsExtension;
                        string lsFileNameWithPath = Path.Combine(System.Web.HttpRuntime.AppDomainAppPath, lsRelative);



                        file.SaveAs(uncPath);
                        liDocument.fdFileUrl = uncPath;
                        liDocument.fdDocTitle = Path.GetFileName(file.FileName);
                        db.tbdocuments.Attach(liDocument);

                        var lvUpdateQuery = db.Entry(liDocument);
                        lvUpdateQuery.Property(e => e.fdFileUrl).IsModified = true;
                        lvUpdateQuery.Property(e => e.fdDocTitle).IsModified = true;
                        db.SaveChanges();
                        tbdocumentmetadata loMeta;
                        int itmNo = 0;
                        List<string> keys = new List<string>();
                        keys = Request.Form.AllKeys
                                  .Select(s => s).ToList();
                        foreach (var key in keys)
                        {
                            if (key != "FileType" && key != "hdnDocumentID" && key != "Status" && key != "Version")
                            {
                                var lsaKey = Request.Form.GetValues(key);
                                var lsKeyValue = lsaKey[0].ToString();
                                itmNo = itmNo + 1;
                                loMeta = new tbdocumentmetadata();
                                loMeta.fdItemNo = itmNo;
                                loMeta.fdKey = key;
                                loMeta.fdValue = lsKeyValue;
                                loMeta.fdDocId = liDocument.fdDocId;
                                db.tbdocumentmetadatas.Add(loMeta);
                                loMeta = null;
                            }
                        }
                        db.SaveChanges();
                        //  transection.Commit();



                        int liUserID = getUserId();
                        var username = db.tbusermasters.Where(s => s.fdUserId == liUserID).FirstOrDefault();

                        var jsonData = "{\"docid\":" + liDocument.fdDocId + ",\"url\":\"csimplifyit://docunow/\"" + liDocument.fdDocGuID + ", "
                           + " \"appname\":\"docunow\",\"defaulttemplate\":" + liDocument.fdDocGuID + ",\"createdon\":" + DateTime.Now + ",\"createdBy\":" + username.fdUserName + "}";

                        string json = new JavaScriptSerializer().Serialize(jsonData);
                        //  var result = JsonConvert.DeserializeObject(jsonData);
                        Bitmap x = QR_Code_Generator.QRCode_Generator(json);
                    // string lsExtension1 = Path.GetExtension(x.);
                    var QrCodeFolderName = db.tbcodelkups.Where(s => s.fdLkUpCode == "QR_Code_URL" && s.fdKeyCode == 1).Select(s => new { folderName = s.fdKey1 }).ToList();

                    tbqrcode qrcd = new tbqrcode();
                        qrcd.fdAssDocID = liDocument.fdDocId;
                        qrcd.fdFileUrl = QrImgPath + QrCodeFolderName[0].folderName.ToString() + liDocument.fdDocGuID + ".png";
                        db.tbqrcodes.Add(qrcd);
                        db.SaveChanges();
                        x.Save(QrImgPath + QrCodeFolderName[0].folderName.ToString() + liDocument.fdDocGuID + ".png");
                        transection.Commit();
                        var imageUrl = qrcd.fdFileUrl;


                        Dictionary<string, object> Result = new Dictionary<string, object>();
                        Result.Add("FileUrl", qrcd.fdFileUrl);
                        var byteArray = System.IO.File.ReadAllBytes(qrcd.fdFileUrl);
                        var base64 = Convert.ToBase64String(byteArray);

                        var result = db.tbdocuments.Where(s => s.fdDocId == liDocument.fdDocId).Select(d => new { d.fdDocId, d.fdVersion }).FirstOrDefault();
                        Result.Add("DocumentID", result.fdDocId);
                        Result.Add("DocumentVersion", result.fdVersion);
                        Result.Add("ImageData", base64);

                        moResponse = RespMsgFormatter.formatResp(1, getMessage("NewVersion"), Result, "UploadPOD");
                    }
                    else
                    {
                        moResponse = RespMsgFormatter.formatResp(0, getMessage("EmptyFile"), null, "UploadPOD");
                    }
                //}
                //else
                //{
                //    moResponse = RespMsgFormatter.formatResp(0, getMessage("Access"), null, "UploadPOD");
                //}
            }
            catch (Exception ex)
            {

                if (transection != null && transection.UnderlyingTransaction != null)
                {
                    transection.Rollback();
                }

                moResponse = RespMsgFormatter.formatResp(0, "message" + ex.Message + "InnerException:" + ex.InnerException, null, "UploadPOD");

            }
            return Json(moResponse, JsonRequestBehavior.AllowGet);
        }


        public int MoveContentToRrchive(int docID)
        {

            try
            {
                var getDocPathDetails = "select doc.fdDocTitle,doc.fdFileUrl,pol.fdSpArchiveUNCpath,pol.fdSpUNCpath,doc.fdDocGuID as fdDocGuID from tbdocuments doc "
                                     + " join tbdocumentclass cls on cls.associatedDocType = doc.fdDocType "
                                     + " join tbstoragepolicy pol on pol.PKID = cls.AssSpId where doc.fdDocId = " + docID + " ";

                var Result = db.Database.SqlQuery<DocPathDetetails>(getDocPathDetails).FirstOrDefault();

                if (Result != null)
                {
                    var directoryPath = Result.fdSpArchiveUNCpath + Result.fdDocTitle;
                    if (Directory.Exists(Result.fdSpUNCpath))
                    {
                        foreach (var file in new DirectoryInfo(Result.fdSpUNCpath).GetFiles())
                        {

                            string lsExtension = Path.GetExtension(Result.fdDocTitle);
                            var savefile = Result.fdDocGuID + lsExtension;
                            if (file.Name == savefile)
                            {
                                // file.MoveTo($@"{newDirectoryPath}\{file.Name}");

                                file.MoveTo(Result.fdSpArchiveUNCpath + file.Name);

                                var getdoc = db.tbdocuments.Where(s => s.fdDocId == docID).FirstOrDefault();
                                getdoc.fdFileUrl =  Result.fdSpArchiveUNCpath + file.Name;
                                db.Entry(getdoc).State = System.Data.Entity.EntityState.Modified;
                                db.SaveChanges();

                                // return 1;
                            }

                        }
                    }
                    else
                    {
                        return 0;
                    }
                    return 1;
                }
                else
                {
                    return 0;
                }

            }
            catch (Exception ex)
            {
                return 0;
            }
            return 0;
        }


        public ActionResult GetDocumentJsonDetailsResults(string requestData)
        {
            try
            {
                int liUserId = getUserId();
                if (liUserId == 0)
                {
                    moResponse = RespMsgFormatter.formatResp(0, getMessage("SessionTimeOut"), null, "fetchContract");
                    return Json(moResponse, JsonRequestBehavior.AllowGet);
                }

                JObject jobj = JObject.Parse(requestData);

                var lsDocName = string.Empty;

                int liDocId = Convert.ToInt32(jobj["docId"].ToString());
                if (liDocId != null && liDocId != 0)
                {
                    var qry = "select doc.fdDocTitle,DATE_FORMAT(doc.fdCreatedOn, '%d-%M-%Y') as fdCreatedOn,ifnull(doc.fdVersion,1) as fdVersion,tbuser.fdUserName as UserName,clk.fdKey1 from tbdocuments doc join tbcodelkup clk on clk.fdKeyCode = doc.fdDocType and clk.fdLkUpCode = 'DOC_CLASS_TYPE' join tbusermaster tbuser on tbuser.fdUserId=doc.fdCreatedBy where doc.fdDocId= @fdId ";

                    MySqlParameter loToId = new MySqlParameter("@fdId", liDocId);
                    var Query = db.Database.SqlQuery<getDocClass>(qry, loToId).FirstOrDefault();
                   var lsDocClass = Query.fdKey1;
                   lsDocName = Path.GetFileName(Query.fdDocTitle);
                    
                };


                int liDoctype = db.tbdocuments.Where(s => s.fdDocId == liDocId).Select(s => s.fdDocType).AsEnumerable().Select(s => s != null ? s.Value : 0).FirstOrDefault();
                var GetModuleCode = db.tbmodulemasters.Where(s => s.fdModuleCode == "DC" + liDoctype).Select(s => s.fdModuleCode).FirstOrDefault();
                if (CanAccess(GetModuleCode, "VIEW", "", liDocId))
                {
                    var getDocumentURL = db.tbdocuments.Where(s => s.fdDocId == liDocId).Select(s => new { s.fdFileUrl, s.fdDocType }).FirstOrDefault();
                    var getDocumentData = db.tbdocumentmetadatas.Where(s => s.fdDocId == liDocId).Select(s => new { s.fdKey, s.fdValue });
                    Dictionary<string, object> Result = new Dictionary<string, object>();
                    Result.Add("FileUrl", getDocumentURL);
                    var byteArray = System.IO.File.ReadAllBytes(getDocumentURL.fdFileUrl);

                    var CreatedTempFileSuccessfully = 0;
                    var tempFileName = System.Guid.NewGuid() + ".pdf";
                    var serverPathFileURL = Server.MapPath("~/tempdocs/" + tempFileName);

                    try
                    {

                        System.IO.File.WriteAllBytes(serverPathFileURL, byteArray.ToArray());
                        CreatedTempFileSuccessfully = 1;
                    }
                    catch (Exception e)
                    {
                        CreatedTempFileSuccessfully = 0;
                    }
                    var base64 = Convert.ToBase64String(byteArray);
                    Result.Add("DocumentData", getDocumentData);
                    
                    Result.Add("DocumentName", lsDocName);
                    Result.Add("DocumentID", liDocId);
                    
                    
                    Result.Add("ImageData", base64);
                    Result.Add("isPDFURLAvailable", CreatedTempFileSuccessfully);
                    Result.Add("PDFURL", "~/tempdocs/0a26d250-6ddc-4ddb-acf3-4911b2465aad.pdf");
                    moResponse = RespMsgFormatter.formatResp(1, getMessage("Success"), Result, "GetDocumentDetails");
                }
                else moResponse = RespMsgFormatter.formatResp(0, getMessage("Access"), null, "GetDocumentDetails");
            }
            catch (Exception ex)
            {
                moResponse = RespMsgFormatter.formatResp(0, "Msg:" + ex.Message + " InnerException:" + ex.InnerException, null, "GetDocumentDetails");
            }
            return Json(moResponse, JsonRequestBehavior.AllowGet);
        }



        public ActionResult UpdateJsonFileUrl(string requestData)
        {
            try
            {
                JObject jobj = new JObject();
                int lvDocID=0;string lvFileUrl;
                if (requestData==null && requestData=="" && requestData==string.Empty)
                {
                    moResponse = RespMsgFormatter.formatResp(0, "Invalid Parameter", null, "UpdateJsonFileUrl");
                    return Json(moResponse, JsonRequestBehavior.AllowGet);
                }
                jobj = JObject.Parse(requestData);
                
                if (jobj["DocId"].ToString()!= null && jobj["DocId"].ToString() != "" && jobj["DocId"].ToString() != string.Empty)
                {
                    lvDocID = Convert.ToInt32(jobj["DocId"].ToString());
                }
                else
                {
                    moResponse = RespMsgFormatter.formatResp(0, "DocID not found", null, "UpdateJsonFileUrl");
                    return Json(moResponse, JsonRequestBehavior.AllowGet);
                }

                if (jobj["FileUrl"].ToString() != null && jobj["FileUrl"].ToString() != "" && jobj["FileUrl"].ToString() != string.Empty && jobj["FileUrl"].ToString()!="null")
                {
                    lvFileUrl = jobj["FileUrl"].ToString();
                }
                else
                {
                    moResponse = RespMsgFormatter.formatResp(0, "FileUrl not found", null, "UpdateJsonFileUrl");
                    return Json(moResponse, JsonRequestBehavior.AllowGet);
                }

                
                var Result = db.tbdocuments.Find(lvDocID);
                if (Result!=null)
                {
                    Result.fdFileUrl = lvFileUrl;
                    db.Entry(Result).State = EntityState.Modified;
                    db.SaveChanges();
                    moResponse = RespMsgFormatter.formatResp(1, "Success", null, "UpdateJsonFileUrl");
                }
                else
                {
                    moResponse = RespMsgFormatter.formatResp(0, "invalid DocID found", null, "UpdateJsonFileUrl");
                    return Json(moResponse, JsonRequestBehavior.AllowGet);
                }

               
            }
            catch(Exception ex)
            {
                moResponse = RespMsgFormatter.formatResp(0, "Msg:" + ex.Message + " InnerException:" + ex.InnerException, null, "UpdateJsonFileUrl");
            }
            return Json(moResponse, JsonRequestBehavior.AllowGet);
        }


        public ActionResult UpdateJsonFileData(string requestData)
        {
            try
            {
                var objReqstJson = Request.Form["ObjJson"];
                var paraReqstJson = Request.Form["ParaJson"];
                JObject objJobjct = new JObject();
                JObject paraJobjct = new JObject();

                if (objReqstJson != null && objReqstJson.ToString() != "" && objReqstJson.ToString() != string.Empty)
                {
                    objJobjct = JObject.Parse(objReqstJson);
                }

                if (paraReqstJson != null && paraReqstJson.ToString() != "" && paraReqstJson.ToString() != string.Empty)
                {
                    paraJobjct = JObject.Parse(paraReqstJson);
                }


                JObject jobj = new JObject();
                int lvDocID = 0; string lvFileUrl;
                if (requestData == null && requestData == "" && requestData == string.Empty)
                {
                    moResponse = RespMsgFormatter.formatResp(0, "Invalid Parameter", null, "UpdateJsonFileUrl");
                    return Json(moResponse, JsonRequestBehavior.AllowGet);
                }
                jobj = JObject.Parse(requestData);

                if (jobj["DocId"].ToString() != null && jobj["DocId"].ToString() != "" && jobj["DocId"].ToString() != string.Empty)
                {
                    lvDocID = Convert.ToInt32(jobj["DocId"].ToString());
                }
                else
                {
                    moResponse = RespMsgFormatter.formatResp(0, "DocID not found", null, "UpdateJsonFileUrl");
                    return Json(moResponse, JsonRequestBehavior.AllowGet);
                }

                
                var liDocument = db.tbdocuments.Find(lvDocID);
                if (liDocument != null)
                {
                    
                    if (objReqstJson != null && objReqstJson.ToString() != "" && objReqstJson.ToString() != string.Empty)
                    {

                        if (objJobjct["Id"].ToString() != "" && objJobjct["Id"].ToString() != null && objJobjct["Id"].ToString() != string.Empty)
                        {
                            liDocument.fdVisionId = objJobjct["Id"].ToString();
                        }

                        if (objJobjct["ETag"].ToString() != "" && objJobjct["ETag"].ToString() != null && objJobjct["ETag"].ToString() != string.Empty)
                        {
                            liDocument.fdVisionETag = objJobjct["ETag"].ToString();
                        }

                        if (objJobjct["Name"].ToString() != "" && objJobjct["Name"].ToString() != null && objJobjct["Name"].ToString() != string.Empty)
                        {
                            liDocument.fdVisonName = objJobjct["Name"].ToString();
                        }

                        if (objJobjct["SelfLink"].ToString() != "" && objJobjct["SelfLink"].ToString() != null && objJobjct["SelfLink"].ToString() != string.Empty)
                        {
                            liDocument.fdVisionSelfLink = objJobjct["SelfLink"].ToString();
                        }




                    }
                    if (paraReqstJson != null && paraReqstJson.ToString() != "" && paraReqstJson.ToString() != string.Empty)
                    {
                        if (paraJobjct["longitude"].ToString() != "" && paraJobjct["longitude"].ToString() != null && paraJobjct["longitude"].ToString() != string.Empty)
                        {
                            liDocument.fdLongitude = paraJobjct["longitude"].ToString();
                        }

                        if (paraJobjct["latitude"].ToString() != "" && paraJobjct["latitude"].ToString() != null && paraJobjct["latitude"].ToString() != string.Empty)
                        {
                            liDocument.fdLatitude = paraJobjct["latitude"].ToString();
                        }

                        if (paraJobjct["address"].ToString() != "" && paraJobjct["address"].ToString() != null && paraJobjct["address"].ToString() != string.Empty)
                        {
                            liDocument.fdAddress = paraJobjct["address"].ToString();
                        }

                    }
                    db.Entry(liDocument).State = EntityState.Modified;
                    db.SaveChanges();
                    moResponse = RespMsgFormatter.formatResp(1, "Success", null, "UpdateJsonFileUrl");
                }
                else
                {
                    moResponse = RespMsgFormatter.formatResp(0, "invalid DocID found", null, "UpdateJsonFileUrl");
                    return Json(moResponse, JsonRequestBehavior.AllowGet);
                }


            }
            catch (Exception ex)
            {
                moResponse = RespMsgFormatter.formatResp(0, "Msg:" + ex.Message + " InnerException:" + ex.InnerException, null, "UpdateJsonFileUrl");
            }
            return Json(moResponse, JsonRequestBehavior.AllowGet);
        }




        //public int MoveContentToRrchive(int docID)
        //{

        //    try
        //    {
        //        var getDocPathDetails = "select doc.fdDocTitle,doc.fdFileUrl,pol.fdSpArchiveUNCpath,pol.fdSpUNCpath from tbdocuments doc "
        //                             + " join tbdocumentclass cls on cls.associatedDocType = doc.fdDocType "
        //                             + " join tbstoragepolicy pol on pol.PKID = cls.AssSpId where doc.fdDocId = " + docID + " ";

        //        var Result = db.Database.SqlQuery<DocPathDetetails>(getDocPathDetails).FirstOrDefault();

        //        if (Result != null)
        //        {
        //            var directoryPath = Result.fdSpArchiveUNCpath + Result.fdDocTitle;
        //            if (Directory.Exists(Result.fdSpUNCpath))
        //            {                       
        //                foreach (var file in new DirectoryInfo(Result.fdSpUNCpath).GetFiles())
        //                {
        //                    if(file.Name== Result.fdDocTitle)
        //                    {
        //                        // file.MoveTo($@"{newDirectoryPath}\{file.Name}");

        //                        file.MoveTo(Result.fdSpArchiveUNCpath + file.Name);
        //                       // return 1;
        //                    }

        //                }
        //            }
        //            else
        //            {
        //                return 0;
        //            }
        //            return 1;
        //        }
        //        else
        //        {
        //            return 0;
        //        }


        //    }
        //    catch(Exception ex)
        //    {
        //        return 0;
        //    }

        //    return 0;
        //}

    }
}