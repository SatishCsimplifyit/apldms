﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MySql.Data.MySqlClient;
using System.Web.Mvc;
using Newtonsoft.Json.Linq;
using PanasonicDMS.Security;
using PanasonicDMS.Models.DbContext;
using PanasonicDMS.Models;
using PanasonicDMS.App_Code;
using System.Resources;
using System.Globalization;
using System.Threading;
using System.Reflection;
using System.Data.SqlClient;
using System.Text;
using System.Security.Cryptography;
using System.IO;

namespace PanasonicDMS.Controllers
{
	public class CommonController : Controller
	{
		// GET: Common
		public ActionResult Common()
		{
			return View();
		}
		private panasonicdmsEntities db = new panasonicdmsEntities();
		private Dictionary<string, object> moResponse;
		private UserLocationMonduleFunction moLocModuleFunction = new UserLocationMonduleFunction();

		protected bool SetModuleName(string ModuleName)
		{
			if (Session == null)
			{
				return false;
			}
			else
			{
				UserInfo userInfo = (UserInfo)Session["userInfo"];
				if (userInfo != null)
				{
					userInfo.ModuleName = ModuleName;
					return true;
				}
				else
				{
					return false;
				}
			}
		}



		//public string Encrypt(string Password)
		//{
		//    string EncryptionKey = string.Empty;
		//    var getEncryptionKey = db.tbcodelkups.Where(s => s.fdLkUpCode == "PASS_ENC_KEY" && s.fdKeyCode == 1).FirstOrDefault();
		//    if (getEncryptionKey != null)
		//    {
		//        EncryptionKey = getEncryptionKey.fdKey1;
		//    }

		//    byte[] PasswordBytes = Encoding.Unicode.GetBytes(Password);
		//    using (Aes encryptor = Aes.Create())
		//    {
		//        Rfc2898DeriveBytes pdb = new
		//            Rfc2898DeriveBytes(EncryptionKey, new byte[]
		//            { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
		//        encryptor.KeySize = 256;
		//        encryptor.BlockSize = 128;
		//        encryptor.Key = pdb.GetBytes(encryptor.KeySize/8);
		//        encryptor.IV = pdb.GetBytes(encryptor.BlockSize/8);

		//        encryptor.Mode = CipherMode.CBC;
		//        using (MemoryStream ms = new MemoryStream())
		//        {
		//            using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write))
		//            {
		//                cs.Write(PasswordBytes, 0, PasswordBytes.Length);
		//                cs.Close();
		//            }
		//            Password = Convert.ToBase64String(ms.ToArray());
		//        }
		//    }
		//    return Password;
		//}

		public string Encrypt(string Password)
		{
			using (var random = new RNGCryptoServiceProvider())
			{
				string EncryptionKey = string.Empty;
				var getEncryptionKey = db.tbcodelkups.Where(s => s.fdLkUpCode == "PASS_ENC_KEY" && s.fdKeyCode == 1).FirstOrDefault();
				if (getEncryptionKey != null)
				{
					EncryptionKey = getEncryptionKey.fdKey1;
				}


				RijndaelManaged objrij = new RijndaelManaged();
				//set the mode for operation of the algorithm   
				objrij.Mode = CipherMode.CBC;
				//set the padding mode used in the algorithm.   
				objrij.Padding = PaddingMode.PKCS7;
				//set the size, in bits, for the secret key.   
				objrij.KeySize = 0x80;
				//set the block size in bits for the cryptographic operation.    
				objrij.BlockSize = 0x80;
				//set the symmetric key that is used for encryption & decryption.    
				byte[] passBytes = Encoding.UTF8.GetBytes(EncryptionKey);
				//set the initialization vector (IV) for the symmetric algorithm    
				byte[] EncryptionkeyBytes = new byte[] { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };

				int len = passBytes.Length;
				if (len > EncryptionkeyBytes.Length)
				{
					len = EncryptionkeyBytes.Length;
				}
				Array.Copy(passBytes, EncryptionkeyBytes, len);

				objrij.Key = EncryptionkeyBytes;
				objrij.IV = EncryptionkeyBytes;

				//Creates symmetric AES object with the current key and initialization vector IV.    
				ICryptoTransform objtransform = objrij.CreateEncryptor();
				byte[] textDataByte = Encoding.UTF8.GetBytes(Password);
				//Final transform the test string.  
				var encryptedResult = Convert.ToBase64String(objtransform.TransformFinalBlock(textDataByte, 0, textDataByte.Length));
							 
				return encryptedResult;
			}
			
		}

		static byte[] EncryptStringToBytes_Aes(string plainText, byte[] Key)
		{
			byte[] encrypted;
			byte[] IV;

			using (Aes aesAlg = Aes.Create())
			{
				aesAlg.Key = Key;

			   aesAlg.GenerateIV();
				IV = aesAlg.IV;

				aesAlg.Mode = CipherMode.CBC;

				var encryptor = aesAlg.CreateEncryptor(aesAlg.Key, aesAlg.IV);

				// Create the streams used for encryption. 
				using (var msEncrypt = new MemoryStream())
				{
					using (var csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write))
					{
						using (var swEncrypt = new StreamWriter(csEncrypt))
						{
							//Write all data to the stream.
							swEncrypt.Write(plainText);
						}
						encrypted = msEncrypt.ToArray();
					}
				}
			}

			var combinedIvCt = new byte[IV.Length + encrypted.Length];
			Array.Copy(IV, 0, combinedIvCt, 0, IV.Length);
			Array.Copy(encrypted, 0, combinedIvCt, IV.Length, encrypted.Length);

			// Return the encrypted bytes from the memory stream. 
			return combinedIvCt;

		}

		//public string AES_Encrypt(byte[] bytesToBeEncrypted, byte[] passwordBytes)
		//{
		//    byte[] encryptedBytes = null;
		//    byte[] saltBytes = new byte[] { 1, 2, 3, 4, 5, 6, 7, 8 };
		//    using (MemoryStream ms = new MemoryStream())
		//    {
		//        using (RijndaelManaged AES = new RijndaelManaged())
		//        {
		//            AES.KeySize = 256;
		//            AES.BlockSize = 128;
		//            var key = new Rfc2898DeriveBytes(passwordBytes, saltBytes, 1000);
		//            AES.Key = key.GetBytes(AES.KeySize / 8);
		//            AES.IV = key.GetBytes(AES.BlockSize / 8);
		//            AES.Mode = CipherMode.CBC;
		//            using (var cs = new CryptoStream(ms, AES.CreateEncryptor(), CryptoStreamMode.Write))
		//            {
		//                cs.Write(bytesToBeEncrypted, 0, bytesToBeEncrypted.Length);
		//                cs.Close();
		//            }
		//            encryptedBytes = ms.ToArray();
		//        }
		//    }
		//    return encryptedBytes;
		//}



		public ActionResult GetHelpURL(string requestData)
		{
			try
			{
				JObject ljoData = JObject.Parse(requestData);
				if (ljoData["ModuleName"] != null && ljoData["ModuleName"].ToString() != "")
				{
					string ModuleName = ljoData["ModuleName"].ToString();
					string HelpURL = db.tbmodulemasters.Where(s => s.fdModuleCode == ModuleName).Select(s => s.HelpUrl).FirstOrDefault();
					if (HelpURL == string.Empty || HelpURL == null)
					{
						HelpURL = "../../DMSDocumentations/index.html";
				 
					}
					moResponse = RespMsgFormatter.formatResp(1, "Succesfull.", HelpURL, "GetHelpURL");
				}
				else
				{
					moResponse = RespMsgFormatter.formatResp(0, "Invalid Request", null, "GetHelpURL");
				}

			}
			catch (Exception ex)
			{
				moResponse = RespMsgFormatter.formatResp(0, "MSG:" + ex.Message + "InnerException:" + ex.InnerException, null, "GetHelpURL");
			}
			return Json(moResponse, JsonRequestBehavior.AllowGet);
		}


		public bool RefreshRoles()
		{
			try
			{
				var userInfo = getUserInfo();
				if (userInfo != null)
				{
					var userId = getUserId();
					userInfo.userRoles = moLocModuleFunction.getAllUserRoleFunctionLocation(userId, 1);
					setLoggedInUserDetails(userInfo);
					return true;
				}
				else
				{
					return false;
				}
			}
			catch(Exception ex)
			{
				return false;
			}
			
			
		}
		protected void setLoggedInUserDetails(UserInfo userInfo)
		{
			Session.Clear();
			if (Session == null)
			{

				var sSession = System.Web.HttpContext.Current.Session;
				sSession["userInfo"] = userInfo;
			}
			else if (Session["userInfo"] == null)
			{
				Session["userInfo"] = userInfo;

			}


		}


		[HttpPost]
		public ActionResult fetchStatus(string requestData)
		{
			try
			{
				string lsLookUpCode = string.Empty; int liKeyCode = 0; bool lbSucces = false; string lsLkupCode = String.Empty;
				string lsStatusQuery = "SELECT fdKey1,fdKeyCode,fdLkUpCode,fdDescription,fdKey2 FROM tbcodelkup where 1=1";
				//MySqlParameter loLkupCodeParam = new MySqlParameter("","");
				MySqlParameter loKeyCodeParam = new MySqlParameter("@KeyCode", "");

				if (string.IsNullOrEmpty(requestData))
				{

				}
				else
				{
					JObject ljRequestJson = JObject.Parse(requestData);

					if (ljRequestJson["lookUpCode"] != null && !string.IsNullOrEmpty(ljRequestJson["lookUpCode"].ToString()))
					{

						JArray laLkUpCode = JArray.Parse(ljRequestJson["lookUpCode"].ToString());
						if (laLkUpCode[0].ToString() == "DOC_CLASS_TYPE")
						{
							lsStatusQuery = lsStatusQuery + " and fdLkUpCode= 'DOC_CLASS_TYPE'";
							List<DMSModelStatus> toReturn = new List<DMSModelStatus>();
							var AllKeys = db.Database.SqlQuery<DMSModelStatus>(lsStatusQuery + " order by fdKey1 ").ToList();
							foreach (var item in AllKeys)
							{
							   if (CanAccess("DC" + item.fdKeyCode, "DRAFT", requestData))
								{
									toReturn.Add(item);
								}
							}
							moResponse = RespMsgFormatter.formatResp(1, getMessage("Success"), toReturn, "fetchStatus");
							return Json(moResponse, JsonRequestBehavior.AllowGet);

						}

						int i = 0;
						foreach (JToken objLkUpCode in laLkUpCode.Children<JToken>())
						{
						   
						   

							if (i == 0)
							{
								lsLkupCode = lsLkupCode + "'" + objLkUpCode.ToString() + "'";
								i = i + 1;
							}
							else
							{
								lsLkupCode = lsLkupCode + "," + "'" + objLkUpCode.ToString() + "'";
							}

						}
						if (lsLkupCode != "_" && lsLkupCode != string.Empty)
						{
							lsLkupCode = lsLkupCode.Replace('"', ' ').Trim();
							lsStatusQuery = lsStatusQuery + " AND fdLkUpCode in (" + lsLkupCode + ")";


						}

					}

					if (ljRequestJson["KeyCode"] != null && !string.IsNullOrEmpty(ljRequestJson["KeyCode"].ToString()))
					{
						lbSucces = int.TryParse(ljRequestJson["KeyCode"].ToString(), out liKeyCode);
						if (lbSucces == true)
						{
							lsStatusQuery = lsStatusQuery + " AND fdKeyCode = @KeyCode";
							loKeyCodeParam = new MySqlParameter("@KeyCode", liKeyCode);
						}

					}


				}


				object[] parameters = new object[] { loKeyCodeParam };
				var fetchStatus = db.Database.SqlQuery<DMSModelStatus>(lsStatusQuery, parameters).ToList();
				moResponse = RespMsgFormatter.formatResp(1, getMessage("Success"), fetchStatus, "fetchStatus");
			}
			catch (Exception ex)
			{
					moResponse = RespMsgFormatter.formatResp(0, "" + ex.Message, null, "fetchStatus");
			}
			return Json(moResponse, JsonRequestBehavior.AllowGet);
		}

		protected int getUserId()
		{

			if (Session == null)
			{
				return 0;
			}
			else
			{
				UserInfo userInfo = (UserInfo)Session["userInfo"];
				if (userInfo != null)
				{
					return userInfo.userId;
				}
				else
				{
					return 0;
				}
			}
		}

		protected UserInfo getUserInfo()
		{

			if (Session == null)
			{
				return null;
			}
			else
			{
				UserInfo userInfo = (UserInfo)Session["userInfo"];
				if (userInfo != null)
				{
					return userInfo;
				}
				else
				{
					return null;
				}
			}
		}
		

		protected bool CanAccess(string moduleCode, string functionCode, string requestData,int DociD=0)
		{
			try
			{

				MySqlParameter moduleCodepmtr = new MySqlParameter("@moduleCode", moduleCode);
				MySqlParameter functionCodepmtr = new MySqlParameter("@functionCode", functionCode);
				MySqlParameter useridpmtr = new MySqlParameter();

				UserInfo userInfoo = new UserInfo();
				List<UserModuleFunctionModel> UserMonduleFunction = new List<UserModuleFunctionModel>();
				SetModuleName(moduleCode);

				string UserToken = string.Empty;
				 if (requestData != null && requestData != "")
				{
					//JObject jobt = JObject.Parse(requestData);
						UserInfo userInfo = (UserInfo)Session["userInfo"];
						if (userInfo != null)
						{
							var lvSearchRoles = userInfo.userRoles.Find(m => m.moduleCode == moduleCode && m.functionCode == functionCode);
							if (lvSearchRoles != null)
							{
								if (lvSearchRoles.functionCode == functionCode && lvSearchRoles.moduleCode == moduleCode)
								{

								user_action_audit audit = new user_action_audit();
								audit.actionName = functionCode;
								audit.actionType = moduleCode;
								audit.documentId = DociD;
								audit.createdBy = getUserId();
								audit.createdOn = DateTime.Now;
								audit.requestData = requestData;
								audit.viewData = "SUCCESS";
								db.user_action_audit.Add(audit);
								db.SaveChanges();

									return true;
								}
								else
								{
								user_action_audit audit = new user_action_audit();
								audit.actionName = functionCode;
								audit.actionType = moduleCode;
								audit.documentId = DociD;
								audit.createdBy = getUserId();
								audit.createdOn = DateTime.Now;
								audit.requestData = requestData;
								audit.viewData = "ACCESS DENIED";
								db.user_action_audit.Add(audit);
								db.SaveChanges();
								return false;
								}

							}
							else
							{
								return false;

							}
						}
						else
						{
							return false;
						}
					
				}
				else
				{
					UserInfo userInfo = (UserInfo)Session["userInfo"];
					if (userInfo != null)
					{
						var lvSearchRoles = userInfo.userRoles.Find(m => m.moduleCode == moduleCode && m.functionCode == functionCode);
						if (lvSearchRoles != null)
						{
							if (lvSearchRoles.functionCode == functionCode && lvSearchRoles.moduleCode == moduleCode)
							{

								user_action_audit audit = new user_action_audit();
								audit.actionName = functionCode;
								audit.actionType = moduleCode;
								audit.documentId = DociD;
								audit.createdBy = getUserId();
								audit.createdOn = DateTime.Now;
								audit.requestData = requestData;
								audit.viewData = "SUCCESS";
								db.user_action_audit.Add(audit);
								db.SaveChanges();
								return true;
							}
							else
							{

								user_action_audit audit = new user_action_audit();
								audit.actionName = functionCode;
								audit.actionType = moduleCode;
								audit.documentId = DociD;
								audit.createdBy = getUserId();
								audit.createdOn = DateTime.Now;
								audit.requestData = requestData;
								audit.viewData = "ACCESS DENIED";
								db.user_action_audit.Add(audit);
								return false;
							}

						}
						else
						{
							user_action_audit audit = new user_action_audit();
							audit.actionName = functionCode;
							audit.actionType = moduleCode;
							audit.documentId = DociD;
							audit.createdBy = getUserId();
							audit.createdOn = DateTime.Now;
							audit.requestData = requestData;
							audit.viewData = "ACCESS DENIED";
							db.user_action_audit.Add(audit);
							db.SaveChanges();
							return false;
						   

						}
					}
					else
					{
						return false;
					}

				}
			}
			catch (Exception ex)
			{
				return false;

			}

		}

		protected UserInfo getUserInfo(string requestData)
		{
			UserInfo userInfo = new UserInfo();

			if (Session == null)
			{
				if (requestData != null && requestData != "")
				{

					JObject jobt = JObject.Parse(requestData);
					if (jobt["ut"] != null && jobt["ut"].ToString() != string.Empty)
					{
						string userToken = jobt["ut"].ToString();
						var getvaliduser = db.tbusermasters.Where(s => s.fdUserToken == userToken).FirstOrDefault();
						if (getvaliduser != null && getvaliduser.ToString() != null && getvaliduser.ToString() != "" && getvaliduser.fdIsActive != 0)
						{
							userInfo = new UserInfo();
							userInfo.userRoles = moLocModuleFunction.getAllUserRoleFunctionLocation(getvaliduser.fdUserId, 1);
							userInfo.userId = getvaliduser.fdUserId;
							userInfo.userName = getvaliduser.fdUserName;
							userInfo.lastLoggedInDate = getvaliduser.fdLastLoggedInDate;
							userInfo.email = getvaliduser.fdUserEmail;
							setLoggedInUserDetails(userInfo);
							//FormsAuthentication.SetAuthCookie(liEmailId,);
							moLocModuleFunction.getUserAssociatedRoleDetails(userInfo);
							return userInfo;

						}
					}


				}

				return null;
			}
			else
			{
				userInfo = (UserInfo)Session["userInfo"];
				if (userInfo != null)
				{
					return userInfo;
				}
				else
				{

					if (requestData != null && requestData != "")
					{

						JObject jobt = JObject.Parse(requestData);
						if (jobt["ut"] != null && jobt["ut"].ToString() != string.Empty)
						{
							string userToken = jobt["ut"].ToString();
							var getvaliduser = db.tbusermasters.Where(s => s.fdUserToken == userToken).FirstOrDefault();
							if (getvaliduser != null && getvaliduser.ToString() != null && getvaliduser.ToString() != "" && getvaliduser.fdIsActive != 0)
							{

								userInfo = new UserInfo();

								userInfo.userRoles = moLocModuleFunction.getAllUserRoleFunctionLocation(getvaliduser.fdUserId, 1);
								userInfo.userId = getvaliduser.fdUserId;
								userInfo.userName = getvaliduser.fdUserName;
								userInfo.lastLoggedInDate = getvaliduser.fdLastLoggedInDate;
								userInfo.email = getvaliduser.fdUserEmail;
								setLoggedInUserDetails(userInfo);
								//FormsAuthentication.SetAuthCookie(liEmailId,);
								moLocModuleFunction.getUserAssociatedRoleDetails(userInfo);
								return userInfo;

							}
						}


					}


					return null;
				}
			}
		}

		public static string getMessage(string Key)
		{
			 string Msg=string.Empty;
			ResourceManager rm = new ResourceManager("PanasonicDMS.MessageFile.MessageFile",
				Assembly.GetExecutingAssembly());
			Msg = rm.GetString(Key, CultureInfo.CurrentCulture);
		  
		  //  Msg = rm.GetString(Key);
		 
			return Msg;
		}


		public ActionResult getModuleRights(string requestData)
		{
			try
			{
				if (requestData != null)
				{
					UserInfo userInfo = (UserInfo)Session["userInfo"];
					if (userInfo != null)
					{
						var lvSearchRoles = userInfo.userRoles.ToList();
						if (lvSearchRoles != null)
						{
							moResponse = RespMsgFormatter.formatResp(1, "Succesfully Executed", lvSearchRoles, "getModuleRights");

						}
						else
						{
							moResponse = RespMsgFormatter.formatResp(-1, "No Function Found!", null, "getModuleRights");

						}
					}
					else
					{
						moResponse = RespMsgFormatter.formatResp(0, "User Not Login", null, "getModuleRights");

					}
				}
				else
				{
					moResponse = RespMsgFormatter.formatResp(0, "No Input Given!", null, "getModuleRights");
				}
			}
			catch (Exception ex)
			{
				moResponse = RespMsgFormatter.formatResp(0, "" + ex.Message, null, "getModuleRights");

			}
			return Json(moResponse, JsonRequestBehavior.AllowGet);
		}




		//public ActionResult getModuleRights(string requestData)
		//{
		//    try
		//    {
		//        if (requestData != null)
		//        {
		//            UserInfo userInfo = (UserInfo)Session["userInfo"];
		//            if (userInfo != null)
		//            {
		//                var lvSearchRoles = userInfo.userRoles.Where(m => m.moduleCode == requestData);
		//                if (lvSearchRoles != null)
		//                {
		//                    moResponse = RespMsgFormatter.formatResp(1, "Succesfully Executed", lvSearchRoles, "getModuleRights");

		//                }
		//                else
		//                {
		//                    moResponse = RespMsgFormatter.formatResp(-1, "No Function Found!", null, "getModuleRights");

		//                }
		//            }
		//            else
		//            {
		//                moResponse = RespMsgFormatter.formatResp(0, "User Not Login", null, "getModuleRights");

		//            }
		//        }
		//        else
		//        {
		//            moResponse = RespMsgFormatter.formatResp(0, "No Input Given!", null, "getModuleRights");
		//        }
		//    }
		//    catch (Exception ex)
		//    {
		//        moResponse = RespMsgFormatter.formatResp(0, "" + ex.Message, null, "getModuleRights");

		//    }
		//    return Json(moResponse, JsonRequestBehavior.AllowGet);
		//}



	}
}