﻿using PanasonicDMS.App_Code;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
//using PanasonicDMS.Models.DbContext;
using PanasonicDMS.Security;
using PanasonicDMS.Models;
using System.Web.Security;
using PanasonicDMS.Models.DbContext;
using System.Data.SqlClient;
using System.Threading.Tasks;
using System.Net.Http;
using System.Web.Script.Serialization;
using System.Text;
using System.Net;
using MySql.Data.MySqlClient;
using System.Data.Entity;

namespace PanasonicDMS.Controllers
{
	public class AuthenticateController : CommonController
	{
		// GET: Login
		public ActionResult Index()
		{
			return View("Login");
		}

		public ActionResult LogOut()
		{
			Response.Cookies.Clear();

			FormsAuthentication.SignOut();

			HttpCookie c = new HttpCookie("login");
			c.Expires = DateTime.Now.AddYears(-2);
			Response.Cookies.Add(c);

			Session.Clear();

			// moResponse = RespMsgFormatter.formatResp(1, "Successfully logout!", null, "LogOut");
			// return Json(moResponse, JsonRequestBehavior.AllowGet);

		   // return View("login");
			return RedirectToAction("Index");
			
		}
		// GET: Authenticate
		private panasonicdmsEntities db = new panasonicdmsEntities();
		private Dictionary<string, object> moResponse;
		private UserLocationMonduleFunction moLocModuleFunction = new UserLocationMonduleFunction();

		public ActionResult LoginUsingActiveDirectory(string requestData)
		{
			try
			{
				string apiUrl = db.tbcodelkups.Where(s => s.fdLkUpCode == "AD_SERVICE" && s.fdKeyCode == 1).Select(d => d.fdKey1).FirstOrDefault();

				JObject jobt = JObject.Parse(requestData);
				tbusermaster user = new tbusermaster();


				string liEmailId = string.Empty;
				string liPassword = string.Empty;
				bool lbSuccess = false;
				bool LogInlbSuccess = false;
				int liPlatfrom = 0;
				int liphoneNo = 0;
				var EncryptionPassword = string.Empty;

				if (jobt["txtEmailID"] != null && jobt["txtEmailID"].ToString() != string.Empty)
				{
					lbSuccess = int.TryParse(jobt["txtEmailID"].ToString(), out liphoneNo);
					if (lbSuccess == true)
					{
						lbSuccess = false;
					}
					else
					{
						liEmailId = jobt["txtEmailID"].ToString();
					}

				}
				else
				{
					moResponse = RespMsgFormatter.formatResp(0, getMessage("EmailID"), null, "LoginUsingActiveDirectory");
					return Json(moResponse, JsonRequestBehavior.AllowGet);
				}


				if (jobt["txtPassword"] != null && jobt["txtPassword"].ToString() != string.Empty)
				{
					liPassword = jobt["txtPassword"].ToString();
					EncryptionPassword = Encrypt(liPassword);
				}
				else
				{
					moResponse = RespMsgFormatter.formatResp(0, getMessage("Password"), null, "LoginUsingActiveDirectory");
					return Json(moResponse, JsonRequestBehavior.AllowGet);
				}



				var jsonInput = "{'userMasterData':{'UserName':'" + liEmailId + "','Password':'" + EncryptionPassword + "','UserType':'PI','LoginSource':'WEB','IMEI':'182.64.201.11','IPAddress':'10.85.28.37:201','RequestAgentName':'Web 10.2','DeviceID':'10.85.28.37:201','DeviceType':'Web'}}";

				var input = new
				{
					userMasterData =new {
						UserName= liEmailId,
						Password= EncryptionPassword,
						UserType="PI",
						LoginSource="WEB",
						IMEI="182.64.201.11",
						IPAddress="10.85.28.37:201",
						RequestAgentName="Web 10.2",
						DeviceID="10.85.28.37:201",
						DeviceType="Web",
					},
				};
			   
				string inputJson = (new JavaScriptSerializer()).Serialize(input);
				string Result = ValidateActiveDirectory(apiUrl, inputJson);
				string getADMessage = string.Empty;
				try
				{
					JObject jobtj = JObject.Parse(Result);
				  //  string flag = jobtj["IsSuccess"].ToString();
				  
					 bool flag = jobtj.SelectToken("IsSuccess").Value<bool>();

					if (jobtj["Message"] != null && jobtj["Message"].ToString() != string.Empty)
					{
						getADMessage = jobtj["Message"].ToString();
					}
				  
					// var getADMessage = jobtj.SelectToken("Message").Value<string>();
					if (flag == true)
					{
						LogIn(requestData);
						return Json(moResponse, JsonRequestBehavior.AllowGet);
					}
					else
					{
						// moResponse = RespMsgFormatter.formatResp(0, getMessage("ADNotExist"), null, "LoginUsingActiveDirectory");
					  //  LogIn(requestData);
						moResponse = RespMsgFormatter.formatResp(0, getADMessage, null, "LoginUsingActiveDirectory");
						return Json(moResponse, JsonRequestBehavior.AllowGet);
					}

				}
				catch (Exception ex)
				{
				  //LogIn(requestData);
				   
					moResponse = RespMsgFormatter.formatResp(0, "Message:" + Result+ "InnerException:" + ex.InnerException +"Stack Trace:" +ex.StackTrace , null, "ValidateUsingActiveDirectory");
					return Json(moResponse, JsonRequestBehavior.AllowGet);
				}
				


			}
			catch (Exception ex)
			{
				moResponse = RespMsgFormatter.formatResp(0, "InnerException:" + ex.InnerException + "Stack Trace:" + ex.StackTrace + "Message:" + ex.Message, null, "ValidateUsingActiveDirectory");

			   
			}
			//  return View();
			return Json(moResponse, JsonRequestBehavior.AllowGet);
		}



		//public async Task<string> ValidateActiveDirectory(string apiUrl, string jsonInput)
		//{
		//    try
		//    {
		//        using (HttpClient client1 = new HttpClient())
		//        {


		//            string inputJson = (new JavaScriptSerializer()).Serialize(jsonInput);
		//            WebClient client = new WebClient();
		//            client.Headers["Content-type"] = "application/json";
		//            client.Encoding = Encoding.UTF8;
		//            string json = client.UploadString(apiUrl, inputJson);


		//            //client.BaseAddress = new Uri(apiUrl);
		//            //client.DefaultRequestHeaders.Accept.Clear();
		//            //client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
		//            ////return true;
		//            //var response = client.PostAsync(apiUrl, new StringContent(
		//            //        new JavaScriptSerializer().Serialize(jsonInput), Encoding.UTF8, "application/json")).Result;

		//            ////string response = await client.(apiUrl, jsonInput);

		//            //// var data = await response.Content.ReadAsStringAsync();
		//            //string data = await response.Content.ReadAsStringAsync();
		//            ////var obj = Newtonsoft.Json.JsonConvert.DeserializeObject(data);
		//            //JObject jobt = JObject.Parse(data);
		//            ////bool flag = jobt.SelectToken("IsSuccess").Value<bool>();
		//            //return data;



		//            //if (response.IsSuccessStatusCode)
		//            //{

		//            //    var data = await response.Content.ReadAsStringAsync();
		//            //    JObject jobt = JObject.Parse(data);
		//            //    bool flag = jobt.SelectToken("IsSuccess").Value<bool>();
		//            //    //if (flag == true)
		//            //    //{
		//            //    //    return true;
		//            //    //}
		//            //    //else
		//            //    //{
		//            //    //    return false;
		//            //    //}
		//            //}
		//            //else
		//            //{
		//            //    return false;
		//            //}
		//        }
		//    }
		//    catch (Exception ex)
		//    {
		//        return "inner Exception:" + ex.InnerException.ToString();
		//    }


		//}

		public string ValidateActiveDirectory(string apiUrl, string jsonInputdata)
		{
			try
			{
			   
				//string inputJson = (new JavaScriptSerializer()).Serialize(jsonInput);
					WebClient client = new WebClient();
					client.Headers["Content-type"] = "application/json";
					client.Encoding = Encoding.UTF8;
					string data = client.UploadString(apiUrl, jsonInputdata);
					JObject jobt = JObject.Parse(data);
					return data;

				   
			}
			catch (Exception ex)
			{
				return "inner Exception:"+ ex.InnerException.ToString() +"stack trace:"+ex.StackTrace.ToString();
			}

			
		}



		//public async Task<bool> ValidateActiveDirectory(string apiUrl, string jsonInput)
		//{
		//    try
		//    {
		//        using (HttpClient client = new HttpClient())
		//        {
		//            client.BaseAddress = new Uri(apiUrl);
		//            client.DefaultRequestHeaders.Accept.Clear();
		//            client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
		//           //return true;
		//            var response = client.PostAsync(apiUrl, new StringContent(
		//                    new JavaScriptSerializer().Serialize(jsonInput), Encoding.UTF8, "application/json")).Result;
		//            ///  HttpResponseMessage response = await client.PostAsJsonAsync("api/values/", postedData);
		//            //string response = await client.(apiUrl, jsonInput);

		//            if (response.IsSuccessStatusCode)
		//            {

		//                var data = await response.Content.ReadAsStringAsync();
		//                JObject jobt = JObject.Parse(data);
		//                bool flag = jobt.SelectToken("IsSuccess").Value<bool>();
		//                if (flag == true)
		//                {
		//                    return true;
		//                }
		//                else
		//                {
		//                    return false;
		//                }
		//            }
		//            else
		//            {
		//                return false;
		//            }
		//        }
		//    }
		//    catch (Exception ex)
		//    {
		//        return false;
		//    }

		//    return true;
		//}



		//[HttpPost]
		public ActionResult LogIn(string requestData)
		{
			try
			{
				
				/* clear session */

				//Response.Cookies.Clear();

				//FormsAuthentication.SignOut();

				//HttpCookie c = new HttpCookie("login");
				//c.Expires = DateTime.Now.AddYears(-2);
				//Response.Cookies.Add(c);

				//Session.Clear();
				//requestData = Request.Form.; ;
				if (requestData.Contains("CallFromAPI"))
				{
					requestData = System.Web.HttpContext.Current.Server.UrlDecode(System.Web.HttpContext.Current.Request.Form.ToString());
				}
				
				

				UserInfo userInfo = new UserInfo();
				List<UserModuleFunctionModel> UserMonduleFunction = new List<UserModuleFunctionModel>();
				JObject jobt = JObject.Parse(requestData);
				
				tbusermaster user = new tbusermaster();

				string liEmailId = string.Empty;
				string liPassword = string.Empty;
				bool lbSuccess;
				int liPlatfrom = 0;
				int liphoneNo = 0;
				var EncryptionPassword = string.Empty;

				if (jobt["txtEmailID"] != null && jobt["txtEmailID"].ToString() != string.Empty)
				{
					lbSuccess = int.TryParse(jobt["txtEmailID"].ToString(), out liphoneNo);
					if (lbSuccess == true)
					{
						lbSuccess = false;
					}
					else
					{
						liEmailId = jobt["txtEmailID"].ToString();
					}

				}
				else
				{
					moResponse = RespMsgFormatter.formatResp(0,getMessage("EmailID"), null, "LogIn");
					return Json(moResponse, JsonRequestBehavior.AllowGet);
				}


				if (jobt["txtPassword"] != null && jobt["txtPassword"].ToString() != string.Empty)
				{
					liPassword = jobt["txtPassword"].ToString();
				   // EncryptionPassword = Encrypt(liPassword);
				}
				else
				{
					moResponse = RespMsgFormatter.formatResp(0,getMessage("Password"), null, "LogIn");
					return Json(moResponse, JsonRequestBehavior.AllowGet);
				}

				if (jobt["TxtPlatfrom"] != null && jobt["TxtPlatfrom"].ToString() != string.Empty)
				{
					liPlatfrom = int.Parse(jobt["TxtPlatfrom"].ToString());
				}


				if (liPlatfrom == 0)
				{
					var result = db.tbusermasters.Where(s => s.fdUserEmail == liEmailId && s.fdUserPassword== liPassword && s.fdIsActive==1).Select(d => d.fdUserId).FirstOrDefault();

					if (result > 0)
					{

						var GetDocCountQuery = "select count(1) as TotalDocument from tbdocuments where 1=1 ";
											 
						var lastLogedIndate = db.tbusermasters.Where(s => s.fdUserId == result).FirstOrDefault();
						MySqlParameter logedDtPmtr = new MySqlParameter("@Date","");
						if (lastLogedIndate.fdLastLoggedInDate!=null)
						{
							logedDtPmtr = new MySqlParameter("@Date", lastLogedIndate.fdLastLoggedInDate);
							GetDocCountQuery = GetDocCountQuery + " and day(fdCreatedOn)>=day(@Date) and  month(fdCreatedOn)>=month(@Date) and year(fdCreatedOn)>=year(@Date)";
						}

						var getDocCountResult = db.Database.SqlQuery<getdoccount>(GetDocCountQuery, logedDtPmtr).FirstOrDefault();


						var liValidation = db.tbusermasters.Where(s => s.fdUserEmail == liEmailId && s.fdIsActive==1).FirstOrDefault();
						userInfo.userRoles = moLocModuleFunction.getAllUserRoleFunctionLocation(liValidation.fdUserId, 1);
						var x= moLocModuleFunction.getAllUserRoleFunctionLocation(liValidation.fdUserId, 1);
						if(x.Count()==0)
						{
							
							moResponse = RespMsgFormatter.formatResp(2, getMessage("CheackRole"), null, "LogIn");
							return Json(moResponse, JsonRequestBehavior.AllowGet);
						}
						userInfo.userId = liValidation.fdUserId;
						userInfo.userName = liValidation.fdUserName;
						userInfo.lastLoggedInDate = liValidation.fdLastLoggedInDate;
						userInfo.email = liValidation.fdUserEmail;
						setLoggedInUserDetails(userInfo);

						FormsAuthentication.SetAuthCookie(userInfo.email, userInfo.rememberme);
						moLocModuleFunction.getUserAssociatedRoleDetails(userInfo);
						liValidation.fdLastLoggedInDate = DateTime.Now;
						db.Entry(liValidation).State = System.Data.Entity.EntityState.Modified;
						db.SaveChanges();
						Dictionary<string, object> objResponce = new Dictionary<string, object>();
						objResponce.Add("UserId", result);
						objResponce.Add("DocCount", getDocCountResult.TotalDocument);
						moResponse = RespMsgFormatter.formatResp(1, getMessage("Login"), objResponce, "LogIn");
						// return RedirectToAction("HomePageLogedIn", "HomePage");
						//return Json(new { newUrl = Url.Action("HomePageLogedIn", "HomePage") });

					}
					else
					{
						moResponse = RespMsgFormatter.formatResp(0, getMessage("NotMatch"), null, "LogIn");
					}
				}
				else if (liPlatfrom == 1)
				{
					var result = 0;
					if (liphoneNo != 0)
					{

						result = db.tbusermasters.Where(s => s.fdUserContactNo == liphoneNo && s.fdUserPassword == liPassword && s.fdIsActive==1).Select(d => d.fdUserId).FirstOrDefault();

					}
					else
					{
						result = db.tbusermasters.Where(s => s.fdUserEmail == liEmailId && s.fdUserPassword == liPassword && s.fdIsActive==1).Select(d => d.fdUserId).FirstOrDefault();

					}


					if (result > 0)
					{

						var liValidation = db.tbusermasters.Where(s => s.fdUserEmail == liEmailId &&
										  s.fdUserPassword == liPassword).FirstOrDefault();
						userInfo.userRoles = moLocModuleFunction.getAllUserRoleFunctionLocation(liValidation.fdUserId, 1);
						userInfo.userId = liValidation.fdUserId;
						userInfo.userName = liValidation.fdUserName;
						userInfo.lastLoggedInDate = liValidation.fdLastLoggedInDate;
						userInfo.email = liValidation.fdUserEmail;
						setLoggedInUserDetails(userInfo);
						//FormsAuthentication.SetAuthCookie(liEmailId,);
						moLocModuleFunction.getUserAssociatedRoleDetails(userInfo);
						liValidation.fdLastLoggedInDate = DateTime.Now;
						db.Entry(liValidation).State = System.Data.Entity.EntityState.Modified;
						db.SaveChanges();
						byte[] time = BitConverter.GetBytes(DateTime.UtcNow.ToBinary());
						byte[] key = Guid.NewGuid().ToByteArray();
						string token = Convert.ToBase64String(time.Concat(key).ToArray());
						token = token.Replace("+", "").Trim();
						// updating last login
						liValidation.fdLastLoggedInDate = DateTime.Now;
						db.Entry(liValidation).State = System.Data.Entity.EntityState.Modified;


						// updating user token
						liValidation.fdUserToken = token;
						db.Entry(liValidation).State = System.Data.Entity.EntityState.Modified;
						db.SaveChanges();
						Dictionary<string, object> ldResult = new Dictionary<string, object>();

						ldResult.Add("ut", token);
						ldResult.Add("UserName", liValidation.fdUserName);
						ldResult.Add("ContactNO", liValidation.fdUserContactNo);
						moResponse = RespMsgFormatter.formatResp(1, getMessage("Login"), ldResult, "login");
						//		return Json(moResponse, JsonRequestBehavior.AllowGet);



					}
					else
					{
						moResponse = RespMsgFormatter.formatResp(0, getMessage("NotMatch"), null, "LogIn");
					}

				}
			}
			catch (Exception ex)
			{
				System.Diagnostics.Debug.Write("MSG: " + ex.Message + "StackTrace: " + ex.StackTrace + "Inner Exception: " + ex.InnerException);
				moResponse = RespMsgFormatter.formatResp(0, "MAG" +ex.Message +"Inner Exception:" + ex.InnerException +"Stack Trace:"+ex.StackTrace , null, "LogIn");

			}
			Response.AppendHeader("Access-Control-Allow-Origin", "*");
			return Json(moResponse, JsonRequestBehavior.AllowGet);
			// return RedirectToAction("Index", "Dashboard");
		}

		[HttpPost]
		public ActionResult CreateUsers(string requestData)
		{
			DbContextTransaction transection = null;
			try
			{
				requestData = Request.Form["ObjJson"];
				int liUserID = 0; var lsPassword = ""; bool lbUpdatePass = false;
				string getModulefunction = string.Empty;
				JObject jobj = JObject.Parse(requestData);
				liUserID = Convert.ToInt32(jobj["fdUserId"].ToString());

				if (liUserID == 0)
				{
					getModulefunction = "CREATE";
				}
				else
				{
					getModulefunction = "UPDATE";
				}

				if (CanAccess("USERMAS", getModulefunction, requestData) == true)
				{

                    transection = db.Database.BeginTransaction();


                    if (requestData != null)
                    {


                        tbusermaster loUsersData = new tbusermaster();
                        var userID = getUserId();


                        var EmailID = (jobj["fdUserEmail"].ToString()).ToUpper();
                        if (jobj["fdUserPassword"] != null && jobj["fdUserPassword"].ToString() != string.Empty)
                        {
                            lsPassword = jobj["fdUserPassword"].ToString();
                            lbUpdatePass = true;
                        }


                        var lvValidateEmail = db.tbusermasters.Where(s => s.fdUserEmail.ToUpper() == EmailID && s.fdIsActive == 1).FirstOrDefault();
                        if (lvValidateEmail != null && Convert.ToInt32(jobj["fdUserId"].ToString()) == 0)
                        {
                            moResponse = RespMsgFormatter.formatResp(0, "Email-Id is already used", null, "CreateUsers");
                            return Json(moResponse, JsonRequestBehavior.AllowGet);
                        }
                        loUsersData.fdUserName = jobj["fdUserName"].ToString();
                        loUsersData.fdUserEmail = jobj["fdUserEmail"].ToString();
                        loUsersData.fdUserPassword = jobj["fdUserPassword"].ToString();
                        loUsersData.fdUserContactNo = Convert.ToInt64(jobj["fdUserContactNo"].ToString());
                        loUsersData.fdUserCity = jobj["fdUserCity"].ToString();
                        loUsersData.fdUserState = jobj["fdUserState"].ToString();
                        loUsersData.fdUserAddress = jobj["fdUserAddress"].ToString();
                        loUsersData.fdDepartment = 1;
                        loUsersData.fdIsActive = Convert.ToInt32(jobj["fdIsActive"]);
                        //db.SaveChanges();
                        if (liUserID != 0)
                        {
                            var getUser = db.tbusermasters.Where(s => s.fdUserEmail.ToUpper() == EmailID).FirstOrDefault();

                            getUser.fdUserContactNo = Convert.ToInt64(jobj["fdUserContactNo"].ToString());
                            getUser.fdUserName = jobj["fdUserName"].ToString();
                            getUser.fdUserAddress = jobj["fdUserAddress"].ToString();
                            getUser.fdIsActive = Convert.ToInt32(jobj["fdIsActive"].ToString());
                            if (lbUpdatePass == true)
                            {
                                getUser.fdUserPassword = jobj["fdUserPassword"].ToString();
                            }
                            db.Entry(getUser).State = System.Data.Entity.EntityState.Modified;
                            db.SaveChanges();
                            moResponse = RespMsgFormatter.formatResp(1, "Details Successfully Updated", null, "CreateUsers");
                            transection.Commit();
                        }
                        else
                        {
                            db.tbusermasters.Add(loUsersData);
                            db.SaveChanges();

                            tbuserroleloclink liroleAdd = new tbuserroleloclink();
                            liroleAdd.fdRoleId = 1083;
                            liroleAdd.fdUserId = loUsersData.fdUserId;
                            liroleAdd.fdCreatedBy = liUserID;
                            liroleAdd.fdLocationId = 1;
                            liroleAdd.fdCreatedOn = DateTime.Now;
                            db.tbuserroleloclinks.Add(liroleAdd);
                            db.SaveChanges();
                            moResponse = RespMsgFormatter.formatResp(1, "Details Successfully Saved", null, "CreateUsers");
                            transection.Commit();
                        }



                    }

                    else
                    {
                        moResponse = RespMsgFormatter.formatResp(0, "No Input Given!", null, "CreateUsers");
                        return Json(moResponse, JsonRequestBehavior.AllowGet);

                    }
                }
				else
				{
					moResponse = RespMsgFormatter.formatResp(0, getMessage("Access"), null, "CreateUsers");

				}
				

			}
			catch (Exception ex)
			{
				System.Diagnostics.Debug.Write("MSG: " + ex.Message + "StackTrace: " + ex.StackTrace + "Inner Exception: " + ex.InnerException);
				moResponse = RespMsgFormatter.formatResp(0, "MAG" + ex.Message + "Inner Exception:" + ex.InnerException + "Stack Trace:" + ex.StackTrace, null, "CreateUsers");
				transection.Rollback();
			}
			return Json(moResponse, JsonRequestBehavior.AllowGet);
		}

	}
}