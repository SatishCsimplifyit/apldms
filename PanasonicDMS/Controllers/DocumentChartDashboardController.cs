﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Highsoft.Web.Mvc.Charts;
using PanasonicDMS.Models.DbContext;
using System.Data.Entity;
using PanasonicDMS.App_Code;
using PanasonicDMS.Models;
using Newtonsoft.Json.Linq;
using System.IO;
using System.Threading.Tasks;
using System.Data.Entity.Core.Objects;
using System.Text;
using MySql.Data.MySqlClient;
using PanasonicDMS.Controllers;
using System.Data.SqlClient;

namespace PannasonicDMS.Controllers
{
    public class DocumentChartDashboardController : CommonController
    {
        private panasonicdmsEntities db = new panasonicdmsEntities();
        // GET: DocumentChartDashboard
        private Dictionary<string, object> moResponse;
        public ActionResult Index()
        {

            //    var DATE = DateTime.Now.ToString("yyyy-MM-dd");
            //    DateTime newDate = DateTime.Now.AddDays(-30);
            //    //var LastDate = DateTime.Now.ToString("yyyyMMdd") + "")-60;
            //    var LastDate = newDate.ToString("yyyy-MM-dd");
            //    //string TEMPLASTDate = "'2019-01-01'";
            //    int userid = getUserId();
            //    string lsDocumentDetails = "SELECT DATE_FORMAT(dt.date, '%d-%b') as date,count(doc.fdDocId) as DocNumber FROM pannasonicdms.tbdocuments doc right join dim_date dt on dt.date = date(doc.fdcreatedOn) and doc.fdCreatedBy = " + userid + " where dt.date between'" + LastDate +  "'and '" + DATE + "' group by dt.date";
            //    var dbReult = db.Database.SqlQuery<DMSClassChart>(lsDocumentDetails).ToList();
            //    List<ColumnSeriesData> DocAddedData = new List<ColumnSeriesData>();
            //    foreach (var ResultData in dbReult)
            //    {
            //        DocAddedData.Add(new ColumnSeriesData { Name = ResultData.date, Y = ResultData.DocNumber });
            //    }

            //    ViewData["DocAddedData"] = DocAddedData;
            int userid = getUserId();
            if (userid == 0)
            {
                TempData["msg"] = "<script>showMessage('red', 'The session is expired, please login again'); </script>";
                return View("Login");
            }
            if (CanAccess("USAGESDASH", "SEARCH", "User Log In ID" + userid.ToString()) == true)
            {
                return View();
            }
            else
            {
                return View();
            }
        }

        public ActionResult Chart_User_access(string requestData)
{
            try
            {
                int userid = getUserId();
                if (userid == 0)
                {
                    return View("Login");
                }
                if (CanAccess("USAGESDASH", "SEARCH", requestData) == true)
                {
                    //  var Message = getMessage("ErrorMessage");

                    var DATE = DateTime.Now.ToString("yyyy-MM-dd");
                    DateTime newDate = DateTime.Now.AddDays(-7);
                    // var LastDate = DateTime.Now.ToString("yyyyMMdd") + "")-60;
                    var LastDate = newDate.ToString("yyyy-MM-dd");
                    //string TEMPLASTDate = "'2019-01-01'";

                    string lsDocumentDetails = " SELECT DATE_FORMAT(dt.date,  '%d-%M') as date,DATE_FORMAT(dt.date, '%Y-%M-%d') as Fulldate,count(doc.fdDocId) as DocNumber FROM tbdocuments doc right join dim_date dt on day(dt.date) = day(doc.fdcreatedOn) and month(dt.date) = month(doc.fdcreatedOn) and year(dt.date) = year(doc.fdcreatedOn)  where 1=1 and  dt.date between'" + LastDate + "'and '" + DATE + "' group by dt.date ";
                    //"SELECT DATE_FORMAT(dt.date, '%d-%b') as date,DATE_FORMAT(dt.date, '%Y-%m-%d %h:%m:%s') as Fulldate, count(doc.fdDocId) as DocNumber FROM tbdocuments doc right join dim_date dt on dt.date = date(doc.fdcreatedOn)  where dt.date between'" + LastDate + "'and '" + DATE + "' group by dt.date";
                    var dbReult = db.Database.SqlQuery<DMSClassChart>(lsDocumentDetails).ToList();
                    List<ColumnSeriesData> DocAddedData = new List<ColumnSeriesData>();
                    foreach (var ResultData in dbReult)
                    {
                        DocAddedData.Add(new ColumnSeriesData { Name = ResultData.date, Y = ResultData.DocNumber, Description = ResultData.Fulldate });
                    }

                    //   ViewData["DocAddedData"] = DocAddedData;

                    // return Json(DocAddedData);
                    moResponse = RespMsgFormatter.formatResp(1, getMessage("Chart"), DocAddedData, "saveUserMasterData");
                }
                else
                {
                    moResponse = RespMsgFormatter.formatResp(0, getMessage("Access"), null, "saveUserMasterData");
                }
            }
            catch(Exception ex)
            {
                moResponse = RespMsgFormatter.formatResp(0, ex.InnerException, null, "saveUserMasterData");
            }
            return Json(moResponse, JsonRequestBehavior.AllowGet);
        }



        public ActionResult Chart_User_access_audit(string requestData)
        {
            try
            {

                int userid = getUserId();
                if (userid == 0)
                {
                    return View("Login");
                }
                if (CanAccess("USAGESDASH", "SEARCH", requestData) == true)
                {
                    var DATE = DateTime.Now.ToString("yyyy-MM-dd");
                    DateTime newDate = DateTime.Now.AddDays(-30);
                    //var LastDate = DateTime.Now.ToString("yyyyMMdd") + "")-60;
                    var LastDate = newDate.ToString("yyyy-MM-dd");
                    //string TEMPLASTDate = "'2019-01-01'";

                    string lsDocumentDetails = "SELECT DATE_FORMAT(dt.date, '%d-%M') as date,DATE_FORMAT(dt.date, '%Y-%M-%d') as Fulldate,count(ua.actionName) as DocNumber FROM user_action_audit ua right join dim_date dt on day(dt.date) = day(ua.createdOn) and month(dt.date) = month(ua.createdOn) and year(dt.date) = year(ua.createdOn) and ua.actionName = 'VIEW' and ua.actionType LIKE '%DC%' and ua.createdBy = " + userid + " and (ua.documentId != '0' or ua.documentId is not null) where dt.date between '" + LastDate + "'  and '" + DATE + "' group by dt.date ";
                    //string lsDocumentDetails = "SELECT DATE_FORMAT(dt.date, '%d-%b') as date,count(doc.fdDocId) as DocNumber FROM pannasonicdms.tbdocuments doc right join dim_date dt on dt.date = date(doc.fdcreatedOn) and doc.fdCreatedBy = " + userid + " where dt.date between '2019-01-01'  and '" + DATE + "' group by dt.date";
                    var dbReult = db.Database.SqlQuery<DMSClassChart>(lsDocumentDetails).ToList();
                    List<ColumnSeriesData> DocAddedData = new List<ColumnSeriesData>();
                    foreach (var ResultData in dbReult)
                    {
                        DocAddedData.Add(new ColumnSeriesData { Name = ResultData.date, Y = ResultData.DocNumber, Description = ResultData.Fulldate });
                    }

                    //ViewData["DocAddedData"] = DocAddedData; 

                    // return Json(DocAddedData);
                    moResponse = RespMsgFormatter.formatResp(1, getMessage("Chart"), DocAddedData, "saveUserMasterData");
                }
                else
                {
                    moResponse = RespMsgFormatter.formatResp(0, getMessage("Access"), null, "saveUserMasterData");
                }
            }
            catch(Exception ex)
            {
                moResponse = RespMsgFormatter.formatResp(0, ex.InnerException, null, "saveUserMasterData");
            }
            return Json (moResponse, JsonRequestBehavior.AllowGet);
        }

        public ActionResult DocUpload_PieBasic()
        {
            try
            {
                int userid = getUserId();
                if (userid == 0)
                {
                    return View("Login");
                }
                if (CanAccess("USAGESDASH", "SEARCH", "") == true)
                {
                    var DATE = DateTime.Now.ToString("yyyy-MM-dd");
                    DateTime newDate = DateTime.Now.AddDays(-30);
                    var LastDate = newDate.ToString("yyyy-MM-dd");
                    string docUploadPieData = "SELECT fdDocType, CLU.fdDescription as DocumentTypeName, count(doc.fdDocId) as DocNumber FROM tbdocuments doc right join tbcodelkup CLU on CLU.fdKeyCode = doc.fdDocType and fdLkUpCode = 'DOC_CLASS_TYPE' where day(doc.fdcreatedOn) between day('" + LastDate + "')  and  day('" + DATE + "') group by doc.fdDocType,CLU.fdDescription";
                    //string lsDocumentDetails = "SELECT DATE_FORMAT(dt.date, '%d-%b') as date,count(ua.actionName) as DocNumber FROM user_action_audit ua right join dim_date dt on dt.date = date(ua.createdOn) and ua.actionName = 'VIEW' and ua.actionType LIKE '%DC%' and ua.createdBy = " + userid + " and ua.documentId != '0' or null where dt.date between '" + LastDate + "'  and '" + DATE + "' group by dt.date; ";
                    var dbReult = db.Database.SqlQuery<DMSClassPieChart>(docUploadPieData).ToList();

                    List<PieSeriesData> pieData = new List<PieSeriesData>();

                    foreach (var ResultData in dbReult)
                    {
                        //DocAddedData.Add(new ColumnSeriesData { Name = ResultData.date, Y = ResultData.DocNumber });
                        pieData.Add(new PieSeriesData { Name = ResultData.DocumentTypeName, Y = ResultData.DocNumber });
                    }

                    //ViewData["DocAddedData"] = DocAddedData; 

                    // return Json(DocAddedData);
                    moResponse = RespMsgFormatter.formatResp(1, getMessage("Chart"), pieData, "saveUserMasterData");
                }
                else
                {
                    moResponse = RespMsgFormatter.formatResp(0, getMessage("Access"), null, "saveUserMasterData");
                }
            }
            catch(Exception ex)
            {
                moResponse = RespMsgFormatter.formatResp(0, ex.InnerException, null, "saveUserMasterData");
            }
            return Json(moResponse, JsonRequestBehavior.AllowGet);

            //pieData.Add(new PieSeriesData { Name = "FireFox", Y = 45.0 });
            //pieData.Add(new PieSeriesData { Name = "IE", Y = 26.8 });
            //pieData.Add(new PieSeriesData { Name = "Chrome", Y = 12.8, Sliced = true, Selected = true });
            //pieData.Add(new PieSeriesData { Name = "Safari", Y = 8.5 });
            //pieData.Add(new PieSeriesData { Name = "Opera", Y = 6.2 });
            //pieData.Add(new PieSeriesData { Name = "Others", Y = 0.7 });

            //ViewData["pieData"] = pieData;

            //return View();
        }



        public ActionResult getuploadedDocument(string requestData)
        {
            try
            {
                int userid = getUserId();
                if (userid == 0)
                {
                    return View("Login");
                }
                if (CanAccess("USAGESDASH", "DOC_ACCESS_REPORT", requestData) == true)
                {
                    if (requestData != null && requestData != "")
                    {
                        DateTime dimdate = new DateTime();
                        //  dimdate = DateTime.Parse(requestData);
                        MySqlParameter lidatePmtr = new MySqlParameter("@dimdate", requestData);

                        var getUploadedDocument = " select dc.fdDocId as fdDocId,ua.auditId as auditId,ua.actionName as actionName, um.fdUserName as fdUserName, "
                                                + " date_format(dc.fdCreatedOn,'%d-%M-%Y %H:%i:%S') as createdOn, dc.fdDocTitle as fdDocTitle, md.fdModuleName as actionType,clk.fdKey1 as DocumentClass from user_action_audit as ua  "
                                                + " join tbusermaster um on um.fdUserId = ua.createdBy "
                                                + " join tbdocuments dc on dc.fdDocId=ua.documentId "
                                                + " join tbcodelkup clk on clk.fdKeyCode = dc.fdDocType and clk.fdLkUpCode = 'DOC_CLASS_TYPE' "
                                                + " join tbmodulemaster md on md.fdModuleCode=ua.actionType "
                                                + " where ua.actionName = 'VIEW' and  dc.fdStatus!=-1 and ua.createdBy=" + userid + " and ua.actionType LIKE '%DC%' and(ua.documentId != '0' or ua.documentId is not null) and day(ua.createdOn) = day(STR_TO_DATE(@dimdate,'%Y-%M-%d')) and month(ua.createdOn) = month(STR_TO_DATE(@dimdate,'%Y-%M-%d')) and  year(ua.createdOn) = year(STR_TO_DATE(@dimdate,'%Y-%M-%d')) ";
                        object[] parameters = new object[] { lidatePmtr };
                        var getresult = db.Database.SqlQuery<uploadeddocument>(getUploadedDocument, parameters).ToList();
                        if (getresult.Count > 0)
                        {
                            moResponse = RespMsgFormatter.formatResp(1, getMessage("Success"), getresult, "getuploadedDocument");
                            return Json(moResponse, JsonRequestBehavior.AllowGet);
                        }
                        else
                        {
                            moResponse = RespMsgFormatter.formatResp(0, getMessage("GetNoResult"), null, "getuploadedDocument");
                            return Json(moResponse, JsonRequestBehavior.AllowGet);
                        }

                    }
                    else
                    {

                        moResponse = RespMsgFormatter.formatResp(0, getMessage("DocSelection"), null, "getuploadedDocument");
                        return Json(moResponse, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    moResponse = RespMsgFormatter.formatResp(0, getMessage("Access"), null, "getuploadedDocument");
                }
            }
            catch(Exception ex)
            {
                moResponse = RespMsgFormatter.formatResp(0, ex.InnerException, null, "getuploadedDocument");
                return Json(moResponse, JsonRequestBehavior.AllowGet);
            }
           return Json(moResponse, JsonRequestBehavior.AllowGet); ;
        }


        public ActionResult getDocumentCalss(string requestData)
        {
            try
            {
                int userid = getUserId();
                if (userid == 0)
                {
                    return View("Login");
                }
                if (CanAccess("USAGESDASH", "UPLOAD_DOC_REPORT", requestData) == true)
                {
                    if (requestData != null && requestData != "")
                {
                    DateTime dimdate = new DateTime();
                    //  dimdate = DateTime.Parse(requestData);
                    MySqlParameter lidatePmtr = new MySqlParameter("@dimdate", requestData);

                    var getDocument = " select dc.fdDocId as fdDocId,dc.fdDocTitle as fdDocTitle,clk.fdKey1 as ClassName,date_format(dc.fdCreatedOn,'%d-%M-%Y %H:%i:%S') as CreatedOn,um.fdUserName as fdUserName,clksts.fdKey1 as DocStatus,ifnull(dc.fdVersion,1) as DocVersion  from tbdocuments dc "
                                     + " join tbcodelkup clk on clk.fdKeyCode = dc.fdDocType and clk.fdLkUpCode = 'DOC_CLASS_TYPE' "
                                     + " join tbusermaster um on um.fdUserId = dc.fdCreatedBy "
                                     + " join tbcodelkup clksts on clksts.fdKeyCode=dc.fdStatus and clksts.fdLkUpCode='DOC_STATUS'"
                                     + " where day(dc.fdCreatedOn) = day(STR_TO_DATE(@dimdate,'%Y-%M-%d')) and month(dc.fdCreatedOn) = month(STR_TO_DATE(@dimdate,'%Y-%M-%d')) and year(dc.fdCreatedOn) = year(STR_TO_DATE(@dimdate,'%Y-%M-%d'))";
                    object[] parameters = new object[] { lidatePmtr };
                    var getresult = db.Database.SqlQuery<GetDocument>(getDocument, parameters).ToList();
                    if (getresult.Count > 0)
                    {
                        moResponse = RespMsgFormatter.formatResp(1, getMessage("Success"), getresult, "getDocumentCalss");
                        return Json(moResponse, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        moResponse = RespMsgFormatter.formatResp(0, getMessage("GetNoResult"), null, "getDocumentCalss");
                        return Json(moResponse, JsonRequestBehavior.AllowGet);
                    }

                }
                else
                {

                    moResponse = RespMsgFormatter.formatResp(0, getMessage("DocSelection"), null, "getDocumentCalss");
                    return Json(moResponse, JsonRequestBehavior.AllowGet);
                }
                }
                else
                {
                    moResponse = RespMsgFormatter.formatResp(0, getMessage("Access"), null, "getuploadedDocument");
                }
            }
            catch (Exception ex)
            {
                moResponse = RespMsgFormatter.formatResp(0, ex.InnerException, null, "getDocumentCalss");
                return Json(moResponse, JsonRequestBehavior.AllowGet);
            }
            return Json(moResponse, JsonRequestBehavior.AllowGet); 
        }


        public ActionResult getTotalUploadedDocumentWithClass()
        {
            try
            {
                int userid = getUserId();
                if (userid == 0)
                {
                    return View("Login");
                }
                if (CanAccess("USAGESDASH", "SEARCH", "") == true)
                {
                    var getDocument = "select clk.fdKey1 as documentClassName,count(1) as TotalCount from tbdocuments dc  "
                               + "  join tbcodelkup clk on clk.fdKeyCode = dc.fdDocType and clk.fdLkUpCode = 'DOC_CLASS_TYPE' "
                               + " where 1 = 1  group by dc.fdDocType,clk.fdKey1 order by dc.fdDocType desc  limit 5";
                    var result = db.Database.SqlQuery<getDocumentClassCount>(getDocument).ToList();
                    if (result.Count > 0)
                    {
                        List<ColumnSeriesData> DocAddedData = new List<ColumnSeriesData>();
                        foreach (var ResultData in result)
                        {
                            DocAddedData.Add(new ColumnSeriesData { Name = ResultData.documentClassName, Y = ResultData.TotalCount });

                        }
                        moResponse = RespMsgFormatter.formatResp(1, getMessage("Success"), DocAddedData, "getTotalUploadedDocumentWithClass");
                        return Json(moResponse, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        moResponse = RespMsgFormatter.formatResp(0, getMessage("GetNoResult"), null, "getTotalUploadedDocumentWithClass");
                        return Json(moResponse, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    moResponse = RespMsgFormatter.formatResp(0, getMessage("Access"), null, "getTotalUploadedDocumentWithClass");
                    return Json(moResponse, JsonRequestBehavior.AllowGet);
                }
                
            }
            catch (Exception ex)
            {
                moResponse = RespMsgFormatter.formatResp(0, ex.InnerException, null, "getTotalUploadedDocumentWithClass");
                return Json(moResponse, JsonRequestBehavior.AllowGet);
            }

           return Json(moResponse, JsonRequestBehavior.AllowGet);
        }


    }
}