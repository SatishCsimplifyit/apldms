﻿

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PanasonicDMS.App_Code;
using PanasonicDMS.Models;
using Newtonsoft.Json.Linq;
using System.IO;
using System.Data.Entity;
using System.Threading.Tasks;
using System.Data.Entity.Core.Objects;
using System.Text;
using PanasonicDMS.Models.DbContext;
using MySql.Data.MySqlClient;
using System.Data.SqlClient;
using System.Globalization;

namespace PanasonicDMS.Controllers
{
    public class DocumentClassController : CommonController
    {
        private Dictionary<string, object> moResponse;
        private panasonicdmsEntities db = new panasonicdmsEntities();
        // GET: DocumentClass
        public ActionResult Index()
        {
            int userid = getUserId();
            if (userid == 0)
            {
                TempData["msg"] = "<script>showMessage('red', 'The session is expired, please login again'); </script>";

                return View("Login");
            }
            if (CanAccess("DOC_CLASS", "SEARCH", "User Log In ID"+ userid.ToString()) == true)
            {
                return View("DocumentClass");
            }
            else
            {
                return View("Login");
            }
        }
        public ActionResult SaveDocumentClass(string requestData)
        {
            DbContextTransaction transaction = null;
            try
            {

                int userid = getUserId();
                if (userid == 0)
                {
                    moResponse = RespMsgFormatter.formatResp(0, getMessage("SessionTimeOut"), null, "SaveDocumentClass");
                    return Json(moResponse, JsonRequestBehavior.AllowGet);
                }
               
                   
                    List<tbdocumentclass> llDocClass = new List<tbdocumentclass>();
                    tbcodelkup loCodeLkUp = new tbcodelkup();
                    JObject loRequestData = JObject.Parse(requestData);
                    int DocClassID = 0;
                    string className = string.Empty;
                    string getModulefunction = string.Empty;

                if (loRequestData["txtParadoxClassID"] != null && loRequestData["txtParadoxClassID"].ToString() != "")
                    {
                        DocClassID = int.Parse(loRequestData["txtParadoxClassID"].ToString());
                    }


                if (DocClassID == 0)
                {
                    getModulefunction = "CREATE";
                }
                else
                {
                    getModulefunction = "UPDATE";
                }

                if (CanAccess("DOC_CLASS", getModulefunction, requestData) == true)
                {
                    if (DocClassID == 0)
                    {

                        if (loRequestData["txtParadoxClassName"] != null && loRequestData["txtParadoxClassName"].ToString() != "")
                        {
                            className = loRequestData["txtParadoxClassName"].ToString();
                            var validateDistinctClassName = db.tbcodelkups.Where(s => s.fdLkUpCode == "DOC_CLASS_TYPE" && s.fdKey1 == className).Select(s => s.fdKeyCode).FirstOrDefault();
                            if (validateDistinctClassName != 0)
                            {
                                moResponse = RespMsgFormatter.formatResp(0, getMessage("Duplicate"), null, "SaveDocumentClass");
                                return Json(moResponse, JsonRequestBehavior.AllowGet);
                            }
                            int? LastDocClassId=0;
                            var result = db.tbcodelkups.Where(s => s.fdLkUpCode == "DOC_CLASS_TYPE").Select(s => s.fdKeyCode).ToList();
                            if(result.Count>0)
                            {
                                LastDocClassId = result.Max();
                            }
                           // int? LastDocClassId = db.tbcodelkups.Where(s => s.fdLkUpCode == "DOC_CLASS_TYPE").Select(s => s.fdKeyCode).Max();
                            loCodeLkUp.fdLkUpCode = "DOC_CLASS_TYPE";
                            loCodeLkUp.fdKeyCode = LastDocClassId == null ? 0 : LastDocClassId.Value + 1;
                            loCodeLkUp.fdDescription = loRequestData["txtParadoxClassName"].ToString();
                            loCodeLkUp.fdKey1 = loRequestData["txtParadoxClassName"].ToString();
                            loCodeLkUp.fdCreatedOn = DateTime.Now;
                            loCodeLkUp.fdCreatedBy = userid;
                            loCodeLkUp.fdModifiedBy = userid;
                            loCodeLkUp.fdModifiedOn = DateTime.Now;
                        }
                        else
                        {
                            moResponse = RespMsgFormatter.formatResp(0, getMessage("ClassName"), null, "SaveDocumentClass");
                            return Json(moResponse, JsonRequestBehavior.AllowGet);
                        }
                    }
                    
                    if (loRequestData["properties"] != null && loRequestData["properties"].ToString() != "")
                    {
                        JArray loProperties = JArray.Parse(loRequestData["properties"].ToString());
                        foreach (var obj in loProperties)
                        {
                            tbdocumentclass loDocColumn = new tbdocumentclass();
                            loDocColumn.Status = 0;
                            if (obj["propertyName"] != null && obj["propertyName"].ToString() != "")
                            {
                                loDocColumn.dcColumnName = obj["propertyName"].ToString();
                            }
                            if (obj["defaultValue"] != null && obj["defaultValue"].ToString() != "")
                            {
                                loDocColumn.defaultValue = obj["defaultValue"].ToString();
                            }
                            if (obj["dataType"] != null && obj["dataType"].ToString() != "")
                            {
                                if (obj["dataType"].ToString() == "1")
                                {
                                    loDocColumn.dcColumnType = "D";
                                }
                                else if (obj["dataType"].ToString() == "2")
                                {
                                    loDocColumn.dcColumnType = "T";
                                }
                                else if (obj["dataType"].ToString() == "3")
                                {
                                    loDocColumn.dcColumnType = "I";
                                }
                                
                                else
                                {
                                    moResponse = RespMsgFormatter.formatResp(0,getMessage("DataType"), null, "SaveDocumentClass");
                                    return Json(moResponse, JsonRequestBehavior.AllowGet);
                                }
                            }
                            if (obj["isRequired"] != null && obj["isRequired"].ToString() != "")
                            {
                                if (obj["isRequired"].ToString() != "No")
                                    loDocColumn.isRequired = 1;
                                else
                                    loDocColumn.isRequired = 0;
                            }
                            if (obj["sequence"] != null && obj["sequence"].ToString() != "")
                            {
                                loDocColumn.sequence = Convert.ToInt32(obj["sequence"].ToString());
                            }
                            loDocColumn.createdBy = userid;
                            loDocColumn.createdOn = DateTime.Now;
                            loDocColumn.updatedBy = userid;
                            loDocColumn.updatedOn = DateTime.Now;
                            loDocColumn.AssSpId = 8; // storage policy name
                            llDocClass.Add(loDocColumn);
                        }
                    }
                    List<tbuserroleloclink> llRoleUserLinkAuthorDepart = new List<tbuserroleloclink>();
                    List<tbuserroleloclink> llRoleUserLinkViewerDepart = new List<tbuserroleloclink>();
                    if (DocClassID == 0)
                    {

                       
                        tbrolemaster loAuthRole = new tbrolemaster();
                        
                        tbrolemaster loViewRole = new tbrolemaster();

                        if (loRequestData["authorDepartments"] != null && loRequestData["authorDepartments"].ToString() != "")
                        {

                            loAuthRole.fdRoleName = loCodeLkUp.fdKey1.Trim() + " Author";
                            loAuthRole.fdDescription = "Author Role For Document Class " + loCodeLkUp.fdKey1.ToUpper();
                            loAuthRole.fdRoleCode = loCodeLkUp.fdKey1.Replace(" ", String.Empty).ToUpper() + "AUTH";
                            loAuthRole.fdCreatedBy = userid;
                            loAuthRole.fdCreatedOn = DateTime.Now;
                            loAuthRole.fdUpdatedBy = userid;
                            loAuthRole.fdStatus = 1;
                            loAuthRole.fdUpdatedOn = DateTime.Now;


                            JArray authorDepartments = JArray.Parse(loRequestData["authorDepartments"].ToString());
                            foreach (int value in authorDepartments)
                            {
                                var loGetAllUsers = db.tbusermasters.Where(s => s.fdDepartment == value).Select(s => s.fdUserId).ToList();
                                foreach (int liUserId in loGetAllUsers)
                                {
                                    tbuserroleloclink loRoleUserLink = new tbuserroleloclink();
                                    loRoleUserLink.fdUserId = liUserId;
                                    loRoleUserLink.fdLocationId = 1;
                                    loRoleUserLink.fdCreatedBy = userid;
                                    loRoleUserLink.fdCreatedOn = DateTime.Now;
                                    llRoleUserLinkAuthorDepart.Add(loRoleUserLink);
                                     
                                }
                            }

                        }

                        if (loRequestData["viewerDepartments"] != null && loRequestData["viewerDepartments"].ToString() != "")
                        {

                            loViewRole.fdRoleName = loCodeLkUp.fdKey1.Trim() + " Viewer";
                            loViewRole.fdDescription = "Viewer Role For Document Class " + loCodeLkUp.fdKey1.ToUpper();
                            loViewRole.fdRoleCode = loCodeLkUp.fdKey1.Replace(" ", String.Empty).ToUpper() + "VIEW";
                            loViewRole.fdCreatedBy = userid;
                            loViewRole.fdStatus = 1;
                            loViewRole.fdCreatedOn = DateTime.Now;
                            loViewRole.fdUpdatedBy = userid;
                            loViewRole.fdUpdatedOn = DateTime.Now;


                            JArray viewerDepartments = JArray.Parse(loRequestData["viewerDepartments"].ToString());
                            foreach (int value in viewerDepartments)
                            {
                                var loGetAllUsers = db.tbusermasters.Where(s => s.fdDepartment == value).Select(s => s.fdUserId).ToList();
                                foreach (int liUserId in loGetAllUsers)
                                {
                                    tbuserroleloclink loRoleUserLink = new tbuserroleloclink();
                                    loRoleUserLink.fdUserId = liUserId;
                                    loRoleUserLink.fdLocationId = 1;
                                    loRoleUserLink.fdCreatedBy = userid;
                                    loRoleUserLink.fdCreatedOn = DateTime.Now;
                                    llRoleUserLinkViewerDepart.Add(loRoleUserLink);
                                   
                                }
                            }
                        }

                        var llFunctionList = db.tbfunctionmasters.Where(s => s.fdFunctionCode == "DRAFT" ||
                        s.fdFunctionCode == "PUBLISH" || s.fdFunctionCode == "EXPIRE" || s.fdFunctionCode == "VIEW" || s.fdFunctionCode== "UPLOAD_DOCUMENT" || s.fdFunctionCode == "UPDATE")
                                            .Select(s => new { s.fdFunctionId, s.fdFunctionCode }).ToList();
                        if (llFunctionList.Count == 0)
                        {
                            moResponse = RespMsgFormatter.formatResp(0, getMessage("Function"), null, "SaveDocumentClass");
                            return Json(moResponse, JsonRequestBehavior.AllowGet);
                        }

                        tbmodulemaster loModule = new tbmodulemaster();

                        loModule.fdModuleCode = "DC" + loCodeLkUp.fdKeyCode;
                        loModule.fdModuleName = "Document Class " + loCodeLkUp.fdKey1;
                        loModule.fdDescription = "Document Class " + loCodeLkUp.fdKey1;
                        loModule.fdCreatedBy = userid;
                        loModule.fdCreatedOn = DateTime.Now;



                        //var liModuleId = db.tbmodulemasters.Where(s => s.fdModuleCode == "DOCUMENT_CLASS").Select(s => s.fdModuleId).FirstOrDefault();
                        //if (liModuleId!=0)
                        //{
                        //    moResponse = RespMsgFormatter.formatResp(0, "No Module Found In DB", null, "SaveDocumentClass");
                        //    return Json(moResponse, JsonRequestBehavior.AllowGet);
                        //}
                        // Saving Data To db

                        transaction = db.Database.BeginTransaction();

                        db.tbcodelkups.Add(loCodeLkUp);
                        DocClassID = loCodeLkUp.fdKeyCode;


                        db.SaveChanges();
                        foreach (var item in llDocClass)
                        {
                            item.associatedDocType = DocClassID;
                        }
                        db.tbdocumentclasses.AddRange(llDocClass);
                        db.SaveChanges();




                        int AuthRoleID = 0;
                        int ViewRoleID = 0;


                        db.tbrolemasters.Add(loAuthRole);
                        db.tbrolemasters.Add(loViewRole);
                        db.SaveChanges();
                        AuthRoleID = loAuthRole.fdRoleId;
                        ViewRoleID = loViewRole.fdRoleId;


                        foreach (var item in llRoleUserLinkAuthorDepart)
                        {
                            item.fdRoleId = AuthRoleID;
                        }

                        db.tbuserroleloclinks.AddRange(llRoleUserLinkAuthorDepart);
                        db.SaveChanges();

                        foreach (var item in llRoleUserLinkViewerDepart)
                        {
                            item.fdRoleId = ViewRoleID;
                        }
                        db.tbuserroleloclinks.AddRange(llRoleUserLinkViewerDepart);
                        db.SaveChanges();

                        db.tbmodulemasters.Add(loModule);
                        db.SaveChanges();

                        // link deparment with doc class id

                        tbdepartment dept = new tbdepartment();

                        if (loRequestData["authorDepartments"] != null && loRequestData["authorDepartments"].ToString() != "")
                        {
                            JArray authorDepartments = JArray.Parse(loRequestData["authorDepartments"].ToString());
                            foreach (int value in authorDepartments)
                            {
                                dept.DocClassID = DocClassID;
                                dept.DepartmentID = value;
                                dept.SecurityType = 1;
                                dept.CreatedBy = userid;
                                dept.CreatedOn = DateTime.Now;
                                dept.Status = 0;
                                db.tbdepartments.Add(dept);
                                db.SaveChanges();
                            }

                            
                        }
                        if (loRequestData["viewerDepartments"] != null && loRequestData["viewerDepartments"].ToString() != "")
                        {
                            JArray viewerDepartments = JArray.Parse(loRequestData["viewerDepartments"].ToString());
                            foreach (int value in viewerDepartments)
                            {
                                dept.DocClassID = DocClassID;
                                dept.DepartmentID = value;
                                dept.SecurityType = 0;
                                dept.CreatedBy = userid;
                                dept.CreatedOn = DateTime.Now;
                                dept.Status = 0;
                                db.tbdepartments.Add(dept);
                                db.SaveChanges();
                            }

                            
                        }

                  


                        List<tbrolemodfunccondlink> llRoleModuleLink = new List<tbrolemodfunccondlink>();
                        foreach (var item in llFunctionList)
                        {
                            tbrolemodfunccondlink loRoleAuthorModuleLink = new tbrolemodfunccondlink();
                            loRoleAuthorModuleLink.fdRoleId = loAuthRole.fdRoleId;
                            loRoleAuthorModuleLink.fdModuleId = loModule.fdModuleId;
                            loRoleAuthorModuleLink.fdFunctionId = item.fdFunctionId;
                            loRoleAuthorModuleLink.fdCreatedBy = userid;
                            loRoleAuthorModuleLink.fdCreatedOn = DateTime.Now;
                            llRoleModuleLink.Add(loRoleAuthorModuleLink);
                            if (item.fdFunctionCode == "VIEW")
                            {
                                tbrolemodfunccondlink loRoleViewModuleLink = new tbrolemodfunccondlink();
                                loRoleViewModuleLink.fdRoleId = loViewRole.fdRoleId;
                                loRoleViewModuleLink.fdModuleId = loModule.fdModuleId;
                                loRoleViewModuleLink.fdFunctionId = item.fdFunctionId;
                                loRoleViewModuleLink.fdCreatedBy = userid;
                                loRoleViewModuleLink.fdCreatedOn = DateTime.Now;
                                llRoleModuleLink.Add(loRoleViewModuleLink);
                                //db.tbrolemodfunccondlinks.AddRange(llRoleModuleLink);
                                //db.SaveChanges();
                            }
                            //db.tbrolemodfunccondlinks.AddRange(llRoleModuleLink);
                            //db.SaveChanges();
                        }
                        db.tbrolemodfunccondlinks.AddRange(llRoleModuleLink);
                        db.SaveChanges();

                       // db.SaveChanges();
                        transaction.Commit();
                        RefreshRoles();
                        moResponse = RespMsgFormatter.formatResp(1, getMessage("SuccessClass"), null, "SaveDocumentClass");
                    }
                    else
                    {
                        var getDoctype = db.tbdocumentclasses.Where(s => s.dcid == DocClassID).Select(d => d.associatedDocType).FirstOrDefault();
                        var getDocClass = db.tbcodelkups.Where(s => s.fdKeyCode == getDoctype).Select(s => s.fdKey1).FirstOrDefault();                        
                        tbdepartment dept = new tbdepartment();

                        if (loRequestData["authorDepartments"] != null && loRequestData["authorDepartments"].ToString() != "")
                        {
                            getDocClass = getDocClass + "AUTH";
                            JArray authorDepartments = JArray.Parse(loRequestData["authorDepartments"].ToString());
                            foreach (int value in authorDepartments)
                            {
                                
                                var ligetRolid = db.tbrolemasters.Where(s => s.fdRoleCode == getDocClass).Select(s=>s.fdRoleId).FirstOrDefault();
                                var loGetAllUsers = db.tbusermasters.Where(s => s.fdDepartment == value).Select(s => s.fdUserId).ToList();
                                
                                
                                foreach (int liUserId in loGetAllUsers)
                                {
                                    tbuserroleloclink loRoleUserLink = new tbuserroleloclink();
                                    loRoleUserLink.fdUserId = liUserId;
                                    loRoleUserLink.fdLocationId = 1;
                                    loRoleUserLink.fdCreatedBy = userid;
                                    loRoleUserLink.fdCreatedOn = DateTime.Now;
                                    loRoleUserLink.fdRoleId = ligetRolid;
                                    llRoleUserLinkAuthorDepart.Add(loRoleUserLink);
                                    


                                }

                                
                            }
                            db.tbuserroleloclinks.AddRange(llRoleUserLinkAuthorDepart);
                            db.SaveChanges();




                            foreach (int values in authorDepartments)
                            {

                                dept.DocClassID = getDoctype;
                                dept.DepartmentID = values;
                                dept.SecurityType = 1;
                                dept.CreatedBy = userid;
                                dept.CreatedOn = DateTime.Now;
                                dept.Status = 0;
                                db.tbdepartments.Add(dept);
                                db.SaveChanges();
                            }
                        }

                        if (loRequestData["viewerDepartments"] != null && loRequestData["viewerDepartments"].ToString() != "")
                        {
                            getDocClass = getDocClass + "VIEW";
                            JArray viewerDepartments = JArray.Parse(loRequestData["viewerDepartments"].ToString());
                            foreach (int value in viewerDepartments)
                            {
                                
                                var ligetRolid = db.tbrolemasters.Where(s => s.fdRoleCode == getDocClass).Select(s => s.fdRoleId).FirstOrDefault();
                                var loGetAllUsers = db.tbusermasters.Where(s => s.fdDepartment == value).Select(s => s.fdUserId).ToList();
                                foreach (int liUserId in loGetAllUsers)
                                {
                                    tbuserroleloclink loRoleUserLink = new tbuserroleloclink();
                                    loRoleUserLink.fdUserId = liUserId;
                                    loRoleUserLink.fdLocationId = 1;
                                    loRoleUserLink.fdCreatedBy = userid;
                                    loRoleUserLink.fdCreatedOn = DateTime.Now;
                                    loRoleUserLink.fdRoleId = ligetRolid;
                                    llRoleUserLinkViewerDepart.Add(loRoleUserLink);
                                    
                                }
                            }
                            db.tbuserroleloclinks.AddRange(llRoleUserLinkViewerDepart);
                            db.SaveChanges();

                            foreach (int value in viewerDepartments)
                            {
                                dept.DocClassID = getDoctype;
                                dept.DepartmentID = value;
                                dept.SecurityType = 0;
                                dept.CreatedBy = userid;
                                dept.CreatedOn = DateTime.Now;
                                dept.Status = 0;
                                db.tbdepartments.Add(dept);
                                db.SaveChanges();
                            }

                        }








                        //var getDoctype = db.tbdocumentclasses.Where(s => s.dcid == DocClassID).Select(d => d.associatedDocType).FirstOrDefault();
                        foreach (var item in llDocClass)
                        {
                            item.associatedDocType = getDoctype;
                        }
                        db.tbdocumentclasses.AddRange(llDocClass);
                        db.SaveChanges();
                        RefreshRoles();
                        moResponse = RespMsgFormatter.formatResp(1, getMessage("UpdateClass"), null, "SaveDocumentClass");
                    }
                }
               else
                    moResponse = RespMsgFormatter.formatResp(0, getMessage("Access"), null, "SaveDocumentClass");

            }
            catch (Exception ex)
            {
                if (transaction != null && transaction.UnderlyingTransaction != null)
                {
                    transaction.Rollback();
                }
                moResponse = RespMsgFormatter.formatResp(0, "Msg:" + ex.Message + "InerException:" + ex.InnerException, null, "SaveDocumentClass");
            }
            return Json(moResponse, JsonRequestBehavior.AllowGet);
        }

        public ActionResult SearchDocumentClass(string requestData)
        {
           try
            {
                int userid = getUserId();
                if (userid == 0)
                {
                    moResponse = RespMsgFormatter.formatResp(0, getMessage("SessionTimeOut"), null, "SaveDocumentClass");
                    return Json(moResponse, JsonRequestBehavior.AllowGet);
                }
                if (CanAccess("DOC_CLASS", "SEARCH", requestData) == true)
                {
                    JObject data = JObject.Parse(requestData);
                    MySqlParameter loClassName = new MySqlParameter("@Classname", "");
                    MySqlParameter loFrom = new MySqlParameter("@fromDate", "");
                    MySqlParameter loTo = new MySqlParameter("@ToDate", "");
                    StringBuilder lsbQuery = new StringBuilder(1000);
                    lsbQuery.Append("select  fdLkUpCode, fdKeyCode, fdKey1,date_format(fdCreatedOn,'%d-%M-%Y') as fdCreatedOn,usr.fdUserName from tbcodelkup lk"
                            + " join tbusermaster usr  on lk.fdCreatedBy = usr.fdUserId where fdlkupcode = 'DOC_CLASS_TYPE'  ");

                    if (data["searchPanadoxClassName"] != null && data["searchPanadoxClassName"].ToString() != string.Empty)
                    {
                        lsbQuery.Append(" and lk.fdKey1 like @Classname");
                        loClassName = new MySqlParameter("@Classname", data["searchPanadoxClassName"].ToString()+"%");
                    }
                    if (data["searchFromDate"] != null && data["searchFromDate"].ToString() != string.Empty)
                    {
                        lsbQuery.Append(" and date(lk.fdCreatedOn)>=STR_TO_DATE(@fromDate,'%d/%m/%Y')");                                                                
                       
                        //var d = (DateTime.ParseExact(data["searchFromDate"].ToString(), "dd/MM/yyyy", CultureInfo.InvariantCulture));
                        loFrom = new MySqlParameter("@fromDate", data["searchFromDate"].ToString());
                    }
                    if (data["searchToDate"] != null && data["searchToDate"].ToString() != string.Empty)
                    {
                        lsbQuery.Append(" and date(lk.fdCreatedOn)<=STR_TO_DATE(@ToDate,'%d/%m/%Y')");
                        //var d = (DateTime.ParseExact(data["searchToDate"].ToString(), "dd/MM/yyyy", CultureInfo.InvariantCulture));
                        loTo = new MySqlParameter("@ToDate", data["searchToDate"].ToString());
                    }
                    var parameter = new Object[] { loFrom, loTo, loClassName };
                    var result = db.Database.SqlQuery<DMSClassSearch>(lsbQuery.Append(" order by fdKeyCode desc limit " + Constant.SEARCH_LIMIT ).ToString(), parameter).ToList();
                    if (result.Count > 0)
                    {
                        moResponse = RespMsgFormatter.formatResp(1, getMessage("Success"), result, "SearchDocumentClass");
                    }
                    else
                    {
                        moResponse = RespMsgFormatter.formatResp(0, getMessage("GetNoResult"), null, "SearchDocumentClass");
                    }
                }
                else
                {
                    moResponse = RespMsgFormatter.formatResp(0, getMessage("Access"), null, "SearchDocumentClass");
                }
            }
            catch(Exception ex)
            {
                moResponse = RespMsgFormatter.formatResp(0, "Msg:" + ex.Message + "InnerException:" + ex.InnerException, "", "SearchDocumentClass");
            }
            return Json(moResponse, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetDocumentClassDetail(string requestData)
        {
            try
            {
                int userid = getUserId();
                if (userid == 0)
                {
                    moResponse = RespMsgFormatter.formatResp(0, getMessage("SessionTimeOut"), null, "SaveDocumentClass");
                    return Json(moResponse, JsonRequestBehavior.AllowGet);
                }
                if (CanAccess("DOC_CLASS", "SEARCH", requestData) == true)
                {
                    int liDocumentId = Convert.ToInt32(requestData);
                    MySqlParameter lodocId = new MySqlParameter("@liDocumentId", liDocumentId);
                    string lsDocumentDetails = "select doccls.dcid as dcid , lkp.fdDescription as DocClassName,lkp.fdKeyCode as DocClassID, coltyp.fdKey1 as DocTypeId, doccls.dcColumnName as ColumnName,doccls.sequence as sequence, "
                                             + " ifnull(coltyp.fdDescription,'') as ColumnType,doccls.isRequired as isRequired,doccls.defaultValue as defaultValue "
                                             + " from tbcodelkup lkp left "
                                             + " join tbdocumentclass doccls on doccls.associatedDocType = lkp.fdKeyCode "
                                             + " left join tbcodelkup coltyp on coltyp.fdKey1=doccls.dcColumnType and coltyp.fdLkUpCode='DOCCLASS_COLMN_TYP' "
                                             // + " left join tbcodelkup coltyp1 on coltyp1.fdKeyCode=lkp.fdKeyCode and coltyp1.fdLkUpCode='DATA_TYPE' "
                                             + " where lkp.fdLkUpCode = 'DOC_CLASS_TYPE' and lkp.fdKeyCode = @liDocumentId and doccls.status =0";

                    string getdepartmentdetails = "select dpt.PKID as PKID,dpt.DepartmentID as DepartmentID,lkp.fdKey1 as DepartmentName,dpt.SecurityType as RightID, "
                                               + " dptright.fdKey1 as Rights from tbdepartment dpt "
                                               + " join tbcodelkup lkp on dpt.DepartmentID = lkp.fdKeyCode  and lkp.fdLkUpCode = 'DEPARTMENT' "
                                               + " join tbcodelkup dptright on dpt.SecurityType = dptright.fdKeyCode  and dptright.fdLkUpCode = 'DEPARMENT_RIGHT' "
                                               + " where dpt.DocClassID=@liDocument and dpt.Status=0";

                    MySqlParameter lodocIdpmtr = new MySqlParameter("@liDocument", liDocumentId);
                    object[] parameters = new object[] { lodocId, lodocIdpmtr };
                    object[] parameter = new object[] { lodocIdpmtr };

                    var dbReult = db.Database.SqlQuery<DMSClassGet>(lsDocumentDetails, parameters).ToList();


                    var dbdepartmentresult = db.Database.SqlQuery<DMSClassDepartmentGet>(getdepartmentdetails, parameters.Select(x => ((ICloneable)x).Clone()).ToArray()).ToList();

                    Dictionary<string, object> ldResult = new Dictionary<string, object>();

                    if (dbReult != null)
                    {
                        ldResult.Add("DocClassDetails", dbReult);
                        ldResult.Add("DepartmentDetails", dbdepartmentresult);
                        moResponse = RespMsgFormatter.formatResp(1, getMessage("Success"), ldResult, "GetDocumentDetail");

                    }
                    else
                    {
                        moResponse = RespMsgFormatter.formatResp(0, getMessage("NoDocDetails"), null, "GetDocumentDetail");
                    }
                }
                else
                {
                    moResponse = RespMsgFormatter.formatResp(0, getMessage("Access"), null, "SearchDocumentClass");
                }

            }
            catch (Exception ex)
            {
                moResponse = RespMsgFormatter.formatResp(0, "message:" + ex.Message + " inner Exception:" + ex.InnerException, null, "GetDocumentDetail");
            }
            return Json(moResponse, JsonRequestBehavior.AllowGet);
        }

        public ActionResult SearchDocumentsUsingSearchBar(string requestData)
        {
            try
            {
                int userid = getUserId();
                if (userid == 0)
                {
                    moResponse = RespMsgFormatter.formatResp(0, getMessage("SessionTimeOut"), null, "SaveDocumentClass");
                    return Json(moResponse, JsonRequestBehavior.AllowGet);
                }
                if (CanAccess("DOC_CLASS", "SEARCH", requestData) == true)
                {
                    JObject jObject = JObject.Parse(requestData);
                    MySqlParameter loName = new MySqlParameter("@searchkeyword", "");
                    string qry =
                        "select fdLkUpCode, fdKeyCode, fdKey1,date_format(fdCreatedOn,'%d-%M-%Y') as fdCreatedOn,usr.fdUserName from tbcodelkup lk"
                        + " join tbusermaster usr  on lk.fdCreatedBy = usr.fdUserId where fdlkupcode = 'DOC_CLASS_TYPE'  ";

                    string value = string.Empty;
                    if (jObject["searchInputKeyword"].ToString() != null && jObject["searchInputKeyword"].ToString() != string.Empty)
                    {
                        qry = qry + "and ( fdKey1 like @searchkeyword or fdUserName like @searchkeyword or date(fdCreatedOn)=STR_TO_DATE(@searchkeyword,'%d-%M-%Y') or  date(fdCreatedOn)=STR_TO_DATE(@searchkeyword,'%d-%m-%Y') )";
                         value = jObject["searchInputKeyword"].ToString();
                        loName = new MySqlParameter("@searchkeyword", value + "%");
                    }

                    //DateTime Searchdt; // "yyyy-MMM-dd", "yyyy-MM-dd"
                    //MySqlParameter lodatepmtr = new MySqlParameter("@searchDate", "");
                    //string[] formats = { "dd-MMM-yyyy", "dd-MM-yyyy" };
                    //if (DateTime.TryParseExact(value, formats, CultureInfo.InvariantCulture, DateTimeStyles.None, out Searchdt))
                    //{
                    //    lodatepmtr = new MySqlParameter("@searchDate", Searchdt);
                    //}

                    qry = qry + " order by fdKeyCode desc";
                    var doclist = db.Database.SqlQuery<DMSClassSearch>(qry, loName).ToList();
                    if (doclist.Count > 0)
                    {
                        moResponse = RespMsgFormatter.formatResp(1, doclist.Count + getMessage("GetResult"), doclist, "searchDocuments");

                    }
                    else
                    {
                        moResponse = RespMsgFormatter.formatResp(0, doclist.Count + getMessage("GetResult"), doclist, "searchDocuments");

                    }
                }
                else
                {
                    moResponse = RespMsgFormatter.formatResp(0, getMessage("Access"), null, "SearchDocumentClass");
                }

                return Json(moResponse, JsonRequestBehavior.AllowGet);
            }
            catch(Exception ex)
            {
                moResponse = RespMsgFormatter.formatResp(0, "message:" + ex.Message + " inner Exception:" + ex.InnerException, null, "SearchDocumentsUsingSearchBar");
            }
            return Json(moResponse, JsonRequestBehavior.AllowGet);
        }


        public ActionResult DeleteProperties(string requestData)
        {
            try
            {
                int userid = getUserId();
                if (userid == 0)
                {
                    moResponse = RespMsgFormatter.formatResp(0, getMessage("SessionTimeOut"), null, "SaveDocumentClass");
                    return Json(moResponse, JsonRequestBehavior.AllowGet);
                }

                if (CanAccess("DOC_TYP_PROP", "DELETE", requestData) == true)
                {
                    JObject jObject = JObject.Parse(requestData);
                    string qry = " select dcid as count from tbdocumentclass where associatedDocType = (select associatedDocType from tbdocumentclass where dcid = @dcid and Status=0) and Status=0";
                
                 
                   MySqlParameter loName = new MySqlParameter("@dcid", Convert.ToInt16((jObject["docID"])));
                   var locount = db.Database.SqlQuery<ValCount>(qry, loName).ToList();

                //JObject jObject = JObject.Parse(requestData);
                  if(locount.Count>=2)
                    {
                    var lvDocClassID = Convert.ToInt16((jObject["docID"]));
                    var lvEmpty = db.tbdocumentclasses.Find(lvDocClassID);
                    lvEmpty.createdBy = userid;
                    lvEmpty.createdOn = DateTime.Now;
                    lvEmpty.Status = -100;
                    db.Entry(lvEmpty).State = EntityState.Modified;
                    db.SaveChanges();
                    moResponse = RespMsgFormatter.formatResp(1, getMessage("DeletePro"), null, "DeletProperties");

                    return Json(moResponse, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    moResponse = RespMsgFormatter.formatResp(0, getMessage("DeleteAddProperty"), null, "DeleteProperties");
                        return Json(moResponse, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    moResponse = RespMsgFormatter.formatResp(0, getMessage("Access"), null, "DeletProperties");
                    return Json(moResponse, JsonRequestBehavior.AllowGet);
                }

            }
            catch (Exception ex)
            {
                moResponse = RespMsgFormatter.formatResp(0, "message:" + ex.Message + " inner Exception:" + ex.InnerException, null, "DeleteProperties");
            }
            return Json(moResponse, JsonRequestBehavior.AllowGet);    
    }


        public ActionResult DeleteAuthorDepartments(string requestData)
        {
            try
            {
                int userid = getUserId();
                if (userid == 0)
                {
                    moResponse = RespMsgFormatter.formatResp(0, getMessage("SessionTimeOut"), null, "SaveDocumentClass");
                    return Json(moResponse, JsonRequestBehavior.AllowGet);
                }

                if (CanAccess("DOC_TYP_DEPT", "DELETE", requestData) == true)
                {

                    JObject jObject = JObject.Parse(requestData);
                

                var lsClassName="";
                var lvDepartmentPKID = Convert.ToInt16((jObject["PKID"]));                

                var lvdpt = db.tbdepartments.Find(lvDepartmentPKID);

                string qry = "select PKID as count from tbdepartment where DocClassID in (select DocClassID from tbdepartment where PKID = @PKID and Status = 0) and Status = 0";

                MySqlParameter loName = new MySqlParameter("@PKID", lvDepartmentPKID);
                var locount = db.Database.SqlQuery<ValCount>(qry, loName).ToList();

                if (locount.Count >= 2)
                {
                    var lvClassName = db.tbcodelkups.Where(s => s.fdKeyCode == lvdpt.DocClassID && s.fdLkUpCode == "DOC_CLASS_TYPE").Select(s => new { ClassName = s.fdKey1 }).ToList();
                    if (lvdpt.SecurityType == 1)
                    {
                        lsClassName = lvClassName[0].ClassName + "AUTH";
                    }
                    else
                    {
                        lsClassName = lvClassName[0].ClassName + "VIEW";
                    }

                    var lsRoleID = db.tbrolemasters.Where(s => s.fdRoleCode == lsClassName).Select(s => new { RoleID = s.fdRoleId }).ToList();
                    var lsUserID = db.tbusermasters.Where(s => s.fdDepartment == lvdpt.DepartmentID).Select(s => new { UserID = s.fdUserId }).ToList();

                    if (lsUserID.Count > 0)
                    {
                        for (int i = 0; i < lsUserID.Count; i++)
                        {
                            var liusrID = lsUserID[i].UserID;
                            var liroleID = lsRoleID[0].RoleID;
                            var lvdlt = db.tbuserroleloclinks.Where(s => s.fdUserId == liusrID && s.fdRoleId == liroleID).FirstOrDefault();

                            if (lvdlt != null)
                            {
                                db.tbuserroleloclinks.Remove(lvdlt);
                                db.SaveChanges();
                            }

                        }

                    }
                    if (lvdpt != null)
                    {
                        lvdpt.UpdatedBy = userid;
                        lvdpt.UpdatedOn = DateTime.Now;
                        lvdpt.Status = -100;
                        db.Entry(lvdpt).State = EntityState.Modified;
                        db.SaveChanges();
                        moResponse = RespMsgFormatter.formatResp(1, getMessage("DeleteDepartment"), null, "DeletProperties");
                    }
                    else
                    {
                        moResponse = RespMsgFormatter.formatResp(0, getMessage("Record Not Found"), null, "DeletProperties");
                            return Json(moResponse, JsonRequestBehavior.AllowGet);
                        }
                }
                else
                {
                    moResponse = RespMsgFormatter.formatResp(0, getMessage("DeleteAddDepartment"), null, "DeleteProperties");
                        return Json(moResponse, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    moResponse = RespMsgFormatter.formatResp(0, getMessage("Access"), null, "DeleteProperties");
                    return Json(moResponse, JsonRequestBehavior.AllowGet);
                }

                return Json(moResponse, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                moResponse = RespMsgFormatter.formatResp(0, "message:" + ex.Message + " inner Exception:" + ex.InnerException, null, "DeleteProperties");
            }
            return Json(moResponse, JsonRequestBehavior.AllowGet);
        }


        public ActionResult DeleteViewerDepartments(string requestData)
        {
            try
            {
                int userid = getUserId();
                if (userid == 0)
                {
                    moResponse = RespMsgFormatter.formatResp(0, getMessage("SessionTimeOut"), null, "SaveDocumentClass");
                    return Json(moResponse, JsonRequestBehavior.AllowGet);
                }

                JObject jObject = JObject.Parse(requestData);

                var lvDepartmentPKID = Convert.ToInt16((jObject["PKID"]));
                var lvEmpty = db.tbdepartments.Find(lvDepartmentPKID);
                lvEmpty.UpdatedBy = userid;
                lvEmpty.UpdatedOn = DateTime.Now;
                lvEmpty.Status = -100;
                db.Entry(lvEmpty).State = EntityState.Modified;
                db.SaveChanges();
                moResponse = RespMsgFormatter.formatResp(1, getMessage("DeletePro"), null, "DeletProperties");

                return Json(moResponse, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                moResponse = RespMsgFormatter.formatResp(0, "message:" + ex.Message + " inner Exception:" + ex.InnerException, null, "DeleteProperties");
            }
            return Json(moResponse, JsonRequestBehavior.AllowGet);
        }



    }
}