//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PanasonicDMS.Models.DbContext
{
    using System;
    using System.Collections.Generic;
    
    public partial class tbuserroleloclink
    {
        public int fdUserId { get; set; }
        public int fdLocationId { get; set; }
        public int fdRoleId { get; set; }
        public int fdCreatedBy { get; set; }
        public System.DateTime fdCreatedOn { get; set; }
        public Nullable<int> fdUpdatedBy { get; set; }
        public System.DateTime fdUpdatedOn { get; set; }
        public int fdRoleLocUserID { get; set; }
    }
}
