﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PanasonicDMS.Models
{
    public class DocumentSearch
    {
        public int fdDocId { get; set; }
        public string fdDocTitle { get; set; }
        public string fdCreatedOn { get; set; }
        public string fdFileUrl { get; set; }
        public string DocClass { get; set; }
        public string user { get; set; }
        public int AuthRight { get; set; }
        public string fdVersion { get; set; }

    }
    public class DMSModelStatus
    {
        public string fdKey1 { get; set; }
        public string fdKey2 { get; set; }
        public int fdKeyCode { get; set; }
        public string fdLkUpCode { get; set; }
        public string fdDescription { get; set; }
    }
    public class getviewrights
    {
        public string fdFunctionCode { get; set; }
        public string fdModuleCode { get; set; }
    }
    public class DMSClassSearch
    {
        public string fdLkUpCode { get; set; }
        public int fdKeyCode { get; set; }
        public string fdKey1 { get; set; }
        public string fdUserName { get; set; }
        public string fdCreatedOn { get; set; }

    }
    public class DMSClassGet
    {
        public int dcid { get; set; }
        public string DocClassName { get; set; }
        public int DocClassID { get; set; }
        public string ColumnName { get; set; }
        public int sequence { get; set; }
        public string ColumnType { get; set; }
        public string DocTypeId { get; set; }
        public int isRequired { get; set; }
        public string defaultValue { get; set; }
    }


    public class DMSClassDepartmentGet
    {
        public int DepartmentID { get; set; }
        public int PKID { get; set; }
        public string DepartmentName { get; set; }
        public int RightID { get; set; }
        public string Rights { get; set; }

    }
    public class DMSClassChart
    {
        public string date { get; set; }
        public string Fulldate { get; set; }
        public int DocNumber { get; set; }
    }
    public class DMSDocumentAudit
    {
        public int documentId { get; set; }
        public string fdUserName { get; set; }
        public string createdOn { get; set; }
        public string fdDocTitle { get; set; }
        public string auditBy { get; set; }
        public string fdCreatedOn { get; set; }
        public int auditId { get; set; }
        public string actionName { get; set; }
        public string fdModuleName { get; set; }

    }
    public class DMSClassPieChart
    {
        public int fdDocType { get; set; }
        public string DocumentTypeName { get; set; }
        public int DocNumber { get; set; }
    }

    public class uploadeddocument
    {
        public int? fdDocId { get; set; }
        public int? auditId { get; set; }
        public string actionName { get; set; }
        public string actionType { get; set; }
        public string fdUserName { get; set; }
        public string createdOn { get; set; }
        public string fdDocTitle { get; set; }
        public string DocumentClass { get; set; }
    }

    public class GetDocument
    {
        public int fdDocId { get; set; }

        public string ClassName { get; set; }
        public string CreatedOn { get; set; }
        public string fdUserName { get; set; }
        public string DocStatus { get; set; }
        public string fdDocTitle { get; set; }
        public int DocVersion { get; set; }
    }

    public class getDocumentClassCount
    {
        public string documentClassName { get; set; }
        public int TotalCount { get; set; }
    }


    public class getDocumentdetails
        {
        public string fdDocId { get; set; }
        public string fdDocTitle { get; set; }
        public string fdDocType { get; set; }
        public string fdFileUrl { get; set; }
        public string fdKey { get; set; }
        public string fdValue { get; set; }
        public string dcColumnName { get; set; }
        public string dcColumnType { get; set; }
        public string dcid { get; set; }
        public string defaultValue { get; set; }
        public string isRequired { get; set; }
        public string sequence { get; set; }
        public string mxValue { get; set; }
        public string minValue { get; set; }
        public string fdStatus { get; set; }
        public string fdVersion { get; set; }
        public string fdCreatedOn { get; set; }
        public string DocClass { get; set; }
        public string fdUserName { get; set; }

    }


    public class getdocumentVersionHistory
    {
        public int fdDocId { get; set; }
        public string fdDocTitle { get; set; }
        public int? fdVersion { get; set; }
        public int? fdParentId { get; set; }
        public string fdCreatedOn { get; set; }
        public string fdUserName { get; set; }
        public string lastRevison { get; set; }
        public string DocStatus { get; set; }
    }



    public class getDocClass
    {
        public string fdDocTitle { get; set; }
        public string fdKey1 { get; set; }
        public string fdCreatedOn { get; set; }
        public int fdVersion { get; set; }
        public string UserName { get; set; }
    }



    public class DocPathDetetails
    {
        public string fdDocTitle { get; set; }
        public string fdFileUrl { get; set; }
        public string fdSpArchiveUNCpath { get; set; }
        public string fdSpUNCpath { get; set; }
        public Guid fdDocGuID { get; set; }
    }


    public class ValCount
    {
        public int count { get; set; }
    }

}