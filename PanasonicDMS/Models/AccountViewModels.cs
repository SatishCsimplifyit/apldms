﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace PanasonicDMS.Models
{
    public class UserDetails
    {
        public int fduserid { get; set; }
        public string fdusername { get; set; }
        public Int64 fdUserContactNo { get; set; }
        public string fdUserEmail { get; set; }
        public int UserStatus { get; set; }
        public string Role { get; set; }
        public Int16? fdDepartment { get; set; }
    }





    public class UserSearch
    {
        public int fduserid { get; set; }
        public string fdusername { get; set; }
        public Int64 fdUserContactNo { get; set; }
        public string fdUserEmail { get; set; }
        public string UserStatus { get; set; }
        public string Role { get; set; }
        public Int16? fdDepartment { get; set; }
    }

    public class UserInfo
    {
        public int userId { get; set; }
        public string userName { get; set; }
        public int? userType { get; set; }
        public string email { get; set; }
        public long? fdUserContactNo { get; set; }
        public string fdMessage { get; set; }
        public DateTime? lastLoggedInDate { get; set; }
        public int? userRefId { get; set; }
        public int? roleId { get; set; }
        public List<UserModuleFunctionModel> userRoles { get; set; }
        public string ModuleName { get; set; }
        public bool rememberme { get; set; }
    }
    public class UserModuleFunctionModel
    {
        public int roleId { get; set; }
        public string moduleCode { get; set; }
        public string functionCode { get; set; }
        public int locationId { get; set; }
        public int userId { get; set; }
    }

    public class ExternalLoginConfirmationViewModel
    {
        [Required]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }

    public class ExternalLoginListViewModel
    {
        public string ReturnUrl { get; set; }
    }

    public class SendCodeViewModel
    {
        public string SelectedProvider { get; set; }
        public ICollection<System.Web.Mvc.SelectListItem> Providers { get; set; }
        public string ReturnUrl { get; set; }
        public bool RememberMe { get; set; }
    }

    public class VerifyCodeViewModel
    {
        [Required]
        public string Provider { get; set; }

        [Required]
        [Display(Name = "Code")]
        public string Code { get; set; }
        public string ReturnUrl { get; set; }

        [Display(Name = "Remember this browser?")]
        public bool RememberBrowser { get; set; }

        public bool RememberMe { get; set; }
    }

    public class ForgotViewModel
    {
        [Required]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }

    public class LoginViewModel
    {
        [Required]
        [Display(Name = "Email")]
        [EmailAddress]
        public string Email { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [Display(Name = "Remember me?")]
        public bool RememberMe { get; set; }
    }

    public class RegisterViewModel
    {
        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }
    }

    public class ResetPasswordViewModel
    {
        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

        public string Code { get; set; }
    }

    public class ForgotPasswordViewModel
    {
        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }
}
