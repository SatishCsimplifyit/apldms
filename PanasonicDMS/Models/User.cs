﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PanasonicDMS.Models
{
    public class User
    {
        public int fdRoleId { get; set; }
        public string fdRoleCode { get; set; }
        public string fdRoleName { get; set; }
        public int Signed { get; set; }
    }
}