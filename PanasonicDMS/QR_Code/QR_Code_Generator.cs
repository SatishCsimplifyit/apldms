﻿using QRCoder;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;

namespace PanasonicDMS.QR_Code
{
    public class QR_Code_Generator
    {
        public static Bitmap QRCode_Generator(string Value)
        {
            QRCodeGenerator qrGenerator = new QRCodeGenerator();
            QRCodeData qrCodeData = qrGenerator.CreateQrCode(Value, QRCodeGenerator.ECCLevel.H);
            // QRCode qrCode = new QRCode(qrCodeData);
            // Bitmap qrCodeImage = qrCode.GetGraphic(20);
            //Set color by using Color-class types


            QRCode qrCode = new QRCode(qrCodeData);
           // qrCode.SetQRCodeData(qrCodeData);
            

            Bitmap qrCodeImage = qrCode.GetGraphic(2, Color.Black, Color.White, true);
            //Set color by using HTML hex color notation
            // Bitmap qrCodeImage = qrCode.GetGraphic(20, "#000ff0", "#0ff000");

            // The another overload enables you to render a logo / image in the center of the QR code.
            //Bitmap qrCodeImage = qrCode.GetGraphic(20, Color.Black, Color.White, (Bitmap)Bitmap.FromFile("C:\\myimage.png"));
            return qrCodeImage;
        }
    }
}