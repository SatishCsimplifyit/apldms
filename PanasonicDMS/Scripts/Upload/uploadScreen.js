


document.addEventListener("DOMContentLoaded", function (event) {
    if (window.innerWidth < 992) {
        var isMobile = true;
        document.querySelector("#sidebar").classList.add("sidebar-close");
        document.querySelector("#sidebar1").classList.add("sidebar-close1");
    }

    document.querySelector("#toggle-sidebar").addEventListener("click", function (event) {
        document.querySelector("#sidebar").classList.toggle("sidebar-close");
        setTimeout(function () {
            if (document.querySelector("#sidebar").classList.contains("sidebar-close")) {
                document.querySelector(".content").classList.remove("hide-on-mob-toggle");
                if (isMobile) {
                    document.querySelector("#sidebar1").style.display = "block";
                }
            } else {
                document.querySelector(".content").classList.add("hide-on-mob-toggle");
                if (isMobile) {
                    document.querySelector("#sidebar1").style.display = "none";
                }
            }
        }, 10);
    });
});




$(document).on('hidden.bs.modal', '.bootbox', function () {
  //  $('#txtDocumentFile').val("")
    $('#txtDocumentFile').focus();
   
});






function ValidateFile()
{
    var uploadFiles = document.getElementById("txtDocumentFile").files;
    var file_data = uploadFiles[0];   // Getting the properties of file from file field


    if ($('#txtDocumentFile').val() == undefined || $('#txtDocumentFile').val() == null || $('#txtDocumentFile').val() == '') {
        if (document.getElementById("txtDocumentFile").value == "") {
            showMessage("red", "Please select the file.", "");
            return false;
        }
    }

    if ( $('#txtDocumentFile').val() != undefined && $('#txtDocumentFile').val() != null && $('#txtDocumentFile').val() != '') {
        var check=file_data.name.split(".")
        var chckpdf = check[check.length-1];
        chckpdf = chckpdf.toLowerCase();
        if (chckpdf.toString().toUpperCase() != "PDF" && chckpdf.toString().toUpperCase() != "PDF" != "JPG" && chckpdf.toString().toUpperCase() != "JPEG" && chckpdf.toString().toUpperCase() != "PNG") {
            $('#txtDocumentFile').val("");
            showMessage("red", "You can upload only jpg,jpeg,png and pdf file.");
            return false;
           
           
        }

    }


}



var DocIds = new Array();
var VinsID = new Array(); var currentIndex = 0;
var myVar;
var isInEditMode;
function DecideAndExecute() {
    if ($('#PlayStop').hasClass("btn-azure")) {
        $('#PlayStop').removeClass("btn-azure");
        $('#PlayStop').addClass("btn-danger");
        $('#PlayStop').html("<i class='fa fa-pause'></i>Stop Slide Show");
        StartSlidehow();
    } else {
        stopFunction();
        $('#PlayStop').removeClass("btn-danger");
        $('#PlayStop').addClass("btn-azure");
        $('#PlayStop').html("<i class='glyphicon glyphicon-play-circle'></i>Start Slideshow");
    }
}
function StartSlidehow() {

    myVar = setTimeout(StartSlidehow, 6000);
    nextPod();
}
function stopFunction() {
    clearTimeout(myVar); // stop the timer
}

$(document).ready(function () {

    
    $('.date_picker').datepicker({
        format: "dd/mm/yyyy"
        }).on('change', function() {
            debugger;
        $('.datepicker').hide();
        });


    documentTag();
    //  setAndSetUserScreenModules();

    //$("#col9s").attr('class', 'col-md-11');
    //$("#col3s").attr('class', 'col-md-1');
    
    //documentSearchTag();
    $("#cols5").attr('class', 'col-md-1');
    $("#cols11").attr('class', 'col-md-11');

   
    $("#fafatimess").attr('class', 'fa fa-bars');
    $("#grdMetaDocDetails").hide();
    $("#podDocImage").hide();
    $(".doc_property").hide();
    $('#btnRestet').show();
    $("#btnUploadDocument").hide();

    $('#cboDoctype').focus();
    /*hide upload Document*/

    var vars = [], hash;
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1);
    hashes = hashes.split('&');
    for (var i = 0; i < hashes.length; i++) {
        hash = hashes[i].split('=');
    };


    var docID = hash[1];
    if (docID != "" && docID != 0 && docID != null) {

        $('input[id^=txtDocumentFile]').hide();
        $('#cboDoctype').hide();
        $('#uploadfile').hide();
        $('#NewDocument').hide();
        $('#EditDocument').show();

    }


    /*hide upload Document*/

    var vars = [], hash;
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1);
    hashes = hashes.split('&');
    for (var i = 0; i < hashes.length; i++) {
        hash = hashes[i].split('=');
    };


    var docID = hash[1];
    if (docID != "" && docID != 0 && docID != null) {

        $('input[id^=txtDocumentFile]').hide();
        $('#cboDoctype').hide();
        $('#uploadfile').hide();
        $('#NewDocument').hide();

        $('.doc_property').hide();
        $('#btnUpdatePodText').hide();
        $('#btnRestet').hide();


        $('#EditDocument').show();

    }



    /*show edit document*/

    $('#EditDocument').hide();


    $('#btnViewHistory').hide();
    $('#btnArchive').hide();
    $('#btnCheckOut').hide();




    $('#query1').hide();
    $('#docDataControl').hide();
    $('#cboDoctype').change(function () { displayForm(); });
    //SerchVinNo('txtVinNo', 'btnVinNo', '/TMS/schVinNo', 'liVinNo', 'hdnVinId');

    SerchVinNo("txtSchVIN", "btnSchVIN", "/TMS/schVinNo", "lstSchVIN", "hdnSchVinNo", '', false);
    var inputData = {};
    var lookUpCodes = [];
    //lookUpCodes.push("CONTRACTDOC_TYPE");
    //lookUpCodes.push("TRCK_DOC_TYP");
    //lookUpCodes.push("WRK_ORDER_DOC_TYP");
    //lookUpCodes.push("WRK_ORDER_UPLOAD_DOC");
    lookUpCodes.push("DOC_CLASS_TYPE");
    inputData.lookUpCode = lookUpCodes;
    ajaxCaller("POST", "Common/fetchStatus", JSON.stringify(inputData), "application/json", "Loading Values").done(function (data) {
        if (data.Status == 1) {
            debugger;
            var html = "";
            var oResult = data.Result;
            $.each(oResult, function (key, value) {
                //if (value.fdLkUpCode == "DOC_TYP") {
                //    $('#cobSchDocType').append($('<option></option>').val(value.fdKeyCode).html(value.fdKey1));                   
                //}
                if (value.fdLkUpCode == "DOC_CLASS_TYPE") {
                    $('#cboDoctype').append($('<option></option>').val(value.fdKeyCode).html(value.fdKey1));
                }
                //if (value.fdLkUpCode == "TRCK_DOC_TYP") {
                //    $('#cboDoctype').append($('<option></option>').val(value.fdKeyCode).html(value.fdKey1));                   
                //}
                //if (value.fdLkUpCode == "WRK_ORDER_DOC_TYP") {
                //    $('#cboDoctype').append($('<option></option>').val(value.fdKeyCode).html(value.fdKey1));                   
                //}
                //if (value.fdLkUpCode == "WRK_ORDER_UPLOAD_DOC") {
                //    $('#cboDoctype').append($('<option></option>').val(value.fdKeyCode).html(value.fdKey1));

                //}
            });
        } else if (data.Status == 2) {
            showMessage("red", data.Description, "");
        } else {
            showMessage("red", data.Description, "");
        }
    });
    debugger;
    $('#cobSchDocType').val(1);
    $('#txtSchValue').prop('disabled', 'disabled');

    var dateToday = new Date();
    prevdate = new Date(dateToday.getFullYear(), dateToday.getMonth() - 1, 1);
    var grdDocSearch = $('#grdDocSearch').DataTable();
    //$("#txtFromDate").val($.datepicker.formatDate('dd-M-yy', prevdate));
    //$("#txtToDate").val($.datepicker.formatDate('dd-M-yy', dateToday));

    $('#grdDocSearch tbody').off('click').on('click', 'tr', function () {
        var data = grdDocSearch.row(this).data();
        var docId = data[0];

        ajaxCaller("POST", "Document/GetDocumentDetails", JSON.stringify(docId), "application/json", "Please wait ! Getting Document Details").done(function (data) {
            if (data.Status == 1) {
                debugger;
                var html = "";
                var oResult = data.Result;
                $('#previevpodDocImage').attr('src', oResult.FileUrl.fdFileUrl);
                //$('#cboDoctype').val(oResult.FileUrl.fdDocType);
                //var dataTable = $('#grdMetaDocDetails').DataTable();
                //dataTable.clear().draw();
                //$.each(data.Result.DocumentData, function (key, value) {

                //    dataTable.row.add([value.fdKey, value.fdValue
                //    ]).draw();
                //    dataTable.columns.adjust().draw();
                //});
                //$('#docTypeControlsContainer').hide();
                //$('#docDataControl').show();
                //$("#DocumentCreate").addClass("active");
                //$("#DocCreate").addClass("active");
                //$("#DocumentSearch").removeClass("active");
                //$("#Docsearch").removeClass("active");
                //$('#txtDocumentFile').val('')

            } else if (data.Status == 2) {
                showMessage("red", data.Description, "");
            } else {
                showMessage("red", data.Description, "");
            }
        });


    });



    $('#grdDocSearch tbody').off('dblclick').on('dblclick', 'tr', function () {
        var data = grdDocSearch.row(this).data();
        var docId = data[0];

        ajaxCaller("POST", "Document/GetDocumentDetails", JSON.stringify(docId), "application/json", "Please wait ! Getting Document Details").done(function (data) {
            if (data.Status == 1) {
                debugger;
                var html = "";
                var oResult = data.Result;

                $('#podDocImage').attr('src', oResult.FileUrl.fdFileUrl);
                //$('#cboDoctype').val(oResult.FileUrl.fdDocType);
                var dataTable = $('#grdMetaDocDetails').DataTable();
                dataTable.clear().draw();
                $('.dataTables_filter').hide();
                $('.dataTables_length').hide();
                $('.dataTables_info').hide();
                $('.dataTables_paginate').hide();
                $.each(data.Result.DocumentData, function (key, value) {

                    dataTable.row.add([value.fdKey, value.fdValue
                    ]).draw();
                    dataTable.columns.adjust().draw();
                });
                //$('#docTypeControlsContainer').hide();
                //$('#docDataControl').show();
                //$("#DocumentCreate").addClass("active");
                //$("#DocCreate").addClass("active");
                //$("#DocumentSearch").removeClass("active");
                //$("#Docsearch").removeClass("active");
                //$('#txtDocumentFile').val('')
                $('#myModal').show();

            } else if (data.Status == 2) {
                showMessage("red", data.Description, "");
            } else {
                showMessage("red", data.Description, "");
            }
        });


    });


    setTimeout(function () {
        geturlUploadData();
    }, 1000);



});
function srchreset() {
    $('#podDocImage').attr('src', "img/preview.jpg");
    var dataTable = $('#grdMetaDocDetails').DataTable();
    dataTable.clear().draw();
    $('#previevpodDocImage').attr('src', "img/preview.jpg");
    var grdItemDataTable = $('#grdDocSearch').DataTable();
    grdItemDataTable.clear().draw();
}
function closeModal() {
    $('#myModal').hide();
}
$("#txtDocumentFile").change(function () {

    readURL(this);
});
function readURL(input) {

    if (input.files && input.files[0]) {
        pdffile_url = URL.createObjectURL(input.files[0]);
        $('#podDocImage').attr('src', pdffile_url);
        $('#podDocImage').show();
        //var reader = new FileReader();

        //reader.onload = function (e) {
        //    $('#podDocImage').attr('src', e.target.result);
        //}

        //reader.readAsDataURL(input.files[0]);
    }
}

function SerchVinNo(textBoxSerchId, serchButtonId, url, listId, inputFieldID, cboID, isComposite) {



    $("#" + textBoxSerchId + "," + "#" + serchButtonId + "").bind("click keyup", function (e) {
        debugger;
        $("#" + inputFieldID + "").val("");
        if ($("#" + textBoxSerchId + "").val().length >= 3) {

            var keyword = {};
            keyword.VinNo = $("#" + textBoxSerchId + "").val();


            if (true) {
                ajaxCaller(
                  "POST",
                  url, JSON.stringify(keyword), "application/json", 'Loading Suggestions...')
                  .done(function (data) {

                      if (data.Status == 1) {
                          if (data.Result.length > 0) {

                              showVinNo(data, textBoxSerchId, listId, inputFieldID, cboID, isComposite);
                          }
                          else {
                              var list = '';

                              list += '<li class="list-group-item list-search-items"   style="width:30%;">' +

                               '<div class="clearfix" ><span>Nothing Found. Try Other Keywords</span></div>' +
                                 '</li>';
                              $("#" + listId + "").show();
                              $("#" + listId + "").empty();
                              $("#" + listId + "").append(list);


                          }
                      }
                      else {
                          showMessage("red", data.Description);
                      }
                  });
            } else {

            }


        } else {

            $(".listParent").hide();

            return;

        }
    });



}


function showVinNo(data, textBoxSerchId, listId, inputFieldID, cboID, isComposite) {

    var oResponse = data;

    if (oResponse.Status == 1) {
        var oResult = oResponse.Result;
        var list = '';
        var listIncrementer = 0;
        if (isComposite == false) {
            debugger;
            $.each(oResult, function (key, value) {
                var id = value.Item1;
                var name = value.Item2;
                var code = value.Item3;
                list += '<li class="list-group-item list-search-items" data-Id="' + id + '" data-Name="' + name + '" id="Code:' + code + '"  style="width:100%;">' +

                '<div class="clearfix" ><span onClick=selectVinNo(this,"' + textBoxSerchId + '","' + listId + '","' + inputFieldID + '","' + cboID + '","' + code + '") id="Code' + code + '">' + name + '</span></div>' +
                '</li>';
            });
        }
        else {
            $.each(oResult, function (key, value) {
                var id = value.Item1;
                var name = value.Item2;
                var code = value.Item3;
                list += '<li class="list-group-item list-search-items" data-Id="' + id + '" data-Name="' + name + '" id="Code:' + code + '"  style="width:100%;">' +

                '<div class="clearfix" ><span onClick=selectVendor(this,"' + textBoxSerchId + '","' + listId + '","' + inputFieldID + ',' + cboID + '") id="Code' + code + '">' + name + '-' + code + '</span></div>' +
                '</li>';
            });
        }


        $("#" + listId + "").show();
        $("#" + listId + "").empty();
        $("#" + listId + "").append(list);
    }
    else {
        showMessage(oResponse.Description);
    }


}


function selectVinNo(id, textBoxSerchId, listId, inputFieldID, cboID, code) {

    debugger;

    $("#" + inputFieldID + "").val(id.parentNode.parentNode.getAttribute("data-id"));
    document.getElementById(textBoxSerchId).value = id.innerText;
    $("#" + listId + "").empty();

    if (cboID != null & cboID != '') {
        $("#" + cboID + "").val(code);
        $("#" + cboID + "").prop("disabled", true);
    }


}

function displayForm() {
    debugger;
    ajaxCaller("POST", "Document/getDocumentForm", $('#cboDoctype').val(), "application/json", "Getting property values").done(function (data) {
        if (data.Status == 1) {
            $(".doc_property").show();
            $('#docTypeControlsContainer').show();
            if (data.Result != null) {
                $('#formControlsContainer').empty();
                $('#docDataControl').hide();
                var formFieldHTML, formControlHTML;
                var itrforcontrol = 0;
                for (var itrtr = 0; itrtr < data.Result.length; itrtr++) {
                    itrforcontrol = itrforcontrol + 1;
                    // getting field
                    if (itrforcontrol == 2) {
                        formControlHTML = formControlHTML + '<div class="col-md-6"> <div class="group">';
                        if (data.Result[itrtr].dcColumnType == "T" && data.Result[itrtr].isRequired == "1") {

                            var TextValue = "";

                            if (data.Result[itrtr].defaultValue != null && data.Result[itrtr].defaultValue != "") {
                                TextValue = data.Result[itrtr].defaultValue;
                            }

                            formControlHTML = formControlHTML + ' <input type="text" autocomplete="off" value="' + TextValue + '" class="isRequired docTypeFormControls" data-formcontrols="' + data.Result[itrtr].dcColumnName + '"  data-formcontrolid="' + data.Result[itrtr].dcid + '"required >'
                                      + ' <span class="highlight"></span>'
                                      + ' <span class="bar"></span>'
                                      + ' <label>' + data.Result[itrtr].dcColumnName + '<span class="asterisk">*</span></label>'
                                      + '</div>'
                                      + ' </div>';

                        }
                        else if (data.Result[itrtr].dcColumnType == "T") {


                            var TextValue = "";

                            if (data.Result[itrtr].defaultValue != null && data.Result[itrtr].defaultValue != "") {
                                TextValue = data.Result[itrtr].defaultValue;
                            }
                            formControlHTML = formControlHTML + ' <input type="text" autocomplete="off"  value="' + TextValue + '"  class=" docTypeFormControls" data-formcontrols="' + data.Result[itrtr].dcColumnName + '"  data-formcontrolid="' + data.Result[itrtr].dcid + '"required >'
                                    + ' <span class="highlight"></span>'
                                    + ' <span class="bar"></span>'
                                    + ' <label>' + data.Result[itrtr].dcColumnName + '</label>'
                                    + '</div>'
                                    + ' </div>';

                        }
                        else if (data.Result[itrtr].dcColumnType == "I" && data.Result[itrtr].isRequired == "1") {
                            var TextValue = "";

                            if (data.Result[itrtr].defaultValue != null && data.Result[itrtr].defaultValue != "") {
                                TextValue = data.Result[itrtr].defaultValue;
                            }

                            formControlHTML = formControlHTML + ' <input type="number" value="' + TextValue + '"  class="docTypeFormControls isRequired number " data-formcontrols="' + data.Result[itrtr].dcColumnName + '"  data-formcontrolid="' + data.Result[itrtr].dcid + '"required >'
                                    + ' <span class="highlight"></span>'
                                    + ' <span class="bar"></span>'
                                    + ' <label>' + data.Result[itrtr].dcColumnName + '<span class="asterisk">*</span></label>'
                                    + '</div>'
                                    + ' </div>';


                        }
                        else if (data.Result[itrtr].dcColumnType == "I") {

                            var TextValue = "";

                            if (data.Result[itrtr].defaultValue != null && data.Result[itrtr].defaultValue != "") {
                                TextValue = data.Result[itrtr].defaultValue;
                            }
                            formControlHTML = formControlHTML + ' <input type="number" value="' + TextValue + '"  class="docTypeFormControls number " data-formcontrols="' + data.Result[itrtr].dcColumnName + '"  data-formcontrolid="' + data.Result[itrtr].dcid + '"required >'
                                   + ' <span class="highlight"></span>'
                                   + ' <span class="bar"></span>'
                                   + ' <label>' + data.Result[itrtr].dcColumnName + '</label>'
                                   + '</div>'
                                   + ' </div>';


                        }

                        else if (data.Result[itrtr].dcColumnType == "D" && data.Result[itrtr].isRequired == "1") {

                            var TextValue = "";

                            if (data.Result[itrtr].defaultValue != null && data.Result[itrtr].defaultValue != "") {
                                TextValue = data.Result[itrtr].defaultValue;
                            }
                            formControlHTML = formControlHTML + ' <input type="text"  value="' + TextValue + '" class="docTypeFormControls isRequired date_picker " data-formcontrols="' + data.Result[itrtr].dcColumnName + '"  data-formcontrolid="' + data.Result[itrtr].dcid + '"required >'
                                  + ' <span class="highlight"></span>'
                                  + ' <span class="bar"></span>'
                                  + ' <label>' + data.Result[itrtr].dcColumnName + '<span class="asterisk">*</span></label>'
                                  + '</div>'
                                  + ' </div>';


                        }
                        else if (data.Result[itrtr].dcColumnType == "D") {

                            var TextValue = "";

                            if (data.Result[itrtr].defaultValue != null && data.Result[itrtr].defaultValue != "") {
                                TextValue = data.Result[itrtr].defaultValue;
                            }
                            formControlHTML = formControlHTML + ' <input type="text" value="' + TextValue + '"  class="docTypeFormControls date_picker " data-formcontrols="' + data.Result[itrtr].dcColumnName + '"  data-formcontrolid="' + data.Result[itrtr].dcid + '"required >'
                                   + ' <span class="highlight"></span>'
                                   + ' <span class="bar"></span>'
                                   + ' <label>' + data.Result[itrtr].dcColumnName + '</label>'
                                   + '</div>'
                                   + ' </div>';


                        }



                    } else {
                        formControlHTML = '<div class="col-md-6"> <div class="group">'
                        if (data.Result[itrtr].dcColumnType == "T" && data.Result[itrtr].isRequired == "1") {

                            var TextValue = "";

                            if (data.Result[itrtr].defaultValue != null && data.Result[itrtr].defaultValue != "") {
                                TextValue = data.Result[itrtr].defaultValue;
                            }
                            formControlHTML = formControlHTML + ' <input type="text" autocomplete="off" value="' + TextValue + '"  class="isRequired docTypeFormControls" data-formcontrols="' + data.Result[itrtr].dcColumnName + '"  data-formcontrolid="' + data.Result[itrtr].dcid + '"required >'
                                      + ' <span class="highlight"></span>'
                                      + ' <span class="bar"></span>'
                                      + ' <label>' + data.Result[itrtr].dcColumnName + '<span class="asterisk">*</span></label>'
                                      + '</div>'
                                      + ' </div>';

                        }
                        else if (data.Result[itrtr].dcColumnType == "T") {


                            var TextValue = "";

                            if (data.Result[itrtr].defaultValue != null && data.Result[itrtr].defaultValue != "") {
                                TextValue = data.Result[itrtr].defaultValue;
                            }
                            formControlHTML = formControlHTML + ' <input type="text" autocomplete="off" value="' + TextValue + '"  class=" docTypeFormControls" data-formcontrols="' + data.Result[itrtr].dcColumnName + '"  data-formcontrolid="' + data.Result[itrtr].dcid + '"required >'
                                    + ' <span class="highlight"></span>'
                                    + ' <span class="bar"></span>'
                                    + ' <label>' + data.Result[itrtr].dcColumnName + '</label>'
                                    + '</div>'
                                    + ' </div>';

                        }
                        else if (data.Result[itrtr].dcColumnType == "I" && data.Result[itrtr].isRequired == "1") {


                            var TextValue = "";

                            if (data.Result[itrtr].defaultValue != null && data.Result[itrtr].defaultValue != "") {
                                TextValue = data.Result[itrtr].defaultValue;
                            }
                            formControlHTML = formControlHTML + ' <input type="number" value="' + TextValue + '"  class="docTypeFormControls isRequired number " data-formcontrols="' + data.Result[itrtr].dcColumnName + '"  data-formcontrolid="' + data.Result[itrtr].dcid + '"required >'
                                    + ' <span class="highlight"></span>'
                                    + ' <span class="bar"></span>'
                                    + ' <label>' + data.Result[itrtr].dcColumnName + '<span class="asterisk">*</span></label>'
                                    + '</div>'
                                    + ' </div>';


                        }
                        else if (data.Result[itrtr].dcColumnType == "I") {

                            var TextValue = "";

                            if (data.Result[itrtr].defaultValue != null && data.Result[itrtr].defaultValue != "") {
                                TextValue = data.Result[itrtr].defaultValue;
                            }
                            formControlHTML = formControlHTML + ' <input type="number" value="' + TextValue + '" class="docTypeFormControls number " data-formcontrols="' + data.Result[itrtr].dcColumnName + '"  data-formcontrolid="' + data.Result[itrtr].dcid + '"required >'
                                   + ' <span class="highlight"></span>'
                                   + ' <span class="bar"></span>'
                                   + ' <label>' + data.Result[itrtr].dcColumnName + '</label>'
                                   + '</div>'
                                   + ' </div>';


                        }

                        else if (data.Result[itrtr].dcColumnType == "D" && data.Result[itrtr].isRequired == "1") {


                            var TextValue = "";

                            if (data.Result[itrtr].defaultValue != null && data.Result[itrtr].defaultValue != "") {
                                TextValue = data.Result[itrtr].defaultValue;
                            }
                            formControlHTML = formControlHTML + ' <input type="text" value="' + TextValue + '"  class="docTypeFormControls isRequired date_picker " data-formcontrols="' + data.Result[itrtr].dcColumnName + '"  data-formcontrolid="' + data.Result[itrtr].dcid + '"required >'
                                    + ' <span class="highlight"></span>'
                                    + ' <span class="bar"></span>'
                                    + ' <label>' + data.Result[itrtr].dcColumnName + '<span class="asterisk">*</span></label>'
                                    + '</div>'
                                    + ' </div>';


                        }
                        else if (data.Result[itrtr].dcColumnType == "D") {

                            var TextValue = "";

                            if (data.Result[itrtr].defaultValue != null && data.Result[itrtr].defaultValue != "") {
                                TextValue = data.Result[itrtr].defaultValue;
                            }
                            formControlHTML = formControlHTML + ' <input type="text"  value="' + TextValue + '" class="docTypeFormControls date_picker " data-formcontrols="' + data.Result[itrtr].dcColumnName + '"  data-formcontrolid="' + data.Result[itrtr].dcid + '"required >'
                                   + ' <span class="highlight"></span>'
                                   + ' <span class="bar"></span>'
                                   + ' <label>' + data.Result[itrtr].dcColumnName + '</label>'
                                   + '</div>'
                                   + ' </div>';


                        }

                    }




                    if (itrforcontrol == 2 || itrtr + 1 == data.Result.length) {
                        $('#formControlsContainer').append('<div class="row">' + formControlHTML + '</div>');
                        itrforcontrol = 0;
                    }
                    //$('#formControlsContainer').append('<div class="row ">' + formFieldHTML + formControlHTML + '</div>');
                    //$('.dateTimePickerFormControl').datetimepicker({
                    //    format: 'DD-MMM-YYYY'
                    //});

                    $('.date_picker').datepicker({
                        format: "dd/mm/yyyy"
                    }).on('change', function () {
                        debugger;
                        $('.datepicker').hide();
                    }).on("keydown", function (e) {
                        e.preventDefault();
                    }).on("keydown", function (e) {
                        e.preventDefault();
                    });
                }
            }
        } else {
            showMessage("red", data.Description, "");
        }
    });


}

$("#cobSchDocType").change(function () {
    debugger;
    $('#txtSchValue').prop('disabled', false);
    if ($("#cobSchDocType").val() == "") {
        $('#txtSchValue').prop('disabled', 'disabled');
    }
    $('#hdnSchValueId').val("");
    $('#txtSchValue').val("");
    $('#txtFromDate').val("");
    $('#txtToDate').val("");
    $('#txtSchVIN').val("");
    $('#hdnSchVinNo').val("");
    SerchOnBoxList("txtSchValue", "btnSchValue", "Document/getSearchBoxList", "lstSchValue", "hdnSchValueId", '', false, "cobSchDocType");
});


function searchDocuments() {

    var inputData = {};
    if ($('#cobSchDocType').val() != null && $('#cobSchDocType').val() != '' && $('#cobSchDocType').val() != undefined) {

        inputData["cobSchDocType"] = $('#cobSchDocType').val();
    }
    //else
    //{
    //    showMessage("red", "Please select Search On");
    //    $('#cobSchDocType').focus();
    //    return;
    //}

    if ($('#hdnSchValueId').val() != null && $('#hdnSchValueId').val() != '' && $('#hdnSchValueId').val() != undefined
        || $('#txtSchValue').val() != null && $('#txtSchValue').val() != '' && $('#txtSchValue').val() != undefined) {

        if ($('#cobSchDocType').val() != 5) {
            inputData["hdnSchValueId"] = $('#hdnSchValueId').val();
        }
    }
    else {
        //if ($('#cobSchDocType').val() != 5) {
        //    showMessage("red", "Please enter " + $("#cobSchDocType option:selected").text());
        //    $('#cobSchDocType').focus();
        //    return;
        //}

    }
    if ($('#txtFromDate').val() != null && $('#txtFromDate').val() != '' && $('#txtFromDate').val() != undefined) {

        inputData["txtFromDate"] = $('#txtFromDate').val();
    }
    if ($('#txtToDate').val() != null && $('#txtToDate').val() != '' && $('#txtToDate').val() != undefined) {

        inputData["txtToDate"] = $('#txtToDate').val();
    }
    if ($('#hdnSchVinNo').val() != null && $('#hdnSchVinNo').val() != '' && $('#hdnSchVinNo').val() != undefined
       || $('#txtSchVIN').val() != null && $('#txtSchVIN').val() != '' && $('#txtSchVIN').val() != undefined) {

        inputData["hdnSchVinNo"] = $('#hdnSchVinNo').val();
    }

    if ($('#txtSchText').val() != null && $('#txtSchText').val() != '' && $('#txtSchText').val() != undefined &&
        $('#txtToDate').val() != null && $('#txtToDate').val() != '' && $('#txtToDate').val() != undefined &&
        $('#txtFromDate').val() != null && $('#txtFromDate').val() != '' && $('#txtFromDate').val() != undefined) {

        inputData["txtText"] = $('#txtSchText').val();
    }
    else if ($('#txtSchText').val() != null && $('#txtSchText').val() != '' && $('#txtSchText').val() != undefined) {
        if ($('#txtFromDate').val() == null || $('#txtFromDate').val() == '' || $('#txtFromDate').val() == undefined) {
            showMessage("red", "Please enter from date");
            $('#txtFromDate').focus();
            return;
        }
        if ($('#txtToDate').val() == null || $('#txtToDate').val() == '' || $('#txtToDate').val() == undefined) {
            showMessage("red", "Please enter to date");
            $('#txtToDate').focus();
            return;
        }

    }

    debugger;
    ajaxCaller("POST", "Document/searchDocument", JSON.stringify(inputData), "application/json", "Searching").done(function (data) {
        if (data.Status == 1) {
            debugger;
            // $('#txtSchValue').prop('disabled', 'disabled');

            var grdDocumentSearch = $('#grdDocumentSearch').DataTable();
            grdDocumentSearch.clear().draw();

            grdDocumentSearch.column(0).visible(false);
            grdDocumentSearch.column(9).visible(false);
            if (data.Result != null) {
                if (data.Result.length > searchCount) {
                    showMessage("green", "There are more than " + searchCount + " documents, Please refine your search.", "");
                } else {
                    showMessage("green", data.Result.length + " Document(s) Found")
                }

            }
            else {
                showMessage("green", " 0 Document(s) Found")
            }

            var i = 0;
            $.each(data.Result, function (key, value) {
                var checkBox = '<div class="checkbox"><label><input class="colored-success chkbox" id="chk-' + i + '" name="form-field-checkbox" type="checkbox" value="true"><span class="text"></span></label>';
                Isreconcle
                if (data.Result[i].fdIsReconciled === 1) {
                    var Isreconcle = "Yes";
                }
                else {
                    var Isreconcle = "No";
                }

                grdDocumentSearch.row.add([
                data.Result[i].DocId,
                checkBox,
                data.Result[i].Docname,
                data.Result[i].WorkOrderNo,
                data.Result[i].fdAssVin,
                Isreconcle,
                data.Result[i].statusText,
                data.Result[i].InvoiceNo,
                data.Result[i].UploadedOn,
                data.Result[i].fdWorkOrderVinId
                ]).draw();



                i = i + 1;
            });

            grdDocumentSearch.columns.adjust().draw();

        } else {
            showMessage("red", data.Description, "");
        }
    });


}

function selectall() {
    debugger;
    var grdDocumentSearch = $('#grdDocumentSearch').DataTable();
    var allPages = grdDocumentSearch.cells().nodes();

    if ($("#selectallcheckbx").is(":checked", true) == true) {

        $(allPages).find('input[type="checkbox"]').prop('checked', true);

    }

    if ($("#selectallcheckbx").is(":checked", true) == false) {

        $(allPages).find('input[type="checkbox"]').prop('checked', false);

    }


}

function downloadPods() {
    var docId = [];
    var grdDocumentSearch = $('#grdDocumentSearch').DataTable();
    var allPages = grdDocumentSearch.cells().nodes();

    debugger;
    for (i = 0; i < grdDocumentSearch.data().length; i++) {

        if ($(allPages).find('input[id="chk-' + i + '"]')[0].checked == true) {
            docId.push(grdDocumentSearch.data()[i][0]);

        }
    }
    if (docId.length > 0) {
        ajaxCaller("POST", "TMS/", JSON.stringify({ "DocId": docId }), "application/json", "Downloading zip").done(function (data) {
            if (data.Status == 1) {

            }
            else {
                showMessage("red", data.Description, "");
            }
        });
    }

}

function downloadPod() {
    debugger;
    var docId = [];
    var fileName = {};
    var grdDocumentSearch = $('#grdDocumentSearch').DataTable();
    var allPages = grdDocumentSearch.cells().nodes();

    if (grdDocumentSearch.data().length == 0) {
        showMessage("red", "No documents found");
        return;
    }
    debugger;
    for (i = 0; i < grdDocumentSearch.data().length; i++) {

        if ($(allPages).find('input[id="chk-' + i + '"]')[0].checked == true) {
            docId.push(grdDocumentSearch.data()[i][0]);

        }
    }


    if ($('#hdnSchValueId').val() != null && $('#hdnSchValueId').val() != '' && $('#hdnSchValueId').val() != undefined
        || $('#txtSchValue').val() != null && $('#txtSchValue').val() != '' && $('#txtSchValue').val() != undefined) {

        fileName = $('#txtSchValue').val();
    }
    //else if ($('#cobSchDocType').val()!=5) {
    //    showMessage("red", "Please enter " + $("#cobSchDocType option:selected").text());
    //    $('#cobSchDocType').focus();
    //    return;
    //}



    if (docId.length > 0) {
        var form = document.createElement("form");
        form.setAttribute("method", "POST");
        form.setAttribute("action", "Document/DownloadPodfiles");
        form.setAttribute("target", "DownloadPodfiles");

        var hiddenField = document.createElement("input");
        hiddenField.setAttribute("type", "hidden");
        hiddenField.setAttribute("name", "requestData");
        hiddenField.setAttribute("value", JSON.stringify({
            "DocId": docId,
            "file": fileName
        }));
        form.appendChild(hiddenField);

        document.body.appendChild(form);
        form.submit();
    }
    else {
        showMessage("red", "Please select document to download")
    }

}

function resetSearch() {
    debugger;
    var grdDocumentSearch = $('#grdDocumentSearch').DataTable();
    grdDocumentSearch.clear().draw();

    $('#txtSchValue').prop('disabled', 'disabled');

    $("#selectallcheckbx").prop('checked', false)
    $('#cobSchDocType').val(0);
    $('#hdnSchValueId').val("");

    $('#txtFromDate').val("");
    $('#txtToDate').val("");
    $('#txtSchVIN').val("");
    $('#hdnSchVinNo').val("");
}

function viewPods() {
    debugger;

    DocIds = [];
    VinsID = [];

    var grdDocumentSearch = $('#grdDocumentSearch').DataTable();
    $('#docTypeControlsContainer').hide();
    var allPages = grdDocumentSearch.cells().nodes();

    if (grdDocumentSearch.data().length == 0) {
        showMessage("red", "No documents found");
        return;
    }
    debugger;
    for (i = 0; i < grdDocumentSearch.data().length; i++) {

        if ($(allPages).find('input[id="chk-' + i + '"]')[0].checked == true) {
            DocIds.push(grdDocumentSearch.data()[i][0]);
            VinsID.push(grdDocumentSearch.data()[i][9]);
            currentIndex = 0;
        }
    }

    $('.nav-tabs a[href="#DocMgmnt_CreateUpdateTab"]').tab('show');
    $('#grdDocumentSearch').show();
    $('#cboDoctype').hide();
    nextPod();
}





function nextPod() {
    if (DocIds != null) {
        if (DocIds.length > 0) {
            if (currentIndex != null) {
                if (currentIndex == DocIds.length - 1) {
                    currentIndex = -1;
                }
                currentIndex = currentIndex + 1;
                var inputData = {};
                inputData.DocID = DocIds[currentIndex];
                //inputData.VinID = VinsID[currentIndex];
                $('#podDocImage').removeClass("imageViewerBorder");
                $('#podDocImage').addClass("imageLoader");
                $('#podDocImage').attr('src', "/images/loader.gif");
                ajaxCallerWithoutMessage("POST", "TMS/GetPodImageUrlAndDetails", JSON.stringify(inputData), "application/json").done(function (data) {
                    if (data.Status == 1) {
                        // seting hiden values for doc and vins
                        $('#hdnVinId').val(VinsID[currentIndex]);
                        $('#hdnDocId').val(DocIds[currentIndex]);
                        var grdMetaDocDetail = $('#grdMetaDocDetail').DataTable();
                        $('#docTypeControlsContainer').hide();
                        $('#grdMetaDocDetail').show();
                        grdMetaDocDetail.clear().draw();
                        $('#podDocImage').addClass("imageViewerBorder");
                        $('#podDocImage').removeClass("imageLoader");
                        $('#DocCriteria').html(data.Result[0].docname);
                        $('#Criteria').html(data.Result[0].criteria);
                        if (data.Result[0].IsReconcile === 1) {

                            $('#Criteria').addClass("podReconcile");
                            $('#Criteria').removeClass("podNotReconcile");
                        } else {
                            $('#Criteria').addClass("podNotReconcile");
                            $('#Criteria').removeClass("podReconcile");
                        }

                        $('#podDocImage').attr('src', data.Result[0].fdUrl);
                        data.Result
                        grdMetaDocDetail.column(0).visible(false);
                        $.each(data.Result, function (key, value) {
                            grdMetaDocDetail.row.add([
                            value.PKID,
                            value.fdKey,
                            value.fdValue

                            ]).draw();

                        });

                        grdMetaDocDetail.columns.adjust().draw();

                    } else {
                        showMessage("red", data.Description, "");
                    }
                });

            }
            else if (currentIndex == DocIds.length) {
                showMessage("red", "Reach To Limit");
                return;
            }
            else {
                showMessage("red", "No Pods Selected To View");
                return;
            }

        }
        else {
            showMessage("red", "No Pods Selected To View");
            return;
        }
    }
    else {
        showMessage("red", "No Pods Selected To View");
        return;
    }

}
function previousPod() {
    if (DocIds != null) {
        if (DocIds.length > 0) {
            if (currentIndex != null && currentIndex != '') {
                if (currentIndex == -1) {
                    currentIndex = DocIds.length;
                }
                currentIndex = currentIndex - 1;
                var inputData = {};

                inputData.DocID = DocIds[currentIndex];
                // inputData.VinID = VinsID[currentIndex];
                $('#podDocImage').removeClass("imageViewerBorder");
                $('#podDocImage').addClass("imageLoader");

                $('#podDocImage').attr('src', "/images/loader.gif");
                ajaxCallerWithoutMessage("POST", "TMS/GetPodImageUrlAndDetails", JSON.stringify(inputData), "application/json").done(function (data) {
                    if (data.Status == 1) {
                        $('#hdnVinId').val(VinsID[currentIndex]);
                        $('#hdnDocId').val(DocIds[currentIndex]);
                        var grdMetaDocDetail = $('#grdMetaDocDetail').DataTable();
                        grdMetaDocDetail.clear().draw();
                        $('#docTypeControlsContainer').hide();
                        $('#grdMetaDocDetail').show();
                        $('#podDocImage').addClass("imageViewerBorder");
                        $('#podDocImage').removeClass("imageLoader");
                        $('#Criteria').html(data.Result[0].criteria);
                        if (data.Result[0].IsReconcile === 1) {

                            $('#Criteria').addClass("podReconcile");
                            $('#Criteria').removeClass("podNotReconcile");
                        } else {
                            $('#Criteria').addClass("podNotReconcile");
                            $('#Criteria').removeClass("podReconcile");
                        }

                        $('#podDocImage').attr('src', data.Result[0].fdUrl);
                        data.Result
                        grdMetaDocDetail.column(0).visible(false);
                        $.each(data.Result, function (key, value) {
                            grdMetaDocDetail.row.add([
                            value.PKID,
                            value.fdKey,
                            value.fdValue
                            ]).draw();

                        });

                        grdMetaDocDetail.columns.adjust().draw();

                    } else {
                        showMessage("red", data.Description, "");
                    }
                });

            }
            else if (currentIndex == 0) {
                showMessage("red", "Can Not Preview.reach linit");
                return;
            }
            else {
                showMessage("red", "No Pods Selected To View");
                return;
            }

        }
        else {
            showMessage("red", "No Pods Selected To View");
            return;
        }
    }
    else {
        showMessage("red", "No Pods Selected To View");
        return;
    }

}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
function SerchOnBoxList(textBoxSerchId, serchButtonId, url, listId, inputFieldID, cboID, isComposite, searchOn) {
    debugger;
    var inputData = {};

    $("#" + textBoxSerchId + "," + "#" + serchButtonId + "").bind("click keyup", function (e) {

        $("#" + inputFieldID + "").val("");
        if ($("#" + textBoxSerchId + "").val().length >= 2 && $('#cobSchDocType').val() != 5 && $('#cobSchDocType').val() != 0) {

            var doctype = $("#" + searchOn + "").val();
            inputData.doctype = doctype;

            var keyword = $("#" + textBoxSerchId + "").val();
            //inputData.keyword = replaceStringForSearch(keyword, "");
            inputData.keyword = keyword;


            if (true) {
                debugger;
                ajaxCaller(
                  "POST",
                  url, JSON.stringify(inputData), "application/json", 'Loading Suggestions')
                  .done(function (data) {

                      if (data.Status == 1) {
                          if (data.Result.length > 0) {

                              showListData(data, textBoxSerchId, listId, inputFieldID, cboID, isComposite);
                          }
                          else {
                              var list = '';

                              list += '<li class="list-group-item list-search-items"   style="width:100%;">' +

                               '<div class="clearfix" ><span  >Nothing Found. Try Other Keywords</span></div>' +
                                 '</li>';
                              $("#" + listId + "").show();
                              $("#" + listId + "").empty();
                              $("#" + listId + "").append(list);


                          }
                      }
                      else {
                          showMessage("red", data.Description);
                      }
                  });
            } else {

            }


        } else {

            $(".listParent").hide();

            return;

        }
    });



}

function showListData(data, textBoxSerchId, listId, inputFieldID, cboID, isComposite) {
    debugger;
    var oResponse = data;

    if (oResponse.Status == 1) {
        var oResult = oResponse.Result;
        var list = '';
        var listIncrementer = 0;
        if (isComposite == false) {
            debugger;
            $.each(oResult, function (key, value) {
                var id = value.Item1;
                var name = value.Item2;
                var code = value.Item3;
                list += '<li class="list-group-item list-search-items" data-Id="' + id + '" data-Name="' + name + '" id="Code:' + code + '"  style="width:100%;">' +

                '<div class="clearfix" ><span onClick=selected(this,"' + textBoxSerchId + '","' + listId + '","' + inputFieldID + '","' + cboID + '") id="Code' + code + '">' + name + '</span></div>' +
                '</li>';
            });
        }
        else {
            $.each(oResult, function (key, value) {
                var id = value.Item1;
                var name = value.Item2;
                var code = value.Item3;
                list += '<li class="list-group-item list-search-items" data-Id="' + id + '" data-Name="' + name + '" id="Code:' + code + '"  style="width:100%;">' +

                '<div class="clearfix" ><span onClick=selected(this,"' + textBoxSerchId + '","' + listId + '","' + inputFieldID + ',' + cboID + '") id="Code' + code + '">' + name + '-' + code + '</span></div>' +
                '</li>';
            });
        }


        $("#" + listId + "").show();
        $("#" + listId + "").empty();
        $("#" + listId + "").append(list);
    }
    else {
        showMessage(oResponse.Description);
    }


}

function selected(id, textBoxSerchId, listId, inputFieldID, cboID, code) {

    debugger;

    $("#" + inputFieldID + "").val(id.parentNode.parentNode.getAttribute("data-id"));
    document.getElementById(textBoxSerchId).value = id.innerText;
    $("#" + listId + "").empty();

    if (cboID != null & cboID != '') {
        $("#" + cboID + "").val(code);
        $("#" + cboID + "").prop("disabled", true);
    }


}


var isItemEditing = null;
var restoreMetaData;
$('#grdMetaDocDetail').on("click", 'button.editPodText', function (e) {
    e.preventDefault();
    debugger;
    var grdMetaDocDetail = $('#grdMetaDocDetail').DataTable();
    var nRow = $(this).parents('tr')[0];
    $(nRow).addClass('selected');

    if (isItemEditing !== null) {
        showMessage("red", "Save Or Cancel")
    } else {
        isItemEditing = 1;
        editMetaRow(grdMetaDocDetail, nRow, nRow._DT_RowIndex);


    }
});

function editMetaRow(oTable, nRow, id) {
    debugger;
    var aData = oTable.data()[id];
    restoreMetaData = aData;
    var jqTds = $('>td', nRow);

    jqTds[1].innerHTML = ' <textarea class="form-control form-control input-sm" rows="20" cols="80">' + aData[2] + '</textarea>'


    jqTds[2].innerHTML = '<div class=row">'
                                 + '<div class="col-md-12 ">'
                                 + ' <button  class="btn btn-azure itemSave"><i class="fa fa-save"></i></button>'
                                 + ' </div>'
                                 + '<div>'
                                + '<div class=row">'
                                 + '  <div class="col-md-12">'
                                 + ' <button  class="btn btn-danger itemCancel"><i class="fa fa-times"></i></button>'
                                 + ' </div>'
                                 + ' </div>';

}

$('#grdMetaDocDetail').on("click", 'button.itemCancel', function (e) {
    debugger;
    e.preventDefault();

    var nRow = $(this).parents('tr')[0];

    var rowIndex = nRow._DT_RowIndex;

    var jqTds = $('>td', nRow);
    var grdMetaDocDetail = $('#grdMetaDocDetail').DataTable();

    grdMetaDocDetail.row(nRow).data(restoreMetaData).draw();

    isItemEditing = null;

});

function editMetaData() {
    var grdMetaDocDetail = $('#grdMetaDocDetail').DataTable();
    var getBody = $('#grdMetaDocDetail').children('tbody');
    restoreMetaData = grdMetaDocDetail.data();
    var jqTrs = $('>tr', getBody);
    debugger;
    if (jqTrs != null) {
        for (var i = 0; i < jqTrs.length; i++) {
            var jqTds = $('>td', jqTrs[i]);
            var aData = grdMetaDocDetail.data()[jqTrs[i]._DT_RowIndex];
            var pkey = grdMetaDocDetail.data()[jqTrs[i]._DT_RowIndex][0];

            jqTds[1].innerHTML = ' <textarea class="form-control form-control input-sm" id="podTextArea' + pkey + '" rows="20" cols="80">' + aData[2] + '</textarea>'
            isInEditMode = 1;
        }
    }
}

function updatePodText() {
    debugger;
    var grdMetaDocDetail = $('#grdMetaDocDetail').DataTable();
    var podData = {};

    if ($('#hdnDocId').val() == 0 || $('#hdnDocId').val() == undefined || $('#hdnDocId').val() == null || $('#hdnDocId').val() == '') {
        if (isInEditMode != 1) {
            if ($('#cboDoctype').val() == 0 || $('#cboDoctype').val() == '' || $('#cboDoctype').val() == undefined) {
                $('#cboDoctype').focus();
                showMessage('red', "Please Select Document Type");
                return false;
            }
            // that means document is being to upload  other wise it will update 
            var isValid = true;// this variable tells if all the validations passed by user;
            $('.isRequired').each(function () {
                if (this.value == "") {
                    isValid = false;
                    $(this).focus();
                    showMessage('red', "Please fill document field");
                    return false;
                }

            });


            if (isValid == false) {
                return false;
            }
            $('.number').each(function () {
                if (!this.value.match(/^\d+$/)) {
                    isValid = false;
                    $(this).focus();
                    showMessage('red', "Only Numeric value allowed for " + $(this).data("formcontrols"));
                    return false;
                }
            });



            if (isValid == false) {
                return false;
            } else {
                var formData = new FormData();
                var inputData = {};
                $('.docTypeFormControls').each(function () {
                    // inputData[$(this).data("formcontrols")] = this.value;
                    formData.append($(this).data("formcontrols"), this.value);
                });
                
                var uploadFiles = document.getElementById("txtDocumentFile").files;
                var file_data = uploadFiles[0];   // Getting the properties of file from file field


                if ($('#txtDocumentID').val() == 0 || $('#txtDocumentID').val() == undefined || $('#txtDocumentID').val() == null || $('#txtDocumentID').val() == '') {
                    if (document.getElementById("txtDocumentFile").value == "") {
                        showMessage("red", "Please select the file.", "");
                        return;
                    }
                }

                if ($('#txtDocumentID').val() == 0 || $('#txtDocumentID').val() == undefined || $('#txtDocumentID').val() == null || $('#txtDocumentID').val() == '') {
                    var check=file_data.name.split(".");
                    var chckpdf = check[check.length - 1];
                    chckpdf = chckpdf.toLowerCase();
                    if (chckpdf.toString().toUpperCase() != "PDF" && chckpdf.toString().toUpperCase() != "JPG" && chckpdf.toString().toUpperCase() != "JPEG" && chckpdf.toString().toUpperCase()!="PNG") {
                        showMessage("red", "please upload supported file only.", "");
                        return;
                    }

                }



                console.log(uploadFiles);
                // Creating object of FormData class
                formData.append("upLoad", file_data);
                formData.append("FileType", $("#cboDoctype").val());
                formData.append("hdnDocumentID", $("#txtDocumentID").val());


                console.log(formData);
                $.ajax({
                    url: "Document/UploadPOD",
                    dataType: 'json',
                    cache: false,
                    contentType: false,
                    processData: false,
                    data: formData,
                    beforeSend: function () {
                        showMessage("loading", "", "Uploading Document Please Wait....");
                    },
                    complete: function () {
                        $('.ajaxLoading').hide();
                    },
                    type: 'post',
                    success: function (data) {
                        if (data.Status == 1) {
                            showMessage("green", data.Description);
                            var oResult = data.Result;
                            // getUploadPod(data.Result[0].fdDocId);
                            $('#docDataControl').hide();
                            $('#txtDocumentID').val(oResult.DocumentID);
                            blob = converBase64toBlob(data.Result.ImageData, { type: 'image/png', encoding: 'utf-8' });

                            var blobURL = URL.createObjectURL(blob);
                            $('#QrCodeImg').attr('src', blobURL);

                        }

                        else {
                            showMessage("red", data.Description);
                        }
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        $('.ajaxLoading').hide();
                        console.log("Status: " + textStatus);
                        console.log("Status: " + XMLHttpRequest);
                        console.log("Message: " + thrownError);
                    }
                });
            }

        }
    }
    else {
        if (isInEditMode != 1) {
            showMessage('red', "Please make description in edit mode by clicking Value button");
            return false;
        }

        podData.DocId = $('#hdnDocId').val();
        podData.VinId = $('#hdnVinId').val();
        var podRequest = [];
        if (grdMetaDocDetail.data().length == 0) {
            showMessage('red', "Nothing To Update");
        }
        for (var i = 0; i < grdMetaDocDetail.data().length; i++) {
            var pod = {};
            pod.Pkey = grdMetaDocDetail.data()[i][0];
            pod.value = $('#podTextArea' + pod.Pkey).val();

            podRequest.push(pod);
        }
        podData.PodRequest = podRequest;
        ajaxCaller("POST",
                     "Document/UpdateMetaData", JSON.stringify(podData), "application/json", 'Updating Metadata')
                     .done(function (data) {
                         if (data.Status == 1) {
                             showMessage("green", data.Description);
                             var grdMetaDocDetail = $('#grdMetaDocDetail').DataTable();
                             var getBody = $('#grdMetaDocDetail').children('tbody');
                             var Result = data.Result;
                             if (Result != null) {
                                 if (Result.fdIsReconciled == 1) {
                                     $('#Criteria').html('<label style="font-weight:900"> Vin No:' + Result.fdAssVin + ' Is Reconciled ?: </label>'
                                         + '<label style="font-weight:900;font-size:medium">Yes</label>');
                                     $('#Criteria')


                                     $('#Criteria').addClass("podReconcile");
                                     $('#Criteria').removeClass("podNotReconcile");


                                 }
                                 else {
                                     $('#Criteria').html('<label style="font-weight:900"> Vin No:' + Result.fdAssVin + ' Is Reconciled ?: </label>'
                                        + '<label style="font-weight:900;font-size:medium">No</label>');
                                     $('#Criteria').addClass("podNotReconcile");
                                     $('#Criteria').removeClass("podReconcile");

                                 }
                             }
                             var jqTrs = $('>tr', getBody);
                             debugger;
                             if (jqTrs != null) {
                                 for (var i = 0; i < jqTrs.length; i++) {
                                     var jqTds = $('>td', jqTrs[i]);
                                     var aData = grdMetaDocDetail.data()[jqTrs[i]._DT_RowIndex];
                                     var pkey = grdMetaDocDetail.data()[jqTrs[i]._DT_RowIndex][0];

                                     jqTds[1].innerHTML = $('#podTextArea' + pkey).val();;

                                 }
                             }
                             isInEditMode = 0;
                         }
                         else {
                             showMessage("red", data.Description);
                         }
                     });
    }
}



function getUploadPod(input) {
    var inputData = {};
    inputData.DocID = input;
    $('#podDocImage').removeClass("imageViewerBorder");
    $('#podDocImage').addClass("imageLoader");
    $('#podDocImage').attr('src', "/images/loader.gif");
    ajaxCallerWithoutMessage("POST", "TMS/GetPodImageUrlAndDetails", JSON.stringify(inputData), "application/json").done(function (data) {
        if (data.Status == 1) {
            $('#docTypeControlsContainer').hide();
            $('#grdMetaDocDetail').show();
            var grdMetaDocDetail = $('#grdMetaDocDetail').DataTable();
            grdMetaDocDetail.clear().draw();
            $('#podDocImage').addClass("imageViewerBorder");
            $('#podDocImage').removeClass("imageLoader");
            $('#podDocImage').addClass("imageResize");
            $('#imageContainer').removeClass("imageDivZoom");

            $('#podDocImage').addClass("imageViewerBorder");
            $('#podDocImage').removeClass("imageLoader");
            if (data.Result[0].criteria != null) {
                $('#Criteria').html(data.Result[0].criteria);
            }
            else {
                $('#Criteria').html("");
            }
            $('#podDocImage').attr('src', data.Result[0].fdUrl);
            $('#openInNewTab').attr('href', data.Result[0].fdUrl);
            data.Result
            grdMetaDocDetail.column(0).visible(false);
            $.each(data.Result, function (key, value) {
                grdMetaDocDetail.row.add([
                value.PKID,
                value.fdKey,
                value.fdValue

                ]).draw();

            });

            grdMetaDocDetail.columns.adjust().draw();

        } else {
            showMessage("red", data.Description, "");
        }
    });

}


function Reconcile() {
    var input = {};
    $('#hdnVinId').val(VinsID[currentIndex]);
    $('#hdnDocId').val(DocIds[currentIndex]);
    input.DocId = $('#hdnDocId').val();
    input.VinId = $('#hdnVinId').val();
    ajaxCaller("POST",
                "TMS/executePodLink", JSON.stringify(input), "application/json", 'Loading Suggestions')
                .done(function (data) {
                    if (data.Status == 1) {
                        showMessage("green", data.Description);
                    }
                    else {
                        showMessage("red", data.Description);
                    }
                });
}


function reset() {
    var grdMetaDocDetail = $('#grdMetaDocDetail').DataTable();
    grdMetaDocDetail.clear().draw();
    $('#grdMetaDocDetail').hide();
    $('#docTypeControlsContainer').show();
    $('#txtDocumentFile').val("");
    isInEditMode = 0;

}

function saveresetdoc(msg) {
    if (msg == 1) {


        $('#txtDocumentID').val('');
        $('#docDataControl').hide();
        // var grdMetaDocDetail = $('#grdMetaDocDetail').DataTable();
        // grdMetaDocDetail.clear().draw();
        $('#txtDocumentFile').val('')
        $('#docTypeControlsContainer').show();
        $('#podDocImage').attr('src', '');
        $('#cboDoctype').show();
        $('#grdMetaDocDetail').hide();
        $('#Criteria').val("");
        $('#cboDoctype').val(0);
        $('#formControlsContainer').empty();
        $('#podDocImage').attr('src', "img/preview.jpg");
        $('#hdnVinId').val("");
        $('#hdnDocId').val("");
        $('#hdnVinId').val("");
        $('#fileInput').text('Click here to select the file :');
        $('#Criteria').html("");
        $('#DocCriteria').html("");
        $('#grdMetaDocDetail').hide();
        $('#txtDocumentFile').prop('disabled', false);
        $('#txtVinNo').prop('disabled', false);
        currentIndex = 0;
        DocIds = [];
        $('#podDocImage').hide();
        $('#txtDocumentID').val("");

        // location.reload(true);
        //window.location.reload();
        window.location.href = "Document";


    }
    else {
        $('#txtDocumentID').val('');
        $('#docDataControl').hide();
        // var grdMetaDocDetail = $('#grdMetaDocDetail').DataTable();
        // grdMetaDocDetail.clear().draw();
        $('#txtDocumentFile').val('')
        $('#docTypeControlsContainer').show();
        $('#podDocImage').attr('src', '');
        $('#cboDoctype').show();
        $('#grdMetaDocDetail').hide();
        $('#Criteria').val("");
        $('#cboDoctype').val(0);
        $('#formControlsContainer').empty();
        $('#podDocImage').attr('src', "img/preview.jpg");
        $('#hdnVinId').val("");
        $('#hdnDocId').val("");
        $('#hdnVinId').val("");
        $('#fileInput').text('Click here to select the file :');
        $('#Criteria').html("");
        $('#DocCriteria').html("");
        $('#grdMetaDocDetail').hide();
        $('#txtDocumentFile').prop('disabled', false);
        $('#txtVinNo').prop('disabled', false);
        currentIndex = 0;
        DocIds = [];
        $('#podDocImage').hide();
        $('#txtDocumentID').val("");

        // location.reload(true);
        //window.location.reload();
        window.location.href = "Document";

    }
   }


function resize() {
    $('#podDocImage').addClass("imageResize");
    $('#imageContainer').removeClass("imageDivZoom");
}
function zoom() {
    $('#podDocImage').removeClass("imageResize");
    $('#imageContainer').addClass("imageDivZoom");
}

function SearchData() {
    var data = {};

    if ($("#txtschTitle").val() != "" && $("#txtschTitle").val() != undefinded) {
        data["txtschTitle"] = $("#txtschTitle").val();
    }

    if ($("#txtschContent").val() != "" && $("#txtschContent").val() != undefinded) {
        data["txtschContent"] = $("#txtschContent").val();
    }
    if ($("#StartDate").val() != "" && $("#StartDate").val() != undefinded) {
        data["StartDate"] = $("#StartDate").val();
    }
    if ($("#EndDate").val() != "" && $("#EndDate").val() != undefinded) {
        data["EndDate"] = $("#EndDate").val();
    }

    ajaxCaller("POST",
                "TMS/executePodLink", JSON.stringify(data), "application/json", 'Loading Suggestions')
                .done(function (data) {

                    if (data.Status == 1) {
                        showMessage("green", data.Description);
                    }
                    else {
                        showMessage("red", data.Description);
                    }

                });

}

function searchDoc() {
    var input = {};
    if ($('#txtschTitle').val() != "" && $('#txtschTitle').val() != undefined) {
        input["txtschTitle"] = $('#txtschTitle').val();
    }
    if ($('#txtschContent').val() != "" && $('#txtschContent').val() != undefined) {
        input["txtschContent"] = $('#txtschContent').val();
    }
    input["NoDAta"] = "undefined";
    ajaxCaller("POST",
                "Document/SerchDocument", JSON.stringify(input), "application/json", 'Loading Results')
                .done(function (data) {
                    if (data.Status == 1) {

                        var grdItemDataTable = $('#grdDocSearch').DataTable();
                        grdItemDataTable.clear().draw();
                        $.each(data.Result, function (key, value) {

                            //grdItemDataTable.column(5).visible(false);
                            grdItemDataTable.row.add([value.fdDocId, value.fdDocTitle, value.DocClass, value.user, value.fdCreatedOn, value.fdFileUrl
                            ]).draw();
                            grdItemDataTable.columns.adjust().draw();
                        });
                        showMessage("green", data.Description);
                        $("body").find(".query1").removeClass("visible");
                    }
                    else {
                        showMessage("red", data.Description);
                    }
                });
}


function searchDocumentsUsingSearchBar() {
    debugger;
    var inputData = {};
    var searchInputKeyword = $("#searchKeyword").val();
    inputData.searchInputKeyword = searchInputKeyword;
    ajaxCaller("POST", "Document/searchDocumentsUsingSearchBar", JSON.stringify(inputData), "application/json", "Please wait!").done(function (data) {
        debugger;
        if (data.Status == 1) {

            var grdItemDataTable = $('#grdDocSearch').DataTable();
            grdItemDataTable.clear().draw();
            $.each(data.Result, function (key, value) {

                //grdItemDataTable.column(5).visible(false);
                grdItemDataTable.row.add([value.fdDocId, value.fdDocTitle, value.DocClass, value.user, value.fdCreatedOn, value.fdFileUrl
                ]).draw();
                grdItemDataTable.columns.adjust().draw();
            });
            showMessage("green", data.Description);
            $("body").find(".query1").removeClass("visible");
        }
        else {
            showMessage("red", data.Description);
        }
    });
}

//Active and inActive selected menu tab

function documentTag() {
    $('#documentSearchTag a').removeClass('active');
    $('#documentChartTag a').removeClass('active');
    $('#documentClassTag a').removeClass('active');
    $('#documentTag a').addClass('active');
    $('#documentAuditTag a').removeClass('active');
    $('#userMasterTag a').removeClass('active');
}
function converBase64toBlob(content, contentType) {
    contentType = contentType || '';
    var sliceSize = 512;
    var byteCharacters = window.atob(content); //method which converts base64 to binary
    var byteArrays = [
    ];
    for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
        var slice = byteCharacters.slice(offset, offset + sliceSize);
        var byteNumbers = new Array(slice.length);
        for (var i = 0; i < slice.length; i++) {
            byteNumbers[i] = slice.charCodeAt(i);
        }
        var byteArray = new Uint8Array(byteNumbers);
        byteArrays.push(byteArray);
    }
    var blob = new Blob(byteArrays, {
        type: contentType,
        name: 'sample.pdf'
    }); //statement which creates the blob
    return blob;
}


function hidescrollbar1() {
    var elem = document.getElementById("podDocImage");
    //elem.contentDocument.body.style.overflow = "hidden";
    elem.contentDocument.body.style.height = "110%";
}

function hidescrollbar() {
    var elem = document.getElementById("podDocImage1");
    //elem.contentDocument.body.style.overflow = "hidden";
    elem.contentDocument.body.style.height = "110%";
}


function geturlUploadData() {
   
    debugger;
    var vars = [], hash;
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1);
    hashes = hashes.split('&');
    for (var i = 0; i < hashes.length; i++) {
        hash = hashes[i].split('=');
    };
    if (hash[1] == undefined) {
        return;
    }
    if (!hash[1].match(/^\d+$/)) {
        return;
    }
    var docID = hash[1];

    ajaxCaller(
                 "POST",
                 'Document/getUploadDocumentdetails', docID, "application/json", 'Loading Suggestions')
                 .done(function (data) {
                     debugger;
                     if (data.Status == 1) {
                         if (data.Result.DocumentData.length > 0) {

                             $("#btnUploadDocument").show();
                             

                             $('.doc_property').show();
                             $('#btnUpdatePodText').show();
                            // $('#btnRestet').show();

                             $('#btnViewHistory').show();
                             $('#btnArchive').show();
                             $('#btnCheckOut').show();


                             var oResult = data.Result.DocumentData;
                             var FileExtention = data.Result.FileExtention;
                             $('#cboDoctype').val(oResult[0].fdDocType).prop('disabled', 'disabled');
                             // $('#txtDocumentFile').prop('disabled', 'disabled')

                             //document.getElementById("txtDocumentFile").disabled = true;
                             // document.getElementById("txtDocumentFile").hidden = true;

                             $('input[id^=txtDocumentFile]').hide();

                             $('#cboDoctype').hide();

                             $('#uploadfile').hide();
                             $('#NewDocument').hide();

                             $('#EditDocument').show();


                             $('#lbltitle').text(oResult[0].fdDocTitle);
                             $('#lblDocumentClass').text(oResult[0].DocClass);
                             $('#lblVersion').text(oResult[0].fdVersion);
                             $('#lblCreatedOn').text(oResult[0].fdCreatedOn);
                             $('#lblCreatedBy').text(oResult[0].fdUserName);

                             if (FileExtention.toUpperCase() != ".JPG" && FileExtention.toUpperCase() != ".JPEG" && FileExtention.toUpperCase() != ".PNG" && FileExtention.toUpperCase() != ".PDF")
                             {
                                 showMessage("red", "unsupported file type to view", "");
                                 return;
                             }

                             if (FileExtention.toUpperCase() != ".PDF") {
                                 if (FileExtention.toUpperCase() == ".PNG") {
                                     blob = converBase64toBlob(data.Result.ImageData, 'image/png');
                                 }
                                 else if (FileExtention.toUpperCase() == ".JPG" || FileExtention.toUpperCase() == ".JPEG") {
                                     blob = converBase64toBlob(data.Result.ImageData, 'image/jpeg');
                                 }

                             }
                             else {
                                 blob = converBase64toBlob(data.Result.ImageData, 'application/pdf');
                             }
                             var blobURL = URL.createObjectURL(blob);



                             $('#podDocImage').attr('src', blobURL + "#view=fitH");

                             if (data.Result.QRCodeImageData != null && data.Result.QRCodeImageData != "") {
                                 blob1 = converBase64toBlob(data.Result.QRCodeImageData, { type: 'image/png', encoding: 'utf-8' });

                                 var blobURL1 = URL.createObjectURL(blob1);
                                 $('#QrCodeImg').attr('src', blobURL1);
                             }


                             $('#podDocImage').show();
                             $('#txtDocumentID').val(oResult[0].fdDocId);

                             $('#docTypeControlsContainer').show();
                             if (data.Result != null) {
                                 $('#formControlsContainer').empty();
                                 $('#docDataControl').hide();
                                 var formFieldHTML, formControlHTML;
                                 var itrforcontrol = 0;
                                 for (var itrtr = 0; itrtr < data.Result.DocumentData.length; itrtr++) {
                                     itrforcontrol = itrforcontrol + 1;
                                     // getting field
                                     if (itrforcontrol == 2) {
                                         formControlHTML = formControlHTML + '<div class="col-md-6"> <div class="group">';
                                         if (data.Result.DocumentData[itrtr].dcColumnType == "T" && data.Result.DocumentData[itrtr].isRequired == "1") {

                                             var TextValue = "";
                                             TextValue = data.Result.DocumentData[itrtr].fdValue;
                                             if (TextValue == null || TextValue == "") {
                                                 TextValue = data.Result.DocumentData[itrtr].defaultValue;
                                             }

                                             formControlHTML = formControlHTML + ' <input type="text" autocomplete="off"  value="' + TextValue + '" class="isRequired docTypeFormControls" data-formcontrols="' + data.Result.DocumentData[itrtr].dcColumnName + '"  data-formcontrolid="' + data.Result.DocumentData[itrtr].dcid + '"required >'
                                                       + ' <span class="highlight"></span>'
                                                       + ' <span class="bar"></span>'
                                                       + ' <label>' + data.Result.DocumentData[itrtr].dcColumnName + '<span class="asterisk">*</span></label>'
                                                       + '</div>'
                                                       + ' </div>';


                                         }
                                         else if (data.Result.DocumentData[itrtr].dcColumnType == "T") {

                                             var TextValue = "";
                                             TextValue = data.Result.DocumentData[itrtr].fdValue;
                                             if (TextValue == null || TextValue == "") {
                                                 TextValue = data.Result.DocumentData[itrtr].defaultValue;
                                             }

                                             formControlHTML = formControlHTML + ' <input type="text" autocomplete="off"  value="' + TextValue + '" class=" docTypeFormControls" data-formcontrols="' + data.Result.DocumentData[itrtr].dcColumnName + '"  data-formcontrolid="' + data.Result.DocumentData[itrtr].dcid + '"required >'
                                                     + ' <span class="highlight"></span>'
                                                     + ' <span class="bar"></span>'
                                                     + ' <label>' + data.Result.DocumentData[itrtr].dcColumnName + '</label>'
                                                     + '</div>'
                                                     + ' </div>';

                                         }
                                         else if (data.Result.DocumentData[itrtr].dcColumnType == "I" && data.Result.DocumentData[itrtr].isRequired == "1") {
                                             var TextValue = "";
                                             TextValue = data.Result.DocumentData[itrtr].fdValue;
                                             if (TextValue == null || TextValue == "") {
                                                 TextValue = data.Result.DocumentData[itrtr].defaultValue;
                                             }

                                             formControlHTML = formControlHTML + ' <input type="number" value="' + TextValue + '" class="docTypeFormControls isRequired number " data-formcontrols="' + data.Result.DocumentData[itrtr].dcColumnName + '"  data-formcontrolid="' + data.Result.DocumentData[itrtr].dcid + '"required >'
                                                     + ' <span class="highlight"></span>'
                                                     + ' <span class="bar"></span>'
                                                     + ' <label>' + data.Result.DocumentData[itrtr].dcColumnName + '<span class="asterisk">*</span></label>'
                                                     + '</div>'
                                                     + ' </div>';


                                         }
                                         else if (data.Result.DocumentData[itrtr].dcColumnType == "I") {

                                             var TextValue = "";
                                             TextValue = data.Result.DocumentData[itrtr].fdValue;
                                             if (TextValue == null || TextValue == "") {
                                                 TextValue = data.Result.DocumentData[itrtr].defaultValue;
                                             }

                                             formControlHTML = formControlHTML + ' <input type="number" value="' + TextValue + '" class="docTypeFormControls number " data-formcontrols="' + data.Result.DocumentData[itrtr].dcColumnName + '"  data-formcontrolid="' + data.Result.DocumentData[itrtr].dcid + '"required >'
                                                    + ' <span class="highlight"></span>'
                                                    + ' <span class="bar"></span>'
                                                    + ' <label>' + data.Result.DocumentData[itrtr].dcColumnName + '</label>'
                                                    + '</div>'
                                                    + ' </div>';


                                         }


                                         else if (data.Result.DocumentData[itrtr].dcColumnType == "D" && data.Result.DocumentData[itrtr].isRequired == "1") {
                                             var TextValue = "";
                                             TextValue = data.Result.DocumentData[itrtr].fdValue;
                                             if (TextValue == null || TextValue == "") {
                                                 TextValue = data.Result.DocumentData[itrtr].defaultValue;
                                             }

                                             formControlHTML = formControlHTML + ' <input type="text" value="' + TextValue + '" class="docTypeFormControls isRequired date_picker " data-formcontrols="' + data.Result.DocumentData[itrtr].dcColumnName + '"  data-formcontrolid="' + data.Result.DocumentData[itrtr].dcid + '"required >'
                                                     + ' <span class="highlight"></span>'
                                                     + ' <span class="bar"></span>'
                                                     + ' <label>' + data.Result.DocumentData[itrtr].dcColumnName + '<span class="asterisk">*</span></label>'
                                                     + '</div>'
                                                     + ' </div>';


                                         }
                                         else if (data.Result.DocumentData[itrtr].dcColumnType == "D") {
                                             var TextValue = "";
                                             TextValue = data.Result.DocumentData[itrtr].fdValue;
                                             if (TextValue == null || TextValue == "") {
                                                 TextValue = TextValue = data.Result.DocumentData[itrtr].defaultValue;
                                             }

                                             formControlHTML = formControlHTML + ' <input type="text" value="' + TextValue + '" class="docTypeFormControls date_picker " data-formcontrols="' + data.Result.DocumentData[itrtr].dcColumnName + '"  data-formcontrolid="' + data.Result.DocumentData[itrtr].dcid + '"required >'
                                                    + ' <span class="highlight"></span>'
                                                    + ' <span class="bar"></span>'
                                                    + ' <label>' + data.Result.DocumentData[itrtr].dcColumnName + '</label>'
                                                    + '</div>'
                                                    + ' </div>';


                                         }


                                     } else {
                                         formControlHTML = '<div class="col-md-6"> <div class="group">'
                                         if (data.Result.DocumentData[itrtr].dcColumnType == "T" && data.Result.DocumentData[itrtr].isRequired == "1") {
                                             var TextValue = "";
                                             TextValue = data.Result.DocumentData[itrtr].fdValue;
                                             if (TextValue == null || TextValue == "") {
                                                 TextValue = data.Result.DocumentData[itrtr].defaultValue;
                                             }


                                             formControlHTML = formControlHTML + ' <input type="text" autocomplete="off"  value="' + TextValue + '" class="isRequired docTypeFormControls" data-formcontrols="' + data.Result.DocumentData[itrtr].dcColumnName + '"  data-formcontrolid="' + data.Result.DocumentData[itrtr].dcid + '"required >'
                                                       + ' <span class="highlight"></span>'
                                                       + ' <span class="bar"></span>'
                                                       + ' <label>' + data.Result.DocumentData[itrtr].dcColumnName + '<span class="asterisk">*</span></label>'
                                                       + '</div>'
                                                       + ' </div>';

                                         }
                                         else if (data.Result.DocumentData[itrtr].dcColumnType == "T") {
                                             var TextValue = "";
                                             TextValue = data.Result.DocumentData[itrtr].fdValue;
                                             if (TextValue == null || TextValue == "") {
                                                 TextValue = TextValue = data.Result.DocumentData[itrtr].defaultValue;
                                             }

                                             formControlHTML = formControlHTML + ' <input type="text" autocomplete="off"  value="' + TextValue + '" class=" docTypeFormControls" data-formcontrols="' + data.Result.DocumentData[itrtr].dcColumnName + '"  data-formcontrolid="' + data.Result.DocumentData[itrtr].dcid + '"required >'
                                                     + ' <span class="highlight"></span>'
                                                     + ' <span class="bar"></span>'
                                                     + ' <label>' + data.Result.DocumentData[itrtr].dcColumnName + '</label>'
                                                     + '</div>'
                                                     + ' </div>';

                                         }
                                         else if (data.Result.DocumentData[itrtr].dcColumnType == "I" && data.Result.DocumentData[itrtr].isRequired == "1") {
                                             var TextValue = "";
                                             TextValue = data.Result.DocumentData[itrtr].fdValue;
                                             if (TextValue == null || TextValue == "") {
                                                 TextValue = data.Result.DocumentData[itrtr].defaultValue;
                                             }


                                             formControlHTML = formControlHTML + ' <input type="number" value="' + TextValue + '" class="docTypeFormControls isRequired number " data-formcontrols="' + data.Result.DocumentData[itrtr].dcColumnName + '"  data-formcontrolid="' + data.Result.DocumentData[itrtr].dcid + '"required >'
                                                     + ' <span class="highlight"></span>'
                                                     + ' <span class="bar"></span>'
                                                     + ' <label>' + data.Result.DocumentData[itrtr].dcColumnName + '<span class="asterisk">*</span></label>'
                                                     + '</div>'
                                                     + ' </div>';


                                         }
                                         else if (data.Result.DocumentData[itrtr].dcColumnType == "I") {
                                             var TextValue = "";
                                             TextValue = data.Result.DocumentData[itrtr].fdValue;
                                             if (TextValue == null || TextValue == "") {
                                                 TextValue = data.Result.DocumentData[itrtr].defaultValue;
                                             }


                                             formControlHTML = formControlHTML + ' <input type="number" value="' + TextValue + '" class="docTypeFormControls number " data-formcontrols="' + data.Result.DocumentData[itrtr].dcColumnName + '"  data-formcontrolid="' + data.Result.DocumentData[itrtr].dcid + '"required >'
                                                    + ' <span class="highlight"></span>'
                                                    + ' <span class="bar"></span>'
                                                    + ' <label>' + data.Result.DocumentData[itrtr].dcColumnName + '</label>'
                                                    + '</div>'
                                                    + ' </div>';


                                         }

                                         else if (data.Result.DocumentData[itrtr].dcColumnType == "D" && data.Result.DocumentData[itrtr].isRequired == "1") {
                                             var TextValue = "";
                                             TextValue = data.Result.DocumentData[itrtr].fdValue;
                                             if (TextValue == null || TextValue == "") {
                                                 TextValue = data.Result.DocumentData[itrtr].defaultValue;
                                             }


                                             formControlHTML = formControlHTML + ' <input type="text" value="' + TextValue + '" class="docTypeFormControls isRequired date_picker " data-formcontrols="' + data.Result.DocumentData[itrtr].dcColumnName + '"  data-formcontrolid="' + data.Result.DocumentData[itrtr].dcid + '"required >'
                                                     + ' <span class="highlight"></span>'
                                                     + ' <span class="bar"></span>'
                                                     + ' <label>' + data.Result.DocumentData[itrtr].dcColumnName + '<span class="asterisk">*</span></label>'
                                                     + '</div>'
                                                     + ' </div>';


                                         }
                                         else if (data.Result.DocumentData[itrtr].dcColumnType == "D") {
                                             var TextValue = "";
                                             TextValue = data.Result.DocumentData[itrtr].fdValue;
                                             if (TextValue == null || TextValue == "") {
                                                 TextValue = data.Result.DocumentData[itrtr].defaultValue;
                                             }


                                             formControlHTML = formControlHTML + ' <input type="text" value="' + TextValue + '" class="docTypeFormControls date_picker" data-formcontrols="' + data.Result.DocumentData[itrtr].dcColumnName + '"  data-formcontrolid="' + data.Result.DocumentData[itrtr].dcid + '"required >'
                                                    + ' <span class="highlight"></span>'
                                                    + ' <span class="bar"></span>'
                                                    + ' <label>' + data.Result.DocumentData[itrtr].dcColumnName + '</label>'
                                                    + '</div>'
                                                    + ' </div>';


                                         }
                                     }

                                     //formControlHTML1 = '<div class="col-md-6"> <div class="group">'

                                     //formControlHTML1 = formControlHTML1 + ' <input type="text" autocomplete="off"  value=' + data.Result[0].fdStatus + ' class=" docTypeFormControls" data-formcontrols="Status"  data-formcontrolid="' + data.Result[0].fdStatus + '"required >'
                                     //                + ' <span class="highlight"></span>'
                                     //                + ' <span class="bar"></span>'
                                     //                + ' <label>Status</label>'
                                     //                + '</div>'
                                     //                + ' </div>';

                                     //formControlHTML2 = '<div class="col-md-6"> <div class="group">'
                                     //formControlHTML2 = formControlHTML2 + ' <input type="number" value=' + data.Result[itrtr].fdValue + ' class="docTypeFormControls number " data-formcontrols="' + data.Result[itrtr].dcColumnName + '"  data-formcontrolid="' + data.Result[itrtr].dcid + '"required >'
                                     //               + ' <span class="highlight"></span>'
                                     //               + ' <span class="bar"></span>'
                                     //               + ' <label>Version</label>'
                                     //               + '</div>'
                                     //               + ' </div>';

                                     //  $('#txtVersion').val(data.Result[0].fdVersion);
                                     //document.getElementById("txtVersion").disabled = true;


                                     if (itrforcontrol == 2 || itrtr + 1 == data.Result.DocumentData.length) {


                                         $('#formControlsContainer').append('<div class="row">' + formControlHTML + '</div>');
                                         // $('#formControlsContainer').append('<div class="row">' + formControlHTML1 +'</div>');
                                         itrforcontrol = 0;
                                     }
                                     //$('#formControlsContainer').append('<div class="row ">' + formFieldHTML + formControlHTML + '</div>');
                                     //$('.dateTimePickerFormControl').datetimepicker({
                                     //    format: 'DD-MMM-YYYY'
                                     //});


                                 }
                                 $('.date_picker').datepicker({
                                     format: "dd/mm/yyyy"
                                 }).on('change', function () {
                                     debugger;
                                     $('.datepicker').hide();
                                 });
                             }



                         }
                         else {

                         }
                     }
                     else {
                         showMessage("red", data.Description);
                     }
                 });
   
}


    
function saveArchivedoc() {
    var docid = $('#txtDocumentID').val();


    bootbox.confirm({
        message: "Are you sure? You want to archive document.<br>Note- Archive Document can not be retrive with out admin help.",
        buttons: {
            confirm: {
                label: 'Yes',
                className: 'btn-danger'
            },
            cancel: {
                label: 'No',
                className: 'btn-primary'
            }
        },
        callback: function (result) {
            if (result == true) {
                if (docid == null || docid == 0) {

                    showMessage("green", "Select Any Document")
                    return;
                }
                var inputData = {};

                inputData.docID = docid;
                ajaxCaller("POST", "Document/saveArchiveDocument", JSON.stringify(inputData), "application/json", "Please wait!").done(function (data) {
                    debugger;
                    if (data.Status == 1) {

                        showMessage("green", data.Description);
                        $('#btnArchive').hide();
                        $('#btnCheckOut').hide();
                        $('#btnCheckIn').hide();
                        $('#btnUpdatePodText').hide();

                    }
                    else {
                        showMessage("red", data.Description);
                    }
                });
            }
        }
    }).find("div.modal-body").addClass("ArchiveBootBoxHeight");

    //bootbox.confirm("Are you sure? You want to archive document.<br>Note- Archive Document can not be retrive with out admin help.", function (result) {
    //            if (result == true) {
    //                if (docid == null || docid == 0) {

    //                    showMessage("green", "Select Any Document")
    //                    return;
    //                }
    //                ajaxCaller("POST", "/Document/saveArchiveDocument", docid, "application/json", "Please wait!").done(function (data) {
    //                    debugger;
    //                    if (data.Status == 1) {

    //                        showMessage("green", data.Description);
    //                        $('#btnArchive').hide();
    //                        $('#btnCheckOut').hide();
    //                        $('#btnCheckIn').hide();
    //                        $('#btnUpdatePodText').hide();

    //                    }
    //                    else {
    //                        showMessage("red", data.Description);
    //                    }
    //                });



    //            }
    //        });

    //ajaxCaller("POST", "/Document/saveArchiveDocument", docid, "application/json", "Please wait!").done(function (data) {
    //    debugger;
    //    if (data.Status == 1) {

    //        showMessage("green", data.Description);
    //        $('#btnArchive').hide();
    //        $('#btnCheckOut').hide();
    //        $('#btnCheckIn').hide();
    //        $('#btnUpdatePodText').hide();

    //    }
    //    else {
    //        showMessage("red", data.Description);
    //    }
    //});


}

function closeModal() {
    $('#myModals').hide();
}

function ViewDocHistory() {

    var docid = $('#txtDocumentID').val();
    ajaxCaller("POST", "Document/getviewDocumenthistory", docid, "application/json", "Please wait!").done(function (data) {
        debugger;
        if (data.Status == 1) {

            var grdDocViewDetails = $('#grdDocViewDetails').DataTable();
            grdDocViewDetails.clear().draw();
            grdDocViewDetails.column(0).visible(false);
            debugger;
            $.each(data.Result, function (key, value) {
                var vlfddoc;

                //if (value.fdDocTitle.includes(":") || value.fdDocTitle.includes("\\")) {
                //    vlfddoc = value.fdDocTitle.split("\\")
                //    value.fdDocTitle = vlfddoc[vlfddoc.length - 1];
                //};
                grdDocViewDetails.row.add([value.fdDocId, value.fdDocTitle, value.lastRevison, value.fdVersion, value.fdUserName, value.fdCreatedOn, value.DocStatus,
                    '<a style="cursor: pointer;" title="View Detail" onClick="viewDoc(' + value.fdDocId + ')"><i class="fa fa-eye"></i></a>'

                ]).draw();

                grdDocViewDetails.columns([0]).order('desc').draw();
                grdDocViewDetails.columns.adjust().draw();


            });


            showMessage("green", data.Description);

            $('#myModals').show();


        }
        else {
            showMessage("red", data.Description);
        }
    });

}

function UploadDocumentNewVersion() {
    // document.getElementById("txtDocumentFile").disabled = false;
    $('#txtDocumentFile').click();
}


$("#txtDocumentFile").on('change', function () {
    //  document.getElementById("txtDocumentFile").disabled = true;

    var grdMetaDocDetail = $('#grdMetaDocDetail').DataTable();
    var podData = {};

    if ($('#txtDocumentID').val() != 0 && $('#txtDocumentID').val() != undefined && $('#txtDocumentID').val() != null && $('#txtDocumentID').val() != '') {
        if (isInEditMode != 1) {
            if ($('#cboDoctype').val() == 0 || $('#cboDoctype').val() == '' || $('#cboDoctype').val() == undefined) {
                $('#cboDoctype').focus();
                showMessage('red', "Please Select Document Type");
                return false;
            }
            // that means document is being to upload  other wise it will update 
            var isValid = true;// this variable tells if all the validations passed by user;
            $('.isRequired').each(function () {
                if (this.value == "") {
                    isValid = false;
                    $(this).focus();
                    showMessage('red', "Please Enter " + $(this).data("formcontrols"));
                    return false;
                }
            });

            if (isValid == false) {
                return false;
            }

            $('.number').each(function () {
                if (!this.value.match(/^\d+$/)) {
                    isValid = false;
                    $(this).focus();
                    showMessage('red', "Only Numeric value allowed for " + $(this).data("formcontrols"));
                    return false;
                }
            });

            if (isValid == false) {
                return false;
            } else {
                var formData = new FormData();
                var inputData = {};
                $('.docTypeFormControls').each(function () {
                    // inputData[$(this).data("formcontrols")] = this.value;
                    formData.append($(this).data("formcontrols"), this.value);
                });
                var uploadFiles = document.getElementById("txtDocumentFile").files;
                var file_data = uploadFiles[0];   // Getting the properties of file from file field

                if (document.getElementById("txtDocumentFile").value == "") {
                    showMessage("red", "Please select the file.", "");
                    return;
                }
                debugger;

                var chckpdf = file_data.name.split(".")[1];
                chckpdf = chckpdf.toLowerCase();
                if (chckpdf.toString().toUpperCase() != "PDF" && chckpdf.toString().toUpperCase() != "JPG" && chckpdf.toString().toUpperCase() != "JPEG" && chckpdf.toString().toUpperCase() != "PNG") {
                    showMessage("red", "Upload only pdf,jpg,jpeg and png file.", "");
                    return;
                }

                console.log(uploadFiles);
                // Creating object of FormData class
                formData.append("upLoad", file_data);
                formData.append("FileType", $("#cboDoctype").val())
                formData.append("hdnDocumentID", $("#txtDocumentID").val())
                console.log(formData);
                $.ajax({
                    url: "Document/UploadNewDocument",
                    dataType: 'json',
                    cache: false,
                    contentType: false,
                    processData: false,
                    data: formData,
                    beforeSend: function () {
                        showMessage("loading", "Please wait...", "Uploading Document Please Wait....");
                    },
                    complete: function () {
                        $('.ajaxLoading').hide();
                    },
                    type: 'post',
                    success: function (data) {
                        if (data.Status == 1) {
                            showMessage("green", data.Description);

                            $('#docDataControl').hide();

                            $('#lblVersion').text("");
                            $('#txtDocumentID').val("");
                            var docid = data.Result.DocumentID;
                            var version = data.Result.DocumentVersion;
                            $('#lblVersion').text(version);
                            $('#txtDocumentID').val(docid);
                            blob = converBase64toBlob(data.Result.ImageData, { type: 'image/png', encoding: 'utf-8' });

                            var blobURL = URL.createObjectURL(blob);
                            $('#QrCodeImg').attr('src', blobURL);
                        }

                        else {
                            showMessage("red", data.Description);
                        }
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        $('.ajaxLoading').hide();
                        console.log("Status: " + textStatus);
                        console.log("Status: " + XMLHttpRequest);
                        console.log("Message: " + thrownError);
                    }
                });
            }

        }
    }


});




function viewDoc(DocId) {
    debugger;

    var docDetail = {}
    docDetail.docId = DocId;
    docDetail.docName = "";
    docDetail.docClass = "";

    ajaxCaller("POST", "Document/GetDocumentDetails", JSON.stringify(docDetail), "application/json", "Please wait ! Getting Document Details").done(function (data) {
        if (data.Status == 1) {
            debugger;
            var html = "";
            var oResult = data.Result;


            if (oResult.FileExtention.toUpperCase() != ".JPG" && oResult.FileExtention.toUpperCase() != ".JPEG" && oResult.FileExtention.toUpperCase() != ".PNG" && oResult.FileExtention.toUpperCase() != ".PDF")
            {
                showMessage("red", "unsupported file type to view", "");
                return;
            }

            if (oResult.FileExtention.toUpperCase() != ".PDF") {
                if (oResult.FileExtention.toUpperCase() == ".PNG") {
                    blob = converBase64toBlob(data.Result.ImageData, 'image/png');
                }
                else if (oResult.FileExtention.toUpperCase() == ".JPG" || oResult.FileExtention.toUpperCase() == ".JPEG") {
                    blob = converBase64toBlob(data.Result.ImageData, 'image/jpeg');
                }

            }
            else {
                blob = converBase64toBlob(data.Result.ImageData, 'application/pdf');
            }

            var blobURL = URL.createObjectURL(blob);
            if (window.navigator && window.navigator.msSaveOrOpenBlob) {
                window.navigator.msSaveOrOpenBlob(blob);
                //$('#podDocImage').attr('src', window.navigator.msSaveOrOpenBlob(blob));
                $('#podDocImage1').attr('src', blobURL + "#view=fitH");

                // PDFObject.embed(blobURL, "#example1");
            }
            else {
                $('#podDocImage1').attr('src', blobURL + "#view=fitH");
            }
            //window.navigator.msSaveOrOpenBlob(blob);
            //$('#podDocImage1').attr('src', blobURL + "#view=FitH");



            //  url = oResult.FileUrl.fdFileUrl;
            //var thePdf = null;
            //var scale = 1;


            // pdfjsLib.webViewerLoad(oResult.FileUrl.fdFileUrl);

            //  PDFView.open(oResult.FileUrl.fdFileUrl, 0);
            //var pdfjsLib = window['pdfjs-dist/build/pdf'];

            //// The workerSrc property shall be specified.
            //pdfjsLib.GlobalWorkerOptions.workerSrc = '//mozilla.github.io/pdf.js/build/pdf.worker.js';

            //// Asynchronous download of PDF
            //var loadingTask = pdfjsLib.getDocument(url);


            //loadingTask.promise.then(function (pdf) {
            //    console.log('PDF loaded');

            //    // Fetch the first page
            //    var pageNumber = 1;
            //    pdf.getPage(pageNumber).then(function (page) {
            //        console.log('Page loaded');

            //        var scale = 1.5;
            //        var viewport = page.getViewport({ scale: scale });

            //        // Prepare canvas using PDF page dimensions
            //        var canvas = document.getElementById('the-canvas');
            //        var context = canvas.getContext('2d');
            //        canvas.height = viewport.height;
            //        canvas.width = viewport.width;

            //        // Render PDF page into canvas context
            //        var renderContext = {
            //            canvasContext: context,
            //            viewport: viewport
            //        };
            //        var renderTask = page.render(renderContext);
            //        renderTask.promise.then(function () {
            //            console.log('Page rendered');
            //        });
            //    });
            //}, function (reason) {
            //    // PDF loading error
            //    console.error(reason);
            //});


            // URL of PDF document
            //var url = "http://mozilla.github.io/pdf.js/examples/learning/helloworld.pdf";

            // Asynchronous download PDF
            //pdfjsLib.getDocument(url)
            //  .then(function (pdf) {
            //      return pdf.getPage(1);
            //  })
            //  .then(function (page) {
            //      // Set scale (zoom) level
            //      var scale = 1.5;

            //      // Get viewport (dimensions)
            //      var viewport = page.getViewport(scale);

            //      // Get canvas#the-canvas
            //      var canvas = document.getElementById('the-canvas');

            //      // Fetch canvas' 2d context
            //      var context = canvas.getContext('2d');

            //      // Set dimensions to Canvas
            //      canvas.height = viewport.height;
            //      canvas.width = viewport.width;

            //      // Prepare object needed by render method
            //      var renderContext = {
            //          canvasContext: context,
            //          viewport: viewport
            //      };

            //      // Render PDF page
            //      page.render(renderContext);
            //  });
            //pdfjsLib.getDocument(url).promise.then(function (pdf) {
            //    thePdf = pdf;
            //    viewer = document.getElementById('pdf-viewer');

            //    for (page = 1; page <= pdf.numPages; page++) {
            //        canvas = document.createElement("canvas");
            //        canvas.className = 'pdf-page-canvas';
            //        viewer.appendChild(canvas);
            //        renderPage(page, canvas);
            //    }
            //});

            //function renderPage(pageNumber, canvas) {
            //    thePdf.getPage(pageNumber).then(function (page) {
            //        viewport = page.getViewport(scale);
            //        canvas.height = viewport.height;
            //        canvas.width = viewport.width;
            //        page.render({ canvasContext: canvas.getContext('2d'), viewport: viewport });
            //    });
            //}
            $('#cboDoctype').val(oResult.FileUrl.fdDocType);
            var dataTable = $('#grdMetaDocDetails').DataTable();
            dataTable.clear().draw();



            $('#documentClass').text(oResult.DocumentClass);
            $('#documentName').text(oResult.DocumentName + " ( DocId:- " + oResult.DocumentID + " )");
            $('#documentVersion').text(oResult.DocVersion);
            $('#documentCreatedBy').text(oResult.UserName);
            $('#documentCreatedOn').text(oResult.DocCreatedOn);
            $('.dataTables_filter').hide();
            $('.dataTables_length').hide();
            $('.dataTables_info').hide();
            $('.dataTables_paginate').hide();
            $.each(data.Result.DocumentData, function (key, value) {

                dataTable.row.add([value.fdKey, value.fdValue
                ]).draw();
                dataTable.columns.adjust().draw();
            });
            //$('#docTypeControlsContainer').hide();
            //$('#docDataControl').show();
            //$("#DocumentCreate").addClass("active");
            //$("#DocCreate").addClass("active");
            //$("#DocumentSearch").removeClass("active");
            //$("#Docsearch").removeClass("active");
            //$('#txtDocumentFile').val('')
            $('#myModal1').show();

        } else if (data.Status == 2) {
            showMessage("red", data.Description, "");
        } else {
            showMessage("red", data.Description, "");
        }
    });


};
function closeModal1() {
    //$("#col5").attr('class', 'col-md-5');
    //$("#col11").attr('class', 'col-md-7');
    $("#col5").attr('class', 'col-md-1');
    $("#col11").attr('class', 'col-md-11');
    $("#fafatimes").attr('class', 'fa fa-bars');
    //$("#grdMetaDocDetails").removeAttr("hidden");
    //$("#grdMetaDocDetails").hide();

    $("#cols5").attr('class', 'col-md-1');
    $("#cols11").attr('class', 'col-md-11');
    $("#grdMetaDocDetails").hide();
    //$("#fafatimess").attr('class', 'fa fa-bars');
    $('#myModal1').hide();
}


document.querySelector("#tglbtn").addEventListener("click", function (event) {

    debugger;
    $("#grdMetaDocDetails").toggle(100, function () {
        if (document.querySelector("#fafatimes").classList.contains("fa-bars")) {
            $("#fafatimes").attr('class', 'fa fa-times');
            $("#fafatimes").attr('title', 'Close Table');
        }
        else if (document.querySelector("#fafatimes").classList.contains("fa-times")) {
            $("#fafatimes").attr('class', 'fa fa-bars');
            $("#fafatimes").attr('title', 'Show Table');
        }

        //if (document.querySelector("#col5").classList.contains("col-md-3")) {
        //    $("#col5").attr('class', 'col-md-1');
        //    $("#col5").addClass("fixedbarSize");

        //}
        //else if (document.querySelector("#col5").classList.contains("col-md-1")) {
        //    $("#col5").attr('class', 'col-md-3');
        //}

        //if (document.querySelector("#col11").classList.contains("col-md-9")) {
        //    $("#col11").attr('class', 'col-md-11');
        //    $("#podDocImage1").removeClass("BarIframeSize");
        //    $("#podDocImage1").addClass("IFrameSize");
        //}
        //else if (document.querySelector("#col11").classList.contains("col-md-11")) {
        //    $("#col11").attr('class', 'col-md-9');
        //    $("#podDocImage1").removeClass("IFrameSize");
        //    $("#podDocImage1").addClass("BarIframeSize");
        //}
    });
});




//document.querySelector("#tglbtns").addEventListener("click", function (event) {
//    debugger;
//    $("#grdMetaDocDetails").toggle(100, function () {
//        if (document.querySelector("#fafatimess").classList.contains("fa-bars")) {
//            $("#fafatimess").attr('class', 'fa fa-times');
//            $("#fafatimess").attr('title', 'Close Table');
//        }
//        else if (document.querySelector("#fafatimess").classList.contains("fa-times")) {
//            $("#fafatimess").attr('class', 'fa fa-bars');
//            $("#fafatimess").attr('title', 'Show Table');
//        }

//        if (document.querySelector("#cols5").classList.contains("col-md-3")) {
//            $("#cols5").attr('class', 'col-md-1');
//            $("#col5").addClass("fixedbarSize1");
//        }
//        else if (document.querySelector("#cols5").classList.contains("col-md-1")) {
//            $("#cols5").attr('class', 'col-md-3');

//        }

//        if (document.querySelector("#cols11").classList.contains("col-md-9")) {
//            $("#cols11").attr('class', 'col-md-11');
//            $("#podDocImage1").removeClass("BarIframeSize");
//            $("#podDocImage1").addClass("IFrameSize");
//        }
//        else if (document.querySelector("#cols11").classList.contains("col-md-11")) {
//            $("#cols11").attr('class', 'col-md-9');
//            $("#podDocImage1").removeClass("IFrameSize");
//            $("#podDocImage1").addClass("BarIframeSize");
//        }
//    });
//});

//function SetRolesLevelAcess(input) {

//      $("#documentChartTag").hide();
//        $("#documentClassTag").hide();
//        $("#documentTag").hide();
//        $("#documentAuditTag").hide();
//        $("#userMasterTag").hide();
//    var IsApproveRejectOccur = false;
//    for (var i = 0; i < input.length; i++) {



//        if (input[i].functionCode == 'SEARCH') {
//            $("#documentChartTag").show();
//            $("#documentClassTag").show();
//            $("#documentTag").show();
//            $("#documentAuditTag").show();
//            $("#userMasterTag").show();


//        }




//    }

//}
//function setAndSetUserScreenModules() {
//    ajaxCaller("POST", "Common/getModuleRights", "UPLOADMAS", "application/json", "Please wait ! Getting Screen Parameters").done(function (data) {
//        console.log(data);
//        rolesRight = data.Result;
//        SetRolesLevelAcess(data.Result);
//    });
//}


//>>>>>>> 8e022ac9cc6f3d9ecbcd8df2337e4e0157ef0d58
