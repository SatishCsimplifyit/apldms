﻿



//document.addEventListener("DOMContentLoaded", function (event) {
//    if (window.innerWidth < 992) {
//        var isMobile = true;
//        document.querySelector("#sidebar").classList.add("sidebar-close");
//        //document.querySelector("#sidebar1").classList.add("sidebar-close1");
//    }

//    document.querySelector("#toggle-sidebar").addEventListener("click", function (event) {
//        document.querySelector("#sidebar").classList.toggle("sidebar-close");
//        setTimeout(function () {
//            if (document.querySelector("#sidebar").classList.contains("sidebar-close")) {
//                document.querySelector(".content").classList.remove("hide-on-mob-toggle");
//                if (isMobile) {
//                    //document.querySelector("#sidebar1").style.display = "block";
//                }
//            } else {
//                document.querySelector(".content").classList.add("hide-on-mob-toggle");
//                if (isMobile) {
//                    //document.querySelector("#sidebar1").style.display = "none";
//                }
//            }
//        }, 10);
//    });


//});


$(document).ready(function () {
	documentChartTag();
  //  setAndSetUserScreenModules();
	scndChrt();
	frstChrt();
	DetailView();

 

	//Active and inActive selected menu tab
	function documentChartTag() {
		$('#documentSearchTag a').removeClass('active');
		$('#documentChartTag a').addClass('active');
		$('#documentClassTag a').removeClass('active');
		$('#documentTag a').removeClass('active');
		$('#documentAuditTag a').removeClass('active');
		$('#userMasterTag  a').removeClass('active');
	}

	function frstChrt() {
		var processed_json = new Array();
		$.getJSON('DocumentChartDashboard/Chart_User_access', function (data) {
			debugger;
			var oResult;
			oResult = data.Result;
			oResultLen = oResult.length;
			var name = [];
			var y = [];
			var FullDate = [];
			for (var i = 0; i < oResultLen; i++) {

				name.push([oResult[i].Name]);
				y.push([oResult[i].Y]);
				processed_json.push([name, y]);
				FullDate.push([oResult[i].Description]);
			}

			// draw chart



			Highcharts.chart('frstChart', {
				chart: {
					type: 'column'
				},
				title: {
					text: 'Documents uploaded in last 7 days'
				},
				xAxis: {
					categories: name
				},
				yAxis: {
					min: 0,
					title: {
						text: 'Number of Document Uploaded'
					},
					stackLabels: {
						enabled: true,
						style: {
							fontWeight: 'bold',
							color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
						}
					}
				},
				legend: {
					enabled: false
				},
				tooltip: {
					headerFormat: '<b>{point.x}</b><br/>',
					pointFormat: 'Total: {point.stackTotal}'
				},
				plotOptions: {
					series: {
						cursor: 'pointer',
						events: {
							click: function (event) {
								debugger;
								var pickDate = event.point.category[0];

								var Fulldate1 = event.point.series.name[event.point.index];

								getUplodedDocumentwithclass(Fulldate1);
							}
						}
					},

					column: {
						stacking: 'normal',
						dataLabels: {
							enabled: false,
							color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white'
						}
					}
				},
				series: [{
					name: 'John',
					data: []
				}, {
					name: 'Jane',
					data: []
				}, {
					name: FullDate,
					data: y
				}]
			});
		});
	}

	var NameArray = new Array();
	function scndChrt() {
		var processed_json = new Array();
		$.getJSON('DocumentChartDashboard/Chart_User_access_audit', function (data) {
			debugger;
			var oResult;
			oResult = data.Result;
			oResultLen = oResult.length;
			var name = [];
			var y = [];
			var FullDate = [];

			for (var i = 0; i < oResultLen; i++) {

				name.push([oResult[i].Name]);
				y.push([oResult[i].Y]);
				processed_json.push([name, y]);
				FullDate.push([oResult[i].Description]);

			}
		  
			NameArray.push(name);
			// draw chart



			Highcharts.chart('scndChart', {
				chart: {
					type: 'column'
				},
				title: {
					text: 'Documents Accessed in last 30 days'
				},
				xAxis: {
					categories: name,
					title: {
						text: name,
						enabled: false

					},
					labels: { enabled: true}
				},
				yAxis: {
					min: 0,
					title: {
						text: 'Number of Document Access'
					},
					stackLabels: {
						enabled: true,
						style: {
							fontWeight: 'bold',
							color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
						}
					}
				},
				legend: {
					enabled: false
				},
				tooltip: {
					headerFormat: '<b>{point.x}</b><br/>',
					pointFormat: 'Total: {point.stackTotal}'
				},
				plotOptions: {
					series: {
						cursor: 'pointer',
						events: {
							click: function (event) {
								debugger;
								var pickDate = event.point.category[0];
								
								var Fulldate1 = event.point.series.name[event.point.index];
							  
								ShowUploadedDocument(Fulldate1);
							}
						}
					},

					column: {
						stacking: 'normal',
						dataLabels: {
							enabled: false,
							color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white'
						}
					}
				},
				series: [{
					//name: 'John',
					data: []
				}, {
					//name: 'Jane',
					data: []
				}, {
					name: FullDate,
					data: y
				}],
				type: 'column',
				events: {
					drilldown: function (e) {
						alert('{point.x}');
					}
				}
			});


		});
	}

	function DetailView() {
		var processed_json = new Array();
		$.getJSON('DocumentChartDashboard/getTotalUploadedDocumentWithClass', function (data) {
			debugger;
			if (data.Status == 1) {
				var oResult;
				oResult = data.Result;
				oResultLen = oResult.length;
				var name = [];
				var y = [];
				var FullDate = [];
				for (var i = 0; i < oResultLen; i++) {

					name.push([oResult[i].Name]);
					y.push([oResult[i].Y]);
					FullDate.push({"name": oResult[i].Name,"y": oResult[i].Y });
					// FullDate.push([oResult[i].Description]);
				}

				Highcharts.chart('container', {
					chart: {
						type: 'pie'
					},
					title: {
						text: 'Top 5 Documents Type'
					},
					subtitle: {
						text: ' '
					},
					tooltip: {
						headerFormat: '<b>{point.name}</b><br/>',
						pointFormat: '<b>{point.name}</b><br/>Total: {point.y}'
						//headerFormat: '<span style="font-size:11px">{point.x}</span><br>',// document class name
						//pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> of total<br/>' // total
					},
					plotOptions: {
						pie: {
							size: 200,
							allowPointSelect: true,
							showInLegend: true,
							cursor: 'pointer',
							dataLabels: {
								enabled: true,
								format: ' {point.y}',
								style: {
									color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
								}
							},
							dataPoints: [
			//{ y: 26, name: "School Aid", exploded: true },
			//{ y: 20, name: "Medical Aid" },
			//{ y: 5, name: "Debt/Capital" },
			//{ y: 3, name: "Elected Officials" },
			//{ y: 7, name: "University" },
			//{ y: 17, name: "Executive" },
			//{ y: 22, name: "Other Local Assistance" }
							]

						}
					},

					legend: {
						cursor: "pointer",
					   // itemclick: explodePie
					},
					//legend: {
					//    display: true,
	
					//    // generateLabels changes from chart to chart,  check the source, 
					//    // this one is from the doughut :
					//    // https://github.com/chartjs/Chart.js/blob/master/src/controllers/controller.doughnut.js#L42
					//    labels: {
					//        generateLabels: function(chart) {
					//            var data = chart.data;
					//            if (data.labels.length && data.datasets.length) {
					//                return data.labels.map(function(label, i) {
					//                    var meta = chart.getDatasetMeta(0);
					//                    var ds = data.datasets[0];
					//                    var arc = meta.data[i];
					//                    var custom = arc && arc.custom || {};
					//                    var getValueAtIndexOrDefault = theHelp.getValueAtIndexOrDefault;
					//                    var arcOpts = chart.options.elements.arc;
					//                    var fill = custom.backgroundColor ? custom.backgroundColor : getValueAtIndexOrDefault(ds.backgroundColor, i, arcOpts.backgroundColor);
					//                    var stroke = custom.borderColor ? custom.borderColor : getValueAtIndexOrDefault(ds.borderColor, i, arcOpts.borderColor);
					//                    var bw = custom.borderWidth ? custom.borderWidth : getValueAtIndexOrDefault(ds.borderWidth, i, arcOpts.borderWidth);
					//                    return {
					//                        // And finally : 
					//                        text: ds.data[i] + "% of the time " + label,
					//                        fillStyle: fill,
					//                        strokeStyle: stroke,
					//                        lineWidth: bw,
					//                        hidden: isNaN(ds.data[i]) || meta.data[i].hidden,
					//                        index: i
					//                    };
					//                });
					//            }
					//            return [];
					//        }
					//    }
					//},
				

					series: [
						{
							
							data: FullDate
						}
					],
					
				});
			}
			else
			{
				showMessage("red", data.Description, "");
			}

		});


		//Highcharts.chart('container', {
		//    chart: {
		//        plotBackgroundColor: null,
		//        plotBorderWidth: null,
		//        plotShadow: false,
		//        type: 'pie'
		//    },
		//    title: {
		//        text: 'Browser market shares in January, 2018'
		//    },
		//    tooltip: {
		//        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
		//    },
		//    plotOptions: {
		//        series: {
		//            cursor: 'pointer',
		//            events: {
		//                click: function (event) {
		//                    debugger;
		//                    var pickDate = event.point.category[0];
		//                    alert(pickDate);
		//                }
		//            }
		//        },

		//        pie: {
		//            allowPointSelect: true,
		//            cursor: 'pointer',
		//            dataLabels: {
		//                enabled: true,
		//                format: '<b>{point.name}</b>: {point.percentage:.1f} %',
		//                style: {
		//                    color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
		//                }
		//            }
		//        }
		//    },
		//    series: [{
		//        name: 'Brands',
		//        colorByPoint: true,
		//        data: [{
		//            name: 'Chrome',
		//            y: 61.41,
		//            sliced: true,
		//            selected: true
		//        }, {
		//            name: 'Internet Explorer',
		//            y: 11.84
		//        }, {
		//            name: 'Firefox',
		//            y: 10.85
		//        }, {
		//            name: 'Edge',
		//            y: 4.67
		//        }, {
		//            name: 'Safari',
		//            y: 4.18
		//        }, {
		//            name: 'Sogou Explorer',
		//            y: 1.64
		//        }, {
		//            name: 'Opera',
		//            y: 1.6
		//        }, {
		//            name: 'QQ',
		//            y: 1.2
		//        }, {
		//            name: 'Other',
		//            y: 2.61
		//        }]
		//    }]
		//});
	}

   
});

function explodePie(e) {
	if (typeof (e.dataSeries.dataPoints[e.dataPointIndex].exploded) === "undefined" || !e.dataSeries.dataPoints[e.dataPointIndex].exploded) {
		e.dataSeries.dataPoints[e.dataPointIndex].exploded = true;
	} else {
		e.dataSeries.dataPoints[e.dataPointIndex].exploded = false;
	}
	e.chart.render();

}

function ShowUploadedDocument(input) {
  
	var dateObj = new Date(input);
	var date = dateObj.getUTCDate();
	var Month = dateObj.getUTCMonth() + 1;
	var year = dateObj.getUTCFullYear();
	var shortMonthName = new Intl.DateTimeFormat("en-US", { month: "short" }).format;
	var shortName = shortMonthName(dateObj);

	var FullDate = date+'-'+shortName+'-'+year;

	ajaxCaller("POST", "DocumentChartDashboard/getuploadedDocument", input, "application/json", "Loading").done(function (data) {
		if (data.Status == 1) {
			debugger;
			var html = "";
			var oResult = data.Result;
			var dataTable = $('#ShowUploadDocument').DataTable();
			dataTable.clear().draw();
			$('#ShowUploadDocument_filter').hide();
			$('#ShowDocumentClass_filter').hide();
			$.each(oResult, function (key, value) {
				dataTable.row.add([value.fdDocId, value.auditId, value.fdDocTitle,value.DocumentClass, value.fdUserName,
				   value.createdOn, value.actionName, value.actionType
				]).draw();

			});
			dataTable.column(0).visible(false);
			dataTable.column(1).visible(false);
			dataTable.columns([1]).order('desc').draw();
			dataTable.columns.adjust().draw();
			$('#lblSchDate').text(FullDate);
			$('#lblAcessCount').text(oResult.length);
			$('#ShowUploadedDocument').modal('show');
		} else if (data.Status == 2) {
			showMessage("red", data.Description, "");
		} else {
			showMessage("red", data.Description, "");
		}
	});


}


function getUplodedDocumentwithclass(input)
	{
   
	var dateObj = new Date(input);
	var date = dateObj.getUTCDate();
	var Month = dateObj.getUTCMonth() + 1;
	var year = dateObj.getUTCFullYear();
	var shortMonthName = new Intl.DateTimeFormat("en-US", { month: "short" }).format;
	var shortName = shortMonthName(dateObj);

	var FullDate = date + '-' + shortName + '-' + year;
	ajaxCaller("POST", "DocumentChartDashboard/getDocumentCalss", input, "application/json", "Loading").done(function (data) {
			if (data.Status == 1) {
				debugger;
				var html = "";
				var oResult = data.Result;
				var dataTable = $('#ShowDocumentClass').DataTable();
				dataTable.clear().draw();
				$('#ShowUploadDocument_filter').hide();
				$('#ShowDocumentClass_filter').hide();
				$.each(oResult, function (key, value) {
					dataTable.row.add([value.fdDocId, value.fdDocTitle,value.ClassName, value.fdUserName,
					   value.CreatedOn, value.DocStatus, value.DocVersion
					]).draw();
					
				});
				dataTable.column(0).visible(false);
				dataTable.columns([0]).order('desc').draw();
			   
				dataTable.columns.adjust().draw();
				$('#lbluploadedDoc').text(FullDate);
				$('#lblUploadCount').text(oResult.length);
				$('#ShowDocumentClassDocument').modal('show');
			} else if (data.Status == 2) {
				showMessage("red", data.Description, "");
			} else {
				showMessage("red", data.Description, "");
			}
		});
}


//function SetRolesLevelAcess(input) {

//    $("#documentChartTag").hide();
//    $("#documentClassTag").hide();
//    $("#documentTag").hide();
//    $("#documentAuditTag").hide();
//    $("#userMasterTag").hide();
//    var IsApproveRejectOccur = false;
//    for (var i = 0; i < input.length; i++) {

//        if (input[i].functionCode == 'SEARCH') {
//            $("#documentChartTag").show();
//            $("#documentClassTag").show();
//            $("#documentTag").show();
//            $("#documentAuditTag").show();
//            $("#userMasterTag").show();

//        }
//    }

//}
//function setAndSetUserScreenModules() {
//    ajaxCaller("POST", "Common/getModuleRights", "USAGESDASH", "application/json", "Please wait ! Getting Screen Parameters").done(function (data) {
//        console.log(data);
//        rolesRight = data.Result;
//        SetRolesLevelAcess(data.Result);
//    });
//}