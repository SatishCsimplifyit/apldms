/// <autosync enabled="true" />
/// <reference path="../assets/js/just-the-docs.js" />
/// <reference path="../assets/js/vendor/lunr.min.js" />
/// <reference path="bootbox/bootbox.js" />
/// <reference path="bootstrap.js" />
/// <reference path="chartdashboard/chartdashboard.js" />
/// <reference path="common.js" />
/// <reference path="datatables.fixedheader.js" />
/// <reference path="datepicker/bootstrap-datepicker.js" />
/// <reference path="date/flatpickr.js" />
/// <reference path="date/jquery.datetimepicker.full.js" />
/// <reference path="date/tail.datetime.js" />
/// <reference path="date/tail.datetime-full.js" />
/// <reference path="documentaudit/documentaudit.js" />
/// <reference path="documentclass/documentclass.js" />
/// <reference path="documentclass/documentclassold.js" />
/// <reference path="highcharts.js" />
/// <reference path="highcharts_exporting.js" />
/// <reference path="home/document.js" />
/// <reference path="home/home.js" />
/// <reference path="jquery.datatables.min.js" />
/// <reference path="jquery.min.js" />
/// <reference path="jquery.validate.js" />
/// <reference path="jquery.validate.unobtrusive.js" />
/// <reference path="jquery-1.10.2.js" />
/// <reference path="lightbox/dist/js/lightbox.js" />
/// <reference path="lightbox/dist/js/lightbox-plus-jquery.js" />
/// <reference path="login/login.js" />
/// <reference path="modernizr-2.6.2.js" />
/// <reference path="pdf/analytics.js" />
/// <reference path="pdf/compatibility.js" />
/// <reference path="pdf/debugger.js" />
/// <reference path="pdf/gtag - copy.js" />
/// <reference path="pdf/gtag.js" />
/// <reference path="pdf/l10n.js" />
/// <reference path="pdf/pdf.js" />
/// <reference path="pdf/pdf.worker.js" />
/// <reference path="pdf/pdfobject.js" />
/// <reference path="pdf/viewer.js" />
/// <reference path="popper.min.js" />
/// <reference path="prettyphoto.js" />
/// <reference path="respond.js" />
/// <reference path="upload/upload.js" />
/// <reference path="upload/uploadscreen.js" />
/// <reference path="usermaster/usermaster.js" />
