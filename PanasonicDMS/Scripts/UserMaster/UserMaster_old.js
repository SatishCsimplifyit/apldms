﻿//var searchTable;
$(document).ready(function () {
    userMasterTag();
    $('#query1').hide();
    $('.user').hide();
    fetchAndSetRoles();
   
    fetchAndSetIntiallyDataForStatus();
    
    $('.searchTable').DataTable({
       
        columns: [
             { title: "User Id" },
            { title: "User Name" },
            { title: "Contact No." },
            { title: "E-Mail" },
            { title: "Status" }
        ]
    });
    $('.roleTable').DataTable({
       
        columns: [
            { title: "Role Code" },
            { title: "Role" },
            { title: "" }
        ]
    });
    $('#roleTable').DataTable({

        columns: [
            { title: "Role Code" },
            { title: "Role" },
            { title: "" }
        ]
    });
    removeItemDataTableControl();
});
var totalRoles=[];
/*
* fetch and set initially User Roles
*/
function fetchAndSetRoles() {
    debugger;
    var inputData = {};

    ajaxCaller("POST", "./UserMaster/fetchAndSetRoles", JSON.stringify(inputData), "application/json", "Please wait! Loading form values").done(function (data) {
        debugger;
        if (data.Status == 1) {
            var html = "";
            var oResult = data.Result;
            $.each(oResult, function (key, value) {
                totalRoles.push(value.fdRoleId);
                html += '<tr><td>' + value.fdRoleCode + '</td><td>' + value.fdRoleName + '</td><td><div class="checkbox"><input class="colored-success" id="userRole-' + value.fdRoleId + '" name="userRoles" type="checkbox" value="true"></div></td></tr>';
            });
            $("#userRolesTableBody").html(html);
        } else if (data.Status == 2) {
            showMessage("red", data.Description, "");
        } else {
            showMessage("red", data.Description, "");
        }
    });
}


//var userStatusArr;
function fetchAndSetIntiallyDataForStatus() {
    var inputData = {};
    var lookUpCodes = [];
    lookUpCodes.push("DEPARTMENT");
    inputData.lookUpCode = lookUpCodes;
    inputData.KeyCode = "";
    ajaxCaller("POST", "./Common/fetchStatus", JSON.stringify(inputData), "application/json", "Please wait! Loading form values").done(function (data) {
        if (data.Status == 1) {
            var oResult = data.Result;
            userStatusArr = oResult;
            $.each(oResult, function (key, value) {
                if (value.fdLkUpCode == "DEPARTMENT") {
                    $('#cboUserDepartment').append($('<option></option>').val(value.fdKeyCode).html(value.fdKey1));
                }
            });
        } else if (data.Status == 2) {
            showMessage("red", data.Description, "");
        } else {
            showMessage("red", data.Description, "");
        }
    });
}

///*
//* search User master
//*/
//function searchUserMaster(message) {
//    if (message == "" || message == undefined) {
//        message = "Searching Users";
//    }
//    var inputData = {};
//    inputData.txtSchUserName = $("#txtSchUserName").val();
//    inputData.txtSchContactNo = $("#txtSchContactNo").val();
//    ajaxCaller("POST", "UserMaster/searchUserMaster", JSON.stringify(inputData), "application/json", message).done(function (data) {
//        searchTable.clear().draw();
//        if (data.Status == 1) { 
//            var oResult = data.Result;
//            if (oResult.length > searchCount) {
//                showMessage("green", "There are more then " + searchCount + " users, Please refine your search.", "");
//            }
//            var searchDataSet = [];
//            debugger;
//            for (i = 0; i < oResult.length; i++) {
//                var dataSet = [];
//                var result = oResult[i];
//                dataSet.push(i + 1);
//                dataSet.push(result.userId);
//                dataSet.push(result.userName);
//                dataSet.push(result.contactNo);
//                dataSet.push(result.userLocation);
//                dataSet.push(result.userEmail);
//                dataSet.push(result.userRole);
//                var status = "";
//                $.each(userStatusArr, function (key, value) {
//                    if (result.fdIsActive == value.fdKeyCode) {
//                        status = value.fdKey1;
//                    }
//                });
//                dataSet.push(status);
//                searchDataSet[i] = dataSet;
//            }
//            searchTable.rows.add(searchDataSet).draw();
//            searchTable.columns.adjust().draw();
//        } else if (data.Status == 2) {
//            showMessage("red", data.Description, "");
//        } else {
//            showMessage("red", data.Description, "");
//        }
//    });
//}

function removeItemDataTableControl() {
    $('#grdSrchUser tbody').off('dblclick').on('dblclick', 'tr', function () {
        var grdItemDataTable = $('#grdSrchUser').DataTable();
        var rowIndex = grdItemDataTable.row(this).index()
        var data = grdItemDataTable.row(this).data();
        var requestData = data[0];
        
        ajaxCaller("POST", "/UserMaster/GetUserDetail", data[0], "application/json", "Please wait ! Getting user details").done(function (data) {
            if (data.Status == 1) {
                debugger;
                var oResult = data.Result;
                var grdItemDataTable = $('#grdSrchUser').DataTable();


                //$('.nav-tabs a[href="#UserCreateUpdateTab"]').tab('show');
                $('.user').show();
                $('.userMaster').hide();
                //if (oResult.id != null && oResult.id != undefined && oResult.id != "")
                //    $("#hdnUserID").val(oResult.id);
                if (oResult.fduserid != null && oResult.fduserid != undefined && oResult.fduserid != "")
                    $("#hdnUserID").val(oResult.fduserid);

                if (oResult.fdDepartment != null && oResult.fdDepartment != undefined && oResult.fdDepartment != ""){
                    $('#cboUserDepartment').val(oResult.fdDepartment);
}
                //if (oResult.firstName != null && oResult.firstName != undefined && oResult.firstName != "")
                //    $("#userName").val(oResult.firstName);

                if (oResult.fdusername != null && oResult.fdusername != undefined && oResult.fdusername != "")
                    $("#userName").val(oResult.fdusername);

                //if (oResult.email != null && oResult.email != undefined && oResult.email != "")
                //    $("#email").val(oResult.email);

                if (oResult.fdUserEmail != null && oResult.fdUserEmail != undefined && oResult.fdUserEmail != "")
                    $("#email").val(oResult.fdUserEmail);

                //if (oResult.contactNumber != null && oResult.contactNumber != undefined && oResult.contactNumber != "")
                //    $("#contactNo").val(oResult.contactNumber);

                if (oResult.fdUserContactNo != null && oResult.fdUserContactNo != undefined && oResult.fdUserContactNo != "")
                    $("#contactNo").val(oResult.fdUserContactNo);

                //if (oResult.dob != null && oResult.dob != undefined && oResult.dob != "")
                //    $("#dateOfBirth").val(oResult.dob);
                //if (oResult.CollegeId != null && oResult.CollegeId != undefined && oResult.CollegeId != "")
                //    $("#hdnSchCollegeID").val(oResult.CollegeId);
                //if (oResult.CollegeName != null && oResult.CollegeName != undefined && oResult.CollegeName != "")
                //    $("#college").val(oResult.CollegeName);
                //if (oResult.CityId != null && oResult.CityId != undefined && oResult.CityId != "")
                //    $("#hdnSchCityID").val(oResult.CityId);
                //if (oResult.CityName != null && oResult.CityName != undefined && oResult.CityName != "")
                //    $("#city").val(oResult.CityName);
                //if (oResult.StateId != null && oResult.StateId != undefined && oResult.StateId != "")
                //    $("#hdnSchStateID").val(oResult.StateId);
                //if (oResult.StateDesc != null && oResult.StateDesc != undefined && oResult.StateDesc != "")
                //    $("#state").val(oResult.StateDesc);
                //if (oResult.CountryId != null && oResult.CountryId != undefined && oResult.CountryId != "")
                //    $("#hdnSchCountryID").val(oResult.CountryId);
                //if (oResult.Countryname != null && oResult.Countryname != undefined && oResult.Countryname != "")
                //    $("#country").val(oResult.Countryname);
                //if (oResult.pincode != null && oResult.pincode != undefined && oResult.pincode != "")
                //    $("#pincode").val(oResult.pincode);
                //if (oResult.password != null && oResult.password != undefined && oResult.password != "")
                //    $("#password").val(oResult.password);
                //if (oResult.password != null && oResult.password != undefined && oResult.password != "")
                //    $("#repassword").val(oResult.password);
                //if (oResult.isActive != null && oResult.isActive != undefined && oResult.isActive != "")
                //    $("#status").val(oResult.isActive);
                //if (oResult.address != null && oResult.address != undefined && oResult.address != "")
                //    $("#address").val(oResult.address);

                if (oResult.UserStatus != null && oResult.UserStatus != undefined && oResult.UserStatus != "")
                    $("#status").val(oResult.UserStatus);

                if (oResult.Role != null && oResult.Role != undefined && oResult.Role != "")
                    var roles = oResult.Role.split(',');

                for (var i = 0; i < totalRoles.length; i++) {
                    $("#userRole-" + totalRoles[i]).prop("checked", false);
                }
               
                if (roles != null && roles!=undefined)
                for (var i = 0; i < roles.length; i++) {
                    $("#userRole-" + roles[i]).prop("checked", true);
                }
                //if (oResult.fdRoleLocUserID != null && oResult.fdRoleLocUserID != undefined && oResult.fdRoleLocUserID != "")
                $('#cboUserDepartment').attr('disabled',true)
                //    $("#hdnRoleLocUserID").val(oResult.fdRoleLocUserID);
                showMessage("green", data.Description, "");

            }
            else {
                showMessage("red", data.Description, "");
            }
        });
        $('#grdSrchUser').off('draw.dt').on('draw.dt', function () {
            removeItemDataTableControl();
        });
    });
}

// Serach and Select College
function getCollegeList(userEnter) {
    var inputData = {};
    inputData.userCollege = userEnter;

    ajaxCaller("POST", "/UserMaster/getCollegeList", JSON.stringify(inputData), "application/json", "Please wait!").done(function (data) {
        if (data.Status == 1) {
            //showMessage("green", data.Description, "");
            debugger;
            showResultList(data, 'college', 'collegesList', 'hdnSchCollegeID');
        }
        else {
            showMessage("red", data.Description, "");
        }
    })
}




//search and select City
function getCitiesList(userEnter) {
    debugger;
    var inputData = {};

    inputData.userCity = userEnter;

    ajaxCaller("POST", "/UserMaster/getCitiesList", JSON.stringify(inputData), "application/json", "Please wait!").done(function (data) {
        if (data.Status == 1) {
            //showMessage("green", data.Description, "");
            debugger;
            showResultList(data, 'city', 'citiesList', 'hdnSchCityID');
        }
        else {
            showMessage("red", data.Description, "");
        }
    })

}

//Search and Select State

function getStateList(userEnter) {
    debugger;
    var inputData = {};

    inputData.userState = userEnter;

    ajaxCaller("POST", "/UserMaster/getStateList", JSON.stringify(inputData), "application/json", "Please wait!").done(function (data) {
        if (data.Status == 1) {
            //showMessage("green", data.Description, "");
            debugger;
            showResultList(data, 'state', 'stateList', 'hdnSchStateID');
        }
        else {
            showMessage("red", data.Description, "");
        }
    })

}


// Search And Select Country

function getCountryList(userEnter) {
    debugger;
    var inputData = {};

    inputData.userCountry = userEnter;

    ajaxCaller("POST", "/UserMaster/getCountryList", JSON.stringify(inputData), "application/json", "Please wait!").done(function (data) {
        if (data.Status == 1) {
            //showMessage("green", data.Description, "");
            debugger;
            showResultList(data, 'country', 'countryList', 'hdnSchCountryID');
        }
        else {
            showMessage("red", data.Description, "");
        }
    })

}
// funtion to save User tab details in user table in DB.
function saveUserMasterData() {
    debugger;
    var userID = $("#hdnUserID").val();
    var userName = $("#userName").val();
    var eMail = $("#email").val();
    var contactNo = $("#contactNo").val();
   
    var password = $("#password").val();
    var repassword = $('#repassword').val();
    var status = $('#status').val();
    var userRoles = [];
    $("input:checkbox[name='userRoles']:checked").each(function () {
        var roleId = this.id.split("-");
        userRoles.push(roleId[1]);
    })
    var roleUserLinkId = $('#hdnRoleLocUserID').val();

    if (userName == "" || userName == undefined || userName == null) {
        showMessage("red", "Please, Enter User Name.", "");
        $("#userName").focus();
        $("#userName").addClass("inputError");
        return false;
    }
    if (eMail == "" || eMail == undefined || eMail == null) {
        showMessage("red", "Please, Enter Email Id.", "");
        $("#email").focus();
        $("#email").addClass("inputError");
        return false;
    }
    if (validateEmailAddress(eMail) == 0) {
        showMessage("red", "Please, EnterValid Email Id.", "");
        $("#email").focus();
        $("#email").addClass("inputError");
        return false;
    }
    if (contactNo == "" || contactNo == undefined || contactNo == null) {
        showMessage("red", "Enter 10 digit mobile number.", "");
        $("#contactNo").addClass("inputError");
        $("#contactNo").focus();
        return false;
    }
    
    if ((userID == "" || userID == undefined || userID == null || userID == 0) || (userID != "" && userID != undefined && userID != null && userID != 0 && password != "" && password!=undefined && password!=null )) {
        if (password == "" || password == undefined || password == null) {
            showMessage("red", "Enter minimum 6 digit User Password.", "");
            $("#password").addClass("inputError");
            $("#password").focus();
            return false;
        }
        if (repassword == "" || repassword == undefined || repassword == null) {
            showMessage("red", "Re-Enter minimum 6 digit User Password.", "");
            $("#repassword").addClass("inputError");
            $("#repassword").focus();
            return false;
        }
        if (password != repassword) {
            showMessage("red", "Password and Re-Password did not matched.", "");
            $("#password").val("");
            $("#repassword").val("");
            return false;
        }
    }

    //if (address == "" || address == undefined || address == null) {
    //    showMessage("red", " Enter User Address.", "");
    //    $("#address").addClass("inputError");
    //    $("#address").focus();
    //    return false;
    //}
    if (status == "" || status == undefined || status == null) {
        showMessage("red", "Select User Status.", "");
        $("#status").addClass("inputError");
        $("#status").focus();
        return false;
    }

    if (userRoles.length < 1) {
        showMessage("red", " Select User Role.", "");
    }

    var inputData = {};
    if ($('#cboUserDepartment').val() != "" && $('#cboUserDepartment').val() != undefined && $('#cboUserDepartment').val() != null) {
        inputData.Department = $('#cboUserDepartment').val();
    }
    inputData.userName = userName;
    inputData.hdnUserId = userID;
    inputData.eMail = eMail;
    inputData.contactNo = contactNo;
    //inputData.dateOfBirth = dateOfBirth;
    //inputData.college = college;
    //inputData.city = city;
    //inputData.state = state;
    //inputData.country = country;
    //inputData.pincode = pincode;
    inputData.password = password;
    inputData.repassword = repassword;
    if (userID != null && userID != undefined) {
        inputData.userId = userID;
    }
    if (roleUserLinkId != null && roleUserLinkId != undefined) {
        inputData.roleUserLinkId = roleUserLinkId;
    }

    if (status == '1') {
        inputData.status = '1';
    } else {
        inputData.status = '0';
    }

   // inputData.address = address;
    inputData.userRoles = userRoles;

    ajaxCaller("POST", "/UserMaster/saveUserMasterData", JSON.stringify(inputData), "application/json", "Please wait ! Saving User").done(function (data) {
        if (data.Status == 1) {
            showMessage("green", data.Description, "");
            //reset form            
            $("#hdnUserId").val("");
        }
        else
            showMessage("red", data.Description, "");
    });
}

function reset() {
    debugger;
    $("#hdnUserID").val("");
    $("#userName").val("");
    $("#email").val("");
    $("#contactNo").val("");
    $("#dateOfBirth").val("");
    $("#college").val("");
    $("#city").val("");
    $("#state").val("");
    $("#country").val("");
    $("#pincode").val("");
    $("#password").val("");
    $('#repassword').val("");
    $('#status').val("");
    $('#address').val("");
    $('#cboUserDepartment').attr('disabled', false);
    $('#cboUserDepartment').val(0);

    for (var i = 1; i < totalRoles.length; i++) {
        $("#userRole-" + i).prop("checked", false);
    }






}

//---- Search Tab----------//

//------fetch User Name List------//

function searchUserMasterTab() {
    debugger;
    var inputData = {};
    var userNamesch = $("#user_Name").val();
    var contactNoSch = $("#contactNum").val();

    inputData.userName = userNamesch;
    inputData.contactNumber = contactNoSch;
    ajaxCaller("POST", "/UserMaster/searchUserMasterTab", JSON.stringify(inputData), "application/json", "Please wait").done(function (data) {
        if (data.Status == 1) {
            debugger;
           // showMessage("green", data.Description);
            var userMasterDataTable = $('#grdSrchUser').DataTable();
            
            userMasterDataTable.clear().draw();
            userMasterDataTable.column(0).visible(false);

            var i = 0;
            $.each(data.Result, function (key, value) {
                userMasterDataTable.row.add([
                    data.Result[i].fduserid,
                    data.Result[i].fdusername,
                    data.Result[i].fdUserContactNo,
                    data.Result[i].fdUserEmail,
                    data.Result[i].UserStatus                   
                ]).draw();

                i = i + 1;
            });

            userMasterDataTable.columns.adjust().draw();
            showMessage("green", data.Description)
        }
        else {
            showMessage("red", data.Description);
        }
    });


}

function reSet() {
    $("#user_Name").val("");
    $("#contactNum").val("");
}


//-----Edit and Update Row Data on Double Click-------//

function editAndUpdateUserMasterRowData() {
    debugger;
    var inputData = {};
    $(document).on("dblclick", "#grdSrchUser tr", function () {
        debugger

        var grdItemDataTable = $('#grdSrchUser').DataTable();
        var rowIndex = grdItemDataTable.row(this).index()
        var data = grdItemDataTable.row(this).data();
        var requestData = data[0];
        inputData.searchUserId = data[0];
        ajaxCaller("POST", "/UserMaster/GetUserDetail", data[0], "application/json", "Please wait ! Getting user details").done(function (data) {
            if (data.Status == 1) {
                debugger;
                var oResult = data.Result;
                var grdItemDataTable = $('#grdSrchUser').DataTable();


                $('.nav-tabs a[href="#UserCreateUpdateTab"]').tab('show');
                //if (oResult.id != null && oResult.id != undefined && oResult.id != "")
                //    $("#hdnUserID").val(oResult.id);
                if (oResult.fduserid != null && oResult.fduserid != undefined && oResult.fduserid != "")
                    $("#hdnUserID").val(oResult.fduserid);

                //if (oResult.firstName != null && oResult.firstName != undefined && oResult.firstName != "")
                //    $("#userName").val(oResult.firstName);

                if (oResult.fdusername != null && oResult.fdusername != undefined && oResult.fdusername != "")
                    $("#userName").val(oResult.fdusername);

                //if (oResult.email != null && oResult.email != undefined && oResult.email != "")
                //    $("#email").val(oResult.email);

                if (oResult.fdUserEmail != null && oResult.fdUserEmail != undefined && oResult.fdUserEmail != "")
                    $("#email").val(oResult.fdUserEmail);

                //if (oResult.contactNumber != null && oResult.contactNumber != undefined && oResult.contactNumber != "")
                //    $("#contactNo").val(oResult.contactNumber);

                if (oResult.fdUserContactNo != null && oResult.fdUserContactNo != undefined && oResult.fdUserContactNo != "")
                    $("#contactNo").val(oResult.fdUserContactNo);

                //if (oResult.dob != null && oResult.dob != undefined && oResult.dob != "")
                //    $("#dateOfBirth").val(oResult.dob);
                //if (oResult.CollegeId != null && oResult.CollegeId != undefined && oResult.CollegeId != "")
                //    $("#hdnSchCollegeID").val(oResult.CollegeId);
                //if (oResult.CollegeName != null && oResult.CollegeName != undefined && oResult.CollegeName != "")
                //    $("#college").val(oResult.CollegeName);
                //if (oResult.CityId != null && oResult.CityId != undefined && oResult.CityId != "")
                //    $("#hdnSchCityID").val(oResult.CityId);
                //if (oResult.CityName != null && oResult.CityName != undefined && oResult.CityName != "")
                //    $("#city").val(oResult.CityName);
                //if (oResult.StateId != null && oResult.StateId != undefined && oResult.StateId != "")
                //    $("#hdnSchStateID").val(oResult.StateId);
                //if (oResult.StateDesc != null && oResult.StateDesc != undefined && oResult.StateDesc != "")
                //    $("#state").val(oResult.StateDesc);
                //if (oResult.CountryId != null && oResult.CountryId != undefined && oResult.CountryId != "")
                //    $("#hdnSchCountryID").val(oResult.CountryId);
                //if (oResult.Countryname != null && oResult.Countryname != undefined && oResult.Countryname != "")
                //    $("#country").val(oResult.Countryname);
                //if (oResult.pincode != null && oResult.pincode != undefined && oResult.pincode != "")
                //    $("#pincode").val(oResult.pincode);
                //if (oResult.password != null && oResult.password != undefined && oResult.password != "")
                //    $("#password").val(oResult.password);
                //if (oResult.password != null && oResult.password != undefined && oResult.password != "")
                //    $("#repassword").val(oResult.password);
                //if (oResult.isActive != null && oResult.isActive != undefined && oResult.isActive != "")
                //    $("#status").val(oResult.isActive);
                //if (oResult.address != null && oResult.address != undefined && oResult.address != "")
                //    $("#address").val(oResult.address);
                
                if (oResult.UserStatus != null && oResult.UserStatus != undefined && oResult.UserStatus != "")
                    $("#status").val(oResult.UserStatus);

                if (oResult.Role != null && oResult.Role != undefined && oResult.Role != "")
                    var roles = oResult.Role.split(',');
                for (var i = 0; i < roles.length; i++) {
                    $("#userRole-" + roles[i]).prop("checked", true);
                }
                   
                //if (oResult.fdRoleLocUserID != null && oResult.fdRoleLocUserID != undefined && oResult.fdRoleLocUserID != "")

                //    $("#hdnRoleLocUserID").val(oResult.fdRoleLocUserID);
                showMessage("green", data.Description, "");

            }
            else {
                showMessage("red", data.Description, "");
            }
        });
    });

}

//Active and inActive selected menu tab
function userMasterTag() {
    $('#documentSearchTag a').removeClass('active');
    $('#documentChartTag a').removeClass('active');
    $('#documentClassTag a').removeClass('active');
    $('#documentTag a').removeClass('active');
    $('#documentAuditTag a').removeClass('active');
    $('#userMasterTag  a').addClass('active');
}

function newUser() {
    $('.user').show();
    $('.userMaster').hide();
}
function cross() {
    $('.userMaster').show();
    $('.user').hide();
}