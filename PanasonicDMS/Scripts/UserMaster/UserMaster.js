﻿

function OpenSearchTab() {
	$(".query1").addClass("visible");
	$('#searchKeyword').val("");
	$('#user_Name').focus();
	reSet();

}

$(document).on('hidden.bs.modal', '.bootbox', function () {
	$('#user_Name').focus();
	$('#userName').focus();

});


$(document).click(function (event) {
	//if you click on anything except the modal itself or the "open modal" link, close the modal
	if (!$(event.target).closest(".query1,.dropdown-form").length) {
		$("body").find(".query1").removeClass("visible");
		$('#searchKeyword').val("");
		$("#user_Name").val("");
		$("#contactNum").val("");
	}
   
});

//var searchTable;
$(document).ready(function () {

	$('#btnreset').hide();
	$('#saveUserMasterData').hide();
	//$('#searchKeyword').focus();
	$('.date_picker').datepicker({
		format: "dd/mm/yyyy"
	}).on('change', function () {
		debugger;
		$('.datepicker').hide();
	}).on("keydown", function (e) {
		e.preventDefault();
	});
   // $('#searchKeyword').focus();
   // setAndSetUserScreenModules();
	searchUserMasterTab(1);
	var searchKeyword = document.getElementById("searchKeyword");
	searchKeyword.addEventListener("keydown", function (event) {
		if (event.keyCode === 13) {
			searchUsersUsingSearchBar();
		}
	});
	userMasterTag();
	$('#query1').hide();
	$('.user').hide();
	
	fetchAndSetRoles();
   
	fetchAndSetIntiallyDataForStatus();
	
	var searchTable = $('.searchTable').DataTable();
	searchTable.column(0).visible(false);

	$('.roleTable').DataTable({
	   
		columns: [

			{ title: "Role Code" },
			{ title: "Role" },
			{ title: "" },
			 { title: "Role Id" },
			  { title: "CheckRole" },
		]
	});
	$('.roleTable').DataTable().column(0).visible(false);
	$('.roleTable').DataTable().column(3).visible(false);
	$('.roleTable').DataTable().column(4).visible(false);
	$('#roleTable').DataTable({

		columns: [
			{ title: "Role Code" },
			{ title: "Role" },
			{ title: "#" },
			 { title: "Role ID" },
		]
	});
	removeItemDataTableControl();

	$('#userRolesTableBody_paginate').bind('click', function () {
		for (var i = 0; i < GettotalRoles.length; i++) {
			$("#userRole-" + GettotalRoles[i]).prop("checked", true);
		}
		//SetRolesLevelAcess(rolesRight);
		//diableBtnForStatus($('#cobStatus').val());
	});

	$('#userRolesTableBody_filter input').bind('keyup', function (e) {
		for (var i = 0; i < GettotalRoles.length; i++) {
			$("#userRole-" + GettotalRoles[i]).prop("checked", true);
		}
	});

	$('#userRolesTableBody_length select option').each(function () {
		for (var i = 0; i < GettotalRoles.length; i++) {
			$("#userRole-" + GettotalRoles[i]).prop("checked", true);
		}
	});
   

	//$('#userRolesTableBody_length input').bind('select', function (e) {
	//    for (var i = 0; i < GettotalRoles.length; i++) {
	//        $("#userRole-" + GettotalRoles[i]).prop("checked", true);
	//    }
	//});
	$('#status').val(1);


	//$('#userRolesTableBody_length input').bind('select', function (e) {
	//    for (var i = 0; i < GettotalRoles.length; i++) {
	//        $("#userRole-" + GettotalRoles[i]).prop("checked", true);
	//    }
	//});
	
	$('select[name="userRolesTableBody_length"]').change(function(e) {
		for (var i = 0; i < GettotalRoles.length; i++) {
			$("#userRole-" + GettotalRoles[i]).prop("checked", true);
		}
});
	
		//$.fn.dataTableExt.afnFiltering.push(function (oSettings, aData, iDataIndex) {
		//    var checked = $('#checkbox').is(':checked');

		//    if (checked && aData[4] == 1) {
		//        return true;
		//    }
		//    if (!checked && aData[4] ==0) {
		//        return true;
		//    }
		//    return false;
		//});
		//var oTable1 = $('#userRolesTableBody').dataTable();
		//$('#checkbox').on("click", function (e) {
		//    console.log('click');
		//    oTable1.fnDraw();
		//});

	//$(':checkbox').on('change', function () {
	//    // clear the previous search
	//    var userRolesTableBody = $('#userRolesTableBody').DataTable();
	//    userRolesTableBody.columns().every(function () {
	//        this.search('');
	//    });

	//    // apply new search
	//    $(':checkbox:checked').each(function () {
	//        console.log($(this).attr('name') + ": " + $(this).val());
	//        userRolesTableBody.column(4).search(0);
	//    });

	//    userRolesTableBody.draw();
	//});
   



	//$("#cboRoles").change(function () {
	//    var userRolesTableBody = $('#userRolesTableBody').DataTable();
	//    var x = $('#cboRoles').val();

	//    table.column($(this).attr('name')).search('Yes');
	//    table.draw();
	//    // userRolesTableBody.columns(4).search(parseInt(x), true).draw();
	   
	//   // userRolesTableBody.api().column([0]).every;
	//});




	$('input[type=radio][name=role]').change(function () {

		var InputData = {};
		InputData.RoleStatus = $(this).val();
		InputData.userID = $('#hdnUserID').val();
		
		//if (InputData = 1) {
		//    $("#myCheck2").prop("checked", true);
		//    $("#myCheck1").prop("checked", false);
		//    $("#myCheck").prop("checked", false);

		//}
		//else if (InputData = 1) {
		//    $("#myCheck1").prop("checked", true);
		//    $("#myCheck").prop("checked", false);
		//    $("#myCheck2").prop("checked", false);
		//}
		//else {
		//    $("#myCheck").prop("checked", true);
		//    $("#myCheck1").prop("checked", false);
		//    $("#myCheck2").prop("checked", false);

		//}



		ajaxCallerWithoutMessage("POST", "UserMaster/GetUserRole", JSON.stringify(InputData), "application/json").done(function (data) {
			debugger;
			if (data.Status == 1) {
				var html = "";
				var oResult = data.Result;
				var dataTable = $('#userRolesTableBody').DataTable();
				dataTable.clear().draw();
				//var UserRoleCheckBox = <div class="checkbox"><input class="colored-success" id="userRole-' + value.fdRoleId + '" name="userRoles" type="checkbox" value="true"></div>
				$.each(oResult, function (key, value) {
					totalRoles.push(value.fdRoleId);

					var UserRoleCheckBox = '<div class="checkbox"><input class="colored-success" id="userRole-' + value.fdRoleId + '" name="userRoles" type="checkbox" value="true"></div>';
					dataTable.row.add([value.fdRoleCode, value.fdRoleName, UserRoleCheckBox, value.fdRoleId, 0
					]).draw();
					if (value.Signed == 1) {
						$("#userRole-" + value.fdRoleId).prop("checked", true);
						$("#userRole-" + value.fdRoleId, dataTable.cells().nodes()).attr('checked', true);
						GettotalRoles.push(value.fdRoleId);
					}
					else if (value.Signed == 0) {
						$("#userRole-" + value.fdRoleId).prop("checked", false);
					}



				});

				dataTable.columns.adjust().draw();
				$(".dataTables_wrapper .dataTables_filter input").val("");

			} else if (data.Status == 2) {
				showMessage("red", data.Description, "");
			} else {
				showMessage("red", data.Description, "");
			}
		});

	});






});





var userRolesTableBody = "";
$(function () {
	 userRolesTableBody = $('#userRolesTableBody').DataTable();
   // otable = $('#dt').dataTable();
})








var totalRoles=[];
/*
* fetch and set initially User Roles
*/
function fetchAndSetRoles() {
	debugger;
	var inputData = {};

	ajaxCaller("POST", "UserMaster/fetchAndSetRoles", JSON.stringify(inputData), "application/json", "Please wait! Loading form values").done(function (data) {
		debugger;
		if (data.Status == 1) {
			var html = "";
			var oResult = data.Result;
			var dataTable = $('#userRolesTableBody').DataTable();
			dataTable.clear().draw();
			//var UserRoleCheckBox = <div class="checkbox"><input class="colored-success" id="userRole-' + value.fdRoleId + '" name="userRoles" type="checkbox" value="true"></div>
			$.each(oResult, function (key, value) {
				totalRoles.push(value.fdRoleId);
				var UserRoleCheckBox = '<div class="checkbox"><input class="colored-success" id="userRole-' + value.fdRoleId + '" name="userRoles" type="checkbox" value="true"></div>';
				dataTable.row.add([value.fdRoleCode, value.fdRoleName, UserRoleCheckBox, value.fdRoleId,0
				]).draw();
			  
			});
			dataTable.columns.adjust().draw();
			//$.each(oResult, function (key, value) {
			//    totalRoles.push(value.fdRoleId);
			//    html += '<tr><td>' + value.fdRoleCode + '</td><td>' + value.fdRoleName + '</td><td><div class="checkbox"><input class="colored-success" id="userRole-' + value.fdRoleId + '" name="userRoles" type="checkbox" value="true"></div></td></tr>';
			//});
			//$("#userRolesTableBody").html(html);
		} else if (data.Status == 2) {
			showMessage("red", data.Description, "");
		} else {
			showMessage("red", data.Description, "");
		}
	});
}


//var userStatusArr;
function fetchAndSetIntiallyDataForStatus() {
	var inputData = {};
	var lookUpCodes = [];
	lookUpCodes.push("DEPARTMENT");
	inputData.lookUpCode = lookUpCodes;
	inputData.KeyCode = "";
	ajaxCaller("POST", "Common/fetchStatus", JSON.stringify(inputData), "application/json", "Please wait! Loading form values").done(function (data) {
		if (data.Status == 1) {
			var oResult = data.Result;
			userStatusArr = oResult;
			$.each(oResult, function (key, value) {
				if (value.fdLkUpCode == "DEPARTMENT") {
					$('#cboUserDepartment').append($('<option></option>').val(value.fdKeyCode).html(value.fdKey1));
				}
			});
		} else if (data.Status == 2) {
			showMessage("red", data.Description, "");
		} else {
			showMessage("red", data.Description, "");
		}
	});
}

///*
//* search User master
//*/
//function searchUserMaster(message) {
//    if (message == "" || message == undefined) {
//        message = "Searching Users";
//    }
//    var inputData = {};
//    inputData.txtSchUserName = $("#txtSchUserName").val();
//    inputData.txtSchContactNo = $("#txtSchContactNo").val();
//    ajaxCaller("POST", "UserMaster/searchUserMaster", JSON.stringify(inputData), "application/json", message).done(function (data) {
//        searchTable.clear().draw();
//        if (data.Status == 1) { 
//            var oResult = data.Result;
//            if (oResult.length > searchCount) {
//                showMessage("green", "There are more then " + searchCount + " users, Please refine your search.", "");
//            }
//            var searchDataSet = [];
//            debugger;
//            for (i = 0; i < oResult.length; i++) {
//                var dataSet = [];
//                var result = oResult[i];
//                dataSet.push(i + 1);
//                dataSet.push(result.userId);
//                dataSet.push(result.userName);
//                dataSet.push(result.contactNo);
//                dataSet.push(result.userLocation);
//                dataSet.push(result.userEmail);
//                dataSet.push(result.userRole);
//                var status = "";
//                $.each(userStatusArr, function (key, value) {
//                    if (result.fdIsActive == value.fdKeyCode) {
//                        status = value.fdKey1;
//                    }
//                });
//                dataSet.push(status);
//                searchDataSet[i] = dataSet;
//            }
//            searchTable.rows.add(searchDataSet).draw();
//            searchTable.columns.adjust().draw();
//        } else if (data.Status == 2) {
//            showMessage("red", data.Description, "");
//        } else {
//            showMessage("red", data.Description, "");
//        }
//    });
//}


var GettotalRoles = [];

var glresult = "";
function removeItemDataTableControl() {
	$('#grdSrchUser tbody').off('dblclick').on('dblclick', 'tr', function () {
		$("#All").prop("checked", true);
		fetchAndSetRoles();
		
		var grdItemDataTable = $('#grdSrchUser').DataTable();
		var rowIndex = grdItemDataTable.row(this).index()
		var data = grdItemDataTable.row(this).data();
		var requestData = data[0];
		
		ajaxCaller("POST", "UserMaster/GetUserDetail", data[0], "application/json", "Please wait ! Getting user details").done(function (data) {
			if (data.Status == 1) {
				debugger;
				var oResult = data.Result;
				glresult = oResult;
				var grdItemDataTable = $('#grdSrchUser').DataTable();


				//$('.nav-tabs a[href="#UserCreateUpdateTab"]').tab('show');
				$('.user').show();
				$('.userMaster').hide();
				//if (oResult.id != null && oResult.id != undefined && oResult.id != "")
				//    $("#hdnUserID").val(oResult.id);
				if (oResult.fduserid != null && oResult.fduserid != undefined && oResult.fduserid != "")
					$("#hdnUserID").val(oResult.fduserid).attr('disabled', true);

				if (oResult.fdDepartment != null && oResult.fdDepartment != undefined && oResult.fdDepartment != ""){
					$('#cboUserDepartment').val(oResult.fdDepartment).prop('disabled', 'disabled');
}
				//if (oResult.firstName != null && oResult.firstName != undefined && oResult.firstName != "")
				//    $("#userName").val(oResult.firstName);

				if (oResult.fdusername != null && oResult.fdusername != undefined && oResult.fdusername != "")
					$("#userName").val(oResult.fdusername).attr('disabled', true);

				//if (oResult.email != null && oResult.email != undefined && oResult.email != "")
				//    $("#email").val(oResult.email);

				if (oResult.fdUserEmail != null && oResult.fdUserEmail != undefined && oResult.fdUserEmail != "")
					$("#email").val(oResult.fdUserEmail).prop('disabled', 'disabled');

				//if (oResult.contactNumber != null && oResult.contactNumber != undefined && oResult.contactNumber != "")
				//    $("#contactNo").val(oResult.contactNumber);

				if (oResult.fdUserContactNo != null && oResult.fdUserContactNo != undefined && oResult.fdUserContactNo != "")
					$("#contactNo").val(oResult.fdUserContactNo).prop('disabled', 'disabled');

				//if (oResult.dob != null && oResult.dob != undefined && oResult.dob != "")
				//    $("#dateOfBirth").val(oResult.dob);
				//if (oResult.CollegeId != null && oResult.CollegeId != undefined && oResult.CollegeId != "")
				//    $("#hdnSchCollegeID").val(oResult.CollegeId);
				//if (oResult.CollegeName != null && oResult.CollegeName != undefined && oResult.CollegeName != "")
				//    $("#college").val(oResult.CollegeName);
				//if (oResult.CityId != null && oResult.CityId != undefined && oResult.CityId != "")
				//    $("#hdnSchCityID").val(oResult.CityId);
				//if (oResult.CityName != null && oResult.CityName != undefined && oResult.CityName != "")
				//    $("#city").val(oResult.CityName);
				//if (oResult.StateId != null && oResult.StateId != undefined && oResult.StateId != "")
				//    $("#hdnSchStateID").val(oResult.StateId);
				//if (oResult.StateDesc != null && oResult.StateDesc != undefined && oResult.StateDesc != "")
				//    $("#state").val(oResult.StateDesc);
				//if (oResult.CountryId != null && oResult.CountryId != undefined && oResult.CountryId != "")
				//    $("#hdnSchCountryID").val(oResult.CountryId);
				//if (oResult.Countryname != null && oResult.Countryname != undefined && oResult.Countryname != "")
				//    $("#country").val(oResult.Countryname);
				//if (oResult.pincode != null && oResult.pincode != undefined && oResult.pincode != "")
				//    $("#pincode").val(oResult.pincode);
				//if (oResult.password != null && oResult.password != undefined && oResult.password != "")
				//    $("#password").val(oResult.password);
				//if (oResult.password != null && oResult.password != undefined && oResult.password != "")
				//    $("#repassword").val(oResult.password);
				//if (oResult.isActive != null && oResult.isActive != undefined && oResult.isActive != "")
				//    $("#status").val(oResult.isActive);
				//if (oResult.address != null && oResult.address != undefined && oResult.address != "")
				//    $("#address").val(oResult.address);
				$("#status").val("");
				if (oResult.UserStatus != null && oResult.UserStatus != undefined && oResult.UserStatus != "")
				{

				}
					
				var x = parseInt(oResult.UserStatus);
				$("#status").val(x);
			 


			   
				if (oResult.Role != null && oResult.Role != undefined && oResult.Role != "")
					var roles = oResult.Role.split(',');


				var EmptyuserRolesTableBody = $('#userRolesTableBody').DataTable();

				for (var i = 0; i < totalRoles.length-1; i++) {
					$("#userRole-" + totalRoles[i]).prop("checked", false);

				   // var row = EmptyuserRolesTableBody.row(i);
				   // EmptyuserRolesTableBody.cell(row, 4).data(0).draw();
				   // $('#userRolesTableBody tr:eq('+i+') th:eq(4)').text(1);
				   
				}
			   
				if (roles != null && roles != undefined)
				{
					var userRolesTableBody = $('#userRolesTableBody').DataTable();
				for (var i = 0; i < roles.length; i++) {
					   // var ChkBox = userRolesTableBody.data()[i][2];
					$("#userRole-" + roles[i]).prop("checked", true);
						$("#userRole-" + roles[i], userRolesTableBody.cells().nodes()).attr('checked', true);

						var getRoleData = userRolesTableBody.rows({ filter: 'applied' }).data();
						// get indexes of sorted data
						var indexes = userRolesTableBody.rows({ filter: 'applied' }).indexes();

						for (var j = 0; j <= getRoleData.length-1; j++)
						{
							if (roles[i] == getRoleData[j][3])
							{
								var row = userRolesTableBody.row(j);
								userRolesTableBody.cell(row, 4).data(1);
				}
						}

						//userRolesTableBody[i][4] = 1;
						//// $.uniform.update("#userRole-" + roles[i]);
						//userRolesTableBody.row(i).data(matchTruckData[i]).draw();
					   // $('#userRolesTableBody tr:eq(' + i + ') th:eq(4)').text(1);
					}
					userRolesTableBody.draw();
				}
			   
				//if (oResult.fdRoleLocUserID != null && oResult.fdRoleLocUserID != undefined && oResult.fdRoleLocUserID != "")
				$('#cboUserDepartment').attr('disabled', true)
				$('#saveUserMasterData').show();
				//    $("#hdnRoleLocUserID").val(oResult.fdRoleLocUserID);
				//showMessage("green", data.Description, "");
				GettotalRoles = roles;
				$("#password").prop('disabled', 'disabled');
				$("#repassword").prop('disabled', 'disabled');
			}
			else {
				showMessage("red", data.Description, "");
			}
		});
		$('#grdSrchUser').off('draw.dt').on('draw.dt', function () {
			removeItemDataTableControl();
		   
		});
	});

	for (var i = 0; i < GettotalRoles.length; i++) {
		$("#userRole-" + GettotalRoles[i]).prop("checked", true);
}
}





// Serach and Select College
function getCollegeList(userEnter) {
	var inputData = {};
	inputData.userCollege = userEnter;

	ajaxCaller("POST", "UserMaster/getCollegeList", JSON.stringify(inputData), "application/json", "Please wait!").done(function (data) {
		if (data.Status == 1) {
			//showMessage("green", data.Description, "");
			debugger;
			showResultList(data, 'college', 'collegesList', 'hdnSchCollegeID');
		}
		else {
			showMessage("red", data.Description, "");
		}
	})
}




//search and select City
function getCitiesList(userEnter) {
	debugger;
	var inputData = {};

	inputData.userCity = userEnter;

	ajaxCaller("POST", "UserMaster/getCitiesList", JSON.stringify(inputData), "application/json", "Please wait!").done(function (data) {
		if (data.Status == 1) {
			//showMessage("green", data.Description, "");
			debugger;
			showResultList(data, 'city', 'citiesList', 'hdnSchCityID');
		}
		else {
			showMessage("red", data.Description, "");
		}
	})

}

//Search and Select State

function getStateList(userEnter) {
	debugger;
	var inputData = {};

	inputData.userState = userEnter;

	ajaxCaller("POST", "UserMaster/getStateList", JSON.stringify(inputData), "application/json", "Please wait!").done(function (data) {
		if (data.Status == 1) {
			//showMessage("green", data.Description, "");
			debugger;
			showResultList(data, 'state', 'stateList', 'hdnSchStateID');
		}
		else {
			showMessage("red", data.Description, "");
		}
	})

}


// Search And Select Country

function getCountryList(userEnter) {
	debugger;
	var inputData = {};

	inputData.userCountry = userEnter;

	ajaxCaller("POST", "UserMaster/getCountryList", JSON.stringify(inputData), "application/json", "Please wait!").done(function (data) {
		if (data.Status == 1) {
			//showMessage("green", data.Description, "");
			debugger;
			showResultList(data, 'country', 'countryList', 'hdnSchCountryID');
		}
		else {
			showMessage("red", data.Description, "");
		}
	})

}
// funtion to save User tab details in user table in DB.
function saveUserMasterData() {
	debugger;
	var userID=0;
	userID = $("#hdnUserID").val();
	var userName = $("#userName").val();
	var eMail = $("#email").val();
	var contactNo = $("#contactNo").val();
   
	var password = $("#password").val();
	var repassword = $('#repassword').val();
	var status = $('#status').val();
	var userRoles = [];

	var roleStatus = $("input[name='role']:checked").val();

	var dataTable = $('#userRolesTableBody').DataTable();
	dataTable.rows().nodes().to$().find("input:checkbox[name='userRoles']:checked").each(function () {
		var roleId = this.id.split("-");
		userRoles.push(roleId[1]);
	});
	//$("input:checkbox[name='userRoles']:checked").each(function () {
	//    var roleId = this.id.split("-");
	//    userRoles.push(roleId[1]);
	//})
	var roleUserLinkId = $('#hdnRoleLocUserID').val();

	if (userName == "" || userName == undefined || userName == null) {
		showMessage("red", "Please, Enter User Name.", "");
		$("#userName").focus();
		$("#userName").addClass("inputError");
		return false;
	}
	if (eMail == "" || eMail == undefined || eMail == null) {
		showMessage("red", "Please, Enter Email Id.", "");
		$("#email").focus();
		$("#email").addClass("inputError");
		return false;
	}
	//if (validateEmailAddress(eMail) == 0) {
	//    showMessage("red", "Please, EnterValid Email Id.", "");
	//    $("#email").focus();
	//    $("#email").addClass("inputError");
	//    return false;
	//}
	if (contactNo == "" || contactNo == undefined || contactNo == null) {
		showMessage("red", "Enter 10 digit mobile number.", "");
		$("#contactNo").addClass("inputError");
		$("#contactNo").focus();
		return false;
	}

	if ((contactNo.length < 10) || (contactNo.length > 10)) {
		alert(" Your Mobile Number must be 1 to 10 Integers");
		$("#contactNo").addClass("inputError");
		$("#contactNo").focus();
		return false;
	}
	
	if (userID == 0) {
		if ((userID == "" || userID == undefined || userID == null || userID == 0) || (userID != "" && userID != undefined && userID != null && userID != 0 && password != "" && password != undefined && password != null)) {
			if (password == "" || password == undefined || password == null) {
				showMessage("red", "Enter minimum 6 digit User Password.", "");
				$("#password").addClass("inputError");
				$("#password").focus();
				return false;
			}
			if (repassword == "" || repassword == undefined || repassword == null) {
				showMessage("red", "Re-Enter minimum 6 digit User Password.", "");
				$("#repassword").addClass("inputError");
				$("#repassword").focus();
				return false;
			}
			if (password != repassword) {
				showMessage("red", "Password and Re-Password did not matched.", "");
				$("#password").val("");
				$("#repassword").val("");
				return false;
			}
		}
	}

	//if (address == "" || address == undefined || address == null) {
	//    showMessage("red", " Enter User Address.", "");
	//    $("#address").addClass("inputError");
	//    $("#address").focus();
	//    return false;
	//}
	if (status == "" || status == undefined || status == null) {
		showMessage("red", "Select User Status.", "");
		$("#status").addClass("inputError");
		$("#status").focus();
		return false;
	}

	if (userRoles.length < 1) {
		showMessage("red", " Select User Role.", "");
	}

	var inputData = {};
	if ($('#cboUserDepartment').val() != "" && $('#cboUserDepartment').val() != undefined && $('#cboUserDepartment').val() != null) {
		inputData.Department = $('#cboUserDepartment').val();
	}
	else
	{
		if(userID==0)
		{
			showMessage("red", "Select Department.", "");
			$("#cboUserDepartment").addClass("inputError");
			$("#cboUserDepartment").focus();
			return false;
		}
	}

	inputData.userName = userName;
	inputData.hdnUserId = userID;
	inputData.eMail = eMail;
	inputData.contactNo = contactNo;
	//inputData.dateOfBirth = dateOfBirth;
	//inputData.college = college;
	//inputData.city = city;
	//inputData.state = state;
	//inputData.country = country;
	//inputData.pincode = pincode;
	inputData.password = password;
	inputData.repassword = repassword;
	inputData.roleStatus = roleStatus;
	if (userID != null && userID != undefined) {
		inputData.userId = userID;
	}
	if (roleUserLinkId != null && roleUserLinkId != undefined) {
		inputData.roleUserLinkId = roleUserLinkId;
	}

	if (status == '1') {
		inputData.status = '1';
	} else {
		inputData.status = '0';
	}

   // inputData.address = address;
	inputData.userRoles = userRoles;

	ajaxCaller("POST", "UserMaster/saveUserMasterData", JSON.stringify(inputData), "application/json", "Please wait ! Saving User").done(function (data) {
		if (data.Status == 1) {
			showMessage("green", data.Description, "");
			//reset form            
			$("#hdnUserId").val("");
		}
		else
			showMessage("red", data.Description, "");
	});
}

function reset(msg) {
	if (msg == 1)
	{
		bootbox.confirm({
			message: "Are you sure you'd like to clear from fields?",
			buttons: {
				confirm: {
					label: 'Yes',
					className: 'btn-danger'
				},
				cancel: {
					label: 'No',
					className: 'btn-primary'
				}
			},
			callback: function (result) {
				if (result == true) {

					debugger;
					if ($("#hdnUserID").val() != "" && $("#hdnUserID").val() != undefined && $("#hdnUserID").val() != null) {
						$('#saveUserMasterData').prop('disabled', 'disabled');
						$("#hdnUserID").val("");
						$("#userName").val("");
						$("#userName").focus();
						$("#email").val("");
						$("#contactNo").val("");
						$("#dateOfBirth").val("");
						$("#college").val("");
						$("#city").val("");
						$("#state").val("");
						$("#country").val("");
						$("#pincode").val("");
						$("#password").val("");
						$('#repassword').val("");
						$('#status').val(1).prop('disabled', 'disabled');
						$('#address').val("");
						$('#cboUserDepartment').attr('disabled', false);
						$('#cboUserDepartment').val(0);

						var grdUserRole = $('#userRolesTableBody').DataTable();


						for (var i = 1; i < grdUserRole.data().length; i++) {
							var Data = grdUserRole.row(i).data();
							var RoleID = Data[3];


							$("#userRole-" + RoleID).prop("checked", false);
						}
						$("#userName").focus();
						//var oResult = glresult;
						//if (oResult.fduserid != null && oResult.fduserid != undefined && oResult.fduserid != "")
						//    $("#hdnUserID").val(oResult.fduserid);

						//if (oResult.fdDepartment != null && oResult.fdDepartment != undefined && oResult.fdDepartment != "") {
						//    $('#cboUserDepartment').val(oResult.fdDepartment);
						//}

						//if (oResult.fdusername != null && oResult.fdusername != undefined && oResult.fdusername != "")
						//    $("#userName").val(oResult.fdusername);

						//if (oResult.fdUserEmail != null && oResult.fdUserEmail != undefined && oResult.fdUserEmail != "")
						//    $("#email").val(oResult.fdUserEmail);

						//if (oResult.fdUserContactNo != null && oResult.fdUserContactNo != undefined && oResult.fdUserContactNo != "")
						//    $("#contactNo").val(oResult.fdUserContactNo);

						//$("#status").val("");
						//if (oResult.UserStatus != null && oResult.UserStatus != undefined && oResult.UserStatus != "") {

						//}

						//var x = parseInt(oResult.UserStatus);
						//$("#status").val(x);


						//if (oResult.Role != null && oResult.Role != undefined && oResult.Role != "")
						//    var roles = oResult.Role.split(',');


						//var EmptyuserRolesTableBody = $('#userRolesTableBody').DataTable();

						//for (var i = 0; i < totalRoles.length; i++) {
						//    $("#userRole-" + totalRoles[i]).prop("checked", false);

						//    var row = EmptyuserRolesTableBody.row(i);
						//    EmptyuserRolesTableBody.cell(row, 4).data(0).draw();
						//    // $('#userRolesTableBody tr:eq('+i+') th:eq(4)').text(1);

						//}

						//if (roles != null && roles != undefined) {
						//    var userRolesTableBody = $('#userRolesTableBody').DataTable();
						//    for (var i = 0; i < roles.length; i++) {
						//        var ChkBox = userRolesTableBody.data()[i][2];
						//        $("#userRole-" + roles[i]).prop("checked", true);
						//        $("#userRole-" + roles[i], userRolesTableBody.cells().nodes()).attr('checked', true);

						//        var getRoleData = userRolesTableBody.rows({ filter: 'applied' }).data();
						//        // get indexes of sorted data
						//        var indexes = userRolesTableBody.rows({ filter: 'applied' }).indexes();

						//        for (var j = 0; j <= getRoleData.length - 1; j++) {
						//            if (roles[i] == getRoleData[j][3]) {
						//                var row = userRolesTableBody.row(j);
						//                userRolesTableBody.cell(row, 4).data(1);
						//            }
						//        }

						//    }

						//}
						//$('#repassword').val("");
					}
					else {
						$('#saveUserMasterData').prop('disabled', 'disabled');
						$("#hdnUserID").val("");
						$("#userName").val("");
					  
						$("#email").val("");
						$("#contactNo").val("");
						$("#dateOfBirth").val("");
						$("#college").val("");
						$("#city").val("");
						$("#state").val("");
						$("#country").val("");
						$("#pincode").val("");
						$("#password").val("");
						$('#repassword').val("");
						$('#status').val(1).prop('disabled', 'disabled');
						$('#address').val("");
						$('#cboUserDepartment').attr('disabled', false);
						$('#cboUserDepartment').val(0);

						var grdUserRole = $('#userRolesTableBody').DataTable();


						for (var i = 1; i < grdUserRole.data().length; i++) {
							var Data = grdUserRole.row(i).data();
							var RoleID = Data[3];


							$("#userRole-" + RoleID).prop("checked", false);
						}
						$("#userName").focus();
					}

					//window.location = "/UserMaster";
				}
			}
		}).find("div.modal-body").addClass("ArchiveBootBoxHeight");
		$("#userName").focus();
	}
	else
	{
		debugger;
		if ($("#hdnUserID").val() != "" && $("#hdnUserID").val() != undefined && $("#hdnUserID").val() != null) {
			var oResult = glresult;
			if (oResult.fduserid != null && oResult.fduserid != undefined && oResult.fduserid != "")
				$("#hdnUserID").val(oResult.fduserid);

			if (oResult.fdDepartment != null && oResult.fdDepartment != undefined && oResult.fdDepartment != "") {
				$('#cboUserDepartment').val(oResult.fdDepartment);
			}

			if (oResult.fdusername != null && oResult.fdusername != undefined && oResult.fdusername != "")
				$("#userName").val(oResult.fdusername);

			if (oResult.fdUserEmail != null && oResult.fdUserEmail != undefined && oResult.fdUserEmail != "")
				$("#email").val(oResult.fdUserEmail);

			if (oResult.fdUserContactNo != null && oResult.fdUserContactNo != undefined && oResult.fdUserContactNo != "")
				$("#contactNo").val(oResult.fdUserContactNo);

			$("#status").val("");
			if (oResult.UserStatus != null && oResult.UserStatus != undefined && oResult.UserStatus != "") {

			}

			var x = parseInt(oResult.UserStatus);
			$("#status").val(x);


			if (oResult.Role != null && oResult.Role != undefined && oResult.Role != "")
				var roles = oResult.Role.split(',');


			var EmptyuserRolesTableBody = $('#userRolesTableBody').DataTable();

			for (var i = 0; i < totalRoles.length; i++) {
				$("#userRole-" + totalRoles[i]).prop("checked", false);

				var row = EmptyuserRolesTableBody.row(i);
				EmptyuserRolesTableBody.cell(row, 4).data(0).draw();
				// $('#userRolesTableBody tr:eq('+i+') th:eq(4)').text(1);

			}

			if (roles != null && roles != undefined) {
				var userRolesTableBody = $('#userRolesTableBody').DataTable();
				for (var i = 0; i < roles.length; i++) {
				  //  var ChkBox = userRolesTableBody.data()[i][2];
					$("#userRole-" + roles[i]).prop("checked", true);
					$("#userRole-" + roles[i], userRolesTableBody.cells().nodes()).attr('checked', true);

					var getRoleData = userRolesTableBody.rows({ filter: 'applied' }).data();
					// get indexes of sorted data
					var indexes = userRolesTableBody.rows({ filter: 'applied' }).indexes();

					for (var j = 0; j <= getRoleData.length - 1; j++) {
						if (roles[i] == getRoleData[j][3]) {
							var row = userRolesTableBody.row(j);
							userRolesTableBody.cell(row, 4).data(1);
						}
					}

				}

			}
			$('#repassword').val("");
			$("#userName").focus();
		}
		else {

			$("#hdnUserID").val("");
			$("#userName").val("");
		   
			$("#email").val("");
			$("#contactNo").val("");
			$("#dateOfBirth").val("");
			$("#college").val("");
			$("#city").val("");
			$("#state").val("");
			$("#country").val("");
			$("#pincode").val("");
			$("#password").val("");
			$('#repassword').val("");
			$('#status').val(1);
			$('#address').val("");
			$('#cboUserDepartment').attr('disabled', false);
			$('#cboUserDepartment').val(0);

			var grdUserRole = $('#userRolesTableBody').DataTable();


			for (var i = 1; i < grdUserRole.data().length; i++) {
				var Data = grdUserRole.row(i).data();
				var RoleID = Data[3];


				$("#userRole-" + RoleID).prop("checked", false);
			}
		}
		$("#userName").focus();

	}
	
}

//---- Search Tab----------//

//------fetch User Name List------//

function searchUserMasterTab(Msg) {
	debugger;
	var inputData = {};
	var userNamesch = $("#user_Name").val();
	var contactNoSch = $("#contactNum").val();

	inputData.userName = userNamesch;
	inputData.contactNumber = contactNoSch;
	ajaxCaller("POST", "UserMaster/searchUserMasterTab", JSON.stringify(inputData), "application/json", "Please wait").done(function (data) {
		if (data.Status == 1) {
			debugger;
		   // showMessage("green", data.Description);
			var userMasterDataTable = $('#grdSrchUser').DataTable();
			
			userMasterDataTable.clear().draw();
			userMasterDataTable.column(0).visible(false);

			var i = 0;
			$.each(data.Result, function (key, value) {
				userMasterDataTable.row.add([
					data.Result[i].fduserid,
					data.Result[i].fdusername,
					data.Result[i].fdUserContactNo,
					data.Result[i].fdUserEmail,
					data.Result[i].UserStatus                   
				]).draw();

				i = i + 1;
			});

			userMasterDataTable.columns([0]).order('desc').draw();
			userMasterDataTable.columns.adjust().draw();
			if (Msg != 1)
			{
			showMessage("green", data.Description)
		}
			
		}
		else {
			showMessage("red", data.Description);
		}
	});


}

function reSet(msg) {
	if (msg == 1)
	{
		bootbox.confirm({
			message: "Are you sure you'd like to clear from fields?",
			buttons: {
				confirm: {
					label: 'Yes',
					className: 'btn-danger'
				},
				cancel: {
					label: 'No',
					className: 'btn-primary'
				}
			},
			callback: function (result) {
				if (result == true) {
					$("#user_Name").val("");
					$("#contactNum").val("");
				}
			}
		}).find("div.modal-body").addClass("ArchiveBootBoxHeight");
	}
	else
	{
		$("#user_Name").val("");
		$("#contactNum").val("");
	}
   
}


//-----Edit and Update Row Data on Double Click-------//

//function editAndUpdateUserMasterRowData() {
//    debugger;
//    var inputData = {};
//    $(document).on("dblclick", "#grdSrchUser tr", function () {
//        debugger

//        var grdItemDataTable = $('#grdSrchUser').DataTable();
//        var rowIndex = grdItemDataTable.row(this).index()
//        var data = grdItemDataTable.row(this).data();
//        var requestData = data[0];
//        inputData.searchUserId = data[0];
//        ajaxCaller("POST", "/UserMaster/GetUserDetail", data[0], "application/json", "Please wait ! Getting user details").done(function (data) {
//            if (data.Status == 1) {
//                debugger;
//                var oResult = data.Result;
//                var grdItemDataTable = $('#grdSrchUser').DataTable();


//                $('.nav-tabs a[href="#UserCreateUpdateTab"]').tab('show');
//                //if (oResult.id != null && oResult.id != undefined && oResult.id != "")
//                //    $("#hdnUserID").val(oResult.id);
//                if (oResult.fduserid != null && oResult.fduserid != undefined && oResult.fduserid != "")
//                    $("#hdnUserID").val(oResult.fduserid);

//                //if (oResult.firstName != null && oResult.firstName != undefined && oResult.firstName != "")
//                //    $("#userName").val(oResult.firstName);

//                if (oResult.fdusername != null && oResult.fdusername != undefined && oResult.fdusername != "")
//                    $("#userName").val(oResult.fdusername);

//                //if (oResult.email != null && oResult.email != undefined && oResult.email != "")
//                //    $("#email").val(oResult.email);

//                if (oResult.fdUserEmail != null && oResult.fdUserEmail != undefined && oResult.fdUserEmail != "")
//                    $("#email").val(oResult.fdUserEmail);

//                //if (oResult.contactNumber != null && oResult.contactNumber != undefined && oResult.contactNumber != "")
//                //    $("#contactNo").val(oResult.contactNumber);

//                if (oResult.fdUserContactNo != null && oResult.fdUserContactNo != undefined && oResult.fdUserContactNo != "")
//                    $("#contactNo").val(oResult.fdUserContactNo);

//                //if (oResult.dob != null && oResult.dob != undefined && oResult.dob != "")
//                //    $("#dateOfBirth").val(oResult.dob);
//                //if (oResult.CollegeId != null && oResult.CollegeId != undefined && oResult.CollegeId != "")
//                //    $("#hdnSchCollegeID").val(oResult.CollegeId);
//                //if (oResult.CollegeName != null && oResult.CollegeName != undefined && oResult.CollegeName != "")
//                //    $("#college").val(oResult.CollegeName);
//                //if (oResult.CityId != null && oResult.CityId != undefined && oResult.CityId != "")
//                //    $("#hdnSchCityID").val(oResult.CityId);
//                //if (oResult.CityName != null && oResult.CityName != undefined && oResult.CityName != "")
//                //    $("#city").val(oResult.CityName);
//                //if (oResult.StateId != null && oResult.StateId != undefined && oResult.StateId != "")
//                //    $("#hdnSchStateID").val(oResult.StateId);
//                //if (oResult.StateDesc != null && oResult.StateDesc != undefined && oResult.StateDesc != "")
//                //    $("#state").val(oResult.StateDesc);
//                //if (oResult.CountryId != null && oResult.CountryId != undefined && oResult.CountryId != "")
//                //    $("#hdnSchCountryID").val(oResult.CountryId);
//                //if (oResult.Countryname != null && oResult.Countryname != undefined && oResult.Countryname != "")
//                //    $("#country").val(oResult.Countryname);
//                //if (oResult.pincode != null && oResult.pincode != undefined && oResult.pincode != "")
//                //    $("#pincode").val(oResult.pincode);
//                //if (oResult.password != null && oResult.password != undefined && oResult.password != "")
//                //    $("#password").val(oResult.password);
//                //if (oResult.password != null && oResult.password != undefined && oResult.password != "")
//                //    $("#repassword").val(oResult.password);
//                //if (oResult.isActive != null && oResult.isActive != undefined && oResult.isActive != "")
//                //    $("#status").val(oResult.isActive);
//                //if (oResult.address != null && oResult.address != undefined && oResult.address != "")
//                //    $("#address").val(oResult.address);
				
//                if (oResult.UserStatus != null && oResult.UserStatus != undefined && oResult.UserStatus != "")
//                    $("#status").val(oResult.UserStatus);

//                if (oResult.Role != null && oResult.Role != undefined && oResult.Role != "")
//                    var roles = oResult.Role.split(',');
//                for (var i = 0; i < roles.length; i++) {
//                    $("#userRole-" + roles[i]).prop("checked", true);
//                }
				   
//                //if (oResult.fdRoleLocUserID != null && oResult.fdRoleLocUserID != undefined && oResult.fdRoleLocUserID != "")

//                //    $("#hdnRoleLocUserID").val(oResult.fdRoleLocUserID);
//                showMessage("green", data.Description, "");

//            }
//            else {
//                showMessage("red", data.Description, "");
//            }
//        });
//    });

//}

//Active and inActive selected menu tab
function userMasterTag() {
	$('#documentSearchTag a').removeClass('active');
	$('#documentChartTag a').removeClass('active');
	$('#documentClassTag a').removeClass('active');
	$('#documentTag a').removeClass('active');
	$('#documentAuditTag a').removeClass('active');
	$('#userMasterTag  a').addClass('active');
}

function newUser() {
	$('.user').show();
	$('#userName').focus();
	$('.userMaster').hide();
	glresult = "";
	$("#hdnUserID").val("");
	reset();
}
function cross() {
	$('.userMaster').show();
	$('.user').hide();
  //  $('#searchKeyword').focus();
	$("#hdnUserID").val("");
	reset();
}



function searchUsersUsingSearchBar() {
	debugger;
	var inputData = {};
	var searchInputKeyword = $("#searchKeyword").val();
	inputData.searchInputKeyword = searchInputKeyword;
	ajaxCaller("POST", "UserMaster/searchUsersUsingSearchBar", JSON.stringify(inputData), "application/json", "Please wait!").done(function (data) {
		debugger;

		if (data.Status == 1) {
			debugger;
			showMessage("green", data.Description);
			var userMasterDataTable = $('#grdSrchUser').DataTable();

			userMasterDataTable.clear().draw();
			userMasterDataTable.column(0).visible(false);

			var i = 0;
			$.each(data.Result, function (key, value) {
				userMasterDataTable.row.add([
					data.Result[i].fduserid,
					data.Result[i].fdusername,
					data.Result[i].fdUserContactNo,
					data.Result[i].fdUserEmail,
					data.Result[i].UserStatus
				]).draw();

				i = i + 1;
			});

			userMasterDataTable.columns([0]).order('desc').draw();
			userMasterDataTable.columns.adjust().draw();
			showMessage("green", data.Description)
		}
		else {
			showMessage("red", data.Description, "");
		}
	});
}


$(document).bind('keypress', function (e) {
	debugger;
	if (document.getElementById("query1").classList.contains('visible')) {
		if (e.keyCode == 13) {
			searchUserMasterTab(this.value);
		}
	}

});

//function SetRolesLevelAcess(input) {

//    $("#documentChartTag").hide();
//    $("#documentClassTag").hide();
//    $("#documentTag").hide();
//    $("#documentAuditTag").hide();
//    $("#userMasterTag").hide();
//    var IsApproveRejectOccur = false;
//    for (var i = 0; i < input.length; i++) {

//        if (input[i].functionCode == 'SEARCH') {
//            $("#documentChartTag").show();
//            $("#documentClassTag").show();
//            $("#documentTag").show();
//            $("#documentAuditTag").show();
//            $("#userMasterTag").show();


//        }




//    }

//}
//function setAndSetUserScreenModules() {
//    ajaxCaller("POST", "Common/getModuleRights", "USERMAS", "application/json", "Please wait ! Getting Screen Parameters").done(function (data) {
//        console.log(data);
//        rolesRight = data.Result;
//        SetRolesLevelAcess(data.Result);
//    });
//}