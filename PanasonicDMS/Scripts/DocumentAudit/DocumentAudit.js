﻿
$(document).on('hidden.bs.modal', '.bootbox', function () {
	$('#searchFromDate').focus();
   
});


$(document).ready(function () {
	$('.date_picker').datepicker({
		format: "dd/mm/yyyy"
	}).on('change', function () {
		debugger;
		$('.datepicker').hide();
	}).on("keydown", function (e) {
		e.preventDefault();
	});
	//amit

	//$('#searchKeyword').focus();
   // setAndSetUserScreenModules();
	GetDetails(1);

	var searchKeyword = document.getElementById("searchKeyword");
	searchKeyword.addEventListener("keydown", function (event) {
		if (event.keyCode === 13) {
			searchDocumentsUsingSearchBar();
		}
	});
	documentAuditTag();
	$('#query1').hide();
 
	var docClassSearchtable = $("#grdAuditSearchtable").DataTable();
	docClassSearchtable.column(0).visible(false);
	docClassSearchtable.column(6).visible(false).order("desc");
	// docClassSearchtable.columns.adjust().draw();
	

});

function GetDetails(Msg) {
	var inputData = {};
	inputData.searchActionName = $('#searchActionName').val();
	inputData.searchFromDate = $('#searchFromDate').val();
	inputData.searchToDate = $('#searchToDate').val();
	inputData.searchUserName = $('#searchUserName').val();



	ajaxCallerWithoutMessage("POST", "DocumentAudit/GetDetails", JSON.stringify(inputData), "application/json").done(function (data) {
		if (data.Status == 1) {
			debugger;
			if (Msg != 1)
			{
				showMessage("green", data.Description);
			}
			
			var docClassSearchtable = $("#grdAuditSearchtable").DataTable();
			//$('#grdAuditSearchtable tbody').empty();
			docClassSearchtable.clear().draw();
			// docClassSearchtable.columns.adjust().draw();
			var DResult = data.Result;
			console.log(DResult);
			var Length = DResult.length;
			for (var i = 0; i < Length;) {

				docClassSearchtable.row.add([
					DResult[i].documentId, DResult[i].actionName,DResult[i].fdModuleName, DResult[i].fdDocTitle, DResult[i].fdUserName,
					 DResult[i].createdOn, DResult[i].auditId
				]).draw();
				i++;
			}
			//docClassSearchtable.columns([5]).order('').draw();
		} else {
			var docClassSearchtable = $("#grdAuditSearchtable").DataTable();
			docClassSearchtable.clear().draw();
			showMessage("red", data.Description, "");
		}
		$("body").find(".query1").removeClass("visible");

	});

}

function searchDocumentsUsingSearchBar() {
	debugger;
	var inputData = {};
	var searchInputKeyword = $("#searchKeyword").val();
	inputData.searchInputKeyword = searchInputKeyword;
	ajaxCaller("POST", "DocumentAudit/searchDocumentsUsingSearchBar", JSON.stringify(inputData), "application/json", "Please wait!").done(function (data) {
		if (data.Status == 1) {
			debugger;
			showMessage("green", data.Description);
			var docClassSearchtable = $("#grdAuditSearchtable").DataTable();
			docClassSearchtable.clear().draw();
			//docClassSearchtable.columns.adjust().draw();
			var DResult = data.Result;
			console.log(DResult);
			var Length = DResult.length;
			for (var i = 0; i < Length;) {

				docClassSearchtable.row.add([
					DResult[i].documentId, DResult[i].actionName, DResult[i].fdModuleName, DResult[i].fdDocTitle, DResult[i].fdUserName,
					 DResult[i].createdOn, DResult[i].auditId
				]).draw();
				i++;
			}

			//docClassSearchtable.columns([5]).order('desc').draw();

		} else {
			var docClassSearchtable = $("#grdAuditSearchtable").DataTable();
			docClassSearchtable.clear().draw();
			showMessage("red", data.Description, "");
		}

	});
}





//Active and inActive selected menu tab
function documentAuditTag() {
	$('#documentSearchTag a').removeClass('active');
	$('#documentChartTag a').removeClass('active');
	$('#documentClassTag a').removeClass('active');
	$('#documentTag a').removeClass('active');
	$('#documentAuditTag a').addClass('active');
	$('#userMasterTag  a').removeClass('active');
}



function queryform1()
{
	$(".query1").addClass("visible");
	$('#searchKeyword').val("");
   
	resetSearchParadoxClass();
	$('#searchFromDate').focus();
}
//$(".dropdown-form").click(function () {
//    $(".query1").addClass("visible");
//});

$(document).click(function (event) {
	//if you click on anything except the modal itself or the "open modal" link, close the modal
	if (!$(event.target).closest(".query1,.dropdown-form").length) {
		$("body").find(".query1").removeClass("visible");
		$('#searchKeyword').val("");
	}
});


function resetSearchParadoxClass(msg)
{
	if (msg == 1)
	{
		bootbox.confirm({
			message: "Are you sure you'd like to clear from fields?",
			buttons: {
				confirm: {
					label: 'Yes',
					className: 'btn-danger'
				},
				cancel: {
					label: 'No',
					className: 'btn-primary'
				}
			},
			callback: function (result) {
				if (result == true) {

					$('#searchActionName').val('');
					$('#searchFromDate').val('');
					$('#searchToDate').val('');
					$('#searchUserName').val('');
				}
			}
		}).find("div.modal-body").addClass("ArchiveBootBoxHeight");
	}
	else
	{
		$('#searchActionName').val('');
		$('#searchFromDate').val('');
		$('#searchToDate').val('');
		$('#searchUserName').val('');
	}
   

}


$(document).bind('keypress', function (e) {
	debugger;
	if (document.getElementById("query1").classList.contains('visible')) {
		if (e.keyCode == 13) {
			GetDetails();
		}
	}

});

//function SetRolesLevelAcess(input) {
//    $("#documentChartTag").hide();
//    $("#documentClassTag").hide();
//    $("#documentTag").hide();
//    $("#documentAuditTag").hide();
//    $("#userMasterTag").hide();

//    var IsApproveRejectOccur = false;
//    for (var i = 0; i < input.length; i++) {

//        if (input[i].functionCode == 'SEARCH') {
//            $("#documentChartTag").show();
//            $("#documentClassTag").show();
//            $("#documentTag").show();
//            $("#documentAuditTag").show();
//            $("#userMasterTag").show();


//        }




//    }

//}
//function setAndSetUserScreenModules() {
//    ajaxCaller("POST", "Common/getModuleRights", "DOCAUDIT", "application/json", "Please wait ! Getting Screen Parameters").done(function (data) {
//        console.log(data);
//        rolesRight = data.Result;
//        SetRolesLevelAcess(data.Result);
//    });
//}