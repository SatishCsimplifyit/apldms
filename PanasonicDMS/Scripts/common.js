
document.addEventListener("DOMContentLoaded", function (event) {
	//tail.DateTime(".tail-datetime-field");
	if (window.innerWidth < 992) {
		var isMobile = true;
		document.querySelector("#sidebar").classList.add("sidebar-close");
		document.querySelector("#sidebar1").classList.add("sidebar-close1");
	}

	document.querySelector("#toggle-sidebar").addEventListener("click", function (event) {
		document.querySelector("#sidebar").classList.toggle("sidebar-close");
		setTimeout(function () {
			if (document.querySelector("#sidebar").classList.contains("sidebar-close")) {
				document.querySelector(".content").classList.remove("hide-on-mob-toggle");
				if (isMobile) {
					document.querySelector("#sidebar1").style.display = "block";
				}
			} else {
				document.querySelector(".content").classList.add("hide-on-mob-toggle");
				if (isMobile) {
					document.querySelector("#sidebar1").style.display = "none";
				}
			}
		}, 10);
	});
});



function ResetSearchkeyword() {
	$('#searchKeyword').val("");
}

function OpenHelp(ModuleName) {
	var requestData = {};
	requestData["ModuleName"] = ModuleName;
	ajaxCaller("POST", "Common/GetHelpURL", JSON.stringify(requestData), "application/json", "Open Help")
		   .done(function (data) {
			   if (data.Status == 1) {
				   var win = window.open(data.Result, '_blank');
				   win.focus();
			   } else {
				   showMessage('red', 'Help not configured. Contact Administrator.');
			   }
		   })
}

$(document).ready(function () {
	//sidebar fix code starts here
	$(document).on('click', '#toggle-sidebar', function () {
		if ($('#sidebar').hasClass('sidebar-close')) {
			$('.content').css('margin-left', '0');
		} else {
			$('.content').css('margin-left', '225px');
		}
	});
	//sidebar fix code ends here


	//tail.DateTime(".tail-datetime-field", {
	//    animate: true,
	//    classNames: false,
	//    closeButton: true,              // New in 0.4.5
	//    dateFormat: "YYYY-mm-dd",
	//    dateStart: false,
	//    dateRanges: [],
	//    dateBlacklist: true,
	//    dateEnd: false,
	//    locale: "en",
	//    position: "bottom",
	//    rtl: "auto",
	//    startOpen: false,
	//    stayOpen: false,
	//    timeFormat: "HH:ii:ss",
	//    timeHours: null,                // New Syntax in 0.4.5
	//    timeMinutes: null,              // New Syntax in 0.4.5
	//    timeSeconds: 0,                 // New Syntax in 0.4.5
	//    timeIncrement: true,            // New in 0.4.5
	//    timeStepHours: 1,               // New in 0.4.3
	//    timeStepMinutes: 5,             // New in 0.4.3
	//    timeStepSeconds: 5,             // New in 0.4.3
	//    today: true,
	//    tooltips: [],
	//    viewDefault: "days",
	//    viewDecades: true,
	//    viewYears: true,
	//    viewMonths: true,
	//    viewDays: true,
	//    weekStart: 0
	//});


	setAndSetUserScreenModuleshome();
	var header = $(".navbar");
	$(window).scroll(function () {
		var scroll = $(window).scrollTop();
		if (scroll >= 50) {

			header.addClass("scrolled");
		} else {

			header.removeClass("scrolled");
		}
	});
	readmore();
	var inputData = {};
	var lookUpCodes = [];
	lookUpCodes.push("INCIDENCE_STATUS");
	lookUpCodes.push("INCIDENCE_PRIORITY");
	//lookUpCodes.push("INVOICE_REGION");
	//lookUpCodes.push("INVOICE_STATUS");
	inputData.lookUpCode = lookUpCodes;

	//ajaxCaller("POST", "/TMS/fetchStatus", JSON.stringify(inputData), "application/json", "Please wait ! Getting Screen Parameters").done(function (data) {
	//    if (data.Status == 1) {
	//        debugger;
	//        var i = 0;
	//        var oResult = data.Result;
	//        $.each(oResult, function (key, value) {
	//            if (value.fdLkUpCode == "INCIDENCE_STATUS") {
	//                $('#cobSchIncidenceStatus').append($('<option></option>').val(value.fdKeyCode).html(value.fdKey1));
	//                $('#cobIncidentStatus').append($('<option></option>').val(value.fdKeyCode).html(value.fdKey1));

	//            }
	//            else if (value.fdLkUpCode == "INCIDENCE_PRIORITY") {
	//                $('#cobPriority').append($('<option></option>').val(value.fdKeyCode).html(value.fdKey1));
	//               // InvoiceRegionfdKeyCode[i] = value.fdKeyCode;
	//               // InvoiceRegionfdKey1[i] = value.fdKey1;
	//               // i++;
	//            }
	//            else if (value.fdLkUpCode == "INVOICE_STATUS") {
	//               // $('#cobSchInvoiceStatus').append($('<option></option>').val(value.fdKeyCode).html(value.fdKey1));
	//            }
	//            else if (value.fdLkUpCode == "INVOICE_PAYMENT_MODE") {
	//                //$('#cboInvoiceType').append($('<option></option>').val(value.fdKeyCode).html(value.fdKey1));
	//            }
	//        });

	//    } else if (data.Status == 2) {
	//        showMessage("red", data.Description, "");
	//    } else {
	//        showMessage("red", data.Description, "");
	//    }
	//});

	// SearchIncidence();

});
$(function () {


});



function readmore() {
	var showChar = 40;
	var ellipsestext = "...";
	var moretext = "more";
	var lesstext = "less";
	$('.more').each(function () {
		var content = $(this).html();

		if (content.length > showChar) {

			var c = content.substr(0, showChar);
			var h = content.substr(showChar - 1, content.length - showChar);

			var html = c + '<span class="moreelipses">' + ellipsestext + '</span>&nbsp;<span class="morecontent"><span>' + h + '</span>&nbsp;&nbsp;<a href="" class="morelink">' + moretext + '</a></span>';

			$(this).html(html);
		}

	});

	$(".morelink").click(function () {
		if ($(this).hasClass("less")) {
			$(this).removeClass("less");
			$(this).html(moretext);
		} else {
			$(this).addClass("less");
			$(this).html(lesstext);
		}
		$(this).parent().prev().toggle();
		$(this).prev().toggle();
		return false;
	});
}


function capture(VerifierOrCreator, countCklistItem, CaseId, CheckListId, ChklistItemId, CheckListCatId) {
	debugger
	showMessage("loading", "Please wait ! Reporting Incident");
	var element = jQuery("#ScreenTOCapture");
	// $('#ScreenTOCapture').
	html2canvas($('#ScreenTOCapture').get(0)).then(function (canvas)
		//html2canvas(element,
		// {
		// background: '#FFFFFF',
		//onrendered:
		//function (canvas) 
	{


		var form = new FormData();
		form.append("fdCaseId", CaseId);
		form.append("fdCheckListId", CheckListId);
		form.append("fdCheckListCatId", CheckListCatId);
		form.append("fdChklistItemId", ChklistItemId);
		form.append("verifierOrCreator", VerifierOrCreator);

		if (VerifierOrCreator == 'C') {
			form.append("Description", $("#textForincident" + countCklistItem).val());
		}
		else if (VerifierOrCreator == 'V') {
			form.append("Description", $("#textForincidentV" + countCklistItem).val());
		}
		else {
			showMessage("red", "Verifier or Creator Parameter not satisfied!");
			return;
		}

		form.append("ImageData", canvas.toDataURL("image/png"));
		if (UploadedDocs.length > 0) {
			for (i = 0; i <= UploadedDocs.length - 1; i++) {
				var fileData = UploadedDocs[i];
				form.append("AttachmentsIDs", fileData);
			}

		}

		debugger
		$.ajax({
			url: 'Ticket/saveIncident',
			data: form,
			processData: false,
			contentType: false,
			type: 'POST',
			success: function (data) {
				if (data.Status == 1) {
					showMessage("green", data.Description, "");
					$('#textForincident' + countCklistItem).val("");
					$('#AttchmentsLabel').html("");
					$('#button_issue_creator_notif_' + countCklistItem).text(data.Result)
					UploadedDocs = "";
					$(".incidence").css('display', 'none');


				} else if (data.Status == 2) {
					showMessage("red", data.Description, "");
				} else {
					showMessage("red", data.Description, "");
				}
			},
			error: function (request, error) {
				console.log(arguments);
				alert(" Can't do because: " + error);
			},
			beforeSend: function () {
				//showMessage("loading", "Please wait ! Reporting Incident");
			},
			complete: function () {
				$('.ajaxLoading').hide();
				$("#IncidentSection").css('display', 'none');
				$("#textForincident").val("");
			}
		});
		// }
	});
}




var UploadedDocs = [];
$("#uploadAttchment").on('change', function () {

	var uploadFiles = this.files;
	console.log(uploadFiles);
	// Getting the properties of file from file field
	var formData = new FormData();                // Creating object of FormData class



	var formData = new FormData();                  // Creating object of FormData class
	for (var i = 0; i < uploadFiles.length; i++) {
		var file_data = uploadFiles[i];
		formData.append("upLoad", file_data);
	}
	console.log(formData);

	$.ajax({

		url: "Merits/UploadPOD",
		dataType: 'json',
		cache: false,
		contentType: false,
		processData: false,
		data: formData,
		beforeSend: function () {
			showMessage("loading", "Please wait...", "Uploading Document Please Wait....");
		},
		complete: function () {
			$('.ajaxLoading').hide();
		},
		type: 'post',
		success: function (data) {
			debugger;
			if (data.Status == 1) {
				debugger;
				for (var i = 0; i < data.Result.length; i++) {
					UploadedDocs.push(data.Result[i].fdDocId);
					$('#AttchmentsLabel').append(data.Result[i].fdDocTitle + '  ' + '<i class="fa fa-check-circle palegreen"></i>' + '</br>');
				}

				showMessage("green", data.Description);
			}
			else {
				showMessage("red", data.Description);
			}
		},
		error: function (XMLHttpRequest, textStatus, errorThrown) {
			$('.ajaxLoading').hide();
			console.log("Status: ");
			console.log("Message: " + thrownError);
		}
	});
});








var searchCount = 75;
var isCtrl = false;
document.onkeyup = function (e) {
	if (e.which == 17)
		isCtrl = false;
}
$("input").blur(function () {
	$(this).removeClass("inputError");
});
document.onkeydown = function (e) {
	if (e.which == 123)
		isCtrl = true;
	if (((e.which == 85) || (e.which == 83) || (e.which == 65) || (e.which == 88)
		|| (e.which == 67) || (e.which == 86) || (e.which == 2)
		|| (e.which == 3) || (e.which == 123) || (e.which == 83))
		&& isCtrl == true) {
		e
		alert('This is Function Disabled');
		return false;
	}
}
// right click code
//var isNS = (navigator.appName == "Netscape") ? 1 : 0;
//if (navigator.appName == "Netscape")
//	document.captureEvents(Event.MOUSEDOWN || Event.MOUSEUP);
//function mischandler() {
//	return false;
//}
//function mousehandler(e) {
//	var myevent = (isNS) ? e : event;
//	var eventbutton = (isNS) ? myevent.which : myevent.button;
//	if ((eventbutton == 2) || (eventbutton == 3))
//		return false;
//}
//document.oncontextmenu = mischandler;
//document.onmousedown = mousehandler;
//document.onmouseup = mousehandler;
//// select content code disable Rajat Rana



//var validationStatus = 1;

//var currentLocationId ;

/*---------------------------------------------------------------------------------------------------------------------*/
/**
 * Function for uploading files of any format.
 */
function getCurrentLocation() {
	var locationId;
	jQuery.ajax({
		type: 'POST',
		url: "currentLocationGet",
		data:
			{
				id: "getCurrentLocation"
			},
		success: function (data) {
			//alert(data);	
			/*var currentLocationIdData = jQuery.parseJSON(data);*/
			var currentLocationIdData = data.response;

			if (currentLocationIdData.Status == 1) {

				currentLocationKey = currentLocationIdData.Result;
				jQuery.each(currentLocationKey, function (key, value) {

					currentLocationId = currentLocationKey[key]['locationId'];
				});

			}

		},
		error: function (XMLHttpRequest, textStatus, errorThrown) {

			jQuery(".ajaxLoading").hide();
			var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
			showMessage("", defaultMessage, "");
		}

	});

}


/**
 * 
 * @param documentBody - When error generated then it will generate an screenshot of document and upload on server.
 */
function generateImageOfApplicationError(documentBody, msg, qusNo) {
	var file;
	html2canvas(documentBody, {
		onrendered: function (canvas) {
			// canvas is the actual canvas element,
			// to append it to the page call for example

			//document.getElementById("canvasImage").src = canvas.toDataURL();


			var dataURL = canvas.toDataURL();

			// document.body.appendChild( canvas );

			var blobBin = atob(dataURL.split(',')[1]);
			var array = [];
			for (var i = 0; i < blobBin.length; i++) {
				array.push(blobBin.charCodeAt(i));
			}
			file = new Blob([new Uint8Array(array)], { type: 'image/png' });
			var reader = new FileReader();

			reader.onload = function (e) {
				$('#shareImage-' + qusNo).attr('src', e.target.result);
			}
			reader.readAsDataURL(file);
			$("#share-" + qusNo).collapse('show');
		}
	});
}


/*function sendEmailOnReportErrorStatus(respond)
{
	   jQuery.ajax({
			type:'POST',
			url:Drupal.settings.basePath + 'CommonActionCallback',
			data:
			{
				id:"sendEmailOnErrorReporting",
				response:respond,
			},
			success:function(data)
			{
				//alert(data);
			},
			error: function(XMLHttpRequest, textStatus, errorThrown) { 

				jQuery(".ajaxLoading").hide();
				var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
				showMessage("", defaultMessage, "");
			}

		});
}
*/
function upload(fileInputId, fileIndex, current_checkbox_id, distributorId) {
	// take the file from the input
	var file = document.getElementById(fileInputId).files[fileIndex];
	var reader = new FileReader();
	reader.readAsBinaryString(file); // alternatively you can use readAsDataURL
	reader.onloadend = function (evt) {
		// create XHR instance
		xhr = new XMLHttpRequest();

		//var basePath = "http://localhost"+Drupal.settings.basePath;

		//var basePath = "/drupal-7.22/sites/all/modules/VestigePOS/VestigePOS/";

		var modifiedFileName = distributorId + fileInputId + file.name;

		// send the file through POST
		xhr.open("POST", '/sites/all/modules/VestigePOS/VestigePOS/Business/FileUpload.php', true);

		xhr.setRequestHeader("X_FILE_NAME", modifiedFileName);

		//xhr.setRequestHeader("X_BASE_PATH", basePath);

		// make sure we have the sendAsBinary method on all browsers
		XMLHttpRequest.prototype.mySendAsBinary = function (text) {
			var data = new ArrayBuffer(text.length);
			var ui8a = new Uint8Array(data, 0);
			for (var i = 0; i < text.length; i++) ui8a[i] = (text.charCodeAt(i) & 0xff);

			if (typeof window.Blob == "function") {
				var blob = new Blob([data]);
			} else {
				var bb = new (window.MozBlobBuilder || window.WebKitBlobBuilder || window.BlobBuilder)();
				bb.append(data);
				var blob = bb.getBlob();
			}

			this.send(blob);
		}

		// let's track upload progress
		var eventSource = xhr.upload || xhr;
		eventSource.addEventListener("progress", function (e) {
			// get percentage of how much of the current file has been sent
			var position = e.position || e.loaded;
			var total = e.totalSize || e.total;
			var percentage = Math.round((position / total) * 100);

			// here you should write your own code how you wish to proces this
		});

		// state change observer - we need to know when and if the file was successfully uploaded
		xhr.onreadystatechange = function () {
			if (xhr.readyState == 4) {
				if (xhr.status == 200) {

					//process status
					//var result = xhr.responseText.evalJSON();

					// alert the message
					//jQuery("#"+fileInputId).hide(); - if not work

					jQuery("#" + fileInputId).attr("disabled", true);

					//alert(xhr.responseText);
					var filename1 = "/imp/mine.tmp"
					var tmpfile_uploaded_info = jQuery.parseJSON(xhr.responseText);
					var tmpfile_location = tmpfile_uploaded_info.uploadedfile;
					jQuery("#" + current_checkbox_id).val(tmpfile_location);
					current_hidden_field = current_checkbox_id + "1";
					//jQuery(".form_hidden_field").append("<input type='text' name="+tmpfile_location+" value="+tmpfile_location+">");
					//jQuery("#"+current_hidden_field).val(tmpfile_location);
					//jQuery("#"+current_checkbox_id).attr("disabled", true);
					jQuery("#" + current_hidden_field).val(tmpfile_location);
					//alert("check box latest value:"+jQuery("#"+current_hidden_field).val());
					showMessage("green", "Document Uploaded", "");

					//alert("Status : success");
				} else {
					// process error
					//alert("Status : failure");
					showMessage("red", "Problem in uploading file", "");
				}
			}
		};

		// start sending
		xhr.mySendAsBinary(evt.target.result);
	};
}

/*---------------------------------------------------------------------------------------------------------------------*/

/**
 * function to round off floating values upto two decimal points
 * 
 */
function formatFloatNumbers(floatNumber) {
	var floatNumber = parseFloat(floatNumber);

	return floatNumber.toFixed(2);
}

/*---------------------------------------------------------------------------------------------------------------------*/

/**
 * function to validate mobile number
 * 
 */
function validateMobileNumber(mobileNumber) {
	var regex = /^[0-9]{10}$/;

	if (regex.test(mobileNumber)) {
		// Valid international phone number
		return 1;
	} else {
		// Invalid international phone number
		return 0;
	}
}

function backGroundColorForDisableField() {
	var kids = jQuery("Body").find('input, textarea, select ');
	for (var i = 0; i <= kids.length; i++) {

		if (jQuery(kids[i]).is(":disabled")) {
			jQuery('#' + kids[i].id).css("background", "#dddddd");
		}



	}
}
/*---------------------------------------------------------------------------------------------------------------------*/

/**
 * function to validate Email Address
 * 
 */
function validateEmailAddress(email) {
	var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
	var emailValidationFlag = regex.test(email);
	if (emailValidationFlag) {
		return 1;
	}
	else {
		return 0;
	}
}
/*---------------------------------------------------------------------------------------------------------------------*/

/**
 * function to validate Email Address
 * 
 */
function validatePinCode(pin) {

	var regex = /^[0-9]{6}$/;

	if (regex.test(pin)) {
		// Valid international phone number
		return 1;
	} else {
		// Invalid international phone number
		return 0;
	}
}


function showErrorReportingMessage(messageCode, defaultMessage, messageParam) {



	if (messageCode == "green") {
		var msg = defaultMessage;
		jQuery("<div style='z-index:1005;'>")
			.appendTo(document.body)
			.text(msg)
			.addClass("notificationGreen ")
			.position({
				my: "center top",
				at: "center top",
				of: window
			})
			.show({
				effect: "blind",
			})
			.delay(5000)
			.hide({
				effect: "blind",
				duration: "slow"
			}, function () {
				jQuery(this).remove();
			});
	}
}

function showMessage(messageCode, defaultMessage, messageParam) {
	jQuery(".notificationGreen").hide();
	jQuery(".notificationRed1").hide();
	jQuery(".notificationYellow").hide();
	jQuery(".notification").hide();
	jQuery(".ajaxLoading").hide();
	if (defaultMessage == null) {
		return false;
	}
	if (defaultMessage.length > 1000) {
		defaultMessage = defaultMessage.substring(0, 1000);
		defaultMessage = defaultMessage + " ...";
	}
	if (messageCode == "red") {
		var msg = defaultMessage;
		jQuery("<div style='z-index:2000;' class='messageCommonProp'>")
			.appendTo(document.body).html(
				"<i style='float:left; margin-top:15px' class='fa fa-exclamation-circle fa-2x' aria-hidden='true'></i><div style='margin: 10px;'>"
				+ msg).addClass(
					"notificationRed1 ")
			.delay(3000).hide({
				effect: "blind",
				duration: "2000"
			}, function () {
				jQuery(this).remove();
			});

		var positionFromLeft = calculatePositionFromLeft($(".notificationRed1").last().width());
		$(".notificationRed1").last().css('left', positionFromLeft);
		$(".notificationRed1").last().css('display', 'inline-block');
	}

	else if (messageCode == "green") {
		var msg = defaultMessage;
		jQuery("<div style='z-index:2000;' class='messageCommonProp'>")
			.appendTo(document.body).text(msg).addClass(
				"notificationGreen ")
			.delay(2000).hide({
				effect: "blind",
				duration: "slow"
			}, function () {
				jQuery(this).remove();
			});

		var positionFromLeft = calculatePositionFromLeft($(".notificationGreen").last().width());
		$(".notificationGreen").last().css('left', positionFromLeft);
		$(".notificationGreen").last().css('display', 'inline-block');
	}
	else if (messageCode == "yellow") {
		var msg = defaultMessage;
		jQuery("<div style='z-index:2000;' class='messageCommonProp'>")
			.appendTo(document.body).html(msg).addClass(
				"notificationYellow ")
			.delay(10000).hide({
				effect: "blind",
				duration: "slow"
			}, function () {
				jQuery(this).remove();
			});

		var positionFromLeft = calculatePositionFromLeft($(".notificationGreen").last().width());
		$(".notificationYellow").last().css('left', positionFromLeft);
		$(".notificationYellow").last().css('display', 'inline-block');
	}

	else if (messageCode == "loading") {
		var msg = defaultMessage;
		jQuery("<div class='ajaxLoading' style='border:0px solid;z-index:2000;background-color: #5BC0DE;'>")
			.appendTo(document.body)
			.html(
				"<img style='float:left;' id='ajaxCallLoadingImage' src='images/loader.gif' width='50' height='45'><div style='float:left;margin: 10px;'>"
				+ msg)
			.addClass("notificationLoading ")


		var positionFromLeft = calculatePositionFromLeft($(".notificationLoading").last().width());
		$(".notificationLoading").last().css('left', positionFromLeft);
		$(".notificationLoading").last().css('display', 'inline-block');

	}

	else {
		var msg = defaultMessage;
		jQuery("<div style='z-index:2000;' class='messageCommonProp'>")
			.appendTo(document.body).html(msg).addClass("notification")
			.delay(5000).hide({
				effect: "blind",
				duration: "slow"
			}, function () {
				jQuery(this).remove();
			});

		var positionFromLeft = calculatePositionFromLeft($(".notification").last().width());
		$(".notification").last().css('left', positionFromLeft);
		$(".notification").last().css('display', 'inline-block');
	}

}

function calculatePositionFromLeft(elementWidth) {
	var windowWidth = $(window).width();
	var positionFromleft = (windowWidth - elementWidth) / 2;
	return positionFromleft;
}



// Module for DateTime set
function displayDate(GMTdateToFormatForDisplay) {

	var parsedDate = jQuery.datepicker.parseDate('yy-mm-dd', GMTdateToFormatForDisplay);
	return jQuery.datepicker.formatDate('d-M-yy', parsedDate);
}

function formatDateForDisplay(GMTdateToFormatForDisplay) {

	var parsedDate = jQuery.datepicker.parseDate('dd-mm-yy', GMTdateToFormatForDisplay);
	return jQuery.datepicker.formatDate('d-M-yy', parsedDate);
}

function getDate(displayDateFormatInGMT) {
	var parsedDate = jQuery.datepicker.parseDate('d-M-yy', displayDateFormatInGMT);
	return jQuery.datepicker.formatDate('yy-mm-dd', parsedDate);
}

function getDifferentFormatDate(displayDateFormatInGMT) {
	var parsedDate = jQuery.datepicker.parseDate('d-M-yy', displayDateFormatInGMT);
	return jQuery.datepicker.formatDate('yy/mm/dd', parsedDate);
}

function dateForCompare(displayDateFormatInGMT) {
	var parsedDate = jQuery.datepicker.parseDate('d-M-yy', displayDateFormatInGMT);
	return jQuery.datepicker.formatDate('dd-mm-yy', parsedDate);
}

function changeDateFormat(GMTdateToFormatForDisplay) {

	var parsedDate = jQuery.datepicker.parseDate('yy-mm-dd', GMTdateToFormatForDisplay);
	return jQuery.datepicker.formatDate('dd-M-yyyy', parsedDate);
}

function IsDateGreater(DateValue1, DateValue2) {

	var DaysDiff;
	Date1 = new Date(DateValue1);
	Date2 = new Date(DateValue2);
	DaysDiff = Math.floor((Date1.getTime() - Date2.getTime()) / (1000 * 60 * 60 * 24));
	if (DaysDiff > 0)
		return true;
	else
		return false;
}

function showCalender() {
	jQuery(".showCalender").datepicker({
		changeMonth: true, changeYear: true, dateFormat: 'd-M-yy', yearRange: '-100:+0', onSelect: function () {

			jQuery(this).focus();

		}
	});
	jQuery(".showCalender").datepicker("setDate", new Date());

	//jQuery('.showCalender').attr('readonly', true);
}

function showCalender2() {
	jQuery(".showCalender2").datepicker({
		changeMonth: true, changeYear: true, dateFormat: 'd-M-yy', yearRange: '-100:+0', onSelect: function () {

			jQuery(this).focus();

		}
	});
	//jQuery('.showCalender').attr('readonly', true);
}


function showReport(reportBaseURL, reportUrl, reportParam, calledUsingPhpJavaBridge, progressCount, loggedInUserFullName) {


	if (calledUsingPhpJavaBridge == 1) {
		jQuery.ajax({
			type: 'POST',
			url: Drupal.settings.basePath + 'POSClient',
			data:
				{
					id: "getInvoiceReport",
					reportUrl: reportBaseURL,
					reportParam: reportParam

				},
			success: function (data) {

				var invoiceReportWin = window.open("", "Customer Invoice", "height=600,width=800");

				invoiceReportWin.document.write(data);




			},
			error: function (XMLHttpRequest, textStatus, errorThrown) {

				jQuery(".ajaxLoading").hide();
				var defaultMessage = " Status: " + XMLHttpRequest.getAllResponseHeaders() + "<br> Check Internet Connection/Router. Contact Local Administrator";
				showMessage("", defaultMessage, "");
			}

		});
	}
	else {


		var form = document.createElement("form");

		form.setAttribute("method", "POST");

		form.setAttribute("action", reportBaseURL + reportUrl);


		form.setAttribute("target", "formresult");
		//var reportName = document.createElement("input");   
		//reportName.setAttribute("name","__report");
		//reportName.setAttribute("value",reportUrl);
		//reportName.setAttribute("type",'hidden');
		//form.appendChild(reportName);

		//var params = new Array();

		for (keys in reportParam) {
			var key = keys;
			var value = reportParam[keys];
			var hiddenField = document.createElement('input');
			hiddenField.setAttribute("name", key);
			hiddenField.setAttribute("value", value);
			hiddenField.setAttribute("type", 'hidden');
			form.appendChild(hiddenField);
		}

		var hiddenField = document.createElement('input');
		hiddenField.setAttribute("name", "GeneratedBy");
		hiddenField.setAttribute("value", loggedInUserFullName);
		hiddenField.setAttribute("type", 'hidden');
		form.appendChild(hiddenField);

		document.body.appendChild(form);

		/*htmlToAppendToWindowOpen = "<html><head><script src='http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js'></script>" +
				"<script src='http://localhost/drupal-7.22/sites/all/modules/VestigePOS/VestigePOS/JS/progressBarScript.js'></script></head><body>" +
				"<div id='dialog'><div id='progressbar' style='background-color:black;'><div class='progress-label'>Loading...</div></div></div></body></html>";*/

		// creating the 'formresult' window with custom features prior to submitting the form
		var result = window.open('', 'formresult', 'scrollbars=no,menubar=no,height=600,width=800,resizable=yes,toolbar=no,status=no');

		if (result) {

			/*var divElement = result.document.createElement("div");
			
			divElement.setAttribute("id", "dialog");
			
			result.document.body.appendChild(divElement);
			
			jQuery("#dialog").append("<div id='progressbar' style='background-color:black;'><div class='progress-label'>Loading...</div></div>");*/

			//result.document.write("<div id='dialog'><div id='progressbar' style='background-color:black;'><div class='progress-label'>Loading...</div></div></div>");

			//result.document.write(htmlToAppendToWindowOpen);

			//showProgressBarForLogReport(progressCount);

			form.submit();
		}
		else {
			alert("Error in submitting to url");
		}
	}



}


function showProgressBar() {

	var progressbar = $("#progressbar"),
		progressLabel = $(".progress-label");

	progressbar.progressbar({
		value: false,
		change: function () {
			progressLabel.text(progressbar.progressbar("value") + "%");
		},
		complete: function () {
			progressLabel.text("Complete!");
		}
	});

	function progress() {
		var val = progressbar.progressbar("value") || 0;

		progressbar.progressbar("value", val + 1);



		if (val < 29) {
			setTimeout(progress, 7000);
		}
	}

	setTimeout(progress, 1000);

}

function fetchvaluesFromJsonOnSelectRow(jsonString, inputKey, inputKeyValue, OutPutKey) {
	var ret;
	ret = '';
	itemDesc = jsonString;

	if (itemDesc.length != 0) {


		jQuery.each(itemDesc, function (key, value) {
			var itemValue = itemDesc[key][inputKey];

			if (itemValue == inputKeyValue) {

				ret = itemDesc[key][OutPutKey];
				return ret;
			}

		});

	}
	return ret;

}

function matchItemsInJQGrid(gridName, columnName, columnValue) {
	var gridRowsForColumn = jQuery("#" + gridName).jqGrid('getCol', columnName, false);
	var found = 0;
	for (var i = 0; i < gridRowsForColumn.length; i++) {

		if (columnValue == gridRowsForColumn[i]) {
			found = 1;
			break;
		}
		else {

			found = 0;
		}
		//		pv_sum+= parseFloat(PV[i]);
	}
	return found;
}
function matchItemsInJQGridUsingTOColumn(gridName, columnName, columnValue, columnName2, columnValue2) {
	var gridRowsForColumn = jQuery("#" + gridName).jqGrid('getCol', columnName, false);
	var gridRowsForColumn2 = jQuery("#" + gridName).jqGrid('getCol', columnName2, false);
	var found = 0;
	for (var i = 0; i < gridRowsForColumn.length; i++) {

		if (columnValue == gridRowsForColumn[i] && gridRowsForColumn2[i] == columnValue2) {
			found = 1;
			break;
		}
		else {

			found = 0;
		}
		//		pv_sum+= parseFloat(PV[i]);
	}
	return found;
}
// it is for sum of row of one column of jqgrid ....
function sumOfColumnValueJQGrid(gridName, columnName) {
	var gridRowsForColumn = jQuery("#" + gridName).jqGrid('getCol', columnName, false);

	var columnValue = 0;
	for (var i = 0; i < gridRowsForColumn.length; i++) {

		columnValue = 0 + columnValue + parseInt(gridRowsForColumn[i]) + 0;
		//		pv_sum+= parseFloat(PV[i]);
	}
	return columnValue;
}

function sumOfColumnValueJQGrid2(gridName, columnName) {
	var gridRowsForColumn = jQuery("#" + gridName).jqGrid('getCol', columnName, false);

	var columnValue = 0;
	for (var i = 0; i < gridRowsForColumn.length; i++) {

		columnValue = 0 + columnValue + parseFloat(gridRowsForColumn[i]) + 0;
		//		pv_sum+= parseFloat(PV[i]);
	}
	return columnValue;
}


//function used to validate age //take two parameter one DOB//another year by which to validate.

function validateAge(selectedDOB, ageToValidate) {
	var validateAgeFlag = 0;

	if (selectedDOB.indexOf('/') == -1) {
		validateAgeFlag = 0;
	}
	else {
		var ddmmyyyy = selectedDOB.split('/');

		if (ddmmyyyy[0] && ddmmyyyy[1] && ddmmyyyy[2]) {
			var mmddyyyy = ddmmyyyy[1] + '/' + ddmmyyyy[0] + '/' + ddmmyyyy[2];


			selectedDOB = new Date(mmddyyyy); //convert string to date type.

			var today = new Date();
			var age = today.getFullYear() - selectedDOB.getFullYear();
			var m = today.getMonth() - selectedDOB.getMonth();
			if (m < 0 || (m === 0 && today.getDate() < selectedDOB.getDate())) {
				age--;
			}


			if (age >= ageToValidate) {
				validateAgeFlag = 1;
			} else {
				validateAgeFlag = 0; //0 otherwise //which is already set.
			}
		}
		else {
			validateAgeFlag = 0;
		}
	}

	return validateAgeFlag;
}

//function to be implemented remained to watch that hwo to return from ajax call in jquery.

/*function fetchTitles()
{
	showMessage("loading", "Loading titles...", "");
	
	jQuery.ajax({
		type:'POST',
		url:Drupal.settings.basePath + 'POSDistributorRegistration',
		data:
		{
			id:"titles",
			
		},
		success:function(data)
		{
			
			return data;
		} //end success
		
	});
}
*/


function checkIntegerOrNot(valueToCheck) {
	var validatedIntegerOrNot = 0;

	var intRegex = /^\d+$/;

	if (intRegex.test(valueToCheck)) {
		validatedIntegerOrNot = 1;
	}
	else {
		validatedIntegerOrNot = 0;
	}

	return validatedIntegerOrNot;
}


function panNumberValidation(valueToCheck) {
	var validatedPanNumberOrNot = 0;

	valueToCheck = valueToCheck.toUpperCase();

	var intRegex = /[A-Z]{3}[PHFATC]{1}[A-Z]{1}[0-9]{4}[A-Z]{1}/;

	if (intRegex.test(valueToCheck)) {
		validatedPanNumberOrNot = 1;
	}
	else {
		validatedPanNumberOrNot = 0;
	}

	return validatedPanNumberOrNot;
}

function convertArrayToJson(data) {
	var json = {};
	jQuery.each(data, function () {
		json[this.name] = this.value || '';
	});

	return json;
}



function ajaxCallerForFiles(method, url, reqData, contentType, message) {
	if (contentType == undefined) {
		return $.ajax({
			type: method,
			url: url,
			data: data,
			error: function (request, error) {
				console.log(arguments);
				alert(" Can't do because: " + error);
			},
			beforeSend: function () {
				showMessage("loading", message + "....", "");
			},
			complete: function () {
				$('.ajaxLoading').hide();
			}
		});
	}
	else {
		return $.ajax({
			type: method,
			url: url,
			enctype: 'multipart/form-data',
			contentType: false,
			processData: false,
			cache: false,
			data: {
				requestData: reqData
			},
			error: function (request, error) {
				console.log(arguments);
				alert(" Can't do because: " + error);
			},
			beforeSend: function () {
				showMessage("loading", message + "...", "");
			},
			complete: function () {
				$('.ajaxLoading').hide();
			}
		});
	}
}




function ajaxCaller(method, url, reqData, contentType, message) {
	if (contentType == undefined) {
		return $.ajax({
			type: method,
			url: url,
			data: data,
			error: function (request, error) {
				console.log(arguments);
				alert(" Can't do because: " + error);
			},
			beforeSend: function () {
				showMessage("loading", message + "....", "");
			},
			complete: function () {
				$('.ajaxLoading').hide();
			}
		});
	}
	else {
		return $.ajax({
			type: method,
			url: url,

			data: {
				requestData: reqData
			},
			error: function (request, error) {
				console.log(arguments);
				alert(" Can't do because: " + error);
			},
			beforeSend: function () {
				showMessage("loading", message + "...", "");
			},
			complete: function () {
				$('.ajaxLoading').hide();
			}
		});
	}
}

function ajaxCallerWithoutMessage(method, url, reqData, contentType) {
	if (contentType == undefined) {
		return $.ajax({
			type: method,
			url: url,
			data: data,
			error: function (request, error) {
				console.log(arguments);
				alert(" Can't do because: " + error);
			}
		});
	}
	else {
		return $.ajax({
			type: method,
			url: url,
			data: {
				requestData: reqData
			},
			error: function (request, error) {
				console.log(arguments);
				alert(" Can't do because: " + error);
			}
		});
	}
}




function globalVars() {

}


function loadSearchResultPage(data, dynamicBodyContainer) {
	$("#" + dynamicBodyContainer).html(data);
}


function convertJsonArrayToJsonObject(inputArray) {
	var outputObject = {};
	$.each(inputArray, function (key, value) {
		outputObject[value.name] = value.value;
	});

	return outputObject;
}

function replaceString(str, replaceBy) {
	var c = str.split("");
	for (var i = 0; i < c.length; i++) {
		c[i] = c[i].replace(/[^a-zA-Z0-9]+/gi, replaceBy);

	}
	return c.join("");
}

var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
/*aded for Loading DatePickers*/
/*$('.date-picker').datepicker({
	format: 'dd-mm-yyyy',
	orientation: 'auto bottom',
	autoclose: true
}).on('changeDate', function (ev) {
	$(this).datepicker('hide');
	var date = ev.date.getDate() + "-" + months[ev.date.getMonth()] + "-" + ev.date.getFullYear();
	if (ev.date.getDate()<=9)
		date = "0" + date;
	$(this).val(date);
});
*/
////$('.date-picker').datetimepicker({
//	format: 'DD-MMM-YYYY'
//});



function replaceStringForSearch(str, replaceBy) {
	var c = str.split("");
	for (var i = 0; i < c.length; i++) {
		c[i] = c[i].replace(/[^a-zA-Z0-9\s]+/gi, replaceBy);

	}
	return c.join("");
}


/* function for  return textBox Serch List -By Manoj Shukla*/

function SerchTextBoxList(textBoxSerchId, serchButtonId, url, listId, inputFieldID, cboID, isComposite) {



	$("#" + textBoxSerchId + "," + "#" + serchButtonId + "").bind("click keyup", function (e) {

		$("#" + inputFieldID + "").val("");
		if ($("#" + textBoxSerchId + "").val().length >= 0) {

			//var requestData = {};
			var key = $("#" + textBoxSerchId + "").val();
			//keyword = replaceStringForSearch(keyword, "")


			if (true) {
				ajaxCaller(
					"POST",
					url, key, "application/json", 'Loading Suggestions')
					.done(function (data) {

						if (data.Status == 1) {
							if (data.Result.length > 0) {

								showResultList(data, textBoxSerchId, listId, inputFieldID, cboID, isComposite);
							}
							else {
								var list = '';

								list += '<li class="list-group-item list-search-items"   style="width:100%;">' +

									'<div class="clearfix" ><span  >Nothing Found. Try Other Keywords</span></div>' +
									'</li>';
								$("#" + listId + "").show();
								$("#" + listId + "").empty();
								$("#" + listId + "").append(list);


							}
						}
						else {
							showMessage("red", data.Description);
						}
					});
			} else {

			}


		} else {

			$(".listParent").hide();

			return;

		}
	});



}

function showResultList(data, textBoxSerchId, listId, inputFieldID, cboID, isComposite) {

	var oResponse = data;

	if (oResponse.Status == 1) {
		var oResult = oResponse.Result;
		var list = '';
		var listIncrementer = 0;
		if (isComposite == false) {
			debugger;
			$.each(oResult, function (key, value) {
				var id = value.Item1;
				var name = value.Item2;
				var code = value.Item3;
				list += '<li class="list-group-item list-search-items" data-Id="' + id + '" data-Name="' + name + '" id="Code:' + code + '"  style="width:100%;">' +

					'<div class="clearfix" ><span onClick=selectVendor(this,"' + textBoxSerchId + '","' + listId + '","' + inputFieldID + '","' + cboID + '","' + code + '") id="Code' + code + '">' + name + '</span></div>' +
					'</li>';
			});
		}
		else {
			debugger;
			$.each(oResult, function (key, value) {
				var id = value.Item1;
				var name = value.Item2;
				var code = value.Item3;
				if (code == "" || code == undefined || code == null) {
					list += '<li class="list-group-item list-search-items" data-Id="' + id + '" data-Name="' + name + '" id="Code:' + code + '"  style="width:100%;">' +

						'<div class="clearfix" ><span onClick=selectVendor(this,"' + textBoxSerchId + '","' + listId + '","' + inputFieldID + ',' + cboID + '") id="Code' + code + '">' + name + '</span></div>' +
						'</li>';
				}
				else {
					list += '<li class="list-group-item list-search-items" data-Id="' + id + '" data-Name="' + name + '" id="Code:' + code + '"  style="width:100%;">' +

						'<div class="clearfix" ><span onClick=selectVendor(this,"' + textBoxSerchId + '","' + listId + '","' + inputFieldID + ',' + cboID + '") id="Code' + code + '">' + name + '-' + code + '</span></div>' +
						'</li>';
				}

			});
		}


		$("#" + listId + "").show();
		$("#" + listId + "").empty();
		$("#" + listId + "").append(list);
	}
	else {
		showMessage(oResponse.Description);
	}


}

function selectVendor(id, textBoxSerchId, listId, inputFieldID, cboID, code) {

	debugger;

	$("#" + inputFieldID + "").val(id.parentNode.parentNode.getAttribute("data-id"));
	document.getElementById(textBoxSerchId).value = id.innerText;
	$("#" + listId + "").empty();

	if (cboID != null & cboID != '') {
		$("#" + cboID + "").val(code);
		$("#" + cboID + "").prop("disabled", true);
	}


}




var UploadedDocs = [];
//$("#uploadAttchment").click(function(e){
$("#inputGroupFile02").on('change', function () {
	//function saveAttachment() {
	//$("input").change(function (e) {
	debugger;

	var uploadFiles = this.files;
	console.log(uploadFiles);
	// Getting the properties of file from file field
	var formData = new FormData();                // Creating object of FormData class



	var formData = new FormData();                  // Creating object of FormData class
	for (var i = 0; i < uploadFiles.length; i++) {
		var file_data = uploadFiles[i];
		formData.append("upLoad", file_data);
	}
	console.log(formData);

	$.ajax({

		url: "/AWPTIPS/UploadPOD",
		dataType: 'json',
		cache: false,
		contentType: false,
		processData: false,
		data: formData,
		beforeSend: function () {
			showMessage("loading", "Please wait...", "Uploading Document Please Wait....");
		},
		complete: function () {
			$('.ajaxLoading').hide();
		},
		type: 'post',
		success: function (data) {
			debugger;
			if (data.Status == 1) {
				debugger;
				for (var i = 0; i < data.Result.length; i++) {
					UploadedDocs.push(data.Result[i].fdDocId);
					$('#AttchmentsLabel').append(data.Result[i].fdDocTitle + '  ' + '<i class="fa fa-check-circle palegreen"></i>' + '</br>');
				}

				showMessage("green", data.Description);
			}
			else {
				showMessage("red", data.Description);
			}
		},
		error: function (XMLHttpRequest, textStatus, errorThrown) {
			$('.ajaxLoading').hide();
			console.log("Status: ");
			console.log("Message: " + thrownError);
		}
	});
	//}
});

$("#textForincident").focus(function () {
	$("#IncidentSection").css('display', 'block');
});

function hideIncidentBox(countId) {
	$(".incidence").css('display', 'none');
}

function OpenIncident(countId) {
	$(".incidence").css('display', 'none');
	$("#IncidentSection" + countId).css('display', 'block');
}

function hideIncidentBoxV(countId) {
	$(".incidence").css('display', 'none');
}

function OpenIncidentV(countId) {
	$(".incidence").css('display', 'none');
	$("#IncidentSectionV" + countId).css('display', 'block');
}

function SetRolesLevelAcessHome(input) {

	$("#documentChartTag").hide();
	$("#documentClassTag").hide();
	$("#documentTag").hide();
	$("#documentAuditTag").hide();
	$("#userMasterTag").hide();
	var IsApproveRejectOccur = false;
	if (input != null && input != undefined && input != "")
		{
	for (var i = 0; i < input.length; i++) {

		//if (input[i].functionCode == 'SEARCH' && input[i].functionCode == 'HOMEMAS') {
		//    $("#documentSearchTag").show();


		//}

		if (input[i].functionCode == 'SEARCH' && input[i].moduleCode == 'USAGESDASH') {

			$("#documentChartTag").show();

		}

		if (input[i].functionCode == 'SEARCH' && input[i].moduleCode == 'DOC_CLASS') {

			$("#documentClassTag").show();


		}
		if (input[i].functionCode == 'SEARCH' && input[i].moduleCode == 'UPLOADMAS') {

			$("#documentTag").show();


		}
		if (input[i].functionCode == 'SEARCH' && input[i].moduleCode == 'DOCAUDIT') {

			$("#documentAuditTag").show();


		}

		if (input[i].functionCode == 'SEARCH' && input[i].moduleCode == 'USERMAS') {

			$("#userMasterTag").show();

		}


		if (input[i].functionCode == 'CREATE' && input[i].moduleCode == 'DOC_CLASS') {

			$("#documentClassTag").show();

		}

		if (input[i].functionCode == 'DRAFT') {

			$("#documentTag").show();

		}


	}
		}


}
function setAndSetUserScreenModuleshome() {
	ajaxCallerWithoutMessage("POST", "Common/getModuleRights", "HOMEMAS", "application/json").done(function (data) {
		console.log(data);
		rolesRight = data.Result;
		SetRolesLevelAcessHome(data.Result);
	});
}
