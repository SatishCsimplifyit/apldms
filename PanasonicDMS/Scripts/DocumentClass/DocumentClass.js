﻿$(document).on('hidden.bs.modal', '.bootbox', function () {
	$('#searchPanadoxClassName').focus();
	$('#txtParadoxClassName').focus();
   
});

var sequence = 1;
$('document').ready(function () {

	$('.date_picker').datepicker({
		format: "dd/mm/yyyy"
		}).on('change', function() {
			debugger;
		$('.datepicker').hide();
		}).on("keydown", function (e) {
			e.preventDefault();
		});
	$('#docClassSearchtable').DataTable({
	fixedHeader: true
} );


	$('#docClassSearchtable_filter').hide();
	$('#grdParadoxProperties_filter').hide();
	$('#grdAuthorDepartments_filter').hide();

	//$('#searchKeyword').focus();
   // setAndSetUserScreenModules();
	documentClassTag();
	SearchParadoxClass(1);



	
	$('#txtfield').show();
	$('#datefield').hide();
	 $('#Numfield').hide();


	var searchKeyword = document.getElementById("searchKeyword");
	searchKeyword.addEventListener("keydown", function (event) {
		if (event.keyCode === 13) {
			resetSearchParadoxClass();
			searchDocumentsUsingSearchBar();
		}
	});



	$('.documentClass').hide();
	$('#query1').hide();
	$('#txtSequence').val(sequence);
	// $('#txtSequence').attr('disabled',true);

	$('.securityTable1').DataTable({

		columns: [
			 { title: "Id" },
			{ title: "Author Department ID" },
			 { title: " Department Name" },
			 { title: " Rights" },
			 { title: " RightsID" },
			{ title: " Action" }
		]
	});
	$('.securityTable2').DataTable({

		columns: [{ title: "Id" },
			{ title: "Author Department ID" },
			{ title: "Viewer Department Name" },
			{ title: " " }
		]
	});


	

	var docClassSearchtable = $('#docClassSearchtable').DataTable();
	docClassSearchtable.column(0).visible(false);
	docClassSearchtable.column(1).visible(false);

	$('#grdParadoxProperties').DataTable({

		columns: [
			 { title: "Id" },
			{ title: "Name" },
			{ title: "Datatype" },
			{ title: "DatatypeID" },
			{ title: "Sequence" },
			{ title: "Required" },           
			{ title: "Default Value" },
			{ title: "Action" }

		]
	});


	var grdParadoxProperties = $("#grdParadoxProperties").DataTable();
	//grdeditdata.column(1).visible(false);
	grdParadoxProperties.column(0).visible(false);
	grdParadoxProperties.column(3).visible(false);



	//grdParadoxProperties.columns.adjust().draw();

	editAndUpdateDocumentClassRowData();



	//var docClassSearchtable = $("#docClassSearchtable").DataTable();
	//.column(0).visible(false);
	$('#cboPropertyDataType').change(function () {
		debugger;
		var datatype = $('#cboPropertyDataType').val();
		if (datatype == 1) {
			$('#datefield').show();
			$('#txtfield').hide();
			$('#Numfield').hide();
		   
			var today = new Date();
			$('#datefield').val((today.getMonth() + 1) + '/' + today.getDate() + '/' + today.getFullYear());
		   // var dateToday = new Date();

		   // $("#dateDefaultValue").val($.datepicker.formatDate('dd/MM/yy', dateToday));

		   // $('#dateDefaultValue').val("");
		}
		else if (datatype == 2) {
			$('#txtfield').show();
			$('#datefield').hide();
			$('#Numfield').hide();
		}
		else
		{
			$('#txtfield').hide();
			$('#datefield').hide();
			$('#Numfield').show();
		}

	});
	



	$('.dt-buttons').hide();
	// $('.dataTables_filter').hide();
	var grdViewerDepartments = $("#grdViewerDepartments").DataTable();
	grdViewerDepartments.column(0).visible(false);
	grdViewerDepartments.column(1).visible(false);

	var grdAuthorDepartments = $("#grdAuthorDepartments").DataTable();
	grdAuthorDepartments.column(0).visible(false);
	grdAuthorDepartments.column(1).visible(false);
	grdAuthorDepartments.column(4).visible(false);


	fetchCheckListInitiallyParameterData();

	var grdAuthorDepartments = $('#grdAuthorDepartments').DataTable();
	$('#grdAuthorDepartments').on("click", 'button.btnRemoveAuthorDepartment', function (e) {
		debugger;
		e.preventDefault();

		var grdAuthorDepartments = $('#grdAuthorDepartments').DataTable();
	   
		var inputData = {};
		//inputData.ClassName = $('#txtParadoxClassName').val();
		// inputData.dcid = grdParadoxProperties.row($(this).parents('tr')).data();
		var nRow = $(this).parents('tr')[0];  

	   
		var data = grdAuthorDepartments.row(nRow._DT_RowIndex).data();

		if (data[0] != null && data[0] != "")
		{
			var chkMlId = [];
			for (i = 0; i < grdAuthorDepartments.data().length ; i++) {
				var chk = grdAuthorDepartments.data()[i][0];
				if (chk != null && chk.toString() != "") {
					chkMlId.push(chk);
				}

			}
			if (chkMlId.length > 1 || grdAuthorDepartments.data().length == 1)
			{
			if (grdAuthorDepartments.data().length > 1)
			{
				bootbox.confirm({
					message: "Are you sure? You want to delete this department",
					buttons: {
						confirm: {
							label: 'Yes',
							className: 'btn-danger'
						},
						cancel: {
							label: 'No',
							className: 'btn-primary'
						}
					},
					callback: function (result) {
						if (result == true) {

						   

							inputData.PKID = data[0];


							ajaxCaller("POST", "DocumentClass/DeleteAuthorDepartments", JSON.stringify(inputData), "application/json", "Deleting..").done(function (data) {
								debugger;
								if (data.Status == 1) {
									//var grdParadoxProperties = $('#grdParadoxProperties').DataTable();
									grdAuthorDepartments.row(nRow._DT_RowIndex).remove().draw();
									showMessage("green", data.Description);

								}
								else {
									showMessage("red", data.Description);
								}
							});
						}
					}
				}).find("div.modal-body").addClass("ArchiveBootBoxHeight");
			}
			else
			{
				showMessage("red","First you need to add at least one department")
			}
			}
			else
			{
				showMessage("red", "First you need to save added department");
				return;
			}
			
		}
		else
		{
			grdAuthorDepartments
		 .row($(this).parents('tr'))
		 .remove()
		.draw();
		}
	});


	var table = $('#grdAuthorDepartments').DataTable();
	$('#grdAuthorDepartments').on("click", 'button.btnRemoveViewrDepartment', function (e) {
		debugger;
		e.preventDefault();
		var grdAuthorDepartments = $('#grdAuthorDepartments').DataTable();	
			grdAuthorDepartments
		 .row($(this).parents('tr'))
		 .remove()
		.draw();
		
	});


	$('#grdParadoxProperties').on("click", 'button.btnRemoveProperties', function (e) {
		e.preventDefault();
		debugger;
		var grdParadoxProperties = $('#grdParadoxProperties').DataTable();
	   
		var inputData = {};
		// inputData.dcid = grdParadoxProperties.row($(this).parents('tr')).data();
		var nRow = $(this).parents('tr')[0];  

		//var rowIndex = grdParadoxProperties.row(this).index();
		var data = grdParadoxProperties.row(nRow._DT_RowIndex).data();
		

		if (data[0] != null && data[0] != "")
		{
			var chkMlId = [];
			for (i = 0; i < grdParadoxProperties.data().length ; i++)
			{
				var chk = grdParadoxProperties.data()[i][0];
				if (chk != null && chk.toString() != "") {
					chkMlId.push(chk);	                
				}

			}
			if (chkMlId.length > 1 || grdParadoxProperties.data().length == 1)
			{
				if (grdParadoxProperties.data().length > 1) {
					bootbox.confirm({
						message: "Are you sure? You want to delete this property",
						buttons: {
							confirm: {
								label: 'Yes',
								className: 'btn-danger'
							},
							cancel: {
								label: 'No',
								className: 'btn-primary'
							}
						},
						callback: function (result) {
							if (result == true) {

								var inputData = {};

								inputData.docID = data[0];


								ajaxCaller("POST", "DocumentClass/DeleteProperties", JSON.stringify(inputData), "application/json", "Deleting..").done(function (data) {
									debugger;
									if (data.Status == 1) {
										//var grdParadoxProperties = $('#grdParadoxProperties').DataTable();
										grdParadoxProperties.row(nRow._DT_RowIndex).remove().draw();
										showMessage("green", data.Description);

									}
									else {
										showMessage("red", data.Description);
									}
								});
							}
						}
					}).find("div.modal-body").addClass("ArchiveBootBoxHeight");
				}
				else {
					showMessage("red", "First you need to add at least one property");
				}
			}
			else
			{
				showMessage("red", "First you need to save added properties");
				return;
			}
			
			
			
		}
		else
		{
			grdParadoxProperties
		 .row($(this).parents('tr'))
		 .remove()
		.draw();
		}
	});
});

function fetchCheckListInitiallyParameterData() {
	debugger;
	var inputData = {};
	var lookUpCodes = [];
	lookUpCodes.push("DEPARTMENT");
	lookUpCodes.push("DATA_TYPE");
	inputData.lookUpCode = lookUpCodes;

	ajaxCallerWithoutMessage("POST", "Common/fetchStatus", JSON.stringify(inputData), "application/json").done(function (data) {
		if (data.Status == 1) {
			//debugger;
			var oResult = data.Result;
			$.each(oResult, function (key, value) {
				if (value.fdLkUpCode == "DEPARTMENT") {
					$('#cboAuthorDepartment').append($('<option></option>').val(value.fdKeyCode).html(value.fdKey1));
					$('#cboViewerDepartment').append($('<option></option>').val(value.fdKeyCode).html(value.fdKey1));
				}
				else if (value.fdLkUpCode == "DATA_TYPE") {
					$('#cboPropertyDataType').append($('<option></option>').val(value.fdKeyCode).html(value.fdKey1));
				}
			});
		} else if (data.Status == 2) {
			showMessage("red", data.Description, "");
		} else {
			showMessage("red", data.Description, "");
		}
	});
}


function Validate() {
	var charactersOnly = document.getElementById('txtParadoxClassName').value;
	if (charactersOnly.search(/^[a-zA-Z0-9]/) === -1) {
		//alert("Only characters");
	   // showMessage("red", "Only characters", "");
		document.getElementById('txtParadoxClassName').value = "";
	}
}

//function Validate1() {
//    var charactersOnly = document.getElementById('txtPropertyName').value;
//    if (charactersOnly.search(/^[a-zA-Z]+$/) === -1) {
//        //alert("Only characters");
//        showMessage("red", "Only characters", "");
//        document.getElementById('txtPropertyName').value = "";
//    }
//}



function addPropertiesToGrid() {
	debugger;
	if ($('#txtParadoxClassName').val() == '' || $('#txtParadoxClassName').val() == undefined || $('#txtParadoxClassName').val() == null) {
		showMessage("red", "Enter Document Type.");
		$('#txtParadoxClassName').focus();
		return;
	}


	if ($('#txtPropertyName').val() == '' || $('#txtPropertyName').val() == undefined || $('#txtPropertyName').val() == null) {
		showMessage("red", "Please Enter Property Name");
		$('#txtPropertyName').focus();
		return;
	}

	if (jQuery("#cboPropertyDataType option:selected").val() == 0 || jQuery("#cboPropertyDataType option:selected").val() == undefined || jQuery("#cboPropertyDataType option:selected").val() == null) {
		showMessage("red", "Please Enter Property Data Type");
		$('#cboPropertyDataType').focus();
		return;
	}

	if ($('#txtSequence').val() == '' || $('#txtSequence').val() == undefined || $('#txtSequence').val() == null) {
		showMessage("red", "Please Enter Squence Value");
		$('#txtSequence').focus();
		return;
	}

	var isRequred;
	var isRequredid = 0;
	if ($('#chkIsRequired').is(":checked")) {
		isRequred = "Yes";
		isRequredid = 1;
	} else {
		isRequred = "No";
		isRequredid = 0;
	}

	var DefaultValue='';
	var DocType = jQuery("#cboPropertyDataType option:selected").val();
	if (DocType == 1)
	{
		DefaultValue = $('#dateDefaultValue').val();
	}
	else if (DocType==2)
	{
		DefaultValue = $('#txtDefaultValue').val();
	}
	else
	{
		DefaultValue = $('#NumDefaultValue').val();
	}

	var grdParadoxProperties = $("#grdParadoxProperties").DataTable();
	grdParadoxProperties.row.add(['',
					 $('#txtPropertyName').val(),					
					jQuery("#cboPropertyDataType option:selected").text(),
					jQuery("#cboPropertyDataType option:selected").val(),
					$('#txtSequence').val(),
					isRequred,
					DefaultValue,
					'<button class="btn btn-red btn-circle btn-sm btnRemoveProperties"><i class="fa fa-times-circle fa-2x"></i></button>'
	]).draw();

	$('#txtPropertyName').val('');
	$('#cboPropertyDataType').val(0);
	$("#txtDefaultValue").val('');
	var sequence =$('#txtSequence').val();
	//sequence = (sequence + 1).valueOf();
	$('#txtSequence').val(parseInt(sequence)+1);
	$('#chkIsRequired').prop('checked', false);

	//$('#NumDefaultValue').val("").hide();
	//$('#txtDefaultValue').val("").show();
	//$('#dateDefaultValue').hide();
}



function AddViewerDepartment() {
	if (jQuery("#cboAuthorDepartment option:selected").val() == 0 || jQuery("#cboAuthorDepartment option:selected").val() == undefined || jQuery("#cboAuthorDepartment option:selected").val() == null) {
		showMessage("red", "Please Select Viewer Department.");
		$('#cboAuthorDepartment').focus();
		return;
	}
	var grdAuthorDepartments = $("#grdAuthorDepartments").DataTable();

	// var grdThcRateCard = $("#grdThcRateCard").DataTable();
	
		for (var i = 0 ; i < grdAuthorDepartments.data().length; i++) {

			if ($("#cboAuthorDepartment").val() == grdAuthorDepartments.data()[i][1]) {
				if (1 == grdAuthorDepartments.data()[i][4]) {
					grdAuthorDepartments.rows(i).nodes().to$()
									.addClass('gridError');
						removeErrorCss = i;
						showMessage("red", 'Duplicate Department');
						return false;                           
				}
			}
		}

	grdAuthorDepartments.row.add(['', jQuery("#cboAuthorDepartment option:selected").val(), 
					 jQuery("#cboAuthorDepartment option:selected").text(),
					'Read & Write',
					1,
					 '<button class="btn btn-red btn-circle btn-sm btnRemoveAuthorDepartment"><i class="fa fa-times-circle fa-2x"></i></button>'
	]).draw();
	$("#cboAuthorDepartment").val(0);

}


function AddAuthorDepartment() {
	if (jQuery("#cboAuthorDepartment option:selected").val() == 0 || jQuery("#cboAuthorDepartment option:selected").val() == undefined || jQuery("#cboAuthorDepartment option:selected").val() == null) {
		showMessage("red", "Please Select Author Department.");
		$('#cboAuthorDepartment').focus();
		return;
	}

	var grdAuthorDepartments = $("#grdAuthorDepartments").DataTable();

	
	grdAuthorDepartments.row.add(['',
					 jQuery("#cboAuthorDepartment option:selected").val(),
					jQuery("#cboAuthorDepartment option:selected").text(),
					 '<button class="btn btn-red btn-circle btn-sm btnRemoveAuthorDepartment"><i class="fa fa-times-circle fa-2x"></i></button>'
	]).draw();
	$("#cboAuthorDepartment").val(0);

}
function SaveParadoxClass() {
	debugger;

	//txtParadoxClassID

	//if ($('#txtParadoxClassID').val() != ''&& $('#txtParadoxClassID').val() != undefined && $('#txtParadoxClassID').val() != null) {
	//	showMessage("red", "Can Not Update Details");
	   
	//	return;
	//}
	var inputData = {};
	inputData["txtParadoxClassID"] = $('#txtParadoxClassID').val();

	if ($('#txtParadoxClassName').val() == '' || $('#txtParadoxClassName').val() == undefined || $('#txtParadoxClassName').val() == null) {
		showMessage("red", "Enter Document Type.");
		$('#txtParadoxClassName').focus();
		return;
	}

	var grdParadoxProperties = $("#grdParadoxProperties").DataTable();
	if (grdParadoxProperties.data().length == 0) {
		$('#txtPropertyName').focus();
	   // $('#DocumentsFields').show();
	   // $('#DocsSecurity').removeClass("active");
		showMessage("red", "Please add atleast one Document Fields.");
		$("#DocField").attr('class', 'nav-link active show');
		$("#DocSec").attr('class', 'nav-link ');
		$("#DocumentsFields").attr('class', 'container tab-pane fade  active show');
		$("#DocsSecurity").attr('class', 'container tab-pane fade');
		return;
	}
	//var grdViewerDepartments = $("#grdViewerDepartments").DataTable();
	//if (grdViewerDepartments.data().length == 0) {
	//	$('#cboAuthorDepartment').focus();
	//	showMessage("red", "Please add atleast one viewer department.");
	//	return;
	//}

	if ($('#txtParadoxClassID').val() == '' || $('#txtParadoxClassID').val() == undefined || $('#txtParadoxClassID').val() == null) {
		var grdAuthorDepartments = $("#grdAuthorDepartments").DataTable();
		if (grdAuthorDepartments.data().length == 0) {
			$('#cboViewerDepartment').focus();
		   // $('#DocumentsFields').removeClass("active");
		   // $('#DocsSecurity').show();
			showMessage("red", "Please add atleast one Document Security.");
			$("#DocField").attr('class', 'nav-link');
			$("#DocSec").attr('class', 'nav-link active show');
			$("#DocumentsFields").attr('class', 'container tab-pane fade');
			$("#DocsSecurity").attr('class', 'container tab-pane fade active show');

			return;
		}
	}
	inputData["txtParadoxClassName"] = $('#txtParadoxClassName').val();
   

	var loaParadoxProperties = [];
	for (var i = 0; i < grdParadoxProperties.data().length; i++) {

		if (grdParadoxProperties.data()[i][0] == "") {
			var lojParadoxProperties = {};
			lojParadoxProperties["propertyName"] = grdParadoxProperties.data()[i][1];
			lojParadoxProperties["dataType"] = grdParadoxProperties.data()[i][3];
			
			lojParadoxProperties["sequence"] = grdParadoxProperties.data()[i][4];
			lojParadoxProperties["isRequired"] = grdParadoxProperties.data()[i][5];
			lojParadoxProperties["defaultValue"] = grdParadoxProperties.data()[i][6];

			loaParadoxProperties.push(lojParadoxProperties);
		}
	}
	inputData["properties"] = loaParadoxProperties;
	var loaAuthorDeparments = [];
	var loaViewerDeparments = [];

	var grdAuthorDepartments = $('#grdAuthorDepartments').DataTable();
	debugger;
	for (var i = 0; i < grdAuthorDepartments.data().length; i++) {
		if (grdAuthorDepartments.data()[i][0] == "") {
			if (grdAuthorDepartments.data()[i][4] == 1)
			{
				loaAuthorDeparments.push(grdAuthorDepartments.data()[i][1]);
			}
			else if (grdAuthorDepartments.data()[i][4] == 0)
			{
				loaViewerDeparments.push(grdAuthorDepartments.data()[i][1]);
			}
			
		}
	}
	inputData["authorDepartments"] = loaAuthorDeparments;
	
	//for (var i = 0; i < grdViewerDepartments.data().length; i++) {
	//	if (grdViewerDepartments.data()[i][0] == "") {
	//		loaViewerDeparments.push(grdViewerDepartments.data()[i][1]);
	//	}
	//}

	//Amit



	inputData["viewerDepartments"] = loaViewerDeparments;
	console.log(inputData);
	ajaxCaller("POST", "DocumentClass/SaveDocumentClass", JSON.stringify(inputData), "application/json", "Saving Paradox..")
		.done(function (data) {
			if (data.Status == 1) {
				showMessage("green", data.Description)
			}
			else {
				showMessage("red", data.Description)
			}
		});
	$('.search-panadox').show();
	$('.documentClass').hide();
	$('#searchKeyword').focus();
	SearchParadoxClass(1);
}
function SearchParadoxClass(msg) {
	debugger;
	var input = {};
	if ($('#searchPanadoxClassName').val() != null && $('#searchPanadoxClassName').val() != undefined && $('#searchPanadoxClassName').val() != "") {
		input['searchPanadoxClassName'] = $('#searchPanadoxClassName').val();
	}
	if ($('#searchFromDate').val() != null && $('#searchFromDate').val() != undefined && $('#searchFromDate').val() != "") {
		input['searchFromDate'] = $('#searchFromDate').val();
	}
	if ($('#searchToDate').val() != null && $('#searchToDate').val() != undefined && $('#searchToDate').val() != "") {
		input['searchToDate'] = $('#searchToDate').val();
	}

	ajaxCaller("POST", "DocumentClass/SearchDocumentClass", JSON.stringify(input), "application/json", "Searching..")
		.done(function (data) {

			if (data.Status == 1) {
				debugger;
				if (msg != 1)
				{
					showMessage("green", data.Description)
				}
				
				var docClassSearchtable = $("#docClassSearchtable").DataTable();
				docClassSearchtable.clear().draw();
				docClassSearchtable.column(0).visible(false);
				docClassSearchtable.column(1).visible(false);
				docClassSearchtable.columns.adjust().draw();
				var DResult = data.Result;

				console.log(DResult);
				var Length = DResult.length;
				for (var i = 0; i < Length;) {

					docClassSearchtable.row.add([DResult[i].fdLkUpCode, DResult[i].fdKeyCode, DResult[i].fdKey1, DResult[i].fdUserName, DResult[i].fdCreatedOn]).draw();
					i++;
				}
			}
			else {
				var docClassSearchtable = $("#docClassSearchtable").DataTable();
				docClassSearchtable.clear().draw();
				showMessage("red", data.Description)
			}
			$("body").find(".query1").removeClass("visible");
		});
}


function resetSearchParadoxClass(msg) {

	if (msg == 1)
	{
		bootbox.confirm({
			message: "Are you sure you'd like to clear from fields?",
			buttons: {
				confirm: {
					label: 'Yes',
					className: 'btn-danger'
				},
				cancel: {
					label: 'No',
					className: 'btn-primary'
				}
			},
			callback: function (result) {
				if (result == true) {
					$("#searchPanadoxClassName").val("");
					$("#searchFromDate").val("");
					$("#searchToDate").val("");
				}
			}
		}).find("div.modal-body").addClass("ArchiveBootBoxHeight");
	}
	else
	{
		$("#searchPanadoxClassName").val("");
		$("#searchFromDate").val("");
		$("#searchToDate").val("");
	}
	
	
	//var schtable = $("#grdschDocumentclassh").DataTable();
	//schtable.clear().draw();
}

function openSeachTab()
{
	$(".query1").addClass("visible");
	$("#searchKeyword").val("");
	//$('#searchPanadoxClassName').focus();
	resetSearchParadoxClass();
}

//$(".dropdown-form").click(function () {
//	$(".query1").addClass("visible");
//});

$(document).click(function (event) {
	
	//if you click on anything except the modal itself or the "open modal" link, close the modal
	if (!$(event.target).closest(".query1,.dropdown-form").length) {
		$("body").find(".query1").removeClass("visible");
		$("#searchPanadoxClassName").val("");
	$("#searchFromDate").val("");
	$("#searchToDate").val("");
	$("#searchKeyword").val("");
		
	}
	
});

//-----Edit and Update Row Data on Double Click-------//

var glresult = "";
function editAndUpdateDocumentClassRowData() {
	debugger;
	$('#docClassSearchtable tbody').off('dblclick').on('dblclick', 'tr', function () {
		var grdItemDataTable = $('#docClassSearchtable').DataTable();
		var rowIndex = grdItemDataTable.row(this).index();
		var data = grdItemDataTable.row(this).data();
		var requestData = data[1];
		var oResult;
		console.log(data);
		ajaxCaller("POST", "DocumentClass/GetDocumentClassDetail", data[1], "application/json", "Please wait ! Getting document class details").done(function (data) {
			if (data.Status == 1) {
				debugger;
				oResult = data.Result.DocClassDetails;
				
				glresult = oResult;

				//$('.nav-tabs a[href="#CreateParadoxClass"]').tab('show');
				$('.documentClass').show();
				$('.search-panadox').hide();
				$('#resetRcrds').hide();
				//if (oResult.id != null && oResult.id != undefined && oResult.id != "")
				//    $("#hdnUserID").val(oResult.id);
				//if (oResult.fduserid != null && oResult.fduserid != undefined && oResult.fduserid != "")
				//	$("#hdnUserID").val(oResult.fduserid);

				if (oResult[0].DocClassName != null && oResult[0].DocClassName != undefined && oResult[0].DocClassName != "") {
					$('#txtParadoxClassName').val(oResult[0].DocClassName).prop('disabled', 'disabled');		    
				}
				$("#chkIsRequired").attr("disabled", true);
				//$('#cboAuthorDepartment').prop('disabled', 'disabled');

				//$('.Read-Write').hide();
				//$('.Read').hide();


				//dcid
				$('#txtParadoxClassID').val(oResult[0].dcid);

				//$('#btnAddProperties').hide();
				//$('#savedocClass').hide();
			  


				var grdParadoxProperties = $('#grdParadoxProperties').DataTable();
				grdParadoxProperties.clear().draw();

				//grdParadoxProperties.column(0).visible(false);
				//grdParadoxProperties.column(3).visible(false);
				//grdParadoxProperties.columns.adjust().draw();

				var TotalSequenceNo=[];
				

				for (var i = 0; i < oResult.length; i++) {
					if (oResult[i].isRequired == 1)
					{
						var isRequired = "Yes";
					}
					else
					{
						var isRequired = "No";
					}

					TotalSequenceNo.push(oResult[i].sequence);
					grdParadoxProperties.row.add([
						oResult[i].dcid,
						oResult[i].ColumnName,                     
						oResult[i].ColumnType,
						  oResult[i].DocTypeId,
						oResult[i].sequence,
						  isRequired,
							oResult[i].defaultValue,
							
							'<button class="btn btn-red btn-circle btn-sm btnRemoveProperties"><i class="fa fa-times-circle fa-2x"></i></button>'
						//'<div class=row"><i class="fa fa-pencil cursorclick" onclick="editDoctemp()"></i> &nbsp&nbsp&nbsp<i class="fa fa-trash" onclick="changeStatus()"></i></div>'
					]).draw();

				}

				$('#txtSequence').val(Math.max.apply(null,TotalSequenceNo)+1);

				var grdAuthorDepartments = $('#grdAuthorDepartments').DataTable();

				grdAuthorDepartments.clear().draw();
			  
				var department = data.Result.DepartmentDetails;
				debugger;
				for (var i = 0; i < department.length; i++) {				   
						grdAuthorDepartments.row.add([department[i].PKID,
						department[i].DepartmentID,
						department[i].DepartmentName,
						department[i].Rights,
						department[i].RightID,
						'<button class="btn btn-red btn-circle btn-sm btnRemoveAuthorDepartment"><i class="fa fa-times-circle fa-2x"></i></button>'
						//'<div class=row"><i class="fa fa-pencil cursorclick" onclick="editDoctemp()"></i> &nbsp&nbsp&nbsp<i class="fa fa-trash  " onclick="changeStatus()"></i></div>'
						]).draw();		

				}

				//showMessage("green", data.Description, "");
			}
			else {
				showMessage("red", data.Description, "");
			}
		});
		$('#grdSrchUser').off('draw.dt').on('draw.dt', function () {
			removeItemDataTableControl();
		});
	});
}
function searchDocumentsUsingSearchBar() {
	debugger;
	var inputData = {};
	var searchInputKeyword = $("#searchKeyword").val();
	inputData.searchInputKeyword = searchInputKeyword;
	ajaxCaller("POST", "DocumentClass/searchDocumentsUsingSearchBar", JSON.stringify(inputData), "application/json", "Please wait!").done(function (data) {
		debugger;

		if (data.Status == 1) {
			debugger;
			showMessage("green", data.Description);
			var docClassSearchtable = $("#docClassSearchtable").DataTable();
			docClassSearchtable.clear().draw();
			docClassSearchtable.column(0).visible(false);
			docClassSearchtable.column(1).visible(false);
			docClassSearchtable.columns.adjust().draw();
			var DResult = data.Result;

			console.log(DResult);
			var Length = DResult.length;
			for (var i = 0; i < Length;) {

				docClassSearchtable.row.add([
					DResult[i].fdLkUpCode, DResult[i].fdKeyCode, DResult[i].fdKey1, DResult[i].fdUserName,
					DResult[i].fdCreatedOn
				]).draw();
				i++;
			}
		} else {
			var docClassSearchtable = $("#docClassSearchtable").DataTable();
			docClassSearchtable.clear().draw();
			showMessage("red", data.Description, "");
		}
	});
}

//Active and inActive selected menu tab
function documentClassTag() {
	$('#documentSearchTag a').removeClass('active');
	$('#documentChartTag a').removeClass('active');
	$('#documentClassTag a').addClass('active');
	$('#documentTag a').removeClass('active');
	$('#documentAuditTag a').removeClass('active');
	$('#userMasterTag a').removeClass('active');
}

function newDocumentClass() {
	$('.documentClass').show();
	$('.search-panadox').hide();
	$('#txtParadoxClassName').focus();
	resetProperty
	$('#resetRcrds').show();
	$('#txtParadoxClassID').val("");
	$("#chkIsRequired").attr("disabled", false);
	$('.Read-Write').show();
	$('.Read').show();
	SaveReset();

}
function cross() {
	$('.search-panadox').show();
	$('.documentClass').hide();
	//$('#searchKeyword').focus();
	resetProperty();
	$('#txtParadoxClassID').val("");
	SaveReset();
}




//function AddViewerDepartment() {
//	if (jQuery("#cboAuthorDepartment option:selected").val() == 0 || jQuery("#cboAuthorDepartment option:selected").val() == undefined || jQuery("#cboAuthorDepartment option:selected").val() == null) {
//		showMessage("red", "Please Select Viewer Department.");
//		$('#cboAuthorDepartment').focus();
//		return;
//	}
//	var grdViewerDepartments = $("#grdAuthorDepartments").DataTable();
//	grdViewerDepartments.row.add(['','',
//					 jQuery("#cboAuthorDepartment option:selected").text(),
//					 //jQuery("#cboAuthorDepartment option:selected").text(""),
//					  'Read and Write',
					  
//					 '<button class="btn btn-red btn-circle btn-sm btnRemoveViewrDepartment"><i class="fa fa-times-circle fa-2x"></i></button>'
//	]).draw();
//	$("#cboAuthorDepartment").val(0);
   
//}


function AddViewerDepartment1() {
	if (jQuery("#cboAuthorDepartment option:selected").val() == 0 || jQuery("#cboAuthorDepartment option:selected").val() == undefined || jQuery("#cboAuthorDepartment option:selected").val() == null) {
		showMessage("red", "Please Select Viewer Department.");
		$('#cboAuthorDepartment').focus();
		return;
	}
	var grdViewerDepartments = $("#grdAuthorDepartments").DataTable();

	for (var i = 0 ; i < grdViewerDepartments.data().length; i++) {

		if ($("#cboAuthorDepartment").val() == grdViewerDepartments.data()[i][1]) {
			if (0 == grdViewerDepartments.data()[i][4]) {
				grdViewerDepartments.rows(i).nodes().to$()
								.addClass('gridError');
			 //   removeErrorCss = i;
				showMessage("red", 'Duplicate Department');
				return false;
			}
		}
	}
	grdViewerDepartments.row.add(['', jQuery("#cboAuthorDepartment option:selected").val(), 
					 jQuery("#cboAuthorDepartment option:selected").text(),
					 //jQuery("#cboAuthorDepartment option:selected").text(""),
					  'Read',
					  0,

					 '<button class="btn btn-red btn-circle btn-sm btnRemoveAuthorDepartment" ><i class="fa fa-times-circle fa-2x"></i></button>'
	]).draw();
	$("#cboAuthorDepartment").val(0);

}



function resetProperty()
{
	$('#txtPropertyName').val('');
	$('#txtSequence').val('');
	$('#cboPropertyDataType').val(0);
	$('#txtDefaultValue').val('');
	$('input[type=checkbox]').prop('checked', false);
	var grdParadoxProperties = $('#grdParadoxProperties').DataTable();
	grdParadoxProperties.clear().draw();
	$('#NumDefaultValue').val("");
	$('#txtDefaultValue').val("");
	$('#dateDefaultValue').val("");

}

function SaveReset(msg)
{
	if (msg == 1)
	{
		bootbox.confirm({
			message: "Are you sure you'd like to clear from fields?",
			buttons: {
				confirm: {
					label: 'Yes',
					className: 'btn-danger'
				},
				cancel: {
					label: 'No',
					className: 'btn-primary'
				}
			},
			callback: function (result) {
				if (result == true) {

					debugger;
					var oResult = glresult;
					if ($('#txtParadoxClassID').val() != "" && $('#txtParadoxClassID').val() != undefined && $('#txtParadoxClassID').val() != null) {
						if (oResult[0].DocClassName != null && oResult[0].DocClassName != undefined && oResult[0].DocClassName != "") {
							$('#txtParadoxClassName').val(oResult[0].DocClassName).prop('disabled', 'disabled');
						}
						$("#chkIsRequired").attr("disabled", false);
						$('#cboAuthorDepartment').prop('disabled', 'disabled');

						$('.Read-Write').hide();
						$('.Read').hide();


						//dcid
						$('#txtParadoxClassID').val(oResult[0].dcid);

						//$('#btnAddProperties').hide();
						//$('#savedocClass').hide();



						var grdParadoxProperties = $('#grdParadoxProperties').DataTable();
						grdParadoxProperties.clear().draw();

						//grdParadoxProperties.column(0).visible(false);
						//grdParadoxProperties.column(3).visible(false);
						//grdParadoxProperties.columns.adjust().draw();

						var TotalSequenceNo = [];


						for (var i = 0; i < oResult.length; i++) {
							if (oResult[i].isRequired == 1) {
								var isRequired = "Yes";
							}
							else {
								var isRequired = "No";
							}

							TotalSequenceNo.push(oResult[i].sequence);
							grdParadoxProperties.row.add([
								oResult[i].dcid,
								oResult[i].ColumnName,
								oResult[i].ColumnType,
								  oResult[i].DocTypeId,
								oResult[i].sequence,
								  isRequired,
									oResult[i].defaultValue,
								'<div class=row"><i class="fa fa-pencil cursorclick" onclick="editDoctemp()"></i> &nbsp&nbsp&nbsp<i class="fa fa-trash" onclick="changeStatus()"></i></div>'
							]).draw();

						}

						$('#txtSequence').val(Math.max.apply(null, TotalSequenceNo) + 1);

						var grdAuthorDepartments = $('#grdAuthorDepartments').DataTable();

						grdAuthorDepartments.clear().draw();

						resetProperty();
						$('#txtParadoxClassName').val('').prop('disabled', false);
						$('#txtParadoxClassID').val('');
						$('#cboAuthorDepartment').val(0).prop('disabled', false);
						var grdAuthorDepartments = $('#grdAuthorDepartments').DataTable();
						grdAuthorDepartments.clear().draw();
						var grdParadoxProperties = $('#grdParadoxProperties').DataTable();
						grdParadoxProperties.clear().draw();

						$('#txtSequence').val(1);
						$('#NumDefaultValue').val("");
						$('#txtDefaultValue').val("");
						$('#dateDefaultValue').val("");
						$('#txtParadoxClassName').focus();
						$('.Read-Write').show();
						$('.Read').show();

						//var department;
						//if (data.Result != null) {
						//     department = data.Result.DepartmentDetails;
						//}
						//for (var i = 0; i < department.length; i++) {
						//    grdAuthorDepartments.row.add([department[i].PKID,
						//        department[i].DepartmentID,
						//        department[i].DepartmentName,
						//        department[i].Rights,
						//        department[i].RightID,
						//        '<div class=row"><i class="fa fa-pencil cursorclick" onclick="editDoctemp()"></i> &nbsp&nbsp&nbsp<i class="fa fa-trash  " onclick="changeStatus()"></i></div>'
						//    ]).draw();

						//}
					} else {
						resetProperty();
						$('#txtParadoxClassName').val('').prop('disabled', false);
						$('#txtParadoxClassID').val('');
						$('#cboAuthorDepartment').val(0).prop('disabled', false);
						var grdAuthorDepartments = $('#grdAuthorDepartments').DataTable();
						grdAuthorDepartments.clear().draw();
						var grdParadoxProperties = $('#grdParadoxProperties').DataTable();
						grdParadoxProperties.clear().draw();

						$('#txtSequence').val(1);
						$('#NumDefaultValue').val("");
						$('#txtDefaultValue').val("");
						$('#dateDefaultValue').val("");
						$('#txtParadoxClassName').focus();
					}

				}
			}
		}).find("div.modal-body").addClass("ArchiveBootBoxHeight");
	}
	else
	{
		debugger;
		var oResult = glresult;
		if ($('#txtParadoxClassID').val() != "" && $('#txtParadoxClassID').val() != undefined && $('#txtParadoxClassID').val() != null) {
			if (oResult[0].DocClassName != null && oResult[0].DocClassName != undefined && oResult[0].DocClassName != "") {
				$('#txtParadoxClassName').val(oResult[0].DocClassName).prop('disabled', 'disabled');
			}
			$("#chkIsRequired").attr("disabled", false);
			$('#cboAuthorDepartment').prop('disabled', 'disabled');

			$('.Read-Write').hide();
			$('.Read').hide();


			//dcid
			$('#txtParadoxClassID').val(oResult[0].dcid);

			//$('#btnAddProperties').hide();
			//$('#savedocClass').hide();



			var grdParadoxProperties = $('#grdParadoxProperties').DataTable();
			grdParadoxProperties.clear().draw();

			//grdParadoxProperties.column(0).visible(false);
			//grdParadoxProperties.column(3).visible(false);
			//grdParadoxProperties.columns.adjust().draw();

			var TotalSequenceNo = [];


			for (var i = 0; i < oResult.length; i++) {
				if (oResult[i].isRequired == 1) {
					var isRequired = "Yes";
				}
				else {
					var isRequired = "No";
				}

				TotalSequenceNo.push(oResult[i].sequence);
				grdParadoxProperties.row.add([
					oResult[i].dcid,
					oResult[i].ColumnName,
					oResult[i].ColumnType,
					  oResult[i].DocTypeId,
					oResult[i].sequence,
					  isRequired,
						oResult[i].defaultValue,
					'<div class=row"><i class="fa fa-pencil cursorclick" onclick="editDoctemp()"></i> &nbsp&nbsp&nbsp<i class="fa fa-trash" onclick="changeStatus()"></i></div>'
				]).draw();

			}

			$('#txtSequence').val(Math.max.apply(null, TotalSequenceNo) + 1);

			var grdAuthorDepartments = $('#grdAuthorDepartments').DataTable();

			grdAuthorDepartments.clear().draw();

			resetProperty();
			$('#txtParadoxClassName').val('').prop('disabled', false);
			$('#txtParadoxClassID').val('');
			$('#cboAuthorDepartment').val(0).prop('disabled', false);
			var grdAuthorDepartments = $('#grdAuthorDepartments').DataTable();
			grdAuthorDepartments.clear().draw();
			var grdParadoxProperties = $('#grdParadoxProperties').DataTable();
			grdParadoxProperties.clear().draw();

			$('#txtSequence').val(1);
			$('#NumDefaultValue').val("");
			$('#txtDefaultValue').val("");
			$('#dateDefaultValue').val("");
			$('#txtParadoxClassName').focus();
			$('.Read-Write').show();
			$('.Read').show();

			//var department;
			//if (data.Result != null) {
			//     department = data.Result.DepartmentDetails;
			//}
			//for (var i = 0; i < department.length; i++) {
			//    grdAuthorDepartments.row.add([department[i].PKID,
			//        department[i].DepartmentID,
			//        department[i].DepartmentName,
			//        department[i].Rights,
			//        department[i].RightID,
			//        '<div class=row"><i class="fa fa-pencil cursorclick" onclick="editDoctemp()"></i> &nbsp&nbsp&nbsp<i class="fa fa-trash  " onclick="changeStatus()"></i></div>'
			//    ]).draw();

			//}
		} else {
			resetProperty();
			$('#txtParadoxClassName').val('').prop('disabled', false);
			$('#txtParadoxClassID').val('');
			$('#cboAuthorDepartment').val(0).prop('disabled', false);
			var grdAuthorDepartments = $('#grdAuthorDepartments').DataTable();
			grdAuthorDepartments.clear().draw();
			var grdParadoxProperties = $('#grdParadoxProperties').DataTable();
			grdParadoxProperties.clear().draw();

			$('#txtSequence').val(1);
			$('#NumDefaultValue').val("");
			$('#txtDefaultValue').val("");
			$('#dateDefaultValue').val("");
			$('#txtParadoxClassName').focus();
		}

	}
	
   

}

//function SetRolesLevelAcess(input) {

//    $("#documentChartTag").hide();
//    $("#documentClassTag").hide();
//    $("#documentTag").hide();
//    $("#documentAuditTag").hide();
//    $("#userMasterTag").hide();
//    var IsApproveRejectOccur = false;
//    for (var i = 0; i < input.length; i++) {

//        if (input[i].functionCode == 'SEARCH') {
			
//            $("#documentClassTag").show();
		   

//        }
//        if (input[i].functionCode == 'CREATE') {
		   
//            $("#documentClassTag").show();
		   


//        }
//    }

//}
//function setAndSetUserScreenModules() {
//    ajaxCaller("POST", "Common/getModuleRights", "DOC_CLASS", "application/json", "Please wait ! Getting Screen Parameters").done(function (data) {
//        console.log(data);
//        rolesRight = data.Result;
//        SetRolesLevelAcess(data.Result);
//    });
//}

$('select').on('change', function () {
	var datatype = $('#cboPropertyDataType').val();
	if (datatype==1)
	{
		//$('#datefield').show();
		//$('#txtfield').hide();
	}
else
	{
		//$('#txtfield').show();
		//$('#datefield').hide();
}
});

$(document).bind('keypress', function (e) {
   
	if (document.getElementById("query1").classList.contains('visible')) {
		if (e.keyCode == 13) {
			SearchParadoxClass();
		}
	}

});


//dsTable