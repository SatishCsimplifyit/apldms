﻿var sequence = 1;
$('document').ready(function () {
    documentClassTag();
    $('.documentClass').hide();
	$('#query1').hide();
	$('#txtSequence').val(sequence);
	// $('#txtSequence').attr('disabled',true);

	$('.securityTable1').DataTable({

		columns: [
			 { title: "Id" },
			{ title: "Author Department ID" },
			 { title: "Author Department Name" },
			{ title: "Remove" }
		]
	});
	$('.securityTable2').DataTable({

		columns: [{ title: "Id" },
			{ title: "Author Department ID" },
			{ title: "Viewer Department Name" },
			{ title: "Remove" }
		]
	});


	$('#docClassSearchtable').DataTable({

		columns: [{ title: "ID" },
			{ title: "Doc ID" },
			{ title: "Doc Class Name" },
			{ title: "Created By" },
			{ title: "Created On" }
		]
	});

	$('#grdParadoxProperties').DataTable({

		columns: [{ title: "Name" },
			{ title: "Datatype" },
			{ title: "sequence" }
		]
	});

	editAndUpdateDocumentClassRowData();



	//var docClassSearchtable = $("#docClassSearchtable").DataTable();
	//.column(0).visible(false);

	var grdParadoxProperties = $("#grdParadoxProperties").DataTable();
	//grdeditdata.column(1).visible(false);
	grdParadoxProperties.column(0).visible(true);
	grdParadoxProperties.column(2).visible(true);
	grdParadoxProperties.columns.adjust().draw();
	$('.dt-buttons').hide();
	// $('.dataTables_filter').hide();
	var grdViewerDepartments = $("#grdViewerDepartments").DataTable();
	grdViewerDepartments.column(0).visible(false);
	grdViewerDepartments.column(1).visible(false);

	var grdAuthorDepartments = $("#grdAuthorDepartments").DataTable();
	grdAuthorDepartments.column(0).visible(false);
	grdAuthorDepartments.column(1).visible(false);

	fetchCheckListInitiallyParameterData();
});

function fetchCheckListInitiallyParameterData() {
	debugger;
	var inputData = {};
	var lookUpCodes = [];
	lookUpCodes.push("DEPARTMENT");
	lookUpCodes.push("DATA_TYPE");
	inputData.lookUpCode = lookUpCodes;

	ajaxCaller("POST", "/Common/fetchStatus", JSON.stringify(inputData), "application/json", "Please wait ! Getting Screen Parameters").done(function (data) {
		if (data.Status == 1) {
			//debugger;
			var oResult = data.Result;
			$.each(oResult, function (key, value) {
				if (value.fdLkUpCode == "DEPARTMENT") {
					$('#cboAuthorDepartment').append($('<option></option>').val(value.fdKeyCode).html(value.fdKey1));
					$('#cboViewerDepartment').append($('<option></option>').val(value.fdKeyCode).html(value.fdKey1));
				}
				else if (value.fdLkUpCode == "DATA_TYPE") {
					$('#cboPropertyDataType').append($('<option></option>').val(value.fdKeyCode).html(value.fdKey1));
				}
			});
		} else if (data.Status == 2) {
			showMessage("red", data.Description, "");
		} else {
			showMessage("red", data.Description, "");
		}
	});
}
function addPropertiesToGrid() {
	debugger;
	if ($('#txtPropertyName').val() == '' || $('#txtPropertyName').val() == undefined || $('#txtPropertyName').val() == null) {
		showMessage("red", "Please Enter Property Name");
		$('#txtPropertyName').focus();
		return;
	}
	if (jQuery("#cboPropertyDataType option:selected").val() == 0 || jQuery("#cboPropertyDataType option:selected").val() == undefined || jQuery("#cboPropertyDataType option:selected").val() == null) {
		showMessage("red", "Please Enter Property Data Type");
		$('#cboPropertyDataType').focus();
		return;
	}
	//if ($('#txtSequence').val() == '' || $('#txtSequence').val() == undefined || $('#txtSequence').val() == null) {
	//    showMessage("red", "Please Enter Squence Value");
	//    $('#txtSequence').focus();
	//    return;
	//}
	var isRequred;
	if ($('#chkIsRequired').is(":checked")) {
		isRequred = "Yes";
	} else {
		isRequred = "No";
	}

	var grdParadoxProperties = $("#grdParadoxProperties").DataTable();
	grdParadoxProperties.row.add([
					 $('#txtPropertyName').val(),
					//jQuery("#cboPropertyDataType option:selected").val(),
					jQuery("#cboPropertyDataType option:selected").text(),
					// jQuery("#txtDefaultValue").val(),
					// isRequred,
					 sequence,
					 '<button class="btn btn-red btn-circle btn-sm btnRemoveProperties"><i class="fa fa-times-circle fa-2x"></i></button>'
	]).draw();

	$('#txtPropertyName').val('');
	$('#cboPropertyDataType').val(0);
	$("#txtDefaultValue").val('');
	$('#txtSequence').val(sequence + 1);
	sequence = sequence + 1;
	$('#chkIsRequired').prop('checked', false);


}
function AddViewerDepartment() {
	if (jQuery("#cboViewerDepartment option:selected").val() == 0 || jQuery("#cboViewerDepartment option:selected").val() == undefined || jQuery("#cboViewerDepartment option:selected").val() == null) {
		showMessage("red", "Please Select Viewer Department.");
		$('#cboViewerDepartment').focus();
		return;
	}
	var grdViewerDepartments = $("#grdViewerDepartments").DataTable();
	grdViewerDepartments.row.add(['',
					 jQuery("#cboViewerDepartment option:selected").val(),
					jQuery("#cboViewerDepartment option:selected").text(),
					 '<button class="btn btn-red btn-circle btn-sm btnRemoveViewrDepartment"><i class="fa fa-times-circle fa-2x"></i></button>'
	]).draw();
	$("#cboViewerDepartment").val(0);

}
function AddAuthorDepartment() {
	if (jQuery("#cboAuthorDepartment option:selected").val() == 0 || jQuery("#cboAuthorDepartment option:selected").val() == undefined || jQuery("#cboAuthorDepartment option:selected").val() == null) {
		showMessage("red", "Please Select Author Department.");
		$('#cboAuthorDepartment').focus();
		return;
	}
	var grdAuthorDepartments = $("#grdAuthorDepartments").DataTable();
	grdAuthorDepartments.row.add(['',
					 jQuery("#cboAuthorDepartment option:selected").val(),
					jQuery("#cboAuthorDepartment option:selected").text(),
					 '<button class="btn btn-red btn-circle btn-sm btnRemoveAuthorDepartment"><i class="fa fa-times-circle fa-2x"></i></button>'
	]).draw();
	$("#cboAuthorDepartment").val(0);

}
function SaveParadoxClass() {
	if ($('#txtParadoxClassName').val() == '' || $('#txtParadoxClassName').val() == undefined || $('#txtParadoxClassName').val() == null) {
		showMessage("red", "Please enter paradox class name.");
		$('#txtParadoxClassName').focus();
		return;
	}

	var grdParadoxProperties = $("#grdParadoxProperties").DataTable();
	if (grdParadoxProperties.data().length == 0) {
		$('#txtPropertyName').focus();
		showMessage("red", "Please add atleast one property.");
		return;
	}
	var grdViewerDepartments = $("#grdViewerDepartments").DataTable();
	if (grdViewerDepartments.data().length == 0) {
		$('#cboAuthorDepartment').focus();
		showMessage("red", "Please add atleast one viewer department.");
		return;
	}
	var grdAuthorDepartments = $("#grdAuthorDepartments").DataTable();
	if (grdAuthorDepartments.data().length == 0) {
		$('#cboViewerDepartment').focus();
		showMessage("red", "Please add atleast one author department.");
		return;
	}
	var inputData = {};
	inputData["txtParadoxClassName"] = $('#txtParadoxClassName').val();
	var loaParadoxProperties = [];
	for (var i = 0; i < grdParadoxProperties.data().length; i++) {

		if (grdParadoxProperties.data()[i][0] == "") {
			var lojParadoxProperties = {};
			lojParadoxProperties["propertyName"] = grdParadoxProperties.data()[i][1];
			lojParadoxProperties["dataType"] = grdParadoxProperties.data()[i][2];
			//lojParadoxProperties["isRequired"] = grdParadoxProperties.data()[i][5];
			lojParadoxProperties["sequence"] = grdParadoxProperties.data()[i][6];
			//lojParadoxProperties["defaultValue"] = grdParadoxProperties.data()[i][4];

			loaParadoxProperties.push(lojParadoxProperties);
		}
	}
	inputData["properties"] = loaParadoxProperties;
	var loaAuthorDeparments = [];
	for (var i = 0; i < grdAuthorDepartments.data().length; i++) {
		if (grdAuthorDepartments.data()[i][0] == "") {
			loaAuthorDeparments.push(grdAuthorDepartments.data()[i][1]);
		}
	}
	inputData["authorDepartments"] = loaAuthorDeparments;
	var loaViewerDeparments = [];
	for (var i = 0; i < grdViewerDepartments.data().length; i++) {
		if (grdViewerDepartments.data()[i][0] == "") {
			loaViewerDeparments.push(grdViewerDepartments.data()[i][1]);
		}
	}
	inputData["viewerDepartments"] = loaViewerDeparments;
	console.log(inputData);
	ajaxCaller("POST", "DocumentClass/SaveDocumentClass", JSON.stringify(inputData), "application/json", "Saving Paradox..")
		.done(function (data) {
			if (data.Status == 1) {
				showMessage("green", data.Description)
			}
			else {
				showMessage("red", data.Description)
			}
		});

}
function SearchParadoxClass() {
	var input = {};
	if ($('#searchPanadoxClassName').val() != null && $('#searchPanadoxClassName').val() != undefined && $('#searchPanadoxClassName').val() != "") {
		input['searchPanadoxClassName'] = $('#searchPanadoxClassName').val();
	}
	if ($('#searchFromDate').val() != null && $('#searchFromDate').val() != undefined && $('#searchFromDate').val() != "") {
		input['searchFromDate'] = $('#searchFromDate').val();
	}
	if ($('#searchToDate').val() != null && $('#searchToDate').val() != undefined && $('#searchToDate').val() != "") {
		input['searchToDate'] = $('#searchToDate').val();
	}

	ajaxCaller("POST", "DocumentClass/SearchDocumentClass", JSON.stringify(input), "application/json", "Searching Paradox..")
		.done(function (data) {

			if (data.Status == 1) {
				debugger;
				showMessage("green", data.Description)
				var docClassSearchtable = $("#docClassSearchtable").DataTable();
				docClassSearchtable.clear().draw();
				docClassSearchtable.column(0).visible(false);
				docClassSearchtable.column(1).visible(false);
				docClassSearchtable.columns.adjust().draw();
				var DResult = data.Result;

				console.log(DResult);
				var Length = DResult.length;
				for (var i = 0; i < Length;) {

					docClassSearchtable.row.add([DResult[i].fdLkUpCode, DResult[i].fdKeyCode, DResult[i].fdKey1, DResult[i].fdUserName, DResult[i].fdCreatedOn]).draw();
					i++;
				}
			}
			else {
				showMessage("red", data.Description)
			}
		});
}
function resetSearchParadoxClass() {
	$("#searchPanadoxClassName").val("");
	$("#searchFromDate").val("");
	$("#searchToDate").val("");
	//var schtable = $("#grdschDocumentclassh").DataTable();
	//schtable.clear().draw();
}


$(".dropdown-form").click(function () {
	$(".query1").addClass("visible");
});

$(document).click(function (event) {
	//if you click on anything except the modal itself or the "open modal" link, close the modal
	if (!$(event.target).closest(".query1,.dropdown-form").length) {
		$("body").find(".query1").removeClass("visible");
	}
});

//-----Edit and Update Row Data on Double Click-------//

function editAndUpdateDocumentClassRowData() {
	debugger;
	$('#docClassSearchtable tbody').off('dblclick').on('dblclick', 'tr', function () {
		var grdItemDataTable = $('#docClassSearchtable').DataTable();
		var rowIndex = grdItemDataTable.row(this).index();
		var data = grdItemDataTable.row(this).data();
		var requestData = data[1];
		var oResult;
		console.log(data);
		ajaxCaller("POST", "/DocumentClass/GetDocumentClassDetail", data[1], "application/json", "Please wait ! Getting document class details").done(function (data) {
			if (data.Status == 1) {
				debugger;
				oResult = data.Result;
				var grdItemDataTable = $('#grdParadoxProperties').DataTable();


				$('.nav-tabs a[href="#CreateParadoxClass"]').tab('show');
				//if (oResult.id != null && oResult.id != undefined && oResult.id != "")
				//    $("#hdnUserID").val(oResult.id);
				//if (oResult.fduserid != null && oResult.fduserid != undefined && oResult.fduserid != "")
				//	$("#hdnUserID").val(oResult.fduserid);

				if (oResult.fdDescription != null &&
					oResult.fdDescription != undefined &&
					oResult.fdDescription != "") {
					$('#txtParadoxClassName').val(oResult.fdDescription);
				}

				showMessage("green", data.Description, "");

				for (var i = 0; i < 1; i++) {
					grdItemDataTable.row.add([
						oResult.dcColumnName,
						oResult.fdKey2,
						oResult.sequence,
						'<div class=row"><i class="fa fa-pencil cursorclick" onclick="editDoctemp()"></i> &nbsp&nbsp&nbsp<i class="fa fa-trash  " onclick="changeStatus()"></i></div>'
					]).draw();

				}
			} else {
				showMessage("red", data.Description, "");
			}
		});
		$('#grdSrchUser').off('draw.dt').on('draw.dt', function () {
			removeItemDataTableControl();
		});
	});
}
function searchDocumentsUsingSearchBar() {
	debugger;
	var inputData = {};
	var searchInputKeyword = $("#searchKeyword").val();
	inputData.searchInputKeyword = searchInputKeyword;
	ajaxCaller("POST", "/DocumentClass/searchDocumentsUsingSearchBar", JSON.stringify(inputData), "application/json", "Please wait!").done(function (data) {
		debugger;

		if (data.Status == 1) {
			debugger;
			showMessage("green", data.Description);
			var docClassSearchtable = $("#docClassSearchtable").DataTable();
			docClassSearchtable.clear().draw();
			docClassSearchtable.column(0).visible(false);
			docClassSearchtable.column(1).visible(false);
			docClassSearchtable.columns.adjust().draw();
			var DResult = data.Result;

			console.log(DResult);
			var Length = DResult.length;
			for (var i = 0; i < Length;) {

				docClassSearchtable.row.add([
					DResult[i].fdLkUpCode, DResult[i].fdKeyCode, DResult[i].fdKey1, DResult[i].fdUserName,
					DResult[i].fdCreatedOn
				]).draw();
				i++;
			}
		} else {
			showMessage("red", data.Description, "");
		}
	});
}

//Active and inActive selected menu tab
function documentClassTag() {
    $('#documentSearchTag a').removeClass('active');
    $('#documentChartTag a').removeClass('active');
    $('#documentClassTag a').addClass('active');
    $('#documentTag a').removeClass('active');
    $('#documentAuditTag a').removeClass('active');
    $('#userMasterTag  a').removeClass('active');
}

function newDocumentClass() {
    $('.documentClass').show();
    $('.search-panadox').hide();
}
function cross() {
    $('.search-panadox').show();
    $('.documentClass').hide();
}