﻿$('document').ready(function () {

    var grdeditdata = $("#grdedidDocumentclass").DataTable();
    grdeditdata.column(1).visible(false);
    grdeditdata.column(2).visible(false);
    grdeditdata.columns.adjust().draw();

    SerchTextBoxList("txtschdocumentclassname", "btnschdocclassname", "/CrtRule/srhcrtrule", "lstSchdocclassname", "hdnSchdocclassnameid", false);
    SerchTextBoxList("txtfieldname", "btnschfieldname", "/CrtRule/srhcrtrule", "lstSchfieldname", "hdnSchfieldnameid", false);
    SerchTextBoxList("txtdocumentclassname", "btndocclassname", "/CrtRule/srhcrtrule", "lstdocclassname", "hdndocclassnameid", false);
   

});

function schdocclassdetails()
{
    debugger;
    var inputdata = {};
    inputdata[""]=$("").val();
}

$("#grdedidDocumentclass").on('click', 'button.svupdocclass', function () {
    debugger;
    var editdocclsstable = $("#grdedidDocumentclass").DataTable();
    var nRow = $(this).parents('tr')[0];
    var rowIndex = nRow._DT_RowIndex;
    var aData = editdocclsstable.data()[rowIndex];
    restoreData = aData;
    var count = restoreData[1];
    var tbhdnid = restoreData[2];
    var inputdata = {};
    inputdata["hdndocclassnametableid"] = tbhdnid;
    inputdata["txtfieldcolumnname"] = $('#txtfieldcolumnname_' + count).val();
    inputdata["cobfiledtype"] = $('#cobfiledtype_' + count).val();
    inputdata["txtminval"] = $('#txtminval_' + count).val();
    inputdata["txtmaxvalue"] = $('#txtmaxvalue_' + count).val();
    inputdata["txtlvquery"] = $('#txtlvquery_' + count).val();

    ajaxCaller(
                   "post",
                   "DocumentClass/savedocclass", JSON.stringify(inputdata), "application/json", "Saving details"
                   ).done(function (data) {
                       if (data.Status == 1) {
                           debugger;
                           showMessage("green", "Document Class Details Saved Successfully...")

                           var grdeditdata = $("#grdedidDocumentclass").DataTable();
                           grdeditdata.clear().draw();
                           var oresult = data.Result;
                           var schdata = oresult.schdocclass;
                           grdeditdata.column(1).visible(true);
                           grdeditdata.column(2).visible(true);
                           grdeditdata.columns.adjust().draw();

                           var slno = 1;
                           var count = 0;
                           var schdataset = [];
                           for (i = 0; i < schdata.length;) {

                               debugger;
                              
                               var button = ' <button type="submit" id="btneditDistance_' + count + '" class="btn btn-palegreen editdocclass"><i class="fa fa-check"></i>Edit</button> <button type="submit" id="btnSaveupDistance_' + count + '" class="btn btn-palegreen svupdocclass"><i class="fa fa-check"></i>save</button>  <button type="button" id="deldata_' + count + '" class="btn btn-danger">Delete</button>';

                               grdeditdata.row.add([slno, count, schdata[i].dcid, schdata[i].dcColumnName, schdata[i].dcColumnType, schdata[i].minValue, schdata[i].maxVal, schdata[i].lov, button]).draw();
                              
                               $('#btnSaveupDistance_' + count).hide();
                               $('#btneditDistance_' + count).show();

                               count++;
                               i++;
                               slno++;
                           }
                           grdeditdata.column(1).visible(false);
                           grdeditdata.column(2).visible(false);
                           grdeditdata.columns.adjust().draw();

                       }
                       else {
                           showMessage("red", data.Description)
                       }
                   });


});

$('#grdedidDocumentclass').on('click', '.btn-danger', function () {
    debugger;
    grdedittbl = $('#grdedidDocumentclass').DataTable();
    var nRow = $(this).parents('tr')[0];
    var rowIndex = nRow._DT_RowIndex;

    var rowdata = grdedittbl.data()[rowIndex][2];
    debugger;
    ajaxCaller("POST", "DocumentClass/deletedata", rowdata, "application/json", "delete details"
        ).done(function (data) {
            if (data.Status == 1)
            {
                showMessage("green", data.Description, " delete details");
            }
            else
            {
                showMessage("red", data.Description)
            }      
        });
    grdedittbl
       .row($(this).parents('tr'))
       .remove()
       .draw();
});


$("#grdedidDocumentclass").on('click', 'button.editdocclass', function () {
    
    debugger;
    var editdocclsstable = $("#grdedidDocumentclass").DataTable();
    var nRow = $(this).parents('tr')[0];
    var rowIndex = nRow._DT_RowIndex;
    editOtherChargeRow(editdocclsstable, nRow, rowIndex)

});

function editOtherChargeRow(oTable, nRow, rowIndex) {
    debugger;
    var aData = oTable.data()[rowIndex];
    restoreData = aData;
    var OtherChargeId = restoreData[1];
    var fieldname = restoreData[3];
    var fieldtype = restoreData[4];
    var minval = restoreData[5];
    var maxval = restoreData[6];
    var lvquery = restoreData[7];
    var jqTds = $('>td', nRow);
    $('#btnSaveupDistance_' + OtherChargeId).show();
    $('#btneditDistance_' + OtherChargeId).hide();
    jqTds[2].innerHTML = ' <select class="form-control input-sm ddropcdn" id="cobfiledtype_' + OtherChargeId + '"><option value="">Select</option><option>1</option><option>2</option><option>3</option><option>4</option></select>';
    $('#cobfiledtype_' + OtherChargeId).val(fieldtype);
    jqTds[3].innerHTML = ' <input type="text" autocomplete="off"  class="form-control input-sm" id="txtminval_' + OtherChargeId + '" placeholder="Please Enter Integer Value" />';
    $('#txtminval_' + OtherChargeId).val(minval);
    jqTds[4].innerHTML = ' <input type="text" autocomplete="off"  class="form-control input-sm" id="txtmaxvalue_' + OtherChargeId + '" placeholder="Please Enter Integer Value" />';
    $('#txtmaxvalue_' + OtherChargeId).val(maxval);
    jqTds[5].innerHTML = ' <input type="text" autocomplete="off"  class="form-control input-sm" id="txtlvquery_' + OtherChargeId + '" placeholder="Enter Filed Column Name....">';
    $('#txtlvquery_' + OtherChargeId).val(lvquery);

}

function savedocumentclass()
{
    if(isValid()==true)
    {
            debugger;
            
                var inputdata = {};
                inputdata["hdndocclassnametableid"] = $("#hdndocclassnametableid").val();
                inputdata["txtfieldcolumnname"] = $("#txtfieldcolumnname").val();
                inputdata["cobfiledtype"] = $("#cobfiledtype").val();
                inputdata["txtminval"] = $("#txtminval").val();
                inputdata["txtmaxvalue"] = $("#txtmaxvalue").val();
                inputdata["txtlvquery"] = $("#txtlvquery").val();
                inputdata["chkselect"] = $("input[type='checkbox']").val();

                ajaxCaller(
                    "post",
                    "/DocumentClass/savedocclass", JSON.stringify(inputdata), "application/json", "Saving details"
                    ).done(function (data) {
                        if (data.Status == 1) {
                            debugger;
                            showMessage("green", "Document Class Details Saved Successfully...")

                            var grdeditdata = $("#grdedidDocumentclass").DataTable();
                            grdeditdata.clear().draw();
                            var oresult = data.Result;
                            var schdata = oresult.schdocclass;
                            grdeditdata.column(1).visible(true);
                            grdeditdata.column(2).visible(true);
                            grdeditdata.columns.adjust().draw();
                           
                            var serial = 1;
                            var count = 0;
                            var schdataset = [];
                            for (i = 0; i < schdata.length;)
                            {
                                
                                debugger;
                               
                                var button = ' <button type="submit" id="btneditDistance_' + count + '" class="btn btn-palegreen editdocclass"><i class="fa fa-check"></i>Edit</button> <button type="submit" id="btnSaveupDistance_' + count + '" class="btn btn-palegreen svupdocclass"><i class="fa fa-check"></i>save</button>  <button type="button" id="deldata_' + count + '" class="btn btn-danger">Delete</button>';

                                grdeditdata.row.add([serial, count, schdata[i].dcid, schdata[i].dcColumnName, schdata[i].dcColumnType, schdata[i].minValue, schdata[i].maxVal, schdata[i].lov, button]).draw();
                              
                                $('#btnSaveupDistance_' + count).hide();

                                count++;
                                i++;
                                serial++;
                            }
                            grdeditdata.column(1).visible(false);
                            grdeditdata.column(2).visible(false);
                            grdeditdata.columns.adjust().draw();

                        }
                        else {
                            showMessage("red", data.Description)
                        }
                    });                  
    }

}

function isValid() {
    debugger;
    var isValid = false;

   /* if ($("#hdndocclassnameid").val() == 0 || $("#hdndocclassnameid").val() == null || $("#hdndocclassnameid").val() == undefined) {
        $("#txtdocumentclassname").focus();
        showMessage("red", 'Please Select Document Class Name');
        return isValid;
    }*/

    if ($("#txtfieldcolumnname").val() == 0 || $("#txtfieldcolumnname").val() == null || $("#txtfieldcolumnname").val() == undefined) {
        $("#txtfieldcolumnname").focus();
        showMessage("red", 'Please Enter Filed Column Name');
        return isValid;
    }

    if ($("#cobfiledtype").val() == 0 || $("#cobfiledtype").val() == null || $("#cobfiledtype").val() == undefined) {
        $("#cobfiledtype").focus();
        showMessage("red", 'Please Select Field Type');
        return isValid;
    }

    if ($("#txtminval").val() == 0 || $("#txtminval").val() == null || $("#txtminval").val() == undefined) {
        $("#txtminval").focus();
        showMessage("red", 'Please Enter Minimum Value');
        return isValid;
    }

    if ($("#txtmaxvalue").val() == 0 || $("#txtmaxvalue").val() == null || $("#txtmaxvalue").val() == undefined) {
        $("#txtmaxvalue").focus();
        showMessage("red", 'Please Enter Maximum Value');
        return isValid;
    }

    isValid = true;
    return isValid;
}

function savereset()
{
    $("#txtfieldcolumnname").val("");
    $("#cobfiledtype").val("");
    $("#txtminval").val("");
    $("#txtmaxvalue").val("");
   
    
    $('input[type=checkbox]').attr('checked', false);
    $("#txtlvquery").val("");
    var schtable = $("#grdedidDocumentclassh").DataTable();
    schtable.clear().draw();
}

function schreset()
{
    $("#hdnSchdocclassnameid").val("");
    $("#txtdocumentclassname").val("");
    $("#lstSchdocclassname").val("");
    $("#hdnSchfieldnameid").val("");
    $("#txtfieldname").val("");
    $("#lstSchfieldname").val("");
    var schtable = $("#grdschDocumentclassh").DataTable();
    schtable.clear().draw();
}