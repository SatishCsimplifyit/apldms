﻿using System;
using System.Collections.Generic;
using System.Web;

namespace PanasonicDMS.App_Code
{
    public class RespMsgFormatter
    {

        public static Dictionary<String, Object> formatResp(Object status, Object desc, Object results, Object tag)
        {
            Dictionary<String, Object> resp = new Dictionary<String, Object>();
            /*description = description.substring(100,300);*/
            if (status != null || desc != null)
            {
                resp.Add("Status", status);
                resp.Add("Description", desc);
                resp.Add("Result", results);
                resp.Add("Tag", tag);
            }

            return resp;
        }
    }
}