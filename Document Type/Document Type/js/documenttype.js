//Common Js Starts
document.addEventListener("DOMContentLoaded", function(event) {
	if(window.innerWidth < 992){
		var isMobile = true;
		document.querySelector("#sidebar").classList.add("sidebar-close");
		document.querySelector("#sidebar1").classList.add("sidebar-close1");
	}

    document.querySelector("#toggle-sidebar").addEventListener("click", function(event){
        document.querySelector("#sidebar").classList.toggle("sidebar-close");
        setTimeout(function(){
        	if(document.querySelector("#sidebar").classList.contains("sidebar-close")){
        		document.querySelector(".content").classList.remove("hide-on-mob-toggle");
        		if(isMobile){
        			document.querySelector("#sidebar1").style.display = "block";
        		}
        	} else {
        		document.querySelector(".content").classList.add("hide-on-mob-toggle");
        		if(isMobile){
        			document.querySelector("#sidebar1").style.display = "none";
        		}
        	}
        },10);
    });

    
});


$(function() {

  $('.search-input input').blur(function() {

    if ($(this).val()) {
      $(this)
        .find('~ label, ~ span:nth-of-type(n+3)')
        .addClass('not-empty');
    } else {
      $(this)
        .find('~ label, ~ span:nth-of-type(n+3)')
        .removeClass('not-empty');
    }
  });

  $('.search-input input ~ span:nth-of-type(4)').click(function() {
    $('.search-input input').val('');
    $('.search-input input')
      .find('~ label, ~ span:nth-of-type(n+3)')
      .removeClass('not-empty');
  });
  
});

//Common Js Ends
$(document).ready(function () {
    // $('.selectDataType').hide();
    // $('.selectAuthorDept').hide();
    // $('.selectViewerDept').hide();
    $('.panadoxTable').DataTable( {
        data: dataSet,
        columns: [
            { title: "Name"},
            { title: "Datatype" },
            { title: "Required" },
            { title: "Sequence" },
            { title: "Remove" }
        ]
    });
    $('.securityTable1').DataTable( {
        data: dataSet1,
        columns: [
            { title: "Author Department Name"},
            { title: "Remove" }
        ]
    });
    $('.securityTable2').DataTable( {
        data: dataSet2,
        columns: [
            { title: "Viewer Department Name"},
            { title: "Remove" }
        ]
    });
});
var dataSet = [
    [ "Panasonic", "Abc", "Yes", "6", "No" ],
    [ "Panasonic", "Abc", "Yes", "6", "No" ],
    [ "Panasonic", "Abc", "Yes", "6", "No" ],
    [ "Panasonic", "Abc", "Yes", "6", "No" ]
];
var dataSet1 = [
    [ "Panasonic", "No" ],
    [ "Panasonic", "No" ],
    [ "Panasonic", "No" ],
    [ "Panasonic", "No" ]
];
var dataSet2 = [
    [ "Panasonic", "No" ],
    [ "Panasonic", "No" ],
    [ "Panasonic", "No" ],
    [ "Panasonic", "No" ]
];

// $(".dataType").click(function () {
//     $(".selectDataType").addClass("visible");
// });
// $('.selectDataType li').click(function () {
//     $("#cboPropertyDataType").val($(this).attr("value"));
//     $('.selectDataType li').hide();
// });
// $('#cboPropertyDataType').focus(function () {
//     $('.selectDataType li').show();
// });
// $(document).click(function(event) {
//   //if you click on anything except the modal itself or the "open modal" link, close the modal
//   if (!$(event.target).closest(".selectDataType,#cboPropertyDataType").length) {
//     $("body").find(".selectDataType").removeClass("visible");
//   }
// });
// $(".authorDept").click(function () {
//     $(".selectAuthorDept").addClass("visible");
// });
// $('.selectAuthorDept li').click(function () {
//     $("#cboAuthorDepartment").val($(this).attr("value"));
//     $('.selectAuthorDept li').hide();
// });
// $('#cboAuthorDepartment').focus(function () {
//     $('.selectAuthorDept li').show();
// });
// $(document).click(function(event) {
//   //if you click on anything except the modal itself or the "open modal" link, close the modal
//   if (!$(event.target).closest(".selectAuthorDept,#cboAuthorDepartment").length) {
//     $("body").find(".selectAuthorDept").removeClass("visible");
//   }
// });
// $(".viewerDept").click(function () {
//     $(".selectViewerDept").addClass("visible");
// });
// $('.selectViewerDept li').click(function () {
//     $("#cboViewerDepartment").val($(this).attr("value"));
//     $('.selectViewerDept li').hide();
// });
// $('#cboViewerDepartment').focus(function () {
//     $('.selectViewerDept li').show();
// });
// $(document).click(function(event) {
//   //if you click on anything except the modal itself or the "open modal" link, close the modal
//   if (!$(event.target).closest(".selectViewerDept,#cboViewerDepartment").length) {
//     $("body").find(".selectViewerDept").removeClass("visible");
//   }
// });
