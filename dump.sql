-- MySQL dump 10.13  Distrib 8.0.13, for Win64 (x86_64)
--
-- Host: localhost    Database: pannasonicdms
-- ------------------------------------------------------
-- Server version	8.0.13

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8mb4 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `tbcodelkup`
--

DROP TABLE IF EXISTS `tbcodelkup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `tbcodelkup` (
  `fdLkUpCode` varchar(30) NOT NULL DEFAULT '',
  `fdKeyCode` int(11) NOT NULL,
  `fdDescription` varchar(500) DEFAULT NULL,
  `fdKey1` varchar(100) DEFAULT NULL,
  `fdkey2` varchar(45) DEFAULT NULL,
  `fdKey3` varchar(45) DEFAULT NULL,
  `fdKey4` varchar(45) DEFAULT NULL,
  `fdKey5` varchar(45) DEFAULT NULL,
  `fdCreatedOn` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `fdCreatedBy` int(11) NOT NULL,
  `fdModifiedOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `fdModifiedBy` int(11) NOT NULL,
  `fdIsSystemDefined` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`fdLkUpCode`,`fdKeyCode`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbcodelkup`
--

LOCK TABLES `tbcodelkup` WRITE;
/*!40000 ALTER TABLE `tbcodelkup` DISABLE KEYS */;
INSERT INTO `tbcodelkup` VALUES ('DATA_TYPE',1,'Integer','Number',NULL,NULL,NULL,NULL,'2018-12-18 13:12:17',1,'2018-12-18 07:42:17',1,1),('DATA_TYPE',3,'Text','Text',NULL,NULL,NULL,NULL,'2018-12-18 13:12:17',1,'2018-12-18 07:42:17',1,1),('DEPARTMENT',1,'Finance','Finance',NULL,NULL,NULL,NULL,'2018-12-18 13:08:14',1,'2018-12-18 07:38:14',1,1),('DEPARTMENT',2,'HR','HR',NULL,NULL,NULL,NULL,'2018-12-18 13:08:14',1,'2018-12-18 07:38:14',1,1),('DEPARTMENT',3,'Operation','Operation',NULL,NULL,NULL,NULL,'2018-12-18 13:08:14',1,'2018-12-18 07:38:14',1,1),('DEPARTMENT',4,'Sales','Sales',NULL,NULL,NULL,NULL,'2018-12-18 13:08:14',1,'2018-12-18 07:38:14',1,1),('DEPARTMENT',5,'IT','IT',NULL,NULL,NULL,NULL,'2018-12-18 13:08:14',1,'2018-12-18 07:38:14',1,1),('DOCCLASS_COLMN_TYP',1,'Date Time Column','D',NULL,NULL,NULL,NULL,'2018-12-11 12:53:58',1,'2018-12-11 07:23:58',1,1),('DOCCLASS_COLMN_TYP',2,'Text Column','T',NULL,NULL,NULL,NULL,'2018-12-11 12:53:58',1,'2018-12-11 07:23:58',1,NULL),('DOCCLASS_COLMN_TYP',3,'Number Column','I',NULL,NULL,NULL,NULL,'2018-12-11 12:53:58',1,'2018-12-11 07:23:58',1,NULL),('DOC_CLASS_TYPE',1,'Doc Class',NULL,NULL,NULL,NULL,NULL,'2015-10-10 00:00:00',1,'2018-07-10 08:26:44',1,1),('DOC_CLASS_TYPE',2,'Policy','Policy',NULL,NULL,NULL,NULL,'2019-01-03 11:05:27',1,'2019-01-03 05:35:27',1,NULL),('DOC_CLASS_TYPE',3,'Panasonic Policy Document','Panasonic Policy Document',NULL,NULL,NULL,NULL,'2019-01-03 13:08:09',1,'2019-01-03 07:38:09',1,NULL),('DOC_CLASS_TYPE',4,'Sample1','Sample1',NULL,NULL,NULL,NULL,'2019-01-10 15:39:15',1,'2019-01-10 10:09:15',1,NULL),('DOC_CLASS_TYPE',5,'SampalParadox','SampalParadox',NULL,NULL,NULL,NULL,'2019-01-15 15:11:11',1,'2019-01-15 09:41:11',1,NULL),('DOC_CLASS_TYPE',6,'SampalParadox','SampalParadox',NULL,NULL,NULL,NULL,'2019-01-15 15:12:22',1,'2019-01-15 09:42:22',1,NULL),('DOC_CLASS_TYPE',7,'NewDocClass','NewDocClass',NULL,NULL,NULL,NULL,'2019-01-16 13:07:19',1,'2019-01-16 07:37:19',1,NULL);
/*!40000 ALTER TABLE `tbcodelkup` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbdocumentclass`
--

DROP TABLE IF EXISTS `tbdocumentclass`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `tbdocumentclass` (
  `dcid` int(11) NOT NULL AUTO_INCREMENT,
  `dcColumnName` varchar(100) NOT NULL,
  `dcColumnType` varchar(1) DEFAULT NULL,
  `minValue` varchar(45) DEFAULT NULL,
  `maxValue` varchar(45) DEFAULT NULL,
  `isRequired` int(11) DEFAULT NULL,
  `lov` varchar(500) DEFAULT NULL,
  `x1` double DEFAULT NULL,
  `x2` double DEFAULT NULL,
  `y1` double DEFAULT NULL,
  `y2` double DEFAULT NULL,
  `associatedDocType` int(11) NOT NULL,
  `sequence` int(11) NOT NULL,
  `createdBy` int(11) NOT NULL,
  `createdOn` datetime NOT NULL,
  `updatedBy` int(11) NOT NULL,
  `updatedOn` datetime NOT NULL,
  `defaultValue` varchar(100) DEFAULT NULL,
  `sqlScript` text,
  PRIMARY KEY (`dcid`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbdocumentclass`
--

LOCK TABLES `tbdocumentclass` WRITE;
/*!40000 ALTER TABLE `tbdocumentclass` DISABLE KEYS */;
INSERT INTO `tbdocumentclass` VALUES (24,'Policy Name','T',NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,2,1,1,'2019-01-03 11:05:28',1,'2019-01-03 11:05:28',NULL,NULL),(25,'Policy Type','T',NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,2,2,1,'2019-01-03 11:05:28',1,'2019-01-03 11:05:28',NULL,NULL),(26,'Policy Code','I',NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,2,3,1,'2019-01-03 11:05:28',1,'2019-01-03 11:05:28',NULL,NULL),(27,'Valid Upto','T',NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,2,4,1,'2019-01-03 11:05:28',1,'2019-01-03 11:05:28',NULL,NULL),(28,'Title','T',NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,3,1,1,'2019-01-03 13:08:10',1,'2019-01-03 13:08:10',NULL,NULL),(29,'Document Expiry Date','T',NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,3,2,1,'2019-01-03 13:08:10',1,'2019-01-03 13:08:10',NULL,NULL),(30,'Document type','T',NULL,NULL,1,NULL,NULL,NULL,NULL,NULL,3,3,1,'2019-01-03 13:08:10',1,'2019-01-03 13:08:10',NULL,NULL),(31,'Property1','I',NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,4,1,1,'2019-01-10 15:39:15',1,'2019-01-10 15:39:15',NULL,NULL),(32,'Property2','I',NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,4,2,1,'2019-01-10 15:39:15',1,'2019-01-10 15:39:15',NULL,NULL),(33,'Property1','I',NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,5,0,1,'2019-01-15 15:11:12',1,'2019-01-15 15:11:12',NULL,NULL),(34,'Property1','I',NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,6,0,1,'2019-01-15 15:11:56',1,'2019-01-15 15:11:56',NULL,NULL),(35,'prop','I',NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,7,1,1,'2019-01-16 13:07:20',1,'2019-01-16 13:07:20','1',NULL);
/*!40000 ALTER TABLE `tbdocumentclass` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbdocumentmetadata`
--

DROP TABLE IF EXISTS `tbdocumentmetadata`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `tbdocumentmetadata` (
  `PKID` int(11) NOT NULL AUTO_INCREMENT,
  `fdDocId` int(11) NOT NULL,
  `fdKey` varchar(500) DEFAULT NULL,
  `fdValue` varchar(5000) DEFAULT NULL,
  `fdItemNo` int(11) NOT NULL,
  `fdDocClassId` int(11) DEFAULT NULL,
  PRIMARY KEY (`PKID`),
  KEY `DocIDForeignKey_idx` (`fdDocId`),
  KEY `fdDocClassIDFK_idx` (`fdDocClassId`),
  CONSTRAINT `DocIDForeignKey` FOREIGN KEY (`fdDocId`) REFERENCES `tbdocuments` (`fddocid`),
  CONSTRAINT `fdDocClassIDFK` FOREIGN KEY (`fdDocClassId`) REFERENCES `tbdocumentclass` (`dcid`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbdocumentmetadata`
--

LOCK TABLES `tbdocumentmetadata` WRITE;
/*!40000 ALTER TABLE `tbdocumentmetadata` DISABLE KEYS */;
INSERT INTO `tbdocumentmetadata` VALUES (1,2,'Title','Document',1,NULL),(2,2,'Document Expiry Date','Doc Type',2,NULL),(3,2,'Document type','Policy',3,NULL),(4,3,'Title','Hello',1,NULL),(5,3,'Document Expiry Date','leleo',2,NULL),(6,3,'Document type','kelo',3,NULL),(7,4,'Title','Hello',1,NULL),(8,4,'Document Expiry Date','leleo',2,NULL),(9,4,'Document type','kelo',3,NULL);
/*!40000 ALTER TABLE `tbdocumentmetadata` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbdocuments`
--

DROP TABLE IF EXISTS `tbdocuments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `tbdocuments` (
  `fdDocId` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Unique Id For Documents',
  `fdDocTitle` varchar(1000) NOT NULL,
  `fdDocDesc` longtext,
  `fdDocType` int(11) DEFAULT NULL,
  `fdCreatedBy` int(11) NOT NULL,
  `fdCreatedOn` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `fdFileUrl` varchar(1000) NOT NULL,
  `fdStatus` int(11) NOT NULL COMMENT 'Status Of Contract Document\n 0  for Inactive \n 1 stands for active ',
  `fdApprovedBy` int(11) DEFAULT NULL,
  `fdApprovedOn` datetime DEFAULT NULL,
  `fdRejectBy` int(11) DEFAULT NULL,
  `fdRejectOn` datetime DEFAULT NULL,
  `fdReason` varchar(200) DEFAULT NULL,
  `fdExpiresOn` date DEFAULT NULL,
  `fdVisionId` varchar(500) DEFAULT NULL,
  `fdVisionSelfLink` varchar(5000) DEFAULT NULL,
  `fdVisionETag` varchar(500) DEFAULT NULL,
  `fdVisonName` varchar(500) DEFAULT NULL,
  `fdFromService` int(11) DEFAULT NULL,
  `fdIsPodLink` int(11) DEFAULT NULL,
  `fdDocChannel` int(11) DEFAULT NULL,
  `fdLongitude` varchar(60) DEFAULT NULL,
  `fdLatitude` varchar(60) DEFAULT NULL,
  `fdAddress` varchar(250) DEFAULT NULL,
  `fdDocumentMimeType` int(11) NOT NULL,
  `fdLocCode` int(11) NOT NULL,
  `fdDocumentFileId` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`fdDocId`),
  UNIQUE KEY `fdContractDocId_UNIQUE` (`fdDocId`),
  KEY `fdApprovedBy_idx` (`fdApprovedBy`),
  KEY `fdRejectBy_idx` (`fdRejectBy`),
  KEY `fkVisionID` (`fdVisionId`(255)),
  KEY `FK_DocumentLocCode_idx` (`fdLocCode`),
  KEY `FK_DocumentCreatedBy_idx` (`fdCreatedBy`),
  CONSTRAINT `FK_DocumentCreatedBy` FOREIGN KEY (`fdCreatedBy`) REFERENCES `tbusermaster` (`fduserid`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbdocuments`
--

LOCK TABLES `tbdocuments` WRITE;
/*!40000 ALTER TABLE `tbdocuments` DISABLE KEYS */;
INSERT INTO `tbdocuments` VALUES (2,'Cracking the Coding Interview.pdf',NULL,3,1,'2019-01-10 14:54:43','Documents/2.pdf',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,NULL),(3,'sample.pdf',NULL,3,2,'2019-01-15 15:03:43','Documents/3.pdf',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,NULL),(4,'sample.pdf',NULL,3,2,'2019-01-15 15:05:58','Documents/4.pdf',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,NULL);
/*!40000 ALTER TABLE `tbdocuments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbfunctionmaster`
--

DROP TABLE IF EXISTS `tbfunctionmaster`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `tbfunctionmaster` (
  `fdFunctionId` int(11) NOT NULL AUTO_INCREMENT,
  `fdFunctionCode` varchar(20) DEFAULT NULL,
  `fdDescription` varchar(500) DEFAULT NULL,
  `fdFunctionName` varchar(50) DEFAULT NULL,
  `fdStatus` int(11) DEFAULT NULL,
  `fdCreatedBy` int(11) DEFAULT NULL,
  `fdCreatedOn` datetime DEFAULT CURRENT_TIMESTAMP,
  `fdUpdatedBy` int(11) DEFAULT NULL,
  `fdUpdatedOn` datetime DEFAULT NULL,
  PRIMARY KEY (`fdFunctionId`),
  KEY `FK_FuncMasterCreatedBy_idx` (`fdCreatedBy`),
  KEY `FK_FuncMasterUpdatedBy_idx` (`fdUpdatedBy`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbfunctionmaster`
--

LOCK TABLES `tbfunctionmaster` WRITE;
/*!40000 ALTER TABLE `tbfunctionmaster` DISABLE KEYS */;
INSERT INTO `tbfunctionmaster` VALUES (1,'DRAFT','Draft','Draft Document',1,1,'2018-12-18 18:47:32',1,'2018-12-18 18:47:32'),(2,'PUBLISH','Publish','Publish Document',1,1,'2018-12-18 18:47:32',1,'2018-12-18 18:47:32'),(3,'EXPIRE','Expire','Expire Document',1,1,'2018-12-18 18:47:32',1,'2018-12-18 18:47:32'),(4,'VIEW','VIEW','VIEW',1,1,'2018-12-18 18:47:32',1,'2018-12-18 18:47:32'),(5,'CREATE','CREATE','CREATE',1,1,'2018-12-18 18:47:32',1,'2018-12-18 18:47:32');
/*!40000 ALTER TABLE `tbfunctionmaster` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbmodulefunctionlink`
--

DROP TABLE IF EXISTS `tbmodulefunctionlink`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `tbmodulefunctionlink` (
  `fdModuleId` int(11) NOT NULL,
  `fdFunctionId` int(11) NOT NULL,
  PRIMARY KEY (`fdModuleId`,`fdFunctionId`),
  KEY `FK_moduleFuncLnkFunctionid_idx` (`fdFunctionId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbmodulefunctionlink`
--

LOCK TABLES `tbmodulefunctionlink` WRITE;
/*!40000 ALTER TABLE `tbmodulefunctionlink` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbmodulefunctionlink` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbmodulemaster`
--

DROP TABLE IF EXISTS `tbmodulemaster`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `tbmodulemaster` (
  `fdModuleId` int(11) NOT NULL AUTO_INCREMENT,
  `fdModuleCode` varchar(20) DEFAULT NULL,
  `fdDescription` varchar(500) DEFAULT NULL,
  `fdModuleName` varchar(50) DEFAULT NULL,
  `fdStatus` int(11) DEFAULT NULL,
  `fdAssemblyName` varchar(500) DEFAULT NULL,
  `fdClassName` varchar(500) DEFAULT NULL,
  `fdParameterId` varchar(500) DEFAULT NULL,
  `fdParamValue` varchar(500) DEFAULT NULL,
  `fdCreatedBy` int(11) DEFAULT NULL,
  `fdCreatedOn` datetime DEFAULT CURRENT_TIMESTAMP,
  `fdUpdatedBy` int(11) DEFAULT NULL,
  `fdUpdatedOn` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`fdModuleId`),
  KEY `FK_ModuleMasterCreatedBy_idx` (`fdCreatedBy`),
  KEY `FK_ModuleMasterUpdatedBy_idx` (`fdUpdatedBy`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbmodulemaster`
--

LOCK TABLES `tbmodulemaster` WRITE;
/*!40000 ALTER TABLE `tbmodulemaster` DISABLE KEYS */;
INSERT INTO `tbmodulemaster` VALUES (1,'DC2','Document Class Policy','Document Class Policy',NULL,NULL,NULL,NULL,NULL,1,'2019-01-03 11:05:28',NULL,'2019-01-03 11:05:28'),(2,'DC3','Document Class Panasonic Policy Document','Document Class Panasonic Policy Document',NULL,NULL,NULL,NULL,NULL,1,'2019-01-03 13:08:10',NULL,'2019-01-03 13:08:10'),(3,'DC4','Document Class Sample1','Document Class Sample1',NULL,NULL,NULL,NULL,NULL,1,'2019-01-10 15:39:15',NULL,'2019-01-10 15:39:15'),(4,'DOC_CLASS','Document Class Module','Document Class Module',NULL,NULL,NULL,NULL,NULL,1,'2019-01-10 15:39:15',NULL,'2019-01-10 15:39:15'),(5,'DC5','Document Class SampalParadox','Document Class SampalParadox',NULL,NULL,NULL,NULL,NULL,1,'2019-01-15 15:11:12',NULL,'2019-01-15 15:11:12'),(6,'DC6','Document Class SampalParadox','Document Class SampalParadox',NULL,NULL,NULL,NULL,NULL,1,'2019-01-15 15:12:32',NULL,'2019-01-15 15:12:32'),(7,'DC7','Document Class NewDocClass','Document Class NewDocClass',NULL,NULL,NULL,NULL,NULL,1,'2019-01-16 13:07:19',NULL,'2019-01-16 13:07:19');
/*!40000 ALTER TABLE `tbmodulemaster` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbrolemaster`
--

DROP TABLE IF EXISTS `tbrolemaster`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `tbrolemaster` (
  `fdRoleId` int(11) NOT NULL AUTO_INCREMENT,
  `fdRoleCode` varchar(100) DEFAULT NULL,
  `fdRoleName` varchar(200) DEFAULT NULL,
  `fdDescription` varchar(500) DEFAULT NULL,
  `fdStatus` int(11) DEFAULT NULL,
  `fdCreatedBy` int(11) DEFAULT NULL,
  `fdCreatedOn` datetime DEFAULT NULL,
  `fdUpdatedBy` int(11) DEFAULT NULL,
  `fdUpdatedOn` datetime DEFAULT NULL,
  PRIMARY KEY (`fdRoleId`),
  KEY `FK_RoleCreatedBy_idx` (`fdCreatedBy`),
  KEY `FK_RoleUpdatedBy_idx` (`fdUpdatedBy`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbrolemaster`
--

LOCK TABLES `tbrolemaster` WRITE;
/*!40000 ALTER TABLE `tbrolemaster` DISABLE KEYS */;
INSERT INTO `tbrolemaster` VALUES (22,'POLICYAUTH','Policy Author','Author Role For Document Class POLICY',1,1,'2019-01-03 11:05:28',1,'2019-01-03 11:05:28'),(23,'POLICYVIEW','Policy Viewer','Viewer Role For Document Class POLICY',1,1,'2019-01-03 11:05:28',1,'2019-01-03 11:05:28'),(24,'PANASONICPOLICYDOCUMENTAUTH','Panasonic Policy Document Author','Author Role For Document Class PANASONIC POLICY DOCUMENT',1,1,'2019-01-03 13:08:10',1,'2019-01-03 13:08:10'),(25,'PANASONICPOLICYDOCUMENTVIEW','Panasonic Policy Document Viewer','Viewer Role For Document Class PANASONIC POLICY DOCUMENT',1,1,'2019-01-03 13:08:10',1,'2019-01-03 13:08:10'),(26,'SAMPLE1AUTH','Sample1 Author','Author Role For Document Class SAMPLE1',1,1,'2019-01-10 15:39:15',1,'2019-01-10 15:39:15'),(27,'SAMPLE1VIEW','Sample1 Viewer','Viewer Role For Document Class SAMPLE1',1,1,'2019-01-10 15:39:15',1,'2019-01-10 15:39:15'),(28,'DOCCLASS','DOCCLASS','DOCCLASS',1,1,'2019-01-10 15:39:15',1,'2019-01-10 15:39:15'),(29,'SAMPALPARADOXAUTH','SampalParadox Author','Author Role For Document Class SAMPALPARADOX',1,1,'2019-01-15 15:11:12',1,'2019-01-15 15:11:12'),(30,'SAMPALPARADOXVIEW','SampalParadox Viewer','Viewer Role For Document Class SAMPALPARADOX',1,1,'2019-01-15 15:11:12',1,'2019-01-15 15:11:12'),(31,'SAMPALPARADOXAUTH','SampalParadox Author','Author Role For Document Class SAMPALPARADOX',1,1,'2019-01-15 15:12:00',1,'2019-01-15 15:12:01'),(32,'SAMPALPARADOXVIEW','SampalParadox Viewer','Viewer Role For Document Class SAMPALPARADOX',1,1,'2019-01-15 15:12:10',1,'2019-01-15 15:12:11'),(33,'NEWDOCCLASSAUTH','NewDocClass Author','Author Role For Document Class NEWDOCCLASS',1,1,'2019-01-16 13:07:20',1,'2019-01-16 13:07:20'),(34,'NEWDOCCLASSVIEW','NewDocClass Viewer','Viewer Role For Document Class NEWDOCCLASS',1,1,'2019-01-16 13:07:20',1,'2019-01-16 13:07:20');
/*!40000 ALTER TABLE `tbrolemaster` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbrolemodfunccondlink`
--

DROP TABLE IF EXISTS `tbrolemodfunccondlink`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `tbrolemodfunccondlink` (
  `fdRoleId` int(11) NOT NULL,
  `fdModuleId` int(11) NOT NULL,
  `fdFunctionId` int(11) NOT NULL,
  `fdConditionId` int(11) DEFAULT NULL,
  `fdPkID` int(11) NOT NULL AUTO_INCREMENT,
  `fdCreatedOn` datetime DEFAULT NULL,
  `fdCreatedBy` int(11) DEFAULT NULL,
  PRIMARY KEY (`fdPkID`),
  KEY `FK_RoleModFunConLnkRoleID_idx` (`fdRoleId`),
  KEY `FK_RoleModFunConLnkModule_idx` (`fdModuleId`),
  KEY `FK_RoleModFunConLnkFunction_idx` (`fdFunctionId`),
  KEY `FK_ROleModFunConLnkCreatedBy_idx` (`fdCreatedBy`),
  CONSTRAINT `FK_RoleModFunConLnkFunction` FOREIGN KEY (`fdFunctionId`) REFERENCES `tbfunctionmaster` (`fdfunctionid`),
  CONSTRAINT `FK_RoleModFunConLnkModule` FOREIGN KEY (`fdModuleId`) REFERENCES `tbmodulemaster` (`fdmoduleid`),
  CONSTRAINT `FK_RoleModFunConLnkRoleID` FOREIGN KEY (`fdRoleId`) REFERENCES `tbrolemaster` (`fdroleid`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbrolemodfunccondlink`
--

LOCK TABLES `tbrolemodfunccondlink` WRITE;
/*!40000 ALTER TABLE `tbrolemodfunccondlink` DISABLE KEYS */;
INSERT INTO `tbrolemodfunccondlink` VALUES (22,1,1,NULL,1,'2019-01-10 12:19:37',1),(22,1,2,NULL,2,'2019-01-10 12:19:37',1),(22,1,3,NULL,3,'2019-01-10 12:19:37',1),(22,1,4,NULL,4,'2019-01-10 12:19:37',1),(23,1,4,NULL,5,'2019-01-10 12:19:37',1),(24,2,1,NULL,6,'2019-01-10 12:19:37',1),(24,2,2,NULL,7,'2019-01-10 12:19:37',1),(24,2,3,NULL,8,'2019-01-10 12:19:37',1),(24,2,4,NULL,9,'2019-01-10 12:19:37',1),(25,2,4,NULL,10,'2019-01-10 12:19:37',1),(26,3,1,NULL,11,'2019-01-10 15:39:15',1),(26,3,2,NULL,12,'2019-01-10 15:39:15',1),(26,3,3,NULL,13,'2019-01-10 15:39:15',1),(26,3,4,NULL,14,'2019-01-10 15:39:15',1),(27,3,4,NULL,15,'2019-01-10 15:39:15',1),(28,4,5,NULL,16,'2019-01-10 15:39:15',1),(29,5,1,NULL,17,'2019-01-15 15:11:12',1),(29,5,2,NULL,18,'2019-01-15 15:11:12',1),(29,5,3,NULL,19,'2019-01-15 15:11:12',1),(29,5,4,NULL,20,'2019-01-15 15:11:12',1),(30,5,4,NULL,21,'2019-01-15 15:11:12',1),(31,6,1,NULL,22,'2019-01-15 15:12:36',1),(31,6,2,NULL,23,'2019-01-15 15:12:40',1),(31,6,3,NULL,24,'2019-01-15 15:12:44',1),(31,6,4,NULL,25,'2019-01-15 15:12:47',1),(32,6,4,NULL,26,'2019-01-15 15:12:50',1),(33,7,1,NULL,27,'2019-01-16 13:07:20',1),(33,7,2,NULL,28,'2019-01-16 13:07:20',1),(33,7,3,NULL,29,'2019-01-16 13:07:20',1),(33,7,4,NULL,30,'2019-01-16 13:07:20',1),(34,7,4,NULL,31,'2019-01-16 13:07:20',1);
/*!40000 ALTER TABLE `tbrolemodfunccondlink` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbusermaster`
--

DROP TABLE IF EXISTS `tbusermaster`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `tbusermaster` (
  `fdUserId` int(11) NOT NULL AUTO_INCREMENT,
  `fdUserName` varchar(100) NOT NULL,
  `fdUserContactNo` bigint(20) DEFAULT NULL,
  `fdUserCity` varchar(100) DEFAULT NULL,
  `fdUserState` varchar(100) DEFAULT NULL,
  `fdUserCountry` varchar(100) DEFAULT NULL,
  `fdUserAddress` varchar(200) DEFAULT NULL,
  `fdUserEmail` varchar(101) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `fdUserPassword` varchar(60) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `fdLastLoggedInDate` datetime DEFAULT NULL,
  `fdIsActive` int(11) NOT NULL,
  `fdDeviceId` varchar(150) DEFAULT NULL,
  `fdUserGoogleCredential` varchar(1000) DEFAULT NULL,
  `fdUserToken` varchar(100) DEFAULT NULL,
  `fdDriveFolderId` varchar(100) DEFAULT NULL,
  `fdUserType` int(11) DEFAULT NULL,
  `fdOTP` varchar(45) DEFAULT NULL,
  `fdOtpCreatedDate` datetime DEFAULT NULL,
  `fdMessage` varchar(1000) DEFAULT NULL,
  `fdDepartment` smallint(6) DEFAULT NULL,
  PRIMARY KEY (`fdUserId`),
  UNIQUE KEY `fdUserEmail_UNIQUE` (`fdUserEmail`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbusermaster`
--

LOCK TABLES `tbusermaster` WRITE;
/*!40000 ALTER TABLE `tbusermaster` DISABLE KEYS */;
INSERT INTO `tbusermaster` VALUES (1,'Admin',0,NULL,NULL,NULL,NULL,'admin@panasonic.com','password','2019-01-18 18:56:36',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1),(2,'Finance',NULL,NULL,NULL,NULL,NULL,'finance@panasonic.com','password','2019-01-15 14:58:50',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(3,'Operation',NULL,NULL,NULL,NULL,NULL,'operation@panasonic.com','password','2019-01-15 14:44:03',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `tbusermaster` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbuserroleloclink`
--

DROP TABLE IF EXISTS `tbuserroleloclink`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `tbuserroleloclink` (
  `fdUserId` int(11) NOT NULL,
  `fdLocationId` int(11) NOT NULL,
  `fdRoleId` int(11) NOT NULL,
  `fdCreatedBy` int(11) NOT NULL,
  `fdCreatedOn` datetime NOT NULL,
  `fdUpdatedBy` int(11) DEFAULT NULL,
  `fdUpdatedOn` datetime NOT NULL,
  `fdRoleLocUserID` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`fdRoleLocUserID`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbuserroleloclink`
--

LOCK TABLES `tbuserroleloclink` WRITE;
/*!40000 ALTER TABLE `tbuserroleloclink` DISABLE KEYS */;
INSERT INTO `tbuserroleloclink` VALUES (1,1,23,1,'2019-01-03 11:05:28',1,'2019-01-03 11:05:28',1),(1,1,24,1,'2019-01-03 11:05:28',1,'2019-01-03 11:05:28',2),(1,1,25,1,'2019-01-03 11:05:28',1,'2019-01-03 11:05:28',3),(1,1,22,1,'2019-01-03 11:05:28',1,'2019-01-03 11:05:28',4),(1,1,26,1,'2019-01-10 15:39:15',NULL,'0001-01-01 00:00:00',5),(2,1,25,1,'2019-01-10 15:39:15',1,'2019-01-03 11:05:28',6),(1,1,28,1,'2019-01-10 15:39:15',1,'2019-01-10 15:39:15',7),(2,1,24,1,'2019-01-10 15:39:15',1,'2019-01-10 15:39:15',8),(1,1,29,1,'2019-01-15 15:11:12',NULL,'0001-01-01 00:00:00',9),(1,1,30,1,'2019-01-15 15:11:12',NULL,'0001-01-01 00:00:00',10),(1,1,31,1,'2019-01-15 15:12:06',NULL,'0001-01-01 00:00:00',11),(1,1,32,1,'2019-01-15 15:12:16',NULL,'0001-01-01 00:00:00',12),(1,1,33,1,'2019-01-16 13:07:20',NULL,'0001-01-01 00:00:00',13),(1,1,34,1,'2019-01-16 13:07:20',NULL,'0001-01-01 00:00:00',14);
/*!40000 ALTER TABLE `tbuserroleloclink` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_action_audit`
--

DROP TABLE IF EXISTS `user_action_audit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `user_action_audit` (
  `auditId` int(11) NOT NULL AUTO_INCREMENT,
  `actionType` varchar(100) DEFAULT NULL,
  `actionName` varchar(200) DEFAULT NULL,
  `requestData` longtext,
  `viewData` varchar(10000) DEFAULT NULL,
  `requestSrc` varchar(45) DEFAULT NULL,
  `documentId` int(11) DEFAULT NULL,
  `createdBy` int(11) NOT NULL,
  `createdOn` datetime NOT NULL,
  PRIMARY KEY (`auditId`),
  KEY `idx_user_action_audit_actionType` (`actionType`),
  KEY `idx_user_action_audit_actionName` (`actionName`),
  KEY `idx_user_action_audit_createdBy` (`createdBy`)
) ENGINE=InnoDB AUTO_INCREMENT=70 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_action_audit`
--

LOCK TABLES `user_action_audit` WRITE;
/*!40000 ALTER TABLE `user_action_audit` DISABLE KEYS */;
INSERT INTO `user_action_audit` VALUES (16,'DC2','DRAFT','{\"lookUpCode\":[\"DOC_CLASS_TYPE\"]}','SUCCESS',NULL,0,1,'2019-01-16 13:01:12'),(17,'DC3','DRAFT','{\"lookUpCode\":[\"DOC_CLASS_TYPE\"]}','SUCCESS',NULL,0,1,'2019-01-16 13:01:12'),(18,'DC4','DRAFT','{\"lookUpCode\":[\"DOC_CLASS_TYPE\"]}','SUCCESS',NULL,0,1,'2019-01-16 13:01:12'),(19,'DC5','DRAFT','{\"lookUpCode\":[\"DOC_CLASS_TYPE\"]}','SUCCESS',NULL,0,1,'2019-01-16 13:01:12'),(20,'DC6','DRAFT','{\"lookUpCode\":[\"DOC_CLASS_TYPE\"]}','SUCCESS',NULL,0,1,'2019-01-16 13:01:12'),(21,'DC2','DRAFT','{\"lookUpCode\":[\"DOC_CLASS_TYPE\"]}','SUCCESS',NULL,0,1,'2019-01-16 13:05:05'),(22,'DC3','DRAFT','{\"lookUpCode\":[\"DOC_CLASS_TYPE\"]}','SUCCESS',NULL,0,1,'2019-01-16 13:05:05'),(23,'DC4','DRAFT','{\"lookUpCode\":[\"DOC_CLASS_TYPE\"]}','SUCCESS',NULL,0,1,'2019-01-16 13:05:05'),(24,'DC5','DRAFT','{\"lookUpCode\":[\"DOC_CLASS_TYPE\"]}','SUCCESS',NULL,0,1,'2019-01-16 13:05:05'),(25,'DC6','DRAFT','{\"lookUpCode\":[\"DOC_CLASS_TYPE\"]}','SUCCESS',NULL,0,1,'2019-01-16 13:05:05'),(26,'DOC_CLASS','CREATE','{\"txtParadoxClassName\":\"NewDocClass\",\"properties\":[{\"propertyName\":\"prop\",\"dataType\":\"1\",\"isRequired\":\"No\",\"sequence\":1,\"defaultValue\":\"1\"}],\"authorDepartments\":[\"1\"],\"viewerDepartments\":[\"1\"]}','SUCCESS',NULL,0,1,'2019-01-16 13:05:18'),(27,'DC2','DRAFT','{\"lookUpCode\":[\"DOC_CLASS_TYPE\"]}','SUCCESS',NULL,0,1,'2019-01-16 13:07:04'),(28,'DC3','DRAFT','{\"lookUpCode\":[\"DOC_CLASS_TYPE\"]}','SUCCESS',NULL,0,1,'2019-01-16 13:07:04'),(29,'DC4','DRAFT','{\"lookUpCode\":[\"DOC_CLASS_TYPE\"]}','SUCCESS',NULL,0,1,'2019-01-16 13:07:05'),(30,'DC5','DRAFT','{\"lookUpCode\":[\"DOC_CLASS_TYPE\"]}','SUCCESS',NULL,0,1,'2019-01-16 13:07:05'),(31,'DC6','DRAFT','{\"lookUpCode\":[\"DOC_CLASS_TYPE\"]}','SUCCESS',NULL,0,1,'2019-01-16 13:07:05'),(32,'DOC_CLASS','CREATE','{\"txtParadoxClassName\":\"NewDocClass\",\"properties\":[{\"propertyName\":\"prop\",\"dataType\":\"1\",\"isRequired\":\"No\",\"sequence\":1,\"defaultValue\":\"1\"}],\"authorDepartments\":[\"1\"],\"viewerDepartments\":[\"1\"]}','SUCCESS',NULL,0,1,'2019-01-16 13:07:10'),(33,'DOC_CLASS','CREATE','{\"txtParadoxClassName\":\"NewDocClass\",\"properties\":[{\"propertyName\":\"prop\",\"dataType\":\"1\",\"isRequired\":\"No\",\"sequence\":1,\"defaultValue\":\"1\"}],\"authorDepartments\":[\"1\"],\"viewerDepartments\":[\"1\"]}','SUCCESS',NULL,0,1,'2019-01-16 13:07:27'),(34,'DC2','DRAFT','{\"lookUpCode\":[\"DOC_CLASS_TYPE\"]}','SUCCESS',NULL,0,1,'2019-01-18 12:18:10'),(35,'DC3','DRAFT','{\"lookUpCode\":[\"DOC_CLASS_TYPE\"]}','SUCCESS',NULL,0,1,'2019-01-18 12:18:10'),(36,'DC4','DRAFT','{\"lookUpCode\":[\"DOC_CLASS_TYPE\"]}','SUCCESS',NULL,0,1,'2019-01-18 12:18:10'),(37,'DC5','DRAFT','{\"lookUpCode\":[\"DOC_CLASS_TYPE\"]}','SUCCESS',NULL,0,1,'2019-01-18 12:18:10'),(38,'DC6','DRAFT','{\"lookUpCode\":[\"DOC_CLASS_TYPE\"]}','SUCCESS',NULL,0,1,'2019-01-18 12:18:10'),(39,'DC7','DRAFT','{\"lookUpCode\":[\"DOC_CLASS_TYPE\"]}','SUCCESS',NULL,0,1,'2019-01-18 12:18:10'),(40,'DC2','DRAFT','{\"lookUpCode\":[\"DOC_CLASS_TYPE\"]}','SUCCESS',NULL,0,1,'2019-01-18 12:54:07'),(41,'DC3','DRAFT','{\"lookUpCode\":[\"DOC_CLASS_TYPE\"]}','SUCCESS',NULL,0,1,'2019-01-18 12:54:07'),(42,'DC4','DRAFT','{\"lookUpCode\":[\"DOC_CLASS_TYPE\"]}','SUCCESS',NULL,0,1,'2019-01-18 12:54:07'),(43,'DC5','DRAFT','{\"lookUpCode\":[\"DOC_CLASS_TYPE\"]}','SUCCESS',NULL,0,1,'2019-01-18 12:54:07'),(44,'DC6','DRAFT','{\"lookUpCode\":[\"DOC_CLASS_TYPE\"]}','SUCCESS',NULL,0,1,'2019-01-18 12:54:07'),(45,'DC7','DRAFT','{\"lookUpCode\":[\"DOC_CLASS_TYPE\"]}','SUCCESS',NULL,0,1,'2019-01-18 12:54:07'),(46,'DC2','DRAFT','{\"lookUpCode\":[\"DOC_CLASS_TYPE\"]}','SUCCESS',NULL,0,1,'2019-01-18 13:17:38'),(47,'DC3','DRAFT','{\"lookUpCode\":[\"DOC_CLASS_TYPE\"]}','SUCCESS',NULL,0,1,'2019-01-18 13:17:38'),(48,'DC4','DRAFT','{\"lookUpCode\":[\"DOC_CLASS_TYPE\"]}','SUCCESS',NULL,0,1,'2019-01-18 13:17:38'),(49,'DC5','DRAFT','{\"lookUpCode\":[\"DOC_CLASS_TYPE\"]}','SUCCESS',NULL,0,1,'2019-01-18 13:17:38'),(50,'DC6','DRAFT','{\"lookUpCode\":[\"DOC_CLASS_TYPE\"]}','SUCCESS',NULL,0,1,'2019-01-18 13:17:38'),(51,'DC7','DRAFT','{\"lookUpCode\":[\"DOC_CLASS_TYPE\"]}','SUCCESS',NULL,0,1,'2019-01-18 13:17:38'),(52,'DC2','DRAFT','{\"lookUpCode\":[\"DOC_CLASS_TYPE\"]}','SUCCESS',NULL,0,1,'2019-01-18 13:30:37'),(53,'DC3','DRAFT','{\"lookUpCode\":[\"DOC_CLASS_TYPE\"]}','SUCCESS',NULL,0,1,'2019-01-18 13:30:37'),(54,'DC4','DRAFT','{\"lookUpCode\":[\"DOC_CLASS_TYPE\"]}','SUCCESS',NULL,0,1,'2019-01-18 13:30:37'),(55,'DC5','DRAFT','{\"lookUpCode\":[\"DOC_CLASS_TYPE\"]}','SUCCESS',NULL,0,1,'2019-01-18 13:30:37'),(56,'DC6','DRAFT','{\"lookUpCode\":[\"DOC_CLASS_TYPE\"]}','SUCCESS',NULL,0,1,'2019-01-18 13:30:37'),(57,'DC7','DRAFT','{\"lookUpCode\":[\"DOC_CLASS_TYPE\"]}','SUCCESS',NULL,0,1,'2019-01-18 13:30:37'),(58,'DC2','DRAFT','{\"lookUpCode\":[\"DOC_CLASS_TYPE\"]}','SUCCESS',NULL,0,1,'2019-01-18 15:44:10'),(59,'DC3','DRAFT','{\"lookUpCode\":[\"DOC_CLASS_TYPE\"]}','SUCCESS',NULL,0,1,'2019-01-18 15:44:10'),(60,'DC4','DRAFT','{\"lookUpCode\":[\"DOC_CLASS_TYPE\"]}','SUCCESS',NULL,0,1,'2019-01-18 15:44:10'),(61,'DC5','DRAFT','{\"lookUpCode\":[\"DOC_CLASS_TYPE\"]}','SUCCESS',NULL,0,1,'2019-01-18 15:44:10'),(62,'DC6','DRAFT','{\"lookUpCode\":[\"DOC_CLASS_TYPE\"]}','SUCCESS',NULL,0,1,'2019-01-18 15:44:10'),(63,'DC7','DRAFT','{\"lookUpCode\":[\"DOC_CLASS_TYPE\"]}','SUCCESS',NULL,0,1,'2019-01-18 15:44:10'),(64,'DC2','DRAFT','{\"lookUpCode\":[\"DOC_CLASS_TYPE\"]}','SUCCESS',NULL,0,1,'2019-01-18 18:56:36'),(65,'DC3','DRAFT','{\"lookUpCode\":[\"DOC_CLASS_TYPE\"]}','SUCCESS',NULL,0,1,'2019-01-18 18:56:36'),(66,'DC4','DRAFT','{\"lookUpCode\":[\"DOC_CLASS_TYPE\"]}','SUCCESS',NULL,0,1,'2019-01-18 18:56:36'),(67,'DC5','DRAFT','{\"lookUpCode\":[\"DOC_CLASS_TYPE\"]}','SUCCESS',NULL,0,1,'2019-01-18 18:56:36'),(68,'DC6','DRAFT','{\"lookUpCode\":[\"DOC_CLASS_TYPE\"]}','SUCCESS',NULL,0,1,'2019-01-18 18:56:36'),(69,'DC7','DRAFT','{\"lookUpCode\":[\"DOC_CLASS_TYPE\"]}','SUCCESS',NULL,0,1,'2019-01-18 18:56:36');
/*!40000 ALTER TABLE `user_action_audit` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-01-22 11:42:37
