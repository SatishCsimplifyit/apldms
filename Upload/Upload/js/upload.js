//Common Js Starts
document.addEventListener("DOMContentLoaded", function(event) {
	if(window.innerWidth < 992){
		var isMobile = true;
		document.querySelector("#sidebar").classList.add("sidebar-close");
		document.querySelector("#sidebar1").classList.add("sidebar-close1");
	}

    document.querySelector("#toggle-sidebar").addEventListener("click", function(event){
        document.querySelector("#sidebar").classList.toggle("sidebar-close");
        setTimeout(function(){
        	if(document.querySelector("#sidebar").classList.contains("sidebar-close")){
        		document.querySelector(".content").classList.remove("hide-on-mob-toggle");
        		if(isMobile){
        			document.querySelector("#sidebar1").style.display = "block";
        		}
        	} else {
        		document.querySelector(".content").classList.add("hide-on-mob-toggle");
        		if(isMobile){
        			document.querySelector("#sidebar1").style.display = "none";
        		}
        	}
        },10);
    });

    
});


$(function() {

  $('.search-input input').blur(function() {

    if ($(this).val()) {
      $(this)
        .find('~ label, ~ span:nth-of-type(n+3)')
        .addClass('not-empty');
    } else {
      $(this)
        .find('~ label, ~ span:nth-of-type(n+3)')
        .removeClass('not-empty');
    }
  });

  $('.search-input input ~ span:nth-of-type(4)').click(function() {
    $('.search-input input').val('');
    $('.search-input input')
      .find('~ label, ~ span:nth-of-type(n+3)')
      .removeClass('not-empty');
  });
  
});

//Common Js Ends

$(document).ready(function() {
    $('#query1').hide();
});

$(".dropdown-form").click(function(){
  $(".query1").addClass("visible");
});

$(document).click(function(event) {
  //if you click on anything except the modal itself or the "open modal" link, close the modal
  if (!$(event.target).closest(".query1,.dropdown-form").length) {
    $("body").find(".query1").removeClass("visible");
  }
});

$(function() {
  $('input[type=file]').change(function(){
    var t = $(this).val();
    var labelText = 'File : ' + t.substr(12, t.length);
    $(this).prev('label').text(labelText);
  })
});